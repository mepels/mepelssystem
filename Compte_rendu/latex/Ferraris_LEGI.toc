\babel@toc {french}{}
\contentsline {chapter}{\numberline {1}Introduction}{3}{chapter.1}%
\contentsline {section}{\numberline {1.1}Système MEPELS}{4}{section.1.1}%
\contentsline {section}{\numberline {1.2}État de l'Art sur la morpho-dynamique des plage sableuses}{4}{section.1.2}%
\contentsline {subsection}{\numberline {1.2.1}Vagues}{4}{subsection.1.2.1}%
\contentsline {subsection}{\numberline {1.2.2}Marée}{7}{subsection.1.2.2}%
\contentsline {subsection}{\numberline {1.2.3}Courant}{7}{subsection.1.2.3}%
\contentsline {subsection}{\numberline {1.2.4}Transport de sédiments}{9}{subsection.1.2.4}%
\contentsline {subsection}{\numberline {1.2.5}Classification des états morpholodynamiques}{10}{subsection.1.2.5}%
\contentsline {subsection}{\numberline {1.2.6}Modèle morphodynamique: Xbeach}{12}{subsection.1.2.6}%
\contentsline {subsection}{\numberline {1.2.7}Références niveaux verticaux}{13}{subsection.1.2.7}%
\contentsline {subsection}{\numberline {1.2.8}Système de Coordonnées}{14}{subsection.1.2.8}%
\contentsline {chapter}{\numberline {2}Utilisation du système: Plage du Truc Vert}{15}{chapter.2}%
\contentsline {section}{\numberline {2.1}Utilisation module pré-traitement}{17}{section.2.1}%
\contentsline {subsection}{\numberline {2.1.1}Récupération du système MEPELS}{17}{subsection.2.1.1}%
\contentsline {subsection}{\numberline {2.1.2}Pré-traitement MNT}{17}{subsection.2.1.2}%
\contentsline {subsubsection}{\numberline {2.1.2.1}Fichier de configuration}{17}{subsubsection.2.1.2.1}%
\contentsline {subsubsection}{\numberline {2.1.2.2}Run pré-traitement}{20}{subsubsection.2.1.2.2}%
\contentsline {section}{\numberline {2.2}Analyse bathymétrique MNT}{23}{section.2.2}%
\contentsline {subsection}{\numberline {2.2.1}Fichier de configuration}{24}{subsection.2.2.1}%
\contentsline {subsection}{\numberline {2.2.2}Run analyse bathymétrique}{24}{subsection.2.2.2}%
\contentsline {section}{\numberline {2.3}Modification bathymétrique MNT}{27}{section.2.3}%
\contentsline {subsection}{\numberline {2.3.1}Fichier de configuration}{27}{subsection.2.3.1}%
\contentsline {subsection}{\numberline {2.3.2}Run modification bathymétrique mnt}{28}{subsection.2.3.2}%
\contentsline {section}{\numberline {2.4}Modèle Morphodynamique et post-traitement}{31}{section.2.4}%
\contentsline {subsection}{\numberline {2.4.1}Modèle Morphodynamique}{31}{subsection.2.4.1}%
\contentsline {subsection}{\numberline {2.4.2}Lancer la simulation: Xbeach}{33}{subsection.2.4.2}%
\contentsline {subsection}{\numberline {2.4.3}Post-traitement}{33}{subsection.2.4.3}%
\contentsline {section}{\numberline {2.5}Cas morphodynamique: Plage du Truc Vert}{38}{section.2.5}%
\contentsline {subsection}{\numberline {2.5.1}Pré-traitement}{38}{subsection.2.5.1}%
\contentsline {subsection}{\numberline {2.5.2}Modèle morphodynamique}{38}{subsection.2.5.2}%
\contentsline {subsection}{\numberline {2.5.3}Lancer la simulations: Xbeach}{38}{subsection.2.5.3}%
\contentsline {subsection}{\numberline {2.5.4}Post-traitement}{39}{subsection.2.5.4}%
\contentsline {chapter}{\numberline {3}Documentation pré-traitement MNT}{47}{chapter.3}%
\contentsline {section}{\numberline {3.1}Script pré-traitement mnt}{47}{section.3.1}%
\contentsline {subsection}{\numberline {3.1.1}Lecture de la bathymétrie}{48}{subsection.3.1.1}%
\contentsline {subsection}{\numberline {3.1.2}Définition de la zone d'étude}{49}{subsection.3.1.2}%
\contentsline {subsection}{\numberline {3.1.3}Rotation de la zone: première possibilité d'utilisation}{50}{subsection.3.1.3}%
\contentsline {subsection}{\numberline {3.1.4}Projection: première utilisation possible}{52}{subsection.3.1.4}%
\contentsline {subsection}{\numberline {3.1.5}Interpolation}{52}{subsection.3.1.5}%
\contentsline {subsection}{\numberline {3.1.6}Remplacement de valeur manquante}{54}{subsection.3.1.6}%
\contentsline {subsection}{\numberline {3.1.7}Rotation de la zone: deuxième possibilité d'utilisation}{56}{subsection.3.1.7}%
\contentsline {subsection}{\numberline {3.1.8}Projection: deuxième possibilité d'utilisation}{56}{subsection.3.1.8}%
\contentsline {subsection}{\numberline {3.1.9}Filtrage}{57}{subsection.3.1.9}%
\contentsline {subsection}{\numberline {3.1.10}Coordonnées local}{57}{subsection.3.1.10}%
\contentsline {subsection}{\numberline {3.1.11}Calcul du gradient bathymétrique}{58}{subsection.3.1.11}%
\contentsline {subsection}{\numberline {3.1.12}Inversion Bathymétrique}{60}{subsection.3.1.12}%
\contentsline {subsection}{\numberline {3.1.13}Sauvegarde}{60}{subsection.3.1.13}%
\contentsline {section}{\numberline {3.2}Script analyse pré-traitement mnt}{60}{section.3.2}%
\contentsline {subsection}{\numberline {3.2.1}Lecture de la bathymétrie}{60}{subsection.3.2.1}%
\contentsline {subsection}{\numberline {3.2.2}Analyse cross-shore}{61}{subsection.3.2.2}%
\contentsline {subsubsection}{\numberline {3.2.2.1}Mean Profile}{61}{subsubsection.3.2.2.1}%
\contentsline {subsubsection}{\numberline {3.2.2.2}Profondeur de fermeture: DOC}{62}{subsubsection.3.2.2.2}%
\contentsline {subsubsection}{\numberline {3.2.2.3}Définition du niveau vertical}{62}{subsubsection.3.2.2.3}%
\contentsline {subsubsection}{\numberline {3.2.2.4}Profil de Dean}{63}{subsubsection.3.2.2.4}%
\contentsline {subsection}{\numberline {3.2.3}Modèle: déferlement}{64}{subsection.3.2.3}%
\contentsline {subsection}{\numberline {3.2.4}Modèle: Type de plage}{65}{subsection.3.2.4}%
\contentsline {section}{\numberline {3.3}Script modification bathymétrique}{65}{section.3.3}%
\contentsline {subsection}{\numberline {3.3.1}Lecture de la bathymétrie}{66}{subsection.3.3.1}%
\contentsline {subsection}{\numberline {3.3.2}Analyse du profil cross-shore}{66}{subsection.3.3.2}%
\contentsline {subsection}{\numberline {3.3.3}Profile de Dean}{66}{subsection.3.3.3}%
\contentsline {subsection}{\numberline {3.3.4}Ajout de Barres et croissant}{66}{subsection.3.3.4}%
\contentsline {subsection}{\numberline {3.3.5}Filtrage}{68}{subsection.3.3.5}%
\contentsline {chapter}{\numberline {4}Autre exemple d'utilisation.}{69}{chapter.4}%
\contentsline {subsection}{\numberline {4.0.1}Pré-traitement MNT: Gravelines}{69}{subsection.4.0.1}%
\contentsline {subsection}{\numberline {4.0.2}Analyse bathymétrique: Gravelines}{71}{subsection.4.0.2}%
\contentsline {subsection}{\numberline {4.0.3}Modification bathymétrique: Gravelines}{74}{subsection.4.0.3}%
\contentsline {chapter}{\numberline {5}Conclusion et Ouvertures}{77}{chapter.5}%
\contentsline {chapter}{\numberline {6}Annexe}{78}{chapter.6}%
\contentsline {section}{\numberline {6.1}Annexe A: Librairie MNT}{78}{section.6.1}%
\contentsline {subsection}{\numberline {6.1.1}Indata: lecture de données}{78}{subsection.6.1.1}%
\contentsline {subsection}{\numberline {6.1.2}Rotation: Rotation de la zone d'étude}{79}{subsection.6.1.2}%
\contentsline {subsection}{\numberline {6.1.3}Projection: changement de système de coordonnée}{80}{subsection.6.1.3}%
\contentsline {subsection}{\numberline {6.1.4}Interpolation: Interpolation des données}{81}{subsection.6.1.4}%
\contentsline {subsection}{\numberline {6.1.5}Remplacement de valeurs manquantes}{85}{subsection.6.1.5}%
\contentsline {subsection}{\numberline {6.1.6}Out data: sauvegarde des données}{89}{subsection.6.1.6}%
\contentsline {section}{\numberline {6.2}Annexe B: Figures pré-traitement plage du Truc Vert}{93}{section.6.2}%
\contentsline {section}{\numberline {6.3}Post-traitement: Morphodynamique}{94}{section.6.3}%
\contentsline {section}{\numberline {6.4}Fichier de configuration pré-traitement: Gravelines}{95}{section.6.4}%
\contentsline {section}{\numberline {6.5}Scripts pré-traitement}{97}{section.6.5}%
\contentsline {subsection}{\numberline {6.5.1}Pré-traitement MNT}{97}{subsection.6.5.1}%
\contentsline {subsection}{\numberline {6.5.2}Analyse bathymétrique MNT}{107}{subsection.6.5.2}%
\contentsline {subsection}{\numberline {6.5.3}Modification bathymétrique MNT}{112}{subsection.6.5.3}%
