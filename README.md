To clone the git repository on a personnal computer : 

LINUX : 

- Open terminal

- Go to the folder where you want to upload the git repository

> git clone https://gricad-gitlab.univ-grenoble-alpes.fr/mepels/mepelssystem.git mepelssystem

- cd mepelssystem


On windows: 
Follow the windows_git_install file 

The latest version of the documentation is available here: https://mepels.gricad-pages.univ-grenoble-alpes.fr/mepelssystem/index.html

