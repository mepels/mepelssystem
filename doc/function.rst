Functions library
*****************

Indata 
========

Bathymetries can be saved in several formats.
We can find bathymetries expressed vectorially (.txt, .xyz), raster (.nc) or a mix of both (.grd).

The files all contain 3 variables, X the points of the mesh in agreement with the x-axis, Y the points of the mesh in agreement with the y-axis and H the values assigned to the points of the mesh.
These variables can be expressed as a vector or a matrix.

Use the **loadtxt** function to read files with .txt and .xyz extensions.
Use of the **Dataset** function imported from the netCDF4 package to read bathymetries with .grd and .nc extensions.

The bathymetric file can be read from a local folder or a web link.

For the .txt and .xyz extensions, the function reads vectors and returns 3 vectors.
For the .grd extension, the function reads two vectors (X, Y) and a matrix(H), generates a mesh from the vectors, then returns 3 matrices.
For the .nc extension, the function reads matrices and returns them.


.. autofunction:: mepelssystem.preprocessing.mnt.libs.indata_function.indata

Interpolation 
=============

The interpolation assigns a value to each point of the mesh.
This value can be real or "NaN" (not a number).

Use the **griddata** function of the **scipy.interpolate** package to perform the interpolation.

To interpolate the whole bathymetry, the function cuts the bathymetry and the interpoles one by one.
For problems of continuity, the zones overlap each other.

Mathematically, the interpolation uses the values present between and around these two points of the mesh to adjust the value of the latter.
The choice of the method (parameter: **methoduse**) allows to modify the mathematical method of link between the points

.. autofunction:: mepelssystem.preprocessing.mnt.libs.interpolation_function.interpolation


Out data
=========
Allows the recording of figures, parameters used and bathymetry.

The recorded figures correspond to the final bathymetry (before the possibility of bathymetry inversion) in 2D and 3D and the gradient figure (2D figure) if the latter is calculated during the 'preprocessing_mnt' script
The parameters are saved in a netCDF file as attributes.
The bathymetry is saved in the same netCDF file and can be viewed via a termianl (under linux).
A ".txt" file is also created to save only the bathymetry in the format (X, Y, H) as a column.

The name of the figures but also of the netCDF file depends on the studied range and the executed functions.
The use or not of a filtering is indicated in the name of the files.

To work the out data function uses different python packages:
- os: to access the backup folder
- yaml: to read the configuration file
- Dataset of the netCDF4 package: to create and write data in a netCDF file (".nc")



.. autofunction:: mepelssystem.preprocessing.mnt.libs.out_data_function.out_data


Projection
==========
The projection takes into account the meshes according to the X and Y axes as well as the coordinate systems (input and output).

The function uses two functions of the pyproj module: 'CRS' and 'Transformer'.
CRS allows python to understand the code assigned to a coordinate system and Transformer to perform the projection between the two coordinate systems.

The projection function then returns the X and Y axis meshes in the new coordinate system.
The implementation of the coordinate system codes is done from the configuration file via the parameters coorIn and CoorOUT respectively the input and output system.


.. autofunction:: mepelssystem.preprocessing.mnt.libs.projection_function.projection

Replacement values
==================
The objective is to replace the missing values on the bathymetric mesh after interpolation so that each point is an attributed value.
Two possibilities are possible, the replacement of values by local or global replacement of the bathymetry (depending on the limit of missing values not to be exceeded, parameter missmore in the configuration file) or the replacement of missing values via the simulation of a gradient.

The function asks for 15 arguments to know the user's choice of method to use (missVal_param), the bathymetric data (H_bathy), and the access paths to subsitution bathymetries (missingBathyPath) or the number of points preceding the missing value to use to simulate the gradient (n).

To work the function imports 6 functions:
- sys: to access the substitution bathymetry file
- interpolation from the interpolation function
- indata from the indata function for loading data
- griddata from the package scipy.interpolate to perform an interpolation
- np from the package numpy
- plt from the package matplotlib.pyplot

In output the function returns the bathymetry with a real value on each point of the mesh.

.. autofunction:: mepelssystem.preprocessing.mnt.libs.replacementValues_function.replacementValues

Rotation
========
This function is used to obtain an identical orientation for all bathymetry studied.
Namely, the long-shore (the beach) on the right, the offshore origin on the left, allowing to be in a local East-North reference frame.

The function takes 6 parameters as arguments in order to obtain the mesh according to x and y, but also the angle and the direction of rotation and of course the point with respect to which the rotation is made.

To work, the function calls only the numpy package.

The function returns the new X and Y meshes after rotation.

.. autofunction:: mepelssystem.preprocessing.mnt.libs.rotation_function.rotation

