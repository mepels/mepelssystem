Make gif through images
***********************

The folowing explaination is valid on GNU/Linux.

The tools used is part of the **imagemagick** package.
It allow to to transform image from a native format to an other (here gif format).

How do :

Open a terminal and go to the file containing the images.

Write the command :

.. code-block::

	$ convert -delay 30 im*.png -loop 2 video.gif

.. note :: delay is the time between to images on the gif, given on hundredth of a second. loop is number of animation (2 means the gif run to time).


Know you've an gif with all your images who's name video.
Open it with any lecteur an look the gif.
