netCDF4 useful orders
*********************



The MEPELS system used netCDF to save the data and information, but also if you want .txt format to save data.
With a netCDF file, recognizable by extension ".nc", data are easy to take a quick look by usinga terminal.

To visualized the data on 2 dimension, the command **ncview** is used like :

.. code-block::

    $ ncview file.nc

And function of the file leave some variables to take a look.

For example, after running the preprocessing file, you have access to the new bathymetry which is ready to be used by XBEACH (see below).

.. image:: ./../images/netcdf_file/netcdf_h_preproc.png
  :width: 600

According to the same file, some information are saved to allow a go back to the original condition before an modification.
But, restarting by the new bathymetry, it's impossible to find the total area of the initial bathymetry.

To go back to the initial conditions you need to change the variables on the configuration file.
The variables to go back are saved on the file.nc.
To read the informations use ncdump function on the terminal.

.. code-block::

    $ ncdump -h file.nc

Then you've access to the variables used in the code file.

.. image:: ./../images/netcdf_file/netcdf_texte_preproc.png
  :width: 600

.. note:: Don't forget, if you want to go back to the initial conditions of the bathymetry you need to take the outputPath of the preprocessing_mnt as your new inputPath.
