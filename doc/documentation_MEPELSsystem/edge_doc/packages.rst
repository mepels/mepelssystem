Packages required
*****************

Below, the list of the differents packages used during the run of the MEPELS system.

Packages used in MEPELS system - Python
=======================================

To add the different packages in your code, write the associated command at the top of the python file.

Dataset
--------

Used to read and write data at format ".nc" .

.. code-block::

    >>> from netCDF4 import Dataset

Gaussian Filter
----------------

Import a gaussien filter.

.. code-block:: python

    >>> from scipy.ndimage.filters import gaussian_filter

Matplotlib
----------

Relative to plotting aptitude, used to create scatter / contourf / plot_surface.

.. code-block:: python

    >>> import matplotlib.pyplot as plt

Relative to builtin colormaps, colormap handling utilities, and the ScalarMappable mixin.

.. code-block:: python

    >>> from matplotlib import cm

Numpy
-----

Allow to make some numerical calcul.

.. code-block:: python

    >>> import numpy as np

Os / Sys
--------

Allow to move in the directories, used for loading and saving files.

.. code-block:: python

    >>> import os
    >>> import sys

Patches
--------

Used to add patches on scatter  or contourf plot.

.. code-block:: python

    >>> from matplotlib import patches

Time
-----

Used to show the during of the run at the end.

.. code-block:: python

    >>> import time

Yaml
-----

Allow to converse with file.yaml, used to read the configuration write for a study.

.. code-block:: python

    >>> import yaml


Package used in MEPELS system - Matlab
======================================

Some packages are not automatically adding with python. 
To add the packages, follow the below instructions.


Import library
==============

This section show you how to load some packages used in the MEPELS system and not include in default python library.
The loading are done with Linux OS.


Dataset import
---------------


Dataset is a package of the netCDF4 library.
It's used to create or read data at the format ".nc".


Installation
.............

Open a terminal.

Write the line below in the terminal.

.. code-block::

    $ sudo apt-get install python3-netcdf4


Add the package at the top of youy python file.

yaml import
------------

The configuration file are write in the format .yaml to allow the codes to load the needed data during the run.

Installation
............

Open a terminal.

Write the line below in the terminal.

.. code-block::

    $ sudo apt-get install python-yaml
    or
    $ sudo yum install python-yaml

Add the package in your python code.


Pyproj import
--------------

use to make projection between different coordinate system.

Installation
.............


Open a terminal.
Go on the folder were you've save your python's scripts.
.. code-block::

    $ python3 -m pip install pyproj
    or
    $ sudo apt-get install pyproj

