New configurations & start compute
************************************

This section presents the actions necessary for the user to perform in the configuration file and the pre-processing module.

The operating system used is linux, so the commands for accessing and opening files are given for linux.

Configuration file
===================

Before launching the pre-processing module, a configuration file related to the pre-processing module must be filled in.

To open this file from the origin of the MEPELS system:

.. code-block::

    >>> MEPELSsystem: cd mepelssystem/preprocessing/mnt/configurations/

Add new configuration
----------------------

From the "config_plage" folder, it is possible to open 3 ".yaml" files (format used for the configuration file compatible with python) near filled: 
- trucvert_configuration.yaml
- biscarrosse_configuration.yaml
- gravelines_configuration.yaml

These three files are pre-filled with parameters used during the creation of the DTM part of the MEPELS system.
They are thus functional files.

If the study concerns another range, it is possible to create a new configuration file.

.. code-block::

    >>> touch filenameconfig.yaml  
	

Then copy and paste the whole content of one of the 3 files into the new file, in order to get all the variables. 
Or by using the command:

.. code-block::

    >>> cp ./trucvert_configuration.yaml ./filename_configuration.yaml

to create a new file with the content of the source file trucvert.yaml.
The trucvert.yaml file is the only file containing notes to help understand the variables.
See section :ref:`input_variable` for more information about variables.


Fill (new) configuration file
-------------------------------

The configuration file is opened with the command:

.. code-block::

    $ gedit filename.yaml
	or
    vi filename.yaml (for advanced users)

The file consists of 8 parts grouping the variables.
Each parameter is preceded by a note indicating its function.

It is advised to take your time when filling the file to avoid errors when using the module.
The access paths of the files are written in relative (with respect to the position of the pre-processing module) to access the bathymetry and the backup folder of the three beaches presented above (green stuff, biscarrosse, graveline).
Writing the path in absolute (from the origin of the folder: D/folder1/folder2/) or web link is preferable to avoid path errors.


Start preprocessing compute
============================

There are two possible ways to start the module.
The use of a python program or the use of the python module from a terminal.
In both cases the command is done from the folder containing the preprocessing module.
To access the pre-processing module folder from the configuration files folder:

.. code-block::

    $ cd ../

In the first case, if the software used is spyder3 (as an example):

.. code-block::

    $ spyder3 preprocessing_mnt.py

allows to open the module in the software.
The use of 'F5' or the press on the run button (green triangle) allows to launch the reading of the module (in the case of the spyder code).

In the second case, it is possible to use a python module.
If you don't have python, you just have to download it:

. code-block::

    $ sudo apt-get update
    $ sudo apt-get install python3.8

The "3.8" represents the desired version.

Once the module is installed:

.. code-block::

   $ >>> ipython
   (ipython) >>> %run preprocessing_mnt.py
   or
   $ >>> python preprocessing_mnt.py

