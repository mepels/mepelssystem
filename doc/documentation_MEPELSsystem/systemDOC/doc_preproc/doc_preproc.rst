Script pre-processing mnt
***************************

The mnt pre-processing script is articulated with 13 functions, 6 of which are incorporated in a library in order to lighten the script (long function or used several times).
All the functions present in the script are not necessarily used for each study.

The sequence of functions in the script is described schematically by the flowchart.

.. image:: ./../../images/run_pre_traitement/pre-traitement_mnt.png
  :width: 600

(Organization chart of the mnt preprocessing script.
Functions with the same name indicate that the function can be used in two different places independently or in parallel in the script.)


Reading bathymetry
==========================

In the configuration file fill in the parameters:


- **local_Path** path to the local bathymetry
- web_path** path to web bathymetry 
- extension** the extension of the bathymetry file
- **delimponct** concerns the punctuation between the columns for '.txt' and '.xyz' files.


The extension parameter allows the indata function to automatically take the reading method adapted to the bathymetric file.
Thus, the indata function returns data in vector form (X, Y and h) for files with extensions ".txt" and ".xyz".
While for the ".grd" and ".nc" files, the indata function returns the data as a matrix (mesh).


Definition of the study area
==============================



In the configuration file fill in the parameters of the **changeStudyArea** module: 

- changeArea** Activate the definition of the study area from the parameters in the configuration file.
- Xorig** Position along the x-axis of the offshore origin.
- Yorig** Y-axis position of the offshore origin.
- Lx** Length of the study area along the x axis (to be defined according to the bathymetry coordinate system)
- Ly** Length of the study area along the y axis (to be defined according to the bathymetry coordinate system)
- origstudy** Seaward side (taken according to the raw bathymetry visualization.


By setting the 'changeArea' parameter to 0, it is possible to enter the parameters from the script.
Also, if the data entered in the configuration file are not suitable, it is possible to try from the script.
However, the values entered from the script will not be saved at the output of the script.


Depending on the side of the origin point (origstudy), the script defines the positions of the lower left and upper right points of the study area in the initial bathymetry reference frame.

Thus, depending on the side of the origin, the script adds or subtracts the lengths to reach the corner points of the study area.

Thus, a regular mesh is defined in the study area.

Rotation of the area: first possibility of use
===================================================================

Fill in the following parameters of the **changeRotation** module in the configuration file:

- **axisrotation** Rotation angle in degrees
- rotArea** Activation of the rotation
- trigosense** Rotation direction (trigonometric)



The rotation function has a special feature.
If it is active, the function is automatically called twice during the two possible calls of the rotation function.

The first call of the rotation function allows to rotate the study area to position it in the cross-shore/long-shore reference frame internal to the bathymetry origin.

The second rotation, which occurs later in the script, will rotate the bathymetry (and not just the study area) to position the entire bathymetry in an East/North reference frame.

**Note**: The direction of rotation is defined with the trigonometric direction.

beginfigure**[H]
centering
includegraphics[width=350pt]../image/preprocessor_mnt_1/bathy_brut_entrer.png**
captionlabelfig:bathy_raw**Visualization of the first figure displayed by the pre-processing module in order to know the usable bathymetric zone. And thus, to be able to define a study area inferior to the dimensions of the bathymetry.**


.. image:: ./../../images/run_pre_traitement/bathy_brut_entrer.png
  :width: 600

(Visualization of the first figure displayed by the pre-processing module in order to know the usable bathymetric zone. And thus, to be able to define a study area inferior to the dimensions of the bathymetry.)

Mathematically, the rotation function starts by transforming the angle (axisrotation) given in degrees into radian.
This transformation is done by multiplying the angle in degrees by pi / 180, giving the parameter **axisrotationR**.
Then for each point of the mesh the function calculates the distance between the point of origin and the point of the mesh (Xold - Xorig) and adds the position of origin (Xorig) to keep the geographical correspondence (otherwise it would be a change of reference).
It remains to multiply this distance by a cosine or a sine (depending on the change of the desired rotation) to obtain the position of the points in the new reference frame.
Note that only the point of origin does not need to have its coordinates changed.

The pair of equations allowing to obtain the new mesh according to x for a rotation in a trigonometric direction is:

.. code-block::

    axisrotationR = axisrotation * np.pi/180.
    X = Xorig + (Xold - Xorig) * np.cos(axisrotationR) - (Yold - Yorig) * np.sin(axisrotationR),

With Xorig the x-position of the origin points and Xold the points of the original mesh.

To know the formulas for changing the mesh reference frame along y and in the anti-trinimetric direction.

**Important**:
If the rotation required to change the bathymetry to an East/North reference frame is in the trigonometric direction, then the first rotation will be in the anti-trigonometric direction and the second in the trigonometric direction.
And vice versa if the bathymetry rotation is to be done in the anti-triginometric direction.

Projection: first possible use
============================================


Fill in the parameters of the **projection** module:

- before** Activate the projection at the first call.
- coorIn_B** Code assigned to the coordinate system of the original bathymetry.
- **coorOut_B** Code assigned to the coordinate system of the desired bathymetry.

As for the rotation function, this function can be called a second time.
Here the projection allows you to change the coordinate system so that you are now in the coordinate system you wish to use.

However it is not recommended to activate this function now, especially in case of missing 'NaN' values.
The script does not (at this time) automatically handle the change of benchmarks for alternative bathymetries.

Projections are handled automatically by python packages.

However, the projection packages are composed of different equations allowing to switch from one coordinate system to another.

Interpolation
===============

Fill in the parameters of the **interpolation** module:

- InterpMethod** Interpolation method.
- partition** Number of bathymetry subdivisions per axis.
- savehraw** Enable saving of the interpolated bathymetry.
- BackupInterp** Enables the use of previously saved interpolated bathymetry.
- folderBackupInterp** Path to save and read interpolated bathymetry.


The interpolation allows to assign to each point of the mesh a value on the regular grid created by the delimited study area.

For this part the script calls the interpolation function.

Mathematically, the interpolation function cuts the bathymetry into subdivisions according to the value assigned to 'partition'.
The slicing starts in the lower left corner of the study area and proceeds in the direction of increasing x.
Once the number of subdivisions is defined along the x axis, the function moves towards the increasing y axis and starts defining subdivisions along the increasing x axis. 

This is equivalent to defining subdivisions of the bathymetry from 1 to 4 (for partition = 2) as shown in the figure refsch:creation_section**.

.. image:: ./../../images/run_pre_traitement/creation_section.png
  :width: 600

(Schematic of the creation of subdivisions for interpolation according to the position of the subdivisions. In green the square section and in blue the rectangular section. Here 2 subdivisions are taken by axis (x;y).)


As the subdivisions are never defined with the same number of grid cells (the bathymetries are not square), the function defines the external subdivisions (blue on the figure above) with larger dimensions in order to interpolate all the values of the area.
This is because the bathymetry is divided by an integer.
This results in the following lines of code:

.. code-block::

    fillx = Nx // partition
    filly = Ny // partition
    endfillx = Nx - (fillx * partition)
    endfilly = Ny - (filly * partition),

With fillx(/y) the number of meshes assigned per subdivision, and endfillx(/y) the positional dimension of the maximum of the subdivision.

The bathymetry is not square, so the outer subdivisions recover the surplus value.
The dimension of the internal subdivisions correspond to the number of bathymetry grid cells divided by the number of subdivisions required.


For each section the function performs an interpolation of order defined by the 'interpMethod' parameter by adding one mesh on the neighboring sections to keep the continuity.
The mathematical equations are available in the interpolation function file.

Once the subdivision is interpolated, it is placed in a new matrix to create a bathymetric mesh with real values or 'NaN' (Not a number) assigned to each point of the mesh.

Depending on the 'savehraw' parameter, the interpolated bathymetry (with values assigned to a regular mesh) can be saved.

Missing value replacement
===================================

Fill in the parameters of the **missingValues** module:

- missVal** Definition of the replacement method
- missmore** Maximum number of missing values ('NaN') before completely changing the bathymetry.
- **partition** Number of subdivisions for interpolation (see refdoc:interp**) only if missingValues = 1
- ptsapprox** Number of points to use to generate a gradient allowing the replacement of a 'NaN' value (missingValues = 2)
- interMethod** Interpolation method (missValues = 1)


Two options for bathymetric replacement are possible.
By using another bathymetry or by using a gradient from the neighbors.
In the case of replacement by another bathymetry, the parameters **missingBathyPath** and **delimPonctMissVal** must be filled.

Note that if the initial bathymetry comes from the web the replacement bathymetry must also come from the web.
Works the same way for a bathymetry recorded locally.

The script calls the value replacement function.

If the missingValues parameter is 1, the user chooses to replace the missing values with a second bathymetry.
The 'missmore' parameter must then be filled in to indicate the number of values above which the bathymetry is completely replaced.

The function searches the bathymetry for all points with a 'NaN' value.

If the number is greater than the value assigned to 'missmore', the bathymetry is replaced, re-interpolated and returned to the script.

If the number is less than the value assigned to 'missmore', the function retrieves the coordinates of the points with 'NaN' values and retrieves the value assigned to the same point in the second bathymetry.
Then it performs an interpolation to assign the value to the points of the mesh of the initial bathymetry.

The second possibility to assign a real value to each point of the regular mesh is to use a gradient.

This gradient requires the filling of the parameters 'missingValues' = 2 and 'ptsapprox'.
This method is an iterative method and may require several passes for the total replacement of the missing values.

The ptsapprox value increases the precision by increasing the number of neighboring points used (initially the configuration files use 4 points).
Before starting the calculation, the function looks for the positions of the 'NaN' values and assigns to these values the value '0' in order to be able to assign them a value using the gradient.
However, in order to obtain a realistic value, the gradient is calculated in the increasing x-direction (taken before the point) or in the decreasing x-direction if the points before the missing value are not accessible (zone border or 'NaN' value).
The same process is performed for the y axis.

Mathematically, the gradient recovers the values of the 'ptsapprox' previous points and calculates the average height difference on these points (average of the height difference between the successive points).
One value is obtained along the x axis and a second along the y axis, the average of the two is kept.
This average is added to the previous point (x-1) to give the point a value.

The code in the case where the points are needed for the gradient calculation is the following:

.. code-block::

    H_bathy = np.nan_to_num(H_bathy)
    for k in range(len(missVal)):
        if missVal[k, 0] > n :
            if H_bathy[missVal[k, 0]-(1), missVal[k, 1]] != 0:
                for kk in range(n):
                     rplcVal = rplcVal + H_bathy[missVal[k, 0]-(1+kk), missVal[k, 1]] - H_bathy[missVal[k, 0]-(2+kk), missVal[k, 1]]
                        
                     rplcValx = rplcVal/n + H_bathy[missVal[k, 0]-1, missVal[k, 1]]
                     rplcVal = 0


If there are still 'NaN' values at the end of an iteration, the function replaces the '0' values by 'NaN' values and performs a new iteration until all the values of the mesh have a real value assigned.

Note that the replacement by the use of gradient can generate an error propagation if the 'NaN' value areas are extended or if there is an abrupt change of direction of the gradient (for example the top of a dune).
Moreover, near the edges of the study area and in the corners, this method may potentially not find a value to assign because there would not be enough values to simulate the gradients.


Rotation of the area: second possibility of use
========================================================

See the Rotation section above

Projection: second possibility of use
================================================



Fill in the parameters of the **projection** module:

- **after** Activate the projection on the second call.
- **coorIn_A** Code assigned to the coordinate system of the original bathymetry.
- **coorOut_A** Code assigned to the desired bathymetry coordinate system.


For operation refer to the first rotation function above.

The use of the projection at this point is recommended because there is no longer any use of external bathymetry.

The advantage of using the projection now is that you can relate the coordinate systems to a coordinate system using a metric definition that is necessary for the rest of the script.

Knowing that for the rest of the script it is preferable that the bathymetry is expressed in a metric coordinate system.

Filtering
==========

The filtering is managed from the script, the latter asks questions to know if it should be done or not.

The filtering step is managed by the gaussian_filter function of the scipy.ndimage.filters package.

This function asks for the bathymetry to be filtered and the standard deviation parameter to be used for the filtering as arguments.


An iterative loop has been created around this function to allow the user to perform several trials.
The user also has the option to save or not the changes made to the bathymetry for the rest of the script.

For more information on the filtering function of python please refer to the web page:
https://docs.scipy.org/doc/scipy/reference/generated/scipy.ndimage.gaussian_filter.html.



Local contact information
===========================

Fill in the **changeCoordGeo** parameter to enable the change of coordinates in a local system.

This function allows all bathymetries having the origin point defined offshore and being defined in a metric coordinate system to be able to define the origin point as the point (0;0) with the x's increasing along the cross-shore axis.

The mathematical principle is relatively simple.
Simply take the values of each point in the mesh and subtract the value of the origin point.
This must be done for the x-defined mesh with the x-value of the origin point and for the y-defined mesh with the y-value of the origin point.

The associated lines of code are:

. code-block::

    Xold = X
    Yold = Y
    X = Xold - xorig
    Y = Yold - yorig,

With xorig and yorig the coordinates of the point of origin and Xold and Yold the values of the points of the mesh according to x and y.

Note that the bathymetry must be defined in a local reference frame to be usable by Xbeach.

The axes of the bathymetry must then look like those obtained on the figure above.

Calculation of the bathymetric gradient
========================================


Fill in the **calGradh** parameter to activate it.

The calculation of the bathymetric gradient allows to observe the variations of height between two successive points.
This function is a good indicator of the acceptance of the replaced values.

In the case of the Truc Vert study seen in the section 'Using the system: Truc Vert beach', the calculation of the gradient gives the figure below.

beginfigure**[H]
centering
includegraphics[width=360pt]../image/preprocessor_mnt_1/calGradH.png**
captionlabelfig:calGradH**Visualization of the bathymetry gradient calculation.**
endfigure**

.. image:: ./../../images/run_pre_traitement/calGradH.png
  :width: 600

(Visualization of the bathymetry gradient calculation.)


The figure reffig:calGradH** shows the percentage of height differences between two points.

Mathematically, the gradient function calculates the difference in height between two points as a function of their distance.

The equation used is:

.. code-block::

    sqrtA^2 + B^2**,

With A the height difference between two successive points normalized by the size of a mesh according to the x and B the height difference between two successive points normalized by the size of a mesh according to the y.

The linked code is:

.. code-block::

    gradh = (((h-np.roll(h, 1, axis=0))/dx)^2 + ((h-np.roll(h, 1, axis=1))/dy)^2)^0.5,

With np.roll() the bathymetry 'h' shifted by one point along x (axis=0) or along y (axis=1).


The percentage must not exceed 0.3 otherwise the sand grains are supposed to move (fall).



Bathymetric inversion
=========================

This step is important since it allows to make the depth positive which is necessary for Xbeach.

Mathematically, the bathymetry inversion is done by multiplying by '-1' the bathymetry inverting all the bathymetry values.

Save as
=============

Fill in the parameters "**savePath_preproc**" and "**savePlotBathy**".

The script launches the save function and saves the data according to the value of the 'SavePlotBathy' parameter in the folder defined by 'savePath_preproc'.
