Script analyse pré-traitement mnt
***********************************
Script analysis pre-processing mnt
***********************************

Bathymetry reading
===========================

Here is the flowchart of the DTM pre-processing analysis script functions.

.. image:: ./../../images/analyse_preproc/analyse_bathy.png
  :width: 600

(Flowchart of the DTM pre-tracking analysis script)

After using the mnt pre-tra-ent script, the studied bathymetry is saved in a netcdf file with several parameters (also present in the configuration file).

To access and read the file, the **inputPath** parameter (parameter preceding the analysis_preprocessing_mnt** part) must be replaced in the configuration file.

Note that the path for reading the bathymetry in the analysis corresponds to the bathymetry recording path of the "pre-tra-ent mnt" script.

The file to be read must **mandatorily** be in netcdf format (.nc extension).
Therefore, the reading and loading of the data calls the 'indata' function (ref{doc:lecture_de_bathy** and ref{libs:indata**).
However the file extension is automatically set to "nc" and the file must be saved locally.

Cross-shore analysis
====================

The cross-shore analysis combines several functions in order to display on a single figure the markers of interest, the different cross-shore profiles and Dean's profile adjusted on the mean profile.

Mean Profile
---------------

The "np.mean" function is used on the long-shore bathymetry to calculate the mean profile based on the cross-shore profiles.

The command is: "np.mean(h, axis=0)";
With h the matrix containing the bathymetry values and axis=0 the indication that it should be calculated as a function of x (cross-shore).

The mean profile can then be plotted on the figure below via the black line.

At the same time, an iterative loop goes through all the cross-shore profiles and plots them on the same figure with grey dashes.

Closing depth: DOC
-----------------------------

The closure depth is calculated with the Hallermeier formulation (Hallermeier, 1978).

The resulting closure point is then plotted on the figure below and is defined by a blue horizontal line.


Definition of the vertical level
--------------------------------

Depending on the vertical standard used during the data recovery, it is possible to draw the horizontal line corresponding to the hydrographic zero standard.

However, it is necessary to fill in the sealevel module:

- **defined_zero** defines the vertical norm used
- **diff_to_zero_hydro** The separation distance (height) to the hydrographic zero norm
- **HTL** Maximum tidal height (average) 
- **LTL** Minimum tidal height (average)
- **HTL_approx** Approximation of the HTL to the highest bathymetric value
- **tidalrange** Tidal range

And the fit_dissipative modulus:

- **sigma** Standard deviation of the Gaussian filter
- nbs_fit** Number of passes of the Gaussian fit


If the height between the vertical standard of measurement and the hydrographic zero standard is known, then the script will recalculate the vertical positions of the reference zero is HTL and LTL.

This action is performed by the following lines of code:

. code-block::

    if cl['sealevel']['diff_to_zero_hydro'] != 'NaN':
        zero_hydro_val = zero_level_val + cl['sealevel']['diff_to_zero_hydro']
        plt.axhline(zero_hydro_val, color='gray', linestyle='-.', label='zero_hydrographic')
    if cl['sealevel']['HTL'] != 'NaN':
        HTL_val = zero_level_val + cl['sealevel']['HTL'] 
        z_max = HTL_val + cl['sealevel']['diff_to_zero_hydro']
        plt.axhline(z_max, color='red', linestyle='-.', label='HTL')
    else:
        print('Tide heigth THL unknow')
        z_max = np.max(profile) * cl['sealevel']['HTL_approx']
    if cl['sealevel']['LTL'] != 'NaN':
        LTL_val = zero_level_val + cl['sealevel']['LTL'] + cl['sealevel']['diff_to_zero_hydro']
        plt.axhline(LTL_val, color='blue', linestyle='-.', label='LTL ')
    else:
        print('Tide height LTL unknow')


Mathematically it is only a matter of adding the difference in height between the vertical norms applied to the HTL and LTL parameter heights respectively.

The objective of positioning these benchmarks (HTL, LTL and the hydrographic zero standard) is to remain consistent with the maritime definitions.

In the figure below, the height of the hydrographic zero standard is given by the gray dashed line, the height of HTL corresponds to the red dashed line and the height of LTL corresponds to the blue dashed line.

Profile of Dean
---------------

The morphodynamically active area is between the position on the cross-shore profile of HTL and DOC.

A vector of the dimension of the active morphodynamic zone is created (xpos-short in the script) and takes the values of the points included in the HTL, DOC interval according to the cross-shore axis.

.. code-block::

    profile _ short = meanProfile[k_x_min: k_x_max]
    xpos_short = xpos[k_x_max] - xpos[k_x_min: k_x_max],

With, k_x_min the position of the DOC and k_x_max the position of the HTL on the full cross-shore profile.

The Dean profile defined by the equation:

.. code-block::

    Dean_profile = A * x^B + C



This profile is the basis of all new bathymetry created digitally.
Three parameters are involved in the description of the Dean profile.
In order to adjust the Dean profile to the profile under analysis, the "curve_fit" function of the "scipy.optimize" package is used.
This function allows, by taking in argument the equation of the Dean function, the positions in x and the associated values in y, as well as the values to be associated to the variables A, B and to define the equation of the Dean profile.


. code-block::

    popt, pcov = curve_fit(func, xpos_short, profile_short, check_finite=False)

The values of the parameters are stored in the "popt" vector.

The function is then used by taking the x-positions and the "popt" vector as arguments.

. code-block::

    zFitted = func(xpos_short, *popt)


The cross-shore profile piece "zFitted" just needs to be reincorporated into the total cross-shore profile.
A one-dimensional vector (identical to the dimension of the cross-shore profile) is created and is filled with the upstream and downstream part of the average profile (before the DOC and after the HTL).
Then the cross-shore profile "zFitted" is added between the two. 


. code-block::

    profile = np.zeros(len(xpos))
    profile[0: k_x_min] = meanProfile[0: k_x_min]
   profile[k_x_max::] = meanProfile[k_x_max::]
    profile[k_x_min: k_x_max] = zFitted[:] ,

With "xpos" the length vector equal to the cross-shore profile.

In order to avoid the effects of "walking", a gaussian filter is applied to the Dean profile. 
This filter smoothes the Dean profile at the connections between the real profile (before DOC and after HTL) and the numerically created profile.

The filtered Dean profile is then displayed on the figure below in green.


.. image:: ./../../images/analyse_preproc/cross-shore_analyse.png
  :width: 600

(Visualization of the markers of interest (HTL, LTL, zero_hydrographic, DOC) and the cross-shore profiles adjusted by a Dean profile, average and all the cross-shore profiles of the bathymetry)



Model: surge
====================

Fill in the modules "Storm" and "mean":

- **T12_y** Maximum wave period over a duration of 12h in the year.
- **H12_Y** Maximum wave height over a period of 12 hours in a year.
- **T_mean** Average wave period.
- **H_mean** Average wave height



The surf model indicates which type of wave breaks on the beach.

The model uses H_b as a function of the surf similarity parameter (related to the slope of the beach).

For average conditions the equation is defined by:

.. code-block::

    zeta_mean = np.tan(Beta) / np.sqrt(Hb / L),

With, H_b a vector with increasing wave heights starting at half the height of H_mean up to one and a half times the height of H12_Y with 100 values, L the wave length:

.. code-block::

    L = (g * T_mean**2 / (2 * np.pi))

And Beta:

.. code-block::

    Beta = np.abs(popt[0] * popt[1] * pow(xpos_short[0], (popt[1] - 1))


Beta uses the "popt" vector calculated during the definition of the Dean profile to define the slope at the x-position (cross-shore) of the DOC.

The breaking wave evolution curve for storm conditions is obtained by replacing H_mean and T_mean by H12_Y and T12_Y respectively.

Thus for the conditions presented in the configuration file of the Truc Vert beach, the figure obtained for the two conditions is the figure below.

For both types of conditions, the waves break under a spilling regime indicating that the waves break far from the beach.

.. image:: ./../../images/analyse_preproc/breaker_type.png
  :width: 600


Model: Beach type
======================


To generate the beach type model proposed by Masselink (1993), the parameters to be filled in are the same as in the "model: breaker" section with the addition of the parameter **D_50** representing the size of the sand grains.

The model defines the Dean number and the normalized tidal range RTR.

The RTR parameter is defined as a function of the tidal range (HTL -LTL) and is normalized by the water height H_b.

The calculation of the number of Dean requires to know the speed of fall of the sand grains.
Thus, by imposing constant the variables of density of sand grains (srho = 2650) and water (rhow = 1024), and the kinematic viscosity of water (visk = 1.3e-3), only the parameter D_50 must be indicated in the configuration file.

The falling speed is defined by:

.. code-block::

    WSED = (visk * (np.sqrt(10.36**2 + 1.049 * D**3) - 10.36)/ D_50)


The number of Dean is then written:

.. code-block::

    Omega_mean = Hb / (WSED * T_mean),

For calm conditions and with T12_Y for storm conditions.

The prediction of the model indicates that the beach keeps bars no matter how they evolve, but that it can also see rip and terraces appear.

.. image:: ./../../images/analyse_preproc/type_de_plage.png
  :width: 600
