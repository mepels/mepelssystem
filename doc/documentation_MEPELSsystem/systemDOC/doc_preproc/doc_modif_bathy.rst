Script bathymetric modification
**********************************

The bathymetric modification script can be used after using the mnt pre-tra-ent analysis script or before.
The pre-tra-ent analysis script is used as an indication of the accessible beach states and the amplitude (cross-shore) of the active morphodynamic zone of the bathymetry. 


The following is a flowchart of the bathymetric modification script functions.


.. image:: ./../../images/run_modif_bathy/modif_bathy.png
  :width: 600

(Flowchart of the bathymetric modification script operation)



Cross-shore profile analysis
==============================

The bathymetric modification script starts by redoing a profile analysis in the same way as the mnt bathymetric analysis script.

Refer to the mnt pre-processing analysis doc for the different steps performed.

Profile of Dean
=================

After recalculating the DOC and HTL positions, the script starts an iterative loop.
This loop defines for each cross-shore profile of the bathymetry an associated Dean profile (Dean, 1991).
The method of creating the different Dean profiles is identical to the method explained in the mnt pre-processing analysis section.


Adding Bars and Crescent
=============================

Now that the bathymetry is adjusted by a Dean profile, it is possible to add bars and crescents.

To activate the addition of bars, fill in the parameter **number_of_bar** representing the number of bar(s) (from 0 to 2).

Next, you need to assign characteristics to the bars.
To do this, fill in the modules "**outerbar**" and "**innerbar**" which have the same variables:

- **width** Width of the bar
- height** Height of the bar
- position** Position of the bar along the cross-shore axis
- **lambda** Wave length to simulate crescents.


The block from line 210 to 293 is responsible for modeling bars and crescents on the bathymetry.

The bars are modeled by a Gaussian function applied on each of the cross-shore profiles by an iterative loop.
Mathematically the Gaussian function is defined by:

.. code-block::

    gaussian = heigth * np.exp((-1/ 2) * ((x - mu) /sigma)**2)


The iterative loop starts by modeling the outerbarre.
A conditional loop checks if the parameter "lambda" of the module "outerbar" is "NaN" in the configuration file.
If "lambda" is "Nan", the loop does not generate a crescent bar and will take the values of the parameters "width", "height" and "position" to model the bar.

Once all the cross-shore profiles have been modified by the addition of a Gaussian, the iterative loop moves on to the modeling of the internal bar following the same scheme as for the external bar.

In case the parameter "lambda" is different from "NaN" (for the innerbar or the outerbar), the conditional loop will not generate a bar in the same way.

The explanation given above for the construction of bars is no longer used identically if lambda is different from "NaN".

Instead the crescents will be added via a sine function modulated by the amplitude of the crescents.
The crescents are defined along the long-shore axis with their amplitudes varying along the cross-shore axis.

Mathematically the creation of the crescents is written:

. code-block::

    func_crescent = ampl * np.sin(2 * np.pi * f * t) + pos_crescent

With ampl = width, pos_crescent = position, the frequency of the crescents:

.. code-block::

    f = 1 / (long_shore_width/nbs_croiss)

With long_shore_width:

.. code-block::

    long_shore_width = abs(max_long_shore) + abs(min_long_shore) 

And, nbs_creasing:

.. code-block::

    nbs_croiss = long_shore_width / lambda


With, max_long_shore and min_long_shore respectively the maximum position according to the long-shore profile and the minimum position according to the long-shore profile.

The gaussian function is then applied using the sine positions on the bathymetry (long-shore profile) instead of using the bar position (func_creasing instead of position).


Filtering
=========

For more information on the filtering function of python refer to the web page:
https://docs.scipy.org/doc/scipy/reference/generated/scipy.ndimage.gaussian_filter.html.