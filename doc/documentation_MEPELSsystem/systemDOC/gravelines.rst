
Some bathymetric data of the **SHOM** are recorded in the ".grd" format, we also find ".xyz" format and formats not yet recognized by the MEPELS system such as ".asc", ".glz" and ".bag" formats.
Below are visualizations obtained with the use of the pre-processing part of the MEPELS system with a ".grd" file taken from the SHOM documentation.
The beach used is the beach of Gravelines (between Calais and Dunkirk) with a different orientation and coordinate system than the beach of Truc Vert.

A chapter has been dedicated to the use of the system, you can refer to it to find the different steps.
The overall operation of the system is similar to the examples of use.
However, the data processing is faster because the ".grd" format already contains a bathymetric mesh.

The objective of this section is to show the versatility of the MEPELS system through the use of a ".grd" file.

The configuration file for the pre-processing of the Gravelines beach is included in the MEPELS system.

Pre-processing MNT: Gravelines
===============================

Compared to the configuration file for the Truc Vert beach, several parameters require modification.

The major parameters to be modified are:

- **web_Path**: the path to the raw bathymetric data file.
- **extension**: the extension of the file to read.
- **date_abthy**: the date of the bathymetric data retrieval.
- **gridsize**: the size of the bathymetry grid.
- **changeStudyArea**: Manages the definition of the study area.
- **changeRotation**: Manages the rotation of the bathymetry.
- **module projection**: Manages the coordinate systems.


**Note** the coordinates of the offshore origin and the dimensions of the study area must be expressed in terms of the bathymetry coordinate system.
The bathymetry of Gravelines has been recorded in "WGS84" format which is expressed in degrees.

Run the mnt pre-processing script.

Choose the beach of Gravelines: 

.. code-block::

    Select your beach:
     1: biscarosse
     2: trucvert
     3: gravelines
     4: other, 
     Enter number: 
     Type in:
     >>> 3


The figure below shows a map of the location of Gravelines after reading the raw data in the ".grd" file.


.. image:: ./../images/gravelines/bathy_raw_gravelines.png
  :width: 600

(Visualization of the raw bathymetry read from the ".grd" file. The defined study area is represented by the red rectangle).

When arriving at the bathymetric inversion step, the script shows two figures.
The first figure below represents the 2D bathymetry of the script output with negative depths and the second figure is the 3D representation of the first figure.


.. image:: ./../images/gravelines/local_coordinate_systeme.png
  :width: 600

(Visualization of the 2D bathymetry at the end of the preprocessing script and before inversion).

.. image:: ./../images/gravelines/bathy3D_local_coord.png
  :width: 600

(Visualization of the 3D bathymetry at the end of the preprocessing script and before inversion).


Note: The local coordinate system is expressed in the same way as for the Truc Vert beach example.

The example of the use of the pre-processing script with the bathymetry of Gravelines recorded in a ".grd" format, highlights the versatility and the functioning of the latter.

**Warning:** Do not forget to tell the script to invert the bathymetry to have the positive depths required by the Xbeach model. 

Bathymetric analysis: Gravelines
===================================

The parameters used for this script are present in the analysis_preprocessing_mnt part of the Gravelines configuration file.

As for the pre-processing part, some parameters of the configuration file are linked to the studied bathymetry.
For the beach of Gravelines the modified parameters are:

- storm module**: period and wave height during a storm
- module mean**: period and average wave height
- module sealevel**: relative to the tides and the sea level reference.


The analysis script works as described in the usage example.
You can follow the example by changing the choice of the beach, choose **Gravelines** (number 3).

Hereafter, the figures obtained by using the bathymetric analysis script for the bathymetry of Gravelines.

.. image:: ./../images/gravelines/cross_shore_analyse.png
  :width: 600

(Visualization of the cross-shore profile of Gravelines beach with the mean profile (black line), a fit with the Dean profile (Dean, 1991) (green line), the long-shore profiles (grey line) and a reduced number of markers to define the morphodynamically active zone between the DOC value (blue line) and the HTL value (red line) defined from the hydrographic zero standard.
The position of the hydrographic zero (grey dotted line) is calculated according to the range, allowing the calculation of LTL (blue dotted line).

The figure above shows an analysis of the croos-shore profile.
With the introduced parameters, the active morphodynamic zone is present between about -3 meters and +3 meters from bathymetry datum.

.. image:: ./../images/gravelines/type_beaches_evolv.png
  :width: 600

(Model allowing the definition of the type of breaker waves as a function of the water height at the breaker point H_b and a similarity parameter in agreement with Bosboom & al. (2021).


The figure below shows that the Gravelines beach does not have a bar in its active morphodynamic zone.
The beach has a predominantly dissipative character.

.. image:: ./../images/gravelines/breaker_type_wave.png
  :width: 600

(Evolution of the beach type according to the model proposed by Masselink (1993) for two types of conditions: Stormy and average).


The figure above shows the type of wave breaking remaining in the spilling area. 
Spilling is characteristic of waves arriving on the beach with low energy Bosboom and Steve (2021, p. 183).

Bathymetric change: Gravelines
=======================================

With the information gathered in the previous section, the remodeled bathymetry must be dissipative.
Applying the Dean Dean (1991) profile is then the only modification to be made.


The last two figures above highlight the dissipative nature of the beach.

Contrary to the cases seen in the "Example use" section, the bathymetric modification of Gravelines will allow to smooth the profile without adding bar(s).

The only parameter to modify in the modif_bathy_preproc part of the Gravelines configuration file is: **number_of_bar** which must be set to 0 and the central positions of the barers which must be set to : **'nan'** for **outerbar / innerbar**.

Run the python script "run_modification_bathymétique_mnt" then choose the range: Gravelines (number 3).

The following figures illustrate the remodeling of the bathymetry to obtain a dissipative profile.

The figure below shows a comparison between the central profile of the original bathymetry and the average profile on the bathymetry adjusted by a Dean profile.

.. image:: ./../images/gravelines/gravelinesCrossfit.png
  :width: 600

(Comparison of the average cross-shore profile of the artificially created bathymetry (red line) without bars and the central cross-shore profile of the initial bathymetry (black line).


.. image:: ./../images/gravelines/graveline3D_fit.png
  :width: 600

(3D bathymetry obtained after applying a Dean profile (Dean, 1991) on all cross-shore profiles).



The figure above shows the 3D remodeled bathymetry after the application of the Dean profile to each cross-shore profile.



The figure below shows the 2D bathymetry after application of the Dean profile and without additional filtering.


.. image:: ./../images/gravelines/gravelines2Dfit.png
  :width: 600

(2D visualization of the remodeled bathymetry without the presence of bar and without additional filtering).
