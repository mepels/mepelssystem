Run analyse_preprocessing_mnt
*****************************

From the mnt folder, run the bathymetric analysis script using the python module:

.. code-block::

    $ python analyse_preprocessing_mnt.py
    
Choose the beach to study, here the beach of Truc Vert (2):

.. code-block::

    Select your beach:
     1: biscarosse
     2: trucvert
     3: gravelines
     4: other, 
     Enter number: 
     Type in:
     >>> 2
     
The script does not require any interaction during its run and it displays 3 figures.
The figure below displays the different cross-shore profiles (gray line) of the bathymetry, with the average cross-shore profile (black line) and the Dean profile estimated from the average cross-shore profile (green line).the figure displays the different cross-shore profiles (gray line) of the bathymetry, with the average cross-shore profile (black line) and the Dean profile estimated from the average cross-shore profile (green line).
The point corresponds to the closure depth/DOC is known reference levels are also displayed.

.. image:: ./../images/runpreproc/cross-shore_analyse.png
  :width: 600

The figure below shows the type of wave breaking taking into account the wave height and the slope of the beach via a similarity parameter (surf similarity).

.. image:: ./../images/runpreproc/breaker_type.png
  :width: 600

Finally, the figure below shows the beach type as a function of the relative tidal range (RTR) and the number of Dean.
The calculations are performed for storm conditions and average conditions (calm weather).
The model groups the beach states into 8 distinct categories.

The Truc Vert beach goes from a beach of riparian bars and possibly terraces to a beach consisting only of bars to end with dissipative bars.

The interval between the curves obtained highlights the possible evolution of the beach.
The curves begin with an RTR equal to 6 and a number of Dean between 1 and about 2.5 showing the presence of bars and rip, to finish with an RTR less than 2 and a number of Dean greater than 8.

.. image:: ./../images/runpreproc/type_de_plage.png
  :width: 600
