Run modif_bathy_preproc
************************

Run the script from the terminal with the python module:


.. code-block::

    $ python modif_bathy_preproc.py
    
Choose the beach to study, here the beach of Truc Vert (2):

.. code-block::

    Select your beach:
     1: biscarosse
     2: trucvert
     3: gravelines
     4: other, 
     Enter number: 
     Taper:
     >>> 2

The module asks if you want to see the 3D result of applying the Dean profile to each cross-shore profile of the bathymetry.

Note: The application of the Dean profile to all the cross-shore profiles of the bathymetry is a necessary step before the remodeling of the bathymetry.

.. code-block::

    Do you want to see Dean profile on the all the cross-shore profiles, 3D 
    Plot ? y/n
    Type: 
    >>> Your choice, no impact on the rest of the script.

The module then draws 2 figures, showing the bathymetry re-modeled according to the parameters entered in the configuration file.

The figure below displays the new bathymetry (in 3D) with the conditions that have been imposed.


.. image:: ./../images/runpreproc/remodelling_modif_bathy3D.png
  :width: 600
  
The figure below shows the comparison between the average remodelled profile (red line) and the central profile of the original bathymetry (black line).
The central positions of the added bars are drawn in blue dotted lines.

.. image:: ./../images/runpreproc/lecture_cross-shore_pts_caract.png
  :width: 600
  
The objective of the bathymetric reconstruction is to artificially create a bathymetry similar to the original bathymetry, with or without bars.
This is done in order to create input bathymetries for morphodynmic models.

.. image:: ./../images/runpreproc/profil_final_2D.png
  :width: 600

The module then draws the bathymetry being created (in 2D) and asks if a filtering is desired.

.. code-block::

    $ Filter bathymetry ? y/n
    Type:
    >>> n
    
The module saves the created bathymetry in a new netcdf file named: "trucvert2008-04-04_nonFilter_bathyDean_2bars.nc" next to the previous netcdf file.
The data of the initial bathymetry and all the parameters written in the first netcdf file are present in this new file.

To access the saved netcdf file.

.. code-block::

    $ cd ./../../inputdata/bathy/trucvert/



