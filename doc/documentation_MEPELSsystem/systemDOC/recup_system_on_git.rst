Récupération du système MEPELS
*******************************


La première étape est la récupération du dossier **MEPELSsystem** disponible sur Git contenant les différents codes et plusieurs exemples.

Les explications ci-après sont adaptées à l'utilisation sur un système **Linux**.

- Depuis un terminal, accéder au dossier (local sur la machine) où vous souhaitez enregistrer le système MEPELS.
- Taper les commandes suivantes:

.. code-block::

    $ git clone https://gricad-gitlab.univ-grenoble-alpes.fr/mepels/mepelssystem.git
