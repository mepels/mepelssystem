Usage example
*************

In this section we make an application of the MEPELS system on a -Truc Vert beache.
The case used is an example that takes the data from the campaign ECORS 2008.

The purpose of this example is to explain briefly how to use the MEPELS system with XBEACH. 
For the interactives part (configuration files) the definition of each parameter is give.
The code used are explains for you want add some modifications, else normally you don't need to take action on this type of code (except for choice config to used and plotting).


.. toctree::
   :maxdepth: 2

   run_preprocessing
   run_analyse_mnt
   run_modif_bathy
..   toctree_doc
..   initconfig2
..   preproc2MNT
..   analyse1
..   preprocMatlab   
..   XBEACH
..   postproc
..   initconfig
..   preprocMNT

Other example
**************

.. toctree::
   :maxdepth: 2
   
   gravelines
