Run preprocessing_mnt
**********************


To **start the calculation** of the DTM pre-processing, go to the mnt folder and start the calculation via the command:

.. code-block::

    $ cd ./../
    $ python preprocessing_mnt.py
    
Once launched the module asks which beach you want to study, choose the Truc Vert beach:

.. code-block::

    Select your beach:
     1: biscarosse
     2: trucvert
     3: gravelines
     4: other, 
     Enter number: 
     Taper:
     >>> 2
     
After reading the data the module displays a first figure showing the raw bathymetry and the study area represented by the red rectangle.
You can close the graphic window.

**During the different displays, remember to close the windows to continue.

.. image:: ./../images/runpreproc/bathy_brut_entrer.png
  :width: 600

The code then asks if the study area is well positioned.
The values visible on the axes depend on the geographical location of the beach and the coordinate system used.
The bathymetric data of the Truc Vert beach is expressed in Lambert3.

**Validate** by typing 'y' in the terminal.

The module interpolates the bathymetric points measured on a regular grid created according to the study area (see \ref{interp}), then replaces the points for which the bathymetric data are missing.

Once the bathymetry is interpolated and any missing values are replaced, the bathymetric area is displayed with the beach at the bottom and the ocean at the top, as shown in the figure below.

.. image:: ./../images/runpreproc/bathy_avant_filtrage.png
  :width: 600

You then have the possibility to filter the bathymetry with a Gaussian filter.
  
.. code-block::

    $ Filter Bathymetry ? [y]/[n]
    Taper: 
    >>> n
    
The figure below shows the 2D bathymetry in the local reference frame.
The origin (0;0) is placed at the center of the long-shore and offshore domain.
The X axis is oriented towards the beach.
The value of the origin point (0, 0) must be positive to be accepted by the morphodynamic models (like all points below the zero ocean level).
In this case the value of the origin point is negative.
  
.. image:: ./../images/runpreproc/bathy2dcoordloc.png
  :width: 600
  

The module asks if it is necessary to invert the values (sign of the values) of the bathymetry.
  
Confirm the reversal request.

.. code-block::

    $ Do you need to invert the bathymetry ? [y]/[n]
    Taper:
    >>> y
  
The mnt pre-processing module is finished, the data is saved in the inputdata folder.

To access the module output saving folder from the mnt folder:

.. code-block::

    $ cd ./../../inputdata/bathy/trucvert/
    
    
You will find 4 files, 2 figures, a ".txt" file and a ".nc" file.
  
