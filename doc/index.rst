.. mepelssystem documentation master file, created by
   sphinx-quickstart on Thu Jan 11 12:22:00 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Mepels-system documentation
===========================


The mepelssystem package provides Python classes useful to perform some plot with bathymetry data.

What is this repository for?
----------------------------

* SHOM Tools
* Version : 0.0.1
* Supported Python Versions : >= 3.4


Deployment instructions
-----------------------

You can get the source code from `gitlab
<https://gricad-gitlab.univ-grenoble-alpes.fr/mepels/mepelssystem>`_.

The development mode is often useful. From the root directory, run::

  python setup.py develop --user


Committing instructions (in development mode)
---------------------------------------------

A good starting point is to follow this `forking tutorial <https://guides.github.com/activities/forking/>`_.

To clone your fork of mepelssystem repository::

  git clone https://github.com/your_username/mepelssystem
  
To get the status of the repository::

  git status

In case of new/modified file(s)::

  git add new_file

To commit a revision on the local repository::

  git commit -m "comment on the revision"

To push the revision on your github mepelssystem repository::

  git pushfileName = outputPath + "_bathy.nc"
                
        x = X[0, 0:int(Nx)]
        y = Y[0:int(Ny), 0]
        
        # open netcdf file
        rootgrp = Dataset(fileName, "w", format="NETCDF4")
        
        # create dimensions (vectors)
        x = rootgrp.createDimension("x", np.size(x))
        y = rootgrp.createDimension("y", np.size(y))
        
        # for more informations on netcdf4
        # https://unidata.github.io/netcdf4-python/#variables-in-a-netcdf-file
        
        var = rootgrp.createVariable("X", "f8", ("x","y"))
        var[:,:] = X
        var.long_name = "X"
        var.units = "meter"
        
        var = rootgrp.createVariable("Y", "f8", ("x","y"))
        var[:,:] = Y
        var.long_name = "Y"
        var.units = "meter"
        
        var = rootgrp.createVariable("h", "f8", ("x","y"))
        var[:,:] = h
        var.long_name = "Bathymetimport_packagery"
        var.units = "meter"
        
        rootgrp.close()


To propose your changes into the main mepelssystem project, follow again the `forking tutorial <https://guides.github.com/activities/forking/>`_.


Core Developers
---------------

* Cyrille.Bonamy@univ-grenoble-alpes.fr
* Sylvain.Ferraris@univ-grenoble-alpes.fr
* Julien.Chauchat@grenoble-inp.fr

Emeritus Developers
------------------------

* etudiants E3

License
-------

mepelssystem is distributed under the GNU General Public License v2 (GPLv2). (A DISCUTER ET MODIFIER SI BESOIN)

.. _GPLv2: https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html


.. User Guide
.. ----------

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   introduction
   function
..   usage_exemple
..   mepelssystem.preprocessing.mnt.config_plage.example_config
..   overview
..   install
..   tutorial
..   Function
..   examples

.. toctree::
   :maxdepth: 2

   documentation_MEPELSsystem/edge_doc/input_variables
   documentation_MEPELSsystem/edge_doc/packages
   documentation_MEPELSsystem/edge_doc/netcdf_info_file
   documentation_MEPELSsystem/edge_doc/gif_edition
   documentation_MEPELSsystem/systemDOC/doc_preproc/doc_preproc
   documentation_MEPELSsystem/systemDOC/doc_preproc/doc_analyse
   documentation_MEPELSsystem/systemDOC/doc_preproc/doc_modif_bathy
   

Modules Reference
-----------------

Here is presented the general organization of the package
and the documentation of the modules, classes and
functions.

.. autosummary::
   :toctree: generated/

..   mepelssystem.preprocessing.mnt.libs.interpolation_function
..   mepelssystem.preprocessing.mnt.libs.replacementValues_function
..   mepelssystem.preprocessing.mnt.libs.projection_function
..   mepelssystem.preprocessing.mnt.libs.rotation_function
.. mepelssystem.preprocessing.mnt.libs.indata_function
.. mepelssystem.preprocessing.mnt.libs.out_data_function

.. toctree::
   :maxdepth: 2
   :caption: Example galleries:

   auto_examples/index
   documentation_MEPELSsystem/user_interaction/preproc_mnt_user
   documentation_MEPELSsystem/systemDOC/intro

More
----


.. toctree::
   :maxdepth: 1

..   changes
..   to_do

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
