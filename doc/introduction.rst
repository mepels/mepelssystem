Introduction
************

Le projet d’études amont MEPELS : Modèle d'Evolution des Plages et Environnements Littoraux Sableux, a pour objectif de développer un système de modélisation numérique visant a décrire et prédire l'environnement côtier et plus particulièrement les littoraux sableux à l'échelle de quelques jours. Ce projet gitlab représente une première ébauche du système MEPELS, sans interface graphique. Le système comporte 3 modules comme illustré sur la figure ci-dessous

.. image:: ./images/module_MEPELS.png
  :width: 600 
  
un module de **pre-pretraitement** -pre-processing en Anglais-, un module **Modèle Morphodynamique** et un module de **post-traitement** -post-processing en Anglais.

Le module de pré-traitement permet de construire un MNT qui servira de données d'entrée pour le module morphodynamique. Enfin le module de post-traitement permet d'afficher (à l'écran) les résultats des simulations morphodynamiques et de les comparer à des observations. 

En l'état actuel, le système fonctionne avec les données bathymétriques de la campagne ECORS2008 (Sénéchal et al., 2010), et plus précisément avec les données de la plage du Truc Vert (50km au Nord du bassin d'Arcachon), de la plage de Biscarrosse (50km au Sud du bassin d'Arcachon), ainsi que celles en libre accès du SHOM à Gravelines (à mi-chemin entre Calais et Dunkerque).

Le modèle de de construction MNT (pré-traitement) est entièrement développé en langage python, en respectant du mieux possible les bonnes pratiques permettant d'assurer la reproductibilité de la recherche scientifique (Loïc Desquilbet, 2019; Geir Kjetil Sandve, 2013). Le module de pré-traitement a également pour objectif de compléter des valeurs potentiellement manquantes dans les données bathymétriques et de générer un fichier d'entrée exploitable par les modèles morphodynamiques. Le second objectif du pré-traitement est de permettre la création d'une bathymétrie idéalisée en s'appuyant sur la classification des états de plages de (Wright and Short, 1984). Enfin, le dernier objectif du module de pré-traitement est de fournir à l'utilisateur des indicateurs sur le type de déferlement, la profondeur de fermeture et l'importance de la marée sur la morphologie de la plage. 

Dans la section 'Paramètres importants caractérisant les plages sableuses' nous présentons les notions, concepts et paramètres de base utilisés dans le module de pré-traitement. 

..
  Dans la section \ref{utilisation}, nous présentons un exemple complet d'utilisation du système sur la plage du Truc Vert (campagne ECORS'2008). Enfin dans la section \ref{exempleGravelines}, le système est utilisé pour le cas d'une plage située dans le détroit du pas-de-calais, à Gravelines. 

.. 
  Le modèle morphodynamique retenu pour la validation du fonctionnement du système de pré-traitement est le modèle Xbeach mais le modèle CROCO est également disponible et les scripts nécessaires au pre- et post-traitement sont fournis avec le système.



Paramètres importants caractérisant les plages sableuses
********************************************************
..
   \label{sec:paramsPlage}

Le module de pré-traitement bathymétrique utilise un certain nombre de concepts et de classifications comme le type de déferlement des vagues, les états morphologiques des plages sableuses, les références de niveaux verticaux ou les systèmes de coordonnées géographiques qu'il est utile de rappeler avant de présenter l'outil numérique développé. 


Classification des types de déferlement
=======================================

Le type de déferlement des vagues est un paramètre important à déterminer au sein du système MEPELS. La figure suivante montre schématiquement les 4 types de déferlement qui sont habituellement classifiés via le nombre d'Irribaren  (Bosboom and Stive, 2021) :
 
.. math:: \epsilon = \frac{tan \alpha}{\sqrt{H_0 / L_0}},

où :math:`\alpha` représente la pente de la plage, :math:`H_0` la hauteur des vagues  et :math:`L_0` la longueur d'onde tout deux pris en eau profonde (au large).


* **spilling** :  :math:`\epsilon <0.5`
* **plunging** :  :math:`0.5< \epsilon <3.3`
* **collapsing** : :math:`3.3< \epsilon <5`
* **surging** :  :math:`\epsilon > 5`
 
La figure ci-dessous, tiré du livre de Bosboom and Stive (2021) p.183, présente les différents type de déferlement en fonction de la cambrure :math:`\epsilon`.
 
.. image:: ./images/breaker_type.png
  :width: 600 




Classification des états morphologiques des plages sableuses
============================================================
..
   \label{Classification_des_etats_morphodynamiques}

Selon Masseling and Short (1993), l'état morphologique des plages sableuses peut être déterminer en fonction de deux paramètres sans dimensions : Le nombre de Dean :math:`\Omega` aussi appelé vitesse de chute adimensionnelle et le marnage relatif appelé Relative Tide Range :math:`RTR` en anglais. 

Le nombre de Dean est défini par:

.. math:: \Omega = \frac{H_b}{\omega_s  T},

où :math:`H_b` représente la hauteur des vagues au point de déferlement, :math:`T` la période des vagues et :math:`\omega_s` la vitesse de chute des sédiments.

Le marnage relatif est défini par:

.. math:: RTR = \frac{MSR}{H_b},

où :math:`MSR` représente le marnage. L'amplitude de la marée peut varier de manière significative au cours du cycle lunaire et au cours des saisons (équinoxe/solstice) et représente un paramètre d'entrée du système MEPELS. 

La figure suivante montre la classification de Masselink and Short (1993) en fonction du nombre de Dean et du RTR. 

.. image:: ./images/Capture.PNG
  :width: 600 
  
..
  Modèle créer par Masselink and Short (1993) sur la base des travaux de Wright and Short (1984) avec l'ajout de l'influence du marnage normalisé (MSR / Hb). Les plages sont différenciées en 8 catégories.

En l''absence de marée (RTR=0), un nombre de Dean faible (<2) caractérise une plage réflective présentant une pente "forte" sur laquelle les vagues viennent déferler prêt du trait de côte. Les vagues ne sont pas assez énergétique pour mobiliser les sédiments de manière importante. Entre ces deux nombre de Dean, la taille du grain peut être mixte de fin à grossier.

Pour un nombre de Dean élevé (>5) la plage est qualifiée de dissipative avec une pente plus faible que dans le cas précédent et exhibant potentiellement une ou plusieurs barres sableuses parallèle au trait de côte. Pour des nombres de Dean intermédiaire (2<:math:`\Omega`<5) la plage présente une morphologie avec des barres prononcées, souvent en croissant voir avec des chenaux et de terrasses (Wright and Short, 1984). La morphologie des plages évolue bien sûr au grès des tempêtes et des saisons (hiver/été). Ces évolutions sont généralement lentes par rapport à la variabilité des forçages, notamment des vagues. 

La description précédente reste valable pour une amplitude de marée faible correspondant à un RTR < 3. Pour des valeurs comprises entre 3 < RTR < 7, les états morphologiques précédemment présenté changent légèrement avec la disparition des barres aux grands nombre de Dean (:math:`\omega` >5) et la formation de terrasse de courant d'arrachement à marée basse pour les faible nombre de Dean (:math:`\omega` <2). 

Lorsque le RTR est compris entre 7<RTR<15, seulement deux catégories subsistent les plages avec terrasse de marée basse pour :math:`\Omega` <2 (reflective à marée haute et dissipative à marée basse) et les plage ultra dissipative pour :math:`\Omega` >2 qui présente une pente très faible et sans barres. 

Enfin, lorsque le RTR est supérieur à RTR>15 le système devient dominé par la marée et la pente de la plage devient extrêment faible (replats de marée ou tidal flats en anglais). 

Profondeur de fermeture
=======================

Un paramètre important pour la morphologie des plages et le système MEPELS est la profondeur de fermeture, ou Deph Of Closure (DOC) en anglais, qui correspond à la profondeur limite au-delà de laquelle la morphologie n'évolue plus. A cette profondeur, la contrainte de cisaillement au fond généré par les vagues et le courant ne sont plus suffisant pour transporter les sédiments, \textit{i.e} inférieure à la contrainte seuil.

Pour le système MEPELS, la formule proposé par Hallermeier (1981) est utilisée :

.. math:: DOC = 2.28  H_{12Y} - 68.5 \frac{H^2_{12Y}}{gT^2_{12Y}},

où :math:`H_{12Y}` représente la hauteur des vagues et :math:`T_{12Y}` leurs période pour un événement intense voir extrême ayant duré au moins 12 heures au cours de la dernière année.


Système de coordonnées usuels
=============================
..
   \label{systeme_coordonnees}

Lorsque l'on travaille avec des données géographiques il faut préciser la référence de niveau vertical et potentiellement changer de système de projection géographique. 


Références de niveau vertical
-----------------------------
..
   \label{norme_for_zero}

Concernant la référence de niveau vertical, il existe trois possibilités fréquemment utilisées : le Nivellement Général de la France (NGF), le niveau de mi-marée et le zéro hydrographique.

La Nivellement Général de la France (NGF) constitue un réseau de repères altimétriques disséminés sur le territoire français. Les repères altimétriques permettent de déterminer l’altitude en chaque point du territoire. En NGF IGN69, l’altitude zéro (NGF 0) de référence est déterminée par le marégraphe de Marseille.

Le zéro hydrographique est le niveau de référence des cartes marines et des annuaires de marée. Cette référence en mer est différente de la référence des altitudes portées sur les cartes terrestres (IGN69). Tous les levés bathymétriques (les profondeurs) sont rapportés au zéro hydrographique. 

Le zéro hydrographique est défini au voisinage du niveau des plus basses mers astronomiques : le marin est ainsi assuré de disposer d'au moins autant d'eau que ce qui est indiqué sur la carte marine.

Deux autres références verticales importantes en hydrographie sont : le niveau moyen, utilisé notamment pour les modélisations de courants et vagues, le niveau des plus hautes mers astronomiques, qui participe à la définition du trait de côte.

Pour plus d'informations, se référer au site du SHOM : http://refmar.shom.fr/fr/sea_level_news_2013/2013_t4/shom-bathyelli-produit-zero-hydrographique-reference-ellipsoide-sur-data.shom.fr

Le système MEPELS doit prendre en compte la référence verticale utilisée pour les données bathymétriques d'entrées et permettre un ajustement de cette référence en sortie afin d'uniformiser les bathymétries venant de sources hétérogènes.

Systèmes de projection géographique
-----------------------------------
..
   \label{projection}

Le terme de projection ne doit pas être compris dans le sens de projection géométrique mais comme une transformation mathématique faisant correspondre des points du globe et des points du plan\footnote{https://fr.wikipedia.org/wiki/Projection_cartographique}. Les données bathymétriques peuvent être disponible au format géographique WGS84 correspondant au standard GPS. La position d'un lieu est alors définie par un couple (Latitude, Longitude) données en degré. 

Certains modèles numériques, dont CROCO, accepte comme données d'entrées le système de coordonnées WGS84 mais d'autres, comme Xbeach, ne le permettent pas. Dés lors il est préférable de se ramener toutes les bathymétries dans un système de projection plane commune. Le système de projection classique (en France tout au moins) est la projection conique conforme de Lambert. Les relations mathématiques entre les coordonnées géographiques  (latitude, longitude) = (:math:`\phi,\lambda`) d'un point du globe et les coordonnées (X,Y) sur la carte Lambert peuvent s'écrire de la manière suivante :

.. math:: 
  :nowrap:
  
   \begin{eqnarray}
   X &=  X_0 + \rho \sin(\theta)\\
   Y &= Y_0 + \rho_0 - \rho \cos(\theta)
   \end{eqnarray}

avec

.. math:: 
  :nowrap:
  
   \begin{eqnarray}   
   \theta & = n(\lambda - \lambda_0)\\
   \rho & = \rho(\varphi)\\
   \rho_0 & =  \rho (\varphi_0)
   \end{eqnarray}

où  (:math:`\rho,\theta`) sont les coordonnées du point projeté sur le cône. L'axe des X est croissant vers l'est et l'axe des Y est croissant vers le nord. 

Dans le cadre du système MEPELS, les plages étudiées jusqu'ici sont situées sur les côtes Française pour lesquelles le système de coordonnées usuel est le système de coordonnées global de la France (RGF93) autrement appelé Lambert93. Dans la nomenclature international, chaque système de coordonnées possède un code différent commençant par **EPSG:**, indiquant qu'il s'agit d'un système de coordonnées. Ci-dessous une liste des systèmes de coordonnées et de leur code, qui ont été rencontré lors de la création du système :

* Lambert93: "EPSG:2154" France Entière [métrique]
* LambertI: "EPSG:27571" Nord de la France [métrique]
* LambertII: "EPSG:27572" Centre de la France [métrique]
* LambertIII: "EPSG:27573" Sud de la France [métrique]
* WGS84: "EPSG:4326" système géodésique mondial [degrés]



Toutefois, il est assez compliqué de réaliser une projection sans altération des distances dans le système de coordonnées final.
En effet, dans certain cas il a été remarqué que des distorsions d'angles pouvaient apparaître dans le système de coordonnées projeté. Il faut donc être vigilant d'utiliser un système de projection adapté à la zone géographique étudiée (Alcaras et al., 2020).



