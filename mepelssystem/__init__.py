import warnings

from ._version import __version__

warnings.simplefilter("always", category=DeprecationWarning)
