#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jul  6 09:58:39 2021

@author: ferraris6s
"""


import numpy as np
import os
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
from datetime import datetime
from scipy.optimize import curve_fit
import sys

sys.path.append("./libs/")
from mydate import datenum
from mydate import datenum_to_datetime

from netCDF4 import Dataset
import yaml


from AnalyseProfiles import AnalyseProfiles
from AnalyseProfiles import func

sys.path.append("./../preprocessing/mnt/libs/")
from replacementValues_function import replacementValues


def analyse(beach_config):
    def find_nearest(array, value):
        array = np.asarray(array)
        idx = (np.abs(array - value)).argmin()
        return array[idx], idx

    with open(
        r"./configurations/" + beach_config + "_config_postproc.yaml"
    ) as file:
        cl = yaml.full_load(file)

    #############################################
    #                 parameters                #
    #############################################

    # data path
    # data bathy
    bathy_file = cl["data_path"]["basepathPreproc"]
    # path saving figures
    figPath = os.path.dirname(cl["data_path"]["basepathXB"]) + "/figures/"
    try:
        os.mkdir(figPath)
    except:
        pass
    figPath = figPath + "compare/"
    try:
        os.mkdir(figPath)
    except:
        pass

    date_0 = datetime.strptime(cl["parameter"]["date_0"], "%Y-%m-%d %H:%M")

    # path saving figures
    date_f = datetime.strptime(cl["parameter"]["date_f"], "%Y-%m-%d %H:%M")
    date_0num = datenum(date_0)
    date_fnum = datenum(date_f)

    xlimit = [date_0num, date_fnum]
    xxtick = np.linspace(date_0num, date_fnum, 6)

    xlimit = [date_0num, date_fnum]
    xxtick = np.linspace(date_0num, date_fnum, 6)
    xxticklabels = [str(datenum_to_datetime(i))[0:10] for i in xxtick]

    #################################################
    #           change frontsize / subplot          #
    #################################################
    # frontsize
    plt.rcParams.update({"font.size": 16})
    mpl.rcParams["lines.linewidth"] = 3
    mpl.rcParams["lines.markersize"] = 5
    mpl.rcParams["lines.markeredgewidth"] = 1

    # gs = gridspec.GridSpec(3, 1)
    # gs.update(left=0.15, right=0.95, top=0.95,
    #           bottom=0.1, wspace=0.125, hspace=0.25)

    # subplot
    figwidth = 14
    figheight = 10
    cmapBathy = cl["plotting"]["colormaps"]
    gs1 = gridspec.GridSpec(
        1,
        1,
        figure=None,
        left=0.15,
        bottom=0.15,
        right=0.8,
        top=0.8,
        wspace=0.3,
        hspace=0.25,
    )
    gs2 = gridspec.GridSpec(
        1,
        2,
        figure=None,
        left=0.15,
        bottom=0.15,
        right=0.8,
        top=0.8,
        wspace=0.3,
        hspace=0.25,
    )
    gs4 = gridspec.GridSpec(
        2,
        2,
        figure=None,
        left=0.15,
        bottom=0.15,
        right=0.8,
        top=0.8,
        wspace=0.3,
        hspace=0.25,
    )

    #%% 1) Read wave and tide data from FORCAGE file
    ncFor = Dataset(cl["data_path"]["Forcage"])

    timeF = ncFor.variables["time"][:]
    dateF = ncFor.variables["date"][:]
    Hs = ncFor.variables["Hs"][:]
    Tp = ncFor.variables["Tp"][:]
    Dir = ncFor.variables["Dir"][:]
    Tide = ncFor.variables["Tide"][:]

    Xvec1 = cl["plotting"]["posvec1_x"]
    Yvec1 = cl["plotting"]["posvec1_y"]

    Xvec3 = cl["plotting"]["posvec3_x"]
    Yvec3 = cl["plotting"]["posvec3_y"]

    #
    # Read Almar et al. (2010) data
    # Data know on the bars
    # if cl['analysis']['after_preprocORxbeach'] == 0:
    #     pass

    # else:

    #     tInner,XInner,tmInner,XmInner,tMInner,XMInner=np.loadtxt(os.path.join(basepathDATA,'BARRES/donnees_barre_interne.csv'),\
    #                         delimiter=",",skiprows=2,
    #                         usecols=(0, 1, 2, 3, 4, 5),
    #                         unpack=True)

    #     tOuter,XOuter,tmOuter,XmOuter,tMOuter,XMOuter=np.loadtxt(os.path.join(basepathDATA,'BARRES/donnees_barre_externe.csv'),\
    #                         delimiter=",",skiprows=2,
    #                         usecols=(0, 1, 2, 3, 4, 5),
    #                         unpack=True)

    #     ####  A modifier  ####
    #     date0Almar = datetime.strptime('2008-03-06 00:00','%Y-%m-%d %H:%M')
    #     dateAlmar_Inner = datenum(date0Almar) + np.array(tInner);
    #     dateAlmar_Outer = datenum(date0Almar) + np.array(tOuter);
    ######################

    #%%
    ###############################
    #         READ BATHY          #
    ###############################
    ncfile = Dataset(bathy_file)

    X = ncfile.variables["X"][:, :]
    Y = ncfile.variables["Y"][:, :]
    if cl["data_path"]["makemodif"] == 0:
        h = ncfile.variables["h"][:, :]
    else:
        h = ncfile.variables["h_modelling"][:, :]

    ncfile_xbeach = Dataset(cl["data_path"]["basepathXB"])
    X_xbeach = ncfile_xbeach.variables["globalx"][:, :]
    Y_xbeach = ncfile_xbeach.variables["globaly"][:, :]
    zb_xbeach = ncfile_xbeach.variables["zb"][:, :, :]
    frame_nbs = len(zb_xbeach[:, 0, 0])
    h_xbeach = zb_xbeach[frame_nbs - 1, :, :]
    size_matrix_xbeach = np.shape(X_xbeach)
    Nx_xbeach = size_matrix_xbeach[1]
    Ny_xbeach = size_matrix_xbeach[0]
    Ymin_xbeach = np.min(Y_xbeach[:, 0])
    Ymax_xbeach = np.max(Y_xbeach[:, 0])

    h = -h  # reversed bathy to have negative elevation
    # verify the NaN values
    h = replacementValues(
        2,
        h,
        "NaN",
        "NaN",
        "NaN",
        "NaN",
        "NaN",
        "NaN",
        "NaN",
        "NaN",
        "NaN",
        "NaN",
        8,
        "NaN",
        "NaN",
    )
    h_xbeach = replacementValues(
        2,
        h_xbeach,
        "NaN",
        "NaN",
        "NaN",
        "NaN",
        "NaN",
        "NaN",
        "NaN",
        "NaN",
        "NaN",
        "NaN",
        8,
        "NaN",
        "NaN",
    )

    size_matrix = np.shape(X)
    Ny = size_matrix[0]
    Nx = size_matrix[1]
    Xmin = 0
    Xmax = np.max(X[0, :])
    Ymin = np.min(Y[:, 0])
    Ymax = np.max(Y[:, 0])

    if cl["plotting"]["zmin"] == "NaN":
        min_lvl = np.min(h)
    else:
        min_lvl = cl["plotting"]["zmin"]
    if cl["plotting"]["zmax"] == "NaN":
        max_lvl = np.max(h)
    else:
        max_lvl = cl["plotting"]["zmax"]

    bedLevels = np.linspace(round(min_lvl), round(max_lvl), 101)
    # bedLticks = np.linspace(round(min_lvl), round(max_lvl), 10, dtype=integer)
    bedLticks = range(
        round(min_lvl), round(max_lvl), (round(max_lvl) - round(min_lvl)) // 10
    )

    bedLtickLabels = [str(i) for i in bedLticks]

    #%% Search cross-shore position correspond to elevation zmin and zmax
    meanProfile = np.mean(h, axis=0)

    val_min_lvl, idxmin = find_nearest(meanProfile, min_lvl)
    val_max_lvl, idxmax = find_nearest(meanProfile, max_lvl)

    pos_min_lvl = X[0, idxmin]
    pos_max_lvl = X[0, idxmax]

    #%%  Plot mean profiles and filted profile  PLOT 1

    fig1 = plt.figure(
        1, figsize=(figwidth, figheight), dpi=100, facecolor="w", edgecolor="w"
    )

    # new method : compute on the modification profile
    X_curve_fit = X[0, idxmin:idxmax]
    # X_curve_down = X[0, 1: idxmin]
    # X_curve_up = X[0, idxmax: :]

    meanProfile_init = np.mean(h, axis=0)
    meanProfile_init_fit = meanProfile_init[idxmin:idxmax]

    popt, pcov = curve_fit(func, X_curve_fit, meanProfile_init_fit)
    zFitted = np.zeros(idxmax - idxmin)
    zFitted[:] = func(X[0, idxmin:idxmax], popt[0], popt[1], popt[2])
    # zFitted[0: idxmin] = meanProfile_init[0: idxmin]
    # zFitted[idxmax: :] = meanProfile_init[idxmax: :]

    zFitted[::-1]

    ax1 = plt.subplot(gs4[0, 0])
    ax1.plot(
        X[0, idxmin:idxmax], meanProfile_init_fit, "-r", label="mean profile"
    )
    ax1.plot(X[0, idxmin:idxmax], zFitted[:], "--b", label="fit profile")
    ax1.set_ylabel("Depth [m]", size=12)
    ax1.set_xlim([pos_min_lvl, pos_max_lvl])
    ax1.set_ylim([cl["plotting"]["zmin"], cl["plotting"]["zmax"]])
    ax1.set_title("Initial profile", size=12)
    ax1.set_xticklabels([])
    ax1.legend(loc="upper left", prop={"size": 10})
    ax1.grid(True)

    ax2 = plt.subplot(gs4[0, 1])

    ax2.plot(X[0, idxmin:idxmax], meanProfile_init_fit - zFitted, "-b")
    ax2.set_yticklabels([])
    # ax2.set_xlim([Xmin, Xmax])
    ax2.set_ylim([cl["plotting"]["zmin"], cl["plotting"]["zmax"]])
    ax2.set_title("Difference fit | final profile", size=12)
    ax2.set_xticklabels([])

    meanProfile_compute = np.mean(h_xbeach, axis=0)
    val_min_lvl_compute, idxmin_compute = find_nearest(
        meanProfile_compute, min_lvl
    )
    val_max_lvl_compute, idxmax_compute = find_nearest(
        meanProfile_compute, max_lvl
    )
    pos_min_lvl_compute = X_xbeach[0, idxmin_compute]
    pos_max_lvl_compute = X_xbeach[0, idxmax_compute]

    X_curve_fit_compute = X_xbeach[0, idxmin_compute:idxmax_compute]

    meanProfile_compute_fit = meanProfile_compute[idxmin_compute:idxmax_compute]
    # meanProfile_compute_down = meanProfile_compute[0: idxmin_compute]
    # meanProfile_compute_up = meanProfile_compute[idxmax_compute: :]

    popt, pcov = curve_fit(func, X_curve_fit_compute, meanProfile_compute_fit)

    zFitted_compute = np.zeros(idxmax_compute - idxmin_compute)
    zFitted_compute = func(
        X_xbeach[0, idxmin_compute:idxmax_compute], popt[0], popt[1], popt[2]
    )
    # zFitted_compute[0: idxmin_compute] = meanProfile_compute[0: idxmin_compute]
    # zFitted_compute[idxmax_compute: :] = meanProfile_compute[idxmax_compute: :]
    zFitted_compute[::-1]

    ax3 = plt.subplot(gs4[1, 0])
    ax3.plot(
        X_xbeach[0, idxmin_compute:idxmax_compute], meanProfile_compute_fit, "-r"
    )
    ax3.plot(
        X_xbeach[0, idxmin_compute:idxmax_compute], zFitted_compute[:], "--b"
    )
    ax3.set_xlabel("Cross-shore [m]", size=12)
    ax3.set_ylabel("Depth [m]", size=12)
    ax3.set_xlim([pos_min_lvl, pos_max_lvl])
    ax3.set_ylim([cl["plotting"]["zmin"], cl["plotting"]["zmax"]])
    ax3.set_title("Final profile", size=12)
    ax2.set_xticklabels([])
    ax3.grid(True)

    ax4 = plt.subplot(gs4[1, 1])
    ax4.plot(
        X_xbeach[0, idxmin_compute:idxmax_compute],
        meanProfile_compute_fit - zFitted_compute,
        "-b",
    )
    ax4.set_xlabel("Cross-shore [m]", size=12)
    ax4.set_yticklabels([])
    ax4.set_xlim([Xmin, Xmax])
    ax4.set_ylim([cl["plotting"]["zmin"], cl["plotting"]["zmax"] + 5])
    ax4.set_title("Difference fit | final profile", size=12)

    plt.show()

    #%% Plot measured bathy

    # for the initial bathymetry
    ## ASk the number of sandbar and a range to search it
    nbs_bar_init = input("Number of Bar(s): \t")
    range_bar = np.zeros((int(nbs_bar_init), 2))
    k = 0
    if nbs_bar_init == "2":
        print("\n Information: Overlapping is tolerated \n")
    while k < int(nbs_bar_init):
        range_bar[k, 0] = input(
            "Begin range position for the " + str(k + 1) + " bar : \t"
        )
        range_bar[k, 1] = input(
            "End range position for the " + str(k + 1) + " bar : \t"
        )
        k += 1
    #%% Search bars on bathymetry  PLOT 2

    # Analyse measured bathy to get bar positions
    if nbs_bar_init != "0":
        [
            meanProfile_init,
            ob2DMax_init,
            ob2DMin_init,
            ib2DMax_init,
            ib2DMin_init,
            Xo_init,
            Do_init,
            Deltao_init,
            Xi_init,
            Di_init,
            Deltai_init,
        ] = AnalyseProfiles(
            Nx, Ny, h[:, :], X[:, :], idxmin, idxmax, nbs_bar_init, range_bar
        )

    # gs4 = gridspec.GridSpec(2, 2)
    # gs4.update(left=0.175, right=0.92, top=0.95,
    #       bottom=0.1, wspace=0.15, hspace=0.15)
    # =============================================================================
    fig2 = plt.figure(
        num=2,
        figsize=(figwidth, figheight),
        dpi=100,
        facecolor="w",
        edgecolor="w",
    )
    ax1 = plt.subplot(gs4[0, 0])
    #% Contour of bed elevation
    cax = ax1.contourf(Y, X, h, bedLevels, cmap=cmapBathy)  # terrain
    ax1.set_ylabel("Cross-shore [m]", size=12)
    ax1.set_title("Inatial Bathymetry", size=12)
    ax1.set_ylim([pos_max_lvl, pos_min_lvl])
    ax1.set_xlim([Ymin, Ymax])
    ax1.set_xticklabels([])

    ax2 = plt.subplot(gs4[1, 0])
    #% Contour of bed elevation
    cax = ax2.contourf(Y, X, h, bedLevels, cmap=cmapBathy)  # terrain
    if nbs_bar_init != "0":
        for j in range(Ny):
            ax2.plot(Y[j, 0], X[j, ob2DMax_init[j]], "k*", label="outter bar top")
            ax2.plot(
                Y[j, 0], X[j, ob2DMin_init[j]], "rx", label="outter bar trough"
            )
        ax2.plot([Ymin, Ymax], [Xo_init, Xo_init], "--k", lw=4, label="outer bar")
        ax2.plot(
            [Ymin, Ymax], [Xo_init + Do_init, Xo_init + Do_init], "--r", lw=2
        )
        ax2.plot(
            [Ymin, Ymax], [Xo_init - Do_init, Xo_init - Do_init], "--r", lw=2
        )

        if nbs_bar_init == "2":
            for j in range(Ny):
                X[j, ib2DMax_init[j]]
                ax2.plot(
                    Y[j, 0], X[j, ib2DMax_init[j]], "b*", label="inner bar top"
                )
                ax2.plot(
                    Y[j, 0], X[j, ib2DMin_init[j]], "gx", label="inner bar trough"
                )
            ax2.plot(
                [Xi_init, Xi_init], [Ymin, Ymax], "--b", lw=4, label="inner bar"
            )
        ax2.plot(
            [Ymin, Ymax], [Xi_init + Di_init, Xi_init + Di_init], "--g", lw=2
        )
        ax2.plot(
            [Ymin, Ymax], [Xi_init - Di_init, Xi_init - Di_init], "--g", lw=2
        )
    ax2.set_ylabel("Cross-shore [m]", size=12)
    ax2.set_xlabel("Long-shore [m]", size=12)
    ax2.set_ylim([pos_max_lvl, pos_min_lvl])
    ax2.set_xlim([Ymin, Ymax])
    ax2.set_title("Initial bathy + bar", size=12)

    # for the compute bathymetry

    # Analyse measured bathy to get bar positions
    if nbs_bar_init != "0":
        [
            meanProfile_compute,
            ob2DMax_compute,
            ob2DMin_compute,
            ib2DMax_compute,
            ib2DMin_compute,
            Xo_compute,
            Do_compute,
            Deltao_compute,
            Xi_compute,
            Di_compute,
            Deltai_compute,
        ] = AnalyseProfiles(
            Nx_xbeach,
            Ny_xbeach,
            h_xbeach,
            X_xbeach,
            idxmin_compute,
            idxmax_compute,
            nbs_bar_init,
            range_bar,
        )

    ax3 = plt.subplot(gs4[0, 1])
    #% Contour of bed elevation
    cax3 = ax3.contourf(
        Y_xbeach, X_xbeach, h_xbeach, bedLevels, cmap=cmapBathy
    )  # terrain
    ax3.set_xlim([Ymin_xbeach, Ymax_xbeach])
    ax3.set_ylim([pos_max_lvl_compute, pos_min_lvl_compute])

    ax3.set_xticklabels(ax3.get_xticklabels(), rotation=45, ha="right")
    cbar3 = fig2.colorbar(
        cax3, orientation="vertical", ticks=bedLticks, fraction=0.075
    )
    cbar3.set_ticklabels(bedLtickLabels)
    ax3.set_yticklabels([])

    ax3.set_title("Final bathymetry", size=12)

    ax4 = plt.subplot(gs4[1, 1])
    #% Contour of bed elevation
    cax4 = ax4.contourf(
        Y_xbeach, X_xbeach, h_xbeach, bedLevels, cmap=cmapBathy
    )  # terrain

    if nbs_bar_init != "0":
        for j in range(Ny_xbeach):
            ax4.plot(Y_xbeach[j, 0], X_xbeach[j, ob2DMax_compute[j]], "k*")
            ax4.plot(Y_xbeach[j, 0], X_xbeach[j, ob2DMin_compute[j]], "rx")
        ax4.plot(
            [Ymin_xbeach, Ymax_xbeach], [Xo_compute, Xo_compute], "--k", lw=4
        )
        ax4.plot(
            [Ymin, Ymax],
            [Xo_compute + Do_compute, Xo_compute + Do_compute],
            "--r",
            lw=2,
        )
        ax4.plot(
            [Ymin, Ymax],
            [Xo_compute - Do_compute, Xo_compute - Do_compute],
            "--r",
            lw=2,
        )

        if nbs_bar_init == "2":
            for j in range(Ny_xbeach):
                ax4.plot(Y_xbeach[j, 0], X_xbeach[j, ib2DMax_compute[j]], "b*")
                ax4.plot(Y_xbeach[j, 0], X_xbeach[j, ib2DMin_compute[j]], "gx")
            ax4.plot(
                [Xi_compute, Xi_compute], [Ymin_xbeach, Ymax_xbeach], "--b", lw=4
            )
            ax4.plot(
                [Ymin, Ymax],
                [Xi_compute + Di_compute, Xi_compute + Di_compute],
                "--g",
                lw=2,
            )
            ax4.plot(
                [Ymin, Ymax],
                [Xi_compute - Di_compute, Xi_compute - Di_compute],
                "--g",
                lw=2,
            )
    cbar4 = fig2.colorbar(
        cax4, orientation="vertical", ticks=bedLticks, fraction=0.075
    )
    cbar4.set_ticklabels(bedLtickLabels)
    ax4.set_xlim([Ymin_xbeach, Ymax_xbeach])
    ax4.set_ylim([pos_max_lvl_compute, pos_min_lvl_compute])
    ax4.set_yticklabels([])
    # ax4.set_xticklabels(ax4.get_xticklabels(), rotation=45, ha='right')
    ax4.set_xlabel("Long-Shore [m]", size=12)
    ax4.set_title("Final bathymetry + bar", size=12)

    # ------------------------------------------------------------------------------
    # #%% Plot isobath and position captors

    fig4 = plt.figure(
        num=4,
        figsize=(figwidth / 2, figheight / 2),
        dpi=100,
        facecolor="w",
        edgecolor="w",
    )

    ax1 = plt.subplot(gs1[0, 0])
    #% Contour of bed elevation

    ax1.plot(Yvec1, Xvec1, "or", ms=12, label="VEC 1")
    ax1.annotate(
        "VEC 1",
        xy=(Xvec1, Yvec1),
        xytext=(Xvec1, Yvec1),
        arrowprops=None,
    )
    ax1.plot(Yvec3, Xvec3, "vr", ms=12, label="VEC 3")
    ax1.annotate(
        "VEC 3",
        xy=(Xvec3, Yvec3),
        xytext=(Xvec3, Yvec3),
        arrowprops=None,
    )

    # ax1.annotate('VEC 3', xy=(Xvec3,Yvec3), xytext=(Xvec3, Yvec3),
    #             arrowprops=dict(facecolor='red', shrink=0.05),
    #             )

    cax = ax1.contourf(Y, X, h, bedLevels, cmap=cmapBathy)  # terrain
    cbar = fig4.colorbar(
        cax, orientation="vertical", ticks=bedLticks, fraction=0.075
    )
    ax1.contour(Y, X, h, bedLticks, colors="k")
    ax1.set_ylabel("Cross-shore [m]", size=12)
    ax1.set_ylim([pos_max_lvl, pos_min_lvl])
    ax1.set_xlim([Ymin, Ymax])
    ax1.set_xlabel("Long-shore [m]", size=12)
    plt.title("Bathymetry with isobath", size=12)
    # plt.xlabel("cross-shore orientation", size=12)
    # plt.ylabel("long-shore orinetation", size=12)
    plt.colormaps()
    plt.legend(loc="upper left", prop={"size": 12})
    plt.show()

    # ------------------------------------------------------------------------------

    fig5 = plt.figure(
        num=5,
        figsize=(figwidth, figheight / 2),
        dpi=100,
        facecolor="w",
        edgecolor="w",
    )

    ax1 = plt.subplot(gs2[0, 0])
    #% Contour of bed elevation
    cax = ax1.contourf(Y, X, h, bedLevels, cmap=cmapBathy)  # terrain
    ax1.contour(Y, X, h, bedLticks, colors="k")
    ax1.set_ylabel("Cross-shore [m]")
    ax1.set_ylim([pos_max_lvl, pos_min_lvl])
    ax1.set_xlim([Ymin, Ymax])
    ax1.set_xlabel("Long-shore [m]")

    ax2 = plt.subplot(gs2[0, 1])
    ax2.plot(Yvec1, Xvec1, "or", ms=12, label="VEC 1")
    ax2.annotate(
        "VEC 1",
        xy=(Xvec1, Yvec1),
        xytext=(Xvec1, Yvec1),
        arrowprops=None,
    )
    ax2.plot(Yvec3, Xvec3, "vr", ms=12, label="VEC 3")
    ax2.annotate(
        "VEC 3",
        xy=(Xvec3, Yvec3),
        xytext=(Xvec3, Yvec3),
        arrowprops=None,
    )
    # cax2 = ax2.contourf(X, Y, h_compute, bedLevels, cmap=cmapBathy) #terrain
    cax2 = ax2.contourf(
        Y_xbeach, X_xbeach, h_xbeach, bedLevels, cmap=cmapBathy
    )  # terrain
    cbar = fig5.colorbar(
        cax2, orientation="vertical", ticks=bedLticks, fraction=0.075
    )
    cbar.set_ticklabels(bedLtickLabels)
    # ax2.contour(X, Y, h_compute, bedLticks, colors='k')
    ax2.contour(Y_xbeach, X_xbeach, h_xbeach, bedLticks, colors="k")
    ax2.set_yticklabels([])
    ax2.set_ylim([pos_max_lvl, pos_min_lvl])
    ax2.set_xlim([Ymin_xbeach, Ymax_xbeach])
    ax2.set_xlabel("Long-shore [m]")

    plt.show(block=False)
    #%% saving data

    fig1.savefig(
        figPath + "MeanProfileAndFit_compare.png",
        dpi=100,
        facecolor="w",
        edgecolor="w",
        format="png",
    )
    fig2.savefig(
        figPath + "MeasuredBathy_compare.png",
        dpi=300,
        facecolor="w",
        edgecolor="w",
        format="png",
    )
    # fig3.savefig(figPath+'MeasuredBarsPositions_compare.png', dpi=300, facecolor='w', edgecolor='w',format='png')
    fig4.savefig(
        figPath + "MeasuredBathyVEC13_compare.png",
        dpi=300,
        facecolor="w",
        edgecolor="w",
        format="png",
    )
    fig5.savefig(
        figPath + "MeasuredBathy_init-_computeVEC13_compare.png",
        dpi=300,
        facecolor="w",
        edgecolor="w",
        format="png",
    )

    return range_bar
