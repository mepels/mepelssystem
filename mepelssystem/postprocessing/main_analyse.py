#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jul 21 16:49:21 2021

@author: ferraris6s
"""


import time
from makeMovieXbeach_Hydro import moviehydro
from postprocTrucVertXbeach_Hydro import analysehydro
from morphoAnalysisPreprocMNT import analyse
from makeMovieXbeach_morpho import moviemorpho
from MorphoAnalysisXbeach import analysemorpho

start_time = time.time()

beach_letter = input(
    "Select your beach:"
    "\n 1: biscarrosse,"
    "\n 2: trucvert,"
    "\n 3: gravelines,"
    "\n 4: other,"
    "\n Enter number : "
)

if beach_letter == "1":
    beach_config = "biscarrosse"
elif beach_letter == "2":
    beach_config = "trucvert"
elif beach_letter == "3":
    beach_config = "gravelines"
elif beach_letter == "4":
    beach_config = input("Enter the yaml file name (without config.yaml):")

rep = "nan"
while rep != "y" or rep != "n":
    rep = input("Make a comparative analyse (before/after Xbeach) ? y/n \t")
    if rep == "y":
        range_bar = analyse(beach_config)
        break
    elif rep == "n":
        break


rep = "nan"
while rep != "y" or rep != "n":
    rep = input("Make a Hydro analyse ? y/n \t")
    if rep == "y" or rep == "n":
        break

if rep == "y":
    rep2 = "nan"
    while rep2 != "1" or rep2 != "2" or rep2 != "3":
        rep2 = input("Make a hydro analyse(1), movie (2) or both(3) ? 1/2/3 \t")
        if rep2 == "1":
            analysehydro(beach_config)
            break
        elif rep2 == "2":
            moviehydro(beach_config)
            break
        elif rep2 == "3":
            analysehydro(beach_config)
            moviehydro(beach_config)
            break
elif rep == "n":
    pass


rep = "nan"
while rep != "y" or rep != "n":
    rep = input("Make a morpho analyse ? y/n \t")
    if rep == "y" or rep == "n":
        break

if rep == "y":
    if "range_bar" in globals():
        pass
    else:
        print(
            "Restart and use the comparative analysis before morphologic analysis"
        )
    rep2 = "nan"
    while rep2 != "1" or rep2 != "2" or rep2 != "3":
        rep2 = input("Make a morpho analyse(1), movie (2) or both(3) ? 1/2/3 \t")
        if rep2 == "1":
            analysemorpho(beach_config, range_bar)
            break
        elif rep2 == "2":
            moviemorpho(beach_config)
            break
        elif rep2 == "3":
            analysemorpho(beach_config, range_bar)
            moviemorpho(beach_config)
            break
elif rep == "n":
    pass

print("--- %s seconds for compute ---" % (time.time() - start_time))
