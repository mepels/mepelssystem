#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Mar 19 17:15:06 2020

@author: chauchat
"""

from netCDF4 import Dataset
import numpy as np
from scipy.interpolate import Rbf
import os
from pylab import *
import matplotlib.gridspec as gridspec
from datetime import datetime as dt

import sys

sys.path.append("./libs/")
from RMS_T import RMS_T
from mydate import datenum
import pickle

# %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
# %%                         User functiions                              %%
# %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
def readCROCO(basepath, casename, CROCOgrid, klevel):
    #%
    #% Read the croco output file
    #%
    ncfileGRD = Dataset(
        basepath + casename + "/TRUCVERT_" + CROCOgrid + "_GRD.nc"
    )
    xu1 = ncfileGRD.variables["x_u"][:, :]
    yu1 = ncfileGRD.variables["y_u"][:, :]
    xv1 = ncfileGRD.variables["x_v"][:, :]
    yv1 = ncfileGRD.variables["y_v"][:, :]
    x1 = ncfileGRD.variables["x_rho"][:, :]
    y1 = ncfileGRD.variables["y_rho"][:, :]

    ncfile = Dataset(basepath + casename + "/TRUCVERT_" + CROCOgrid + "_his.nc")
    #% Netcdf output file
    timeCroco = ncfile.variables["time"][:]
    #% (s)
    srho = ncfile.variables["s_rho"][:]
    Nlevel = np.size(srho)

    zeta = ncfile.variables["zeta"][:, :, :]
    h = ncfile.variables["h"][:, :]
    hrm = ncfile.variables["hrm"][:, :, :] * np.sqrt(2)
    u = ncfile.variables["u"][:, :, :, :]
    v = ncfile.variables["v"][:, :, :, :]

    #
    # Locate VEC 1 & 3 stations
    #
    Xvec1 = np.max(x1) - 191.3 + 100
    Yvec1 = -430.27

    Xvec3 = np.max(x1) - 234.3 + 100
    Yvec3 = -455.8

    Ivec1 = np.max(np.where(x1[0, :] <= Xvec1)) + 1
    Jvec1 = np.max(np.where(y1[:, 0] <= Yvec1))
    Ivec3 = np.max(np.where(x1[0, :] <= Xvec3)) + 1
    Jvec3 = np.max(np.where(y1[:, 0] <= Yvec3))

    Nt = np.size(timeCroco, 0)

    Uvec1Croco = np.zeros((Nt))
    Vvec1Croco = np.zeros((Nt))
    Hvec1Croco = np.zeros((Nt))

    Uvec3Croco = np.zeros((Nt))
    Vvec3Croco = np.zeros((Nt))
    Hvec3Croco = np.zeros((Nt))
    VecLevel = 1
    for k in range(Nt):
        # ipu = Rbf(xu1,yu1,u[k,klevel,:,:], epsilon=2)
        # ipv = Rbf(xv1,yv1,v[k,klevel,:,:]);
        # iph = Rbf(x1,y1,hrm[k,:,:]);
        klevel1 = np.where(
            (1 + srho[:]) * (zeta[k, Jvec1, Ivec1] + h[Jvec1, Ivec1]) <= VecLevel
        )
        if np.size(klevel1) == 0:
            klevel1 = Nlevel
        else:
            klevel1 = np.max(klevel1)
        Uvec1Croco[k] = u[k, klevel1, Jvec1, Ivec1]
        Vvec1Croco[k] = v[k, klevel1, Jvec1, Ivec1]
        Hvec1Croco[k] = hrm[k, Jvec1, Ivec1]

        klevel3 = np.where(
            (1 + srho[:]) * (zeta[k, Jvec3, Ivec3] + h[Jvec3, Ivec3]) <= VecLevel
        )
        if np.size(klevel3) == 0:
            klevel3 = Nlevel
        else:
            klevel3 = np.max(klevel3)
        Uvec3Croco[k] = u[k, klevel3, Jvec3, Ivec3]
        Vvec3Croco[k] = v[k, klevel3, Jvec3, Ivec3]
        Hvec3Croco[k] = hrm[k, Jvec3, Ivec3]

    return (
        timeCroco,
        Hvec1Croco,
        Uvec1Croco,
        Vvec1Croco,
        Hvec3Croco,
        Uvec3Croco,
        Vvec3Croco,
    )


# Change fontsize
#
matplotlib.rcParams.update({"font.size": 16})
mpl.rcParams["lines.linewidth"] = 3
mpl.rcParams["lines.markersize"] = 5
mpl.rcParams["lines.markeredgewidth"] = 1
#
# Change subplot sizes
#
gs = gridspec.GridSpec(3, 1)
gs.update(left=0.1, right=0.95, top=0.95, bottom=0.1, wspace=0.125, hspace=0.25)
gs2 = gridspec.GridSpec(3, 2)
gs2.update(left=0.1, right=0.95, top=0.95, bottom=0.1, wspace=0.15, hspace=0.25)
#
# Figure size
#
figwidth = 14
figheight = 10

#%
#% Path setup
#%
basepathDATA = "/.fsnet/project/meige/2019/19MEPELS/Simulations/TRUCVERT/DATA/"
basepathCROCO = "../beaches/trucvert/croco/"

# List all the files in the folder
configName = "default"
makePlot = 1


if configName == "default":
    codelist = ["run0"]
    CrocoLegend = ["run0"]
    CROCOgrid = ["70x100sh"]
    klevel = [10]


figName = "CROCOsensitivityVEC13" + configName + ".png"
pickle_file = "CROCO_RMS" + configName + ".py"

Ncas = len(codelist)
RMS = np.zeros((Ncas, 7))

date_0 = dt.strptime("2008-4-6 00:00", "%Y-%m-%d %H:%M")
date_m = dt.strptime("2008-4-7 00:00", "%Y-%m-%d %H:%M")
date_f = dt.strptime("2008-4-8 00:00", "%Y-%m-%d %H:%M")

xlimit = [datenum(date_0), datenum(date_f)]
xxtick = [datenum(date_0), datenum(date_m), datenum(date_f)]
xxticklabels = [
    date_0.strftime("%Y-%m-%d"),
    date_m.strftime("%Y-%m-%d"),
    date_f.strftime("%Y-%m-%d"),
]

#% 1) Read wave and tide data from FORCAGE file
ncFor = Dataset(os.path.join(basepathDATA, "HYDRO/ForcageLEGIFull.nc"))

timeF = ncFor.variables["time"][:]
dateF = ncFor.variables["date"][:]
Hs = ncFor.variables["Hs"][:]
Tp = ncFor.variables["Tp"][:]
Dir = ncFor.variables["Dir"][:]
Tide = ncFor.variables["Tide"][:]


#%% 2) Extract data from VEC1 and VEC3
nc1 = Dataset(os.path.join(basepathDATA, "HYDRO/VEC1.nc"))
time_1 = nc1.variables["time"][:]
date_1 = nc1.variables["date"][:]
Hs_1 = nc1.variables["Hs"][:]
Ux_VEC1 = nc1.variables["Ux"][:]
Uy_VEC1 = nc1.variables["Uy"][:]
U_VEC1 = nc1.variables["Umag"][:]

#% From file VEC3
nc3 = Dataset(os.path.join(basepathDATA, "HYDRO/VEC3.nc"))
time_3 = nc3.variables["time"][:]
date_3 = nc3.variables["date"][:]
Hs_3 = nc3.variables["Hs"][:]
Ux_VEC3 = nc3.variables["Ux"][:]
Uy_VEC3 = nc3.variables["Uy"][:]
U_VEC3 = nc3.variables["Umag"][:]

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
#%
#%  READ CROCO results
#%
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
dateCROCO_0 = datenum(date_0)

for i in range(Ncas):
    #    %
    #    %  Read the grid file and check the topography
    #    %
    [
        timeCroco,
        Hvec1Croco,
        Uvec1Croco,
        Vvec1Croco,
        Hvec3Croco,
        Uvec3Croco,
        Vvec3Croco,
    ] = readCROCO(basepathCROCO, codelist[i], CROCOgrid[i], klevel[i])

    dateCROCO = dateCROCO_0 + timeCroco / (24 * 3600)

    #%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    #%%%%                        write data in txt file                                          %%%%
    #%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    # Variables of xboutput.nc

    [RMSH1, RMSU1, RMSV1] = RMS_T(
        date_1,
        Hs_1,
        -Ux_VEC1,
        -Uy_VEC1,
        dateCROCO,
        Hvec1Croco,
        Uvec1Croco,
        Vvec1Croco,
    )
    [RMSH3, RMSU3, RMSV3] = RMS_T(
        date_3,
        Hs_3,
        -Ux_VEC3,
        -Uy_VEC3,
        dateCROCO,
        Hvec3Croco,
        Uvec3Croco,
        Vvec3Croco,
    )

    RMSM = (RMSH1 + RMSH3 + RMSU1 + RMSU3 + RMSV1 + RMSV3) / 6

    RMS[i, 0] = RMSM
    RMS[i, 1] = RMSH1
    RMS[i, 2] = RMSH3
    RMS[i, 3] = RMSU1
    RMS[i, 4] = RMSU3
    RMS[i, 5] = RMSV1
    RMS[i, 6] = RMSV3

    print("case:", codelist[i], " RMS=", RMSM)
    print(
        "     ",
        " RMS U=",
        0.5 * (RMSU1 + RMSU3),
        " RMS V=",
        0.5 * (RMSV1 + RMSV3),
        " RMS H=",
        0.5 * (RMSH1 + RMSH3),
    )

    if makePlot == 1:
        #    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        #    % Plot velocity time series from vec3 XY
        #    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        figure(
            num=1,
            figsize=(figwidth, figheight),
            dpi=60,
            facecolor="w",
            edgecolor="w",
        )

        ax1 = subplot(gs2[0, 0])
        if i == 0:
            (l1,) = ax1.plot(dateF, Hs, ":k", lw=2, label="Off-shore")
            (l3,) = ax1.plot(date_1, Hs_1, "xk", lw=2, label="VEC 1")

        (l5,) = ax1.plot(dateCROCO, Hvec1Croco, lw=2, label=CrocoLegend[i])

        #    if i==Ncas-1:
        #       ax1.legend(fontsize = 10)
        ax1.set_ylabel("Hs (m)")
        ax1.set_xticks(xxtick)
        ax1.set_xticklabels([])
        ax1.set_xlim(xlimit)
        ax1.set_ylim([0, 2])
        ax1.grid()

        ax2 = subplot(gs2[1, 0])
        if i == 0:
            (l1,) = ax2.plot(date_1, -Ux_VEC1, "xk", lw=2, label="VEC 1")

        (l2,) = ax2.plot(dateCROCO, Uvec1Croco, lw=2, label=codelist[i])

        ax2.set_ylabel("U (m/s)")
        ax2.set_xticks(xxtick)
        ax2.set_xticklabels([])
        ax2.set_xlim(xlimit)
        ax2.set_ylim([-0.5, 0.65])
        ax2.grid()

        ax3 = subplot(gs2[2, 0])
        if i == 0:
            (l1,) = ax3.plot(date_1, -Uy_VEC1, "xk", lw=2, label="VEC 1")

        (l2,) = ax3.plot(dateCROCO, Vvec1Croco, lw=2, label=codelist[i])

        ax3.set_ylabel("V (m/s)")
        ax3.set_xlim(xlimit)
        ax3.set_ylim([-1, 0.25])
        ax3.set_xticks(xxtick)
        ax3.set_xticklabels(xxticklabels)
        ax3.grid()

        ax4 = subplot(gs2[0, 1])
        if i == 0:
            (l1,) = ax4.plot(dateF, Hs, ":k", lw=2, label="Off-shore")
            (l3,) = ax4.plot(date_3, Hs_3, "xk", lw=2, label="VEC 3")

        (l5,) = ax4.plot(dateCROCO, Hvec3Croco, lw=2, label=CrocoLegend[i])

        if i == Ncas - 1:
            ax4.legend(fontsize=10, loc="upper left")
        ax4.grid()
        ax4.set_xticks(xxtick)
        ax4.set_xticklabels([])
        ax4.set_yticklabels([])
        ax4.set_xlim(xlimit)
        ax4.set_ylim([0, 2])

        ax5 = subplot(gs2[1, 1])
        if i == 0:
            (l1,) = ax5.plot(date_3, -Ux_VEC3, "xk", lw=2, label="VEC 3")

        (l2,) = ax5.plot(dateCROCO, Uvec3Croco, lw=2, label=codelist[i])

        ax5.set_xticks(xxtick)
        ax5.set_xticklabels([])
        ax5.set_yticklabels([])
        ax5.set_xlim(xlimit)
        ax5.set_ylim([-0.5, 0.65])
        ax5.grid()

        ax6 = subplot(gs2[2, 1])
        if i == 0:
            (l1,) = ax6.plot(date_3, -Uy_VEC3, "xk", lw=2, label="VEC 1")

        (l2,) = ax6.plot(dateCROCO, Vvec3Croco, lw=2, label=codelist[i])

        ax6.set_xlim(xlimit)
        ax6.set_ylim([-1, 0.25])
        ax6.grid()
        ax6.set_yticklabels([])
        ax6.set_xticks(xxtick)
        ax6.set_xticklabels(xxticklabels)

    savefig(
        "../Figures/CROCO_Hydro/" + figName,
        dpi=300,
        facecolor="w",
        edgecolor="w",
        format="png",
    )

RMSdict = {
    "RMSM": RMS[:, 0],
    "RMSH1": RMS[:, 1],
    "RMSH3": RMS[:, 2],
    "RMSU1": RMS[:, 3],
    "RMSU3": RMS[:, 4],
    "RMSV1": RMS[:, 5],
    "RMSV3": RMS[:, 6],
    "params": params,
}

pickle.dump(RMSdict, open(basepathCROCO + pickle_file, "wb"))
