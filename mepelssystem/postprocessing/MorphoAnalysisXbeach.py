#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Mar 19 17:15:06 2020
@author: chauchat & ferraris
"""

from netCDF4 import Dataset
import numpy as np
import os

# from pylab import *
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec

# from scipy.interpolate import griddata
# from datetime import *
from scipy.optimize import curve_fit

# from scipy.signal import argrelextrema
import yaml
import sys

sys.path.append("./libs/")
from mydate import datenum
from mydate import datenum_to_datetime
from peakdet import peakdet

# from time import sleep
from datetime import datetime

# import BSS
sys.path.append("./../preprocessing/mnt/libs/")
from replacementValues_function import replacementValues


def func(x, a, b, c):
    return a * pow(x, b) + c


def analysemorpho(beach_config, range_bar):

    #
    # Change fontsize
    #
    plt.matplotlib.rcParams.update({"font.size": 16})
    mpl.rcParams["lines.linewidth"] = 3
    mpl.rcParams["lines.markersize"] = 5
    mpl.rcParams["lines.markeredgewidth"] = 1

    # gs = gridspec.GridSpec(3, 1)
    # gs.update(left=0.1, right=0.95, top=0.95,
    #           bottom=0.1, wspace=0.125, hspace=0.25)

    gs1 = gridspec.GridSpec(
        1,
        1,
        figure=None,
        left=0.15,
        bottom=0.1,
        right=0.8,
        top=0.8,
        wspace=0.3,
        hspace=0.15,
    )
    gs2 = gridspec.GridSpec(
        2,
        1,
        figure=None,
        left=0.15,
        bottom=0.1,
        right=0.8,
        top=0.8,
        wspace=0.3,
        hspace=0.15,
    )
    gs3 = gridspec.GridSpec(
        3,
        1,
        figure=None,
        left=0.15,
        bottom=0.1,
        right=0.8,
        top=0.8,
        wspace=0.3,
        hspace=0.15,
    )

    #%% 1) Load xbeach model results
    with open(
        r"./configurations/" + beach_config + "_config_postproc.yaml"
    ) as file:
        cl = yaml.full_load(file)
    # basepathDATA = cl['data_path']['basepathDATA']

    date_0 = datetime.strptime(cl["parameter"]["date_0"], "%Y-%m-%d %H:%M")
    date_m = datetime.strptime(cl["parameter"]["date_m"], "%Y-%m-%d %H:%M")
    date_f = datetime.strptime(cl["parameter"]["date_f"], "%Y-%m-%d %H:%M")

    xlimit = [datenum(date_0), datenum(date_f)]
    xxtick = [datenum(date_0), datenum(date_m), datenum(date_f)]
    xxticklabels = [date_0.strftime("%Y-%m-%d"), date_f.strftime("%Y-%m-%d")]

    bedLevels = np.linspace(cl["plotting"]["bedlevel_min"], 5, 101)
    bedLticks = np.linspace(cl["plotting"]["bedlevel_min"], 5, 11)
    bedLtickLabels = [str(i) for i in bedLticks]

    cmapBathy = cl["plotting"]["colormaps"]

    # ErosionLevels = np.linspace(-6,6,33)

    #
    # Change subplot sizes
    #
    # gs = gridspec.GridSpec(7, 1)
    # gs.update(left=0.1, right=0.95, top=0.95,
    #           bottom=0.1, wspace=0.15, hspace=0.4)
    #
    # Figure size
    #
    figwidth = 14
    figheight = 10

    #%
    #% Plot setup
    #%
    ndown = 5

    figPath = os.path.dirname(cl["data_path"]["basepathXB"]) + "/figures/"
    try:
        os.mkdir(figPath)
    except:
        pass
    figPath = figPath + "analyseMorpho/"
    try:
        os.mkdir(figPath)
    except:
        pass

    #% 1) Read wave and tide data from FORCAGE file
    ncFor = Dataset(cl["data_path"]["Forcage"])

    # timeF = ncFor.variables['time'][:];
    dateF = ncFor.variables["date"][:]
    Hs = ncFor.variables["Hs"][:]
    # Tp   = ncFor.variables['Tp'][:];
    # Dir  = ncFor.variables['Dir'][:];
    # Tide = ncFor.variables['Tide'][:];

    #
    # Read Almar et al. (2010) data
    #
    date0Almar = datetime.strptime('2008-03-06 00:00','%Y-%m-%d %H:%M')
    
    if cl['parameter']['codename'] == 'XBEACH':
        #    %  Read the grid file and check the topography
        #    %
        ncFil   = Dataset(cl['data_path']['basepathXB'])
    
        timeXB = ncFil.variables['globaltime'][:]
    
        x1  = ncFil.variables['globalx'][:,:]
        y1  = ncFil.variables['globaly'][:,:]
        zb  = ncFil.variables['zb'][:,:]
        u   = ncFil.variables['u'][:,:,:]
        v   = ncFil.variables['v'][:,:,:]
        hh  = ncFil.variables['hh'][:,:,:]
        hh = -hh
        h = zb[0, :, :]
        hmorph = zb[:, :, :]
        Nt = np.size(timeXB)
        Nx = np.size(x1,1)
        Ny = np.size(x1,0)
    
        date = datenum(date_0) + np.array(timeXB) / (24 * 3600)
    
        kplotIndex = np.where(date >= datenum(date_f))
    if cl['parameter']['codename'] == 'CROCO':
        #%
        #% Read the croco output file
        #%
        ncfileGRD  = Dataset(cl['data_path']['basepathCROCO']+cl['parameter']['gridfile']);    
        x1   = ncfileGRD.variables['x_rho'][:,:];
        y1   = ncfileGRD.variables['y_rho'][:,:];
    
        ncfile  = Dataset(cl['data_path']['basepathCROCO']+cl['parameter']['codelist']);    
        #% Netcdf output file
        timeCROCO = ncfile.variables['time'][:];       #% (s)
        hmorph = ncfile.variables['hmorph'][:,:,:];
        Nt=np.size(timeCROCO);
        Nx=np.size(x1,1);
        Ny=np.size(x1,0);
    
        hmorph = - hmorph
        #hmorph=np.flip(hmorph,axis=1);
        #hmorph=np.flip(hmorph,axis=2);
        h = hmorph[0,:,:]
        
        date = datenum(date_0) + np.array(timeCROCO) / (24 * 3600)
        kplotIndex = np.where(date >= datenum(date_f))
        
    if np.size(kplotIndex) == 0:
        kplot = Nt - 1
    else:
        kplot = np.min(np.where(date >= datenum(date_f)))
    Xmin = np.min(x1)
    Xmax = np.max(x1)
    Ymin = np.min(y1)
    Ymax = np.max(y1)

    # #% rotate referential
    # zb = np.flip(zb, axis=1);
    # zb = np.flip(zb, axis=2);
    # u = np.flip(u, axis=1);
    # u = np.flip(u, axis=2);
    # v = np.flip(v, axis=1);
    # v = np.flip(v, axis=2);

    h = zb[0, :, :]
    hmorph = zb[:, :, :]
    # deltaH = hmorph[:, :, :] - h

    meanProfile = np.zeros((Nt, Nx))
    # Zp = np.zeros(Nt)
    Xo = np.zeros((Nt, 1))
    Xi = np.zeros((Nt, 1))
    Do = np.zeros((Nt, 1))
    Di = np.zeros((Nt, 1))
    Deltao = np.zeros((Nt, 1))
    Deltai = np.zeros((Nt, 1))

    # =============================================================================

    meanProfile[:, :] = np.mean(hmorph[:, :, :], axis=1)

    popt, pcov = curve_fit(func, x1[0, :], meanProfile[0, :])
    zFitted = func(x1[0, :], popt[0], popt[1], popt[2])
    #% Determine the position of the outer bar
    #% specify the cross-shore position of the maximum and minimum depth
    print(range_bar)
    if len(range_bar) > 1:  # a modifier pour que ça fonctionne
        outerbar = np.where(
            np.logical_and(
                x1[0, :] >= range_bar[0, 0], x1[0, :] <= range_bar[0, 1]
            )
        )
        outerbar2DMax = np.zeros((Ny, Nt), dtype=np.uint32)
        outerbar2DMin = np.zeros((Ny, Nt), dtype=np.uint32)
        for k in range(Nt):
            # % loop over the alongshore direction
            for j in range(Ny):
                # %remove the mean bed slope from bathymetry
                ZperturbY = hmorph[k, j, :] - zFitted
                # % search for the local minimum/maximum for the innerbar
                [temp1, temp3] = peakdet(
                    ZperturbY[outerbar[0][:]], outerbar[0][:]
                )
                if np.size(temp1) == 0:
                    outerbar2DMax[j, k] = outerbar2DMax[j - 1, k]
                else:
                    outerbar2DMax[j, k] = int(temp1[0])

                if np.size(temp3) == 0:
                    outerbar2DMin[j, k] = outerbar2DMin[j - 1, k]
                else:
                    outerbar2DMin[j, k] = int(temp3[0])
            Xo[k] = np.mean(x1[0, outerbar2DMax[:, k]])
            Do[k] = np.mean(
                np.abs(x1[0, outerbar2DMax[:, k]] - x1[1, outerbar2DMin[:, k]])
            )
            Deltao[k] = np.abs(
                np.min(hmorph[k, :, outerbar2DMax[:, k]])
                - np.max(hmorph[k, :, outerbar2DMin[:, k]])
            )

        if np.size(range_bar) > 2:
            innerbar = np.where(
                np.logical_and(
                    x1[0, :] >= range_bar[1, 0], x1[0, :] <= range_bar[1, 1]
                )
            )
            #% Initialize the pointer
            innerbar2DMax = np.zeros((Ny, Nt), dtype=np.uint32)
            innerbar2DMin = np.zeros((Ny, Nt), dtype=np.uint32)

            #% Initialize the pointer

            for k in range(Nt):
                # % search for the local minimum/maximum for the innerbar
                [temp1, temp3] = peakdet(
                    ZperturbY[innerbar[0][:]], innerbar[0][:]
                )
                if np.size(temp1) == 0:
                    innerbar2DMax[j, k] = innerbar2DMax[j - 1, k]
                else:
                    innerbar2DMax[j, k] = int(temp1[0])

                if np.size(temp3) == 0:
                    innerbar2DMin[j, k] = innerbar2DMin[j - 1, k]
                else:
                    innerbar2DMin[j, k] = int(temp3[0])

                Xi[k] = np.mean(x1[0, innerbar2DMax[:, k]])
                Di[k] = np.mean(
                    np.abs(
                        x1[0, innerbar2DMax[:, k]] - x1[0, innerbar2DMin[:, k]]
                    )
                )
                Deltai[k] = np.abs(
                    np.min(hmorph[k, :, innerbar2DMax[:, k]])
                    - np.max(hmorph[k, :, innerbar2DMin[:, k]])
                )
        # =============================================================================

        # if os.path.exists(basepathDATA+'BARRES/') == True:
        #       fig1 = plt.figure(1,figsize=(figwidth, figheight))

        #       dateAlmar_Inner = datenum(date0Almar) + np.array(tInner)
        #       dateAlmar_Outer = datenum(date0Almar) + np.array(tOuter)
        #       ax1 = plt.subplot(gs3[0, 0])
        #       ax1.plot(dateXB, Xo, '-r')
        #       ax1.plot(dateXB, Xi, '--b')
        #       ax1.plot(dateAlmar_Inner, XInner, 'om')
        #       ax1.plot(dateAlmar_Outer, XOuter, 'ok')
        #       ax1.set_ylabel('cross-shore loc. [m]', size=12)
        #       XminInner = np.min(XInner)
        #       XmaxInner = np.max(XInner)
        #       ax1.set_ylim((Xmax, Xmin))
        #       ax1.set_xlim(xlimit)
        #       ax1.set_xticks(xxtick)
        #       ax1.set_xticklabels([])

        #       ax2 = plt.subplot(gs3[1, 0])
        #       ax2.plot(dateXB, 2*Do, '-r')
        #       ax2.plot(dateXB, 2*Di, '--b')
        #       ax2.plot(dateAlmar_Inner, (XMInner - XmInner) + (Xmax - (XMInner - XmInner)), 'om')
        #       ax2.plot(dateAlmar_Outer, XMOuter - XmOuter + (Xmax - (XMOuter - XmOuter)), 'ok')
        #       XminAx2 = np.min(XMInner - XmInner)
        #       XmaxAx2 = np.max(XMInner - XmInner)
        #       ax1.set_ylim((XminAx2, XmaxAx2))
        #       ax2.set_ylabel('Bar thickness [m]', size=12)
        #       ax2.set_xlim(xlimit)
        #       ax2.set_xticks(xxtick)
        #       ax2.set_xticklabels([])
        #       ax3 = plt.subplot(gs3[2, 0])
        #       ax3.plot(dateF, Hs, '-r')
        #       ax3.set_xlim(xlimit)
        #       ax3.set_ylabel('Hs [m]', size=12)
        #       ax3.set_xticks(xxtick)
        #       ax3.set_xticklabels([])
        #       ax3.set_xlabel('date', size=12)

        #       fig1.savefig(
        #           figPath + "moveBar",
        #           dpi=100,
        #           facecolor='w',
        #           edgecolor='w',
        #           orientation='portrait',
        #           papertype=None,
        #           format="png",
        #           transparent=False,
        #           bbox_inches=None,
        #           pad_inches=0.1,
        #           metadata=None
        #           )

        fig2 = plt.figure(
            num=2,
            figsize=(figwidth / 2, figheight / 2),
            dpi=100,
            facecolor="w",
            edgecolor="w",
        )

        ax1 = plt.subplot(gs1[0, 0])
        cax = ax1.contourf(y1, x1, hmorph[kplot, :, :], bedLevels, cmap=cmapBathy)
        for j in range(Ny):
            ax1.plot(y1[j, 0], x1[j, outerbar2DMax[j, kplot]], "k*")
            ax1.plot(y1[j, 0], x1[j, outerbar2DMin[j, kplot]], "rx")
        ax1.plot([Ymin, Ymax], [Xo[kplot], Xo[kplot]], "--k", lw=4)
        ax1.plot(
            [Ymin, Ymax],
            [Xo[kplot] + Do[kplot], Xo[kplot] + Do[kplot]],
            "--r",
            lw=2,
        )
        ax1.plot(
            [Ymin, Ymax],
            [Xo[kplot] - Do[kplot], Xo[kplot] - Do[kplot]],
            "--r",
            lw=2,
        )
        if len(range_bar) > 2:
            for j in range(Ny):
                ax1.plot(y1[j, 0], x1[j, innerbar2DMax[j, kplot]], "b*")
                ax1.plot(y1[j, 0], x1[j, innerbar2DMin[j, kplot]], "gx")

            ax1.plot([Ymax, Ymin], [Xi[kplot], Xi[kplot]], "--b", lw=4)
            ax1.plot(
                [Ymax, Ymin],
                [Xi[kplot] + Di[kplot], Xi[kplot] + Di[kplot]],
                "--g",
                lw=2,
            )
            ax1.plot(
                [Ymax, Ymin],
                [Xi[kplot] - Di[kplot], Xi[kplot] - Di[kplot]],
                "--g",
                lw=2,
            )

        cbar = plt.colorbar(
            cax, orientation="horizontal", ticks=bedLticks, fraction=0.055
        )
        cbar.set_ticklabels(bedLtickLabels)
        # % Add velocity vectors
        # Qscale = 10
        # Q=ax1.quiver(y1[0:Nx:ndown,0:Ny:ndown],x1[0:Nx:ndown,0:Ny:ndown],\
        #            v[k,0:Nx:ndown,0:Ny:ndown],u[k,0:Nx:ndown,0:Ny:ndown],\
        #             scale=Qscale);
        # qk = ax1.quiverkey(Q, 0.75, 0.975, 1, r'1 m/s', labelpos='E',
        #                coordinates='figure')
        #% Add VEC1 position
        ax1.set_xlabel("Long-shore [m]", size=12)
        ax1.set_ylabel("Cross-shore [m]", size=12)
        ax1.set_ylim([Xmax, Xmin])
        ax1.set_xlim([Ymin, Ymax])
        ax1.set_title(datenum_to_datetime(date[k]).strftime('%Y-%m-%d %H:%M'))
        fig2.savefig(
            figPath + "posBarsOnBathy.png",
            dpi=100,
            facecolor="w",
            edgecolor="w",
            orientation="portrait",
            format="png",
            transparent=False,
            bbox_inches=None,
            pad_inches=0.1,
            metadata=None,
        )
    # =============================================================================

    #
    # Plot the predicted bathymetry on april 4th 2008
    #
    Qscale = 10

    fig3 = plt.figure(
        num=3,
        figsize=(figwidth / 2, figheight / 2),
        dpi=100,
        facecolor="w",
        edgecolor="w",
    )
    ax1 = plt.subplot(gs1[0, 0])
    #% Contour of bed elevation
    cax = ax1.contourf(y1, x1, hmorph[kplot, :, :], bedLevels, cmap=cmapBathy)
    # terrain
    cbar = plt.colorbar(
        cax, orientation="horizontal", ticks=bedLticks, fraction=0.055
    )
    cbar.set_ticklabels(bedLtickLabels)
    #% Add velocity vectors
    # Q = ax1.quiver(y1[0:Nx:ndown,0:Ny:ndown],x1[0:Nx:ndown,0:Ny:ndown],\
    #           v[k,0:Nx:ndown,0:Ny:ndown],u[k,0:Nx:ndown,0:Ny:ndown],\
    #             scale=Qscale);
    # qk = ax1.quiverkey(Q, 0.75, 0.975, 1, r'1 m/s', labelpos='E',
    #               coordinates='figure')
    #% Add VEC1 position
    ax1.set_xlabel("Long-shore [m]", size=12)
    ax1.set_ylabel("Cross-shore [m]", size=12)
    ax1.set_ylim([Xmax, Xmin])
    ax1.set_xlim([Ymin, Ymax])
    # ax1.set_title(datenum_to_datetime(date[k]).strftime('%Y-%m-%d %H:%M'))
    fig3.savefig(
        figPath + "visu_bathy_final.png",
        dpi=100,
        facecolor="w",
        edgecolor="w",
        orientation="portrait",
        format="png",
        transparent=False,
        bbox_inches=None,
        pad_inches=0.1,
        metadata=None,
    )

    bathy_file = cl["data_path"]["basepathPreproc"]
    ncfile = Dataset(bathy_file)

    X = ncfile.variables["X"][:, :]
    Y = ncfile.variables["Y"][:, :]
    if cl["data_path"]["makemodif"] == 0:
        h = ncfile.variables["h"][:, :]
    else:
        h = ncfile.variables["h_modelling"][:, :]

    ncfile_xbeach = Dataset(cl["data_path"]["basepathXB"])
    X_xbeach = ncfile_xbeach.variables["globalx"][:, :]
    Y_xbeach = ncfile_xbeach.variables["globaly"][:, :]
    zb_xbeach = ncfile_xbeach.variables["zb"][:, :, :]
    frame_nbs = len(zb_xbeach[:, 0, 0])
    h_xbeach = zb_xbeach[frame_nbs - 1, :, :]
    # size_matrix_xbeach = np.shape(X_xbeach)
    # Nx_xbeach = size_matrix_xbeach[1]
    # Ny_xbeach = size_matrix_xbeach[0]
    # Xmax_xbeach = np.max(X_xbeach[0, :])
    # Ymin_xbeach = np.min(Y_xbeach[:, 0])
    # Ymax_xbeach = np.max(Y_xbeach[:, 0])

    h = -h  # reversed bathy to have negative elevation
    # verify the NaN values
    h = replacementValues(
        2,
        h,
        "NaN",
        "NaN",
        "NaN",
        "NaN",
        "NaN",
        "NaN",
        "NaN",
        "NaN",
        "NaN",
        "NaN",
        4,
        "NaN",
        "NaN",
    )
    h_xbeach = replacementValues(
        2,
        h_xbeach,
        "NaN",
        "NaN",
        "NaN",
        "NaN",
        "NaN",
        "NaN",
        "NaN",
        "NaN",
        "NaN",
        "NaN",
        4,
        "NaN",
        "NaN",
    )

    size_matrix = np.shape(X)
    Ny = size_matrix[0]
    Nx = size_matrix[1]
    # Xmin = 0
    # Xmax = np.max(X[0, :])
    # Ymin = np.min(Y[:, 0])
    # Ymax = np.max(Y[:, 0])

    # Xraw, Yraw, Zraw = np.loadtxt(
    #                             '/.fsnet/project/meige/2019/19MEPELS/ECORS_MEPELS/Data_ECORS2009/data_topo_bathy/Bathymetries/Large/Dune_topo_bathy11_2XYZ.txt',
    #                             delimiter=",",
    #                             usecols=(0, 1, 2),
    #                             unpack=True,
    #                             )
    #% Flip Z axis
    # Zraw = -Zraw;
    h11_2 = h

    # Xraw, Yraw, Zraw = np.loadtxt(
    #                             '/.fsnet/project/meige/2019/19MEPELS/ECORS_MEPELS/Data_ECORS2009/data_topo_bathy/Bathymetries/Large/Dune_topo_bathy4_avrilXYZ.txt',
    #                             delimiter=",",
    #                             usecols=(0, 1, 2),
    #                             unpack=True,
    #                             )
    #% Flip Z axis
    # Zraw = -Zraw;
    h4_4 = h_xbeach

    #
    # plot measured bathy
    #
    fig4 = plt.figure(
        num=4,
        figsize=(figwidth / 2, figheight),
        dpi=100,
        facecolor="w",
        edgecolor="w",
    )
    ax1 = plt.subplot(gs2[0, 0])
    #% Contour of bed elevation
    cax = ax1.contourf(Y, X, h11_2, cmap=cmapBathy)  # terrain
    ax1.set_ylabel("Cross-shore [m]", size=12)
    ax1.set_ylim([Xmax, Xmin])
    ax1.set_xticklabels([])
    ax1.set_xlim([Ymin, Ymax])

    ax2 = plt.subplot(gs2[1, 0])
    #% Contour of bed elevation
    cax = ax2.contourf(Y_xbeach, X_xbeach, h4_4, cmap=cmapBathy)  # terrain
    cbar = plt.colorbar(
        cax, orientation="horizontal", ticks=bedLticks, fraction=0.055
    )
    cbar.set_ticklabels(bedLtickLabels)
    ax2.set_xlabel("Long-shore [m]", size=12)
    ax2.set_ylabel("Cross-shore [m]", size=12)
    ax2.set_ylim([Xmax, Xmin])
    ax2.set_xlim([Ymin, Ymax])

    fig4.savefig(
        figPath + "visu_comp_bathy_init_final.png",
        dpi=100,
        facecolor="w",
        edgecolor="w",
        orientation="portrait",
        format="png",
        transparent=False,
        bbox_inches=None,
        pad_inches=0.1,
        metadata=None,
    )
    plt.show()
    #% Compute skill estimator
    # hFinal = hmorph[Nt-1, :, :]
    # BSS_val = BSS(hFinal,h4_4,h11_2,Nx,Ny)
