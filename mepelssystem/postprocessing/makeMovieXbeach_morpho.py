#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Mar 19 17:15:06 2020

@author: chauchat
"""

from netCDF4 import Dataset
import numpy as np

# from scipy import interpolate

import os

# from pylab import *
import matplotlib.gridspec as gridspec
from datetime import datetime as dt
import yaml
import sys
import matplotlib.pyplot as plt
import matplotlib as mpl

sys.path.append("./libs/")
from mydate import datenum

# from mydate import datenum_to_datetime


def moviemorpho(beach_config):
    #
    # Change fontsize
    #
    plt.matplotlib.rcParams.update({"font.size": 16})
    mpl.rcParams["lines.linewidth"] = 3
    mpl.rcParams["lines.markersize"] = 5
    mpl.rcParams["lines.markeredgewidth"] = 1
    #
    # Change subplot sizes
    #
    # gs = gridspec.GridSpec(3, 1)
    # gs.update(left=0.1, right=0.95, top=0.95,
    #           bottom=0.05, wspace=0.15, hspace=0.2)
    gs = gridspec.GridSpec(
        3,
        1,
        figure=None,
        left=0.15,
        bottom=0.15,
        right=0.8,
        top=0.8,
        wspace=0.3,
        hspace=0.35,
    )
    #
    # Figure size
    #
    figwidth = 14
    figheight = 10

    #%
    #% Path setup
    #%
    with open(
        r"./configurations/" + beach_config + "_config_postproc.yaml"
    ) as file:
        cl = yaml.full_load(file)

    # basepathDATA = cl['data_path']['basepathDATA']
    basepathXB = cl["data_path"]["basepathXB"]

    # plot one over ndown grid pointsfor velocity arrows
    ndown = 6

    #%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    #%
    #%  READ XBEACH results
    #%
    #%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    date_0 = dt.strptime(cl["parameter"]["date_0"], "%Y-%m-%d %H:%M")
    dateXB_0 = datenum(date_0)

    #%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    #%
    #%        Read the results from the directories
    #%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    figPath = os.path.dirname(cl["data_path"]["basepathXB"]) + "/figures/"
    try:
        os.mkdir(figPath)
    except:
        pass
    figPath = figPath + "movieMorpho/"
    try:
        os.mkdir(figPath)
    except:
        pass

    #    %
    #    %  Read the grid file and check the topography
    #    %
    ncFil = Dataset(basepathXB)

    # dumline=os.popen('ncdump -h '+basepathXB+'|grep -i "_parameters:wavemodel_str ="', "r").read()
    # print(dumline)
    # wavemodel = dumline.split("=")[1].split(";")[0]
    # print ('Wavemodel:', wavemodel)

    timeXB = ncFil.variables["globaltime"][:]

    x1 = ncFil.variables["globalx"][:, :]
    y1 = ncFil.variables["globaly"][:, :]
    zb = ncFil.variables["zb"][:, :, :]
    u = ncFil.variables["u"][:, :, :]
    v = ncFil.variables["v"][:, :, :]

    Nt = np.size(timeXB)
    Nx = np.size(x1, 0)
    Ny = np.size(x1, 1)

    dateXB = dateXB_0 + np.array(timeXB) / (24 * 3600)

    # dateXB = np.linspace(1,(48*60),97)
    #
    # Load data
    #
    date_0 = dt.strptime(cl["parameter"]["date_0"], "%Y-%m-%d %H:%M")
    date_m = dt.strptime(cl["parameter"]["date_m"], "%Y-%m-%d %H:%M")
    date_f = dt.strptime(cl["parameter"]["date_f"], "%Y-%m-%d %H:%M")

    xlimit = [datenum(date_0), datenum(date_f)]
    xxtick = [datenum(date_0), datenum(date_m), datenum(date_f)]
    xxticklabels = [
        date_0.strftime("%Y-%m-%d"),
        date_m.strftime("%Y-%m-%d"),
        date_f.strftime("%Y-%m-%d"),
    ]

    #% 1) Read wave and tide data from FORCAGE file
    ncFor = Dataset(cl["data_path"]["Forcage"])

    timeF = ncFor.variables["time"][:]
    dateF = ncFor.variables["date"][:]
    Hs = ncFor.variables["Hs"][:]
    Tp = ncFor.variables["Tp"][:]
    Dir = ncFor.variables["Dir"][:]
    Tide = ncFor.variables["Tide"][:]

    #    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    #    %
    #    %          Plots
    #    %
    #    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    bedLevels = np.linspace(
        cl["plotting"]["bedlevel_min"], cl["plotting"]["zmax"], 101
    )
    bedLticks = np.linspace(
        cl["plotting"]["bedlevel_min"], cl["plotting"]["zmax"], 9
    )
    bedLtickLabels = [str(i) for i in bedLticks]

    # Flip X axis
    Xmax = np.max(x1)
    Xmin = np.min(x1)
    # x1=xmax-x1
    # ymin=np.min(y1)
    # ymax=np.max(y1)

    Qscale = 20
    # var = 0
    # kk = 1
    for k in range(Nt):  # use Nt
        # if var == 4:

        print("image:", k)
        fig = plt.figure(
            num=1,
            figsize=(figwidth, figheight),
            dpi=100,
            facecolor="w",
            edgecolor="w",
        )

        ax1 = plt.subplot(gs[0:2, 0])
        #% Contour of bed elevation
        ax1.set_ylim([Xmax, Xmin])
        cax = ax1.contourf(
            y1, x1, zb[k, :, :], bedLevels, cmap=cl["plotting"]["colormaps"]
        )  # terrain
        cbar = fig.colorbar(
            cax, orientation="vertical", ticks=bedLticks, fraction=0.075
        )
        cbar.set_ticklabels(bedLtickLabels)
        ax1.contour(y1, x1, zb[k, :, :], bedLticks, colors="k")

        #% Add velocity vectors
        Q = ax1.quiver(
            y1[0:Nx:ndown, 0:Ny:ndown],
            x1[0:Nx:ndown, 0:Ny:ndown],
            v[k, 0:Nx:ndown, 0:Ny:ndown],
            u[k, 0:Nx:ndown, 0:Ny:ndown],
            scale=Qscale,
        )
        # qk = ax1.quiverkey(Q, 0.75, 0.975, 1, r'1 m/s', labelpos='E',
        #                coordinates='figure')
        #% Add VEC1 position
        ax1.set_xlabel("Long-shore [m]", size=12)
        ax1.set_ylabel("Cross-shore [m]", size=12)
        ax1.set_ylim([Xmin, Xmax])
        ax1.set_ylim([cl["study_area"]["Xmin"], cl["study_area"]["Xmax"]])
        ax1.set_xlim([cl["study_area"]["Ymin"], cl["study_area"]["Ymax"]])
        # ax1.set_title(datenum_to_datetime(dateXB[k]).strftime('%Y-%m-%d %H:%M'))

        ax2 = plt.subplot(gs[2, 0])

        ax2.plot(dateF, Hs, "-k", lw=2, label="Off-shore")
        print(np.shape(dateF))
        print(np.shape(dateXB))
        kplot = np.min(np.where(dateF >= dateXB[k]))
        ax2.plot(dateF[kplot], Hs[kplot], "or", lw=2, ms=12)
        ax2.set_ylabel("Hs [m]", size=12)
        ax2.set_xticks(xxtick)
        ax2.set_xticklabels([])
        ax2.set_xlim(xlimit)
        # ax2.set_ylim([0, np.nanmax(Tide)])
        ax2.set_xticklabels(xxticklabels)

        ax2.grid()
        ax1.set_ylim([Xmax, Xmin])
        plt.show(block=False)

        fig.savefig(
            figPath + "im_" + str(000 + kk) + ".png", dpi=100, format="png"
        )
        plt.close("all")
        #     kk += 1
        # var += 1
        # if var == 5:
        #     var = 0
