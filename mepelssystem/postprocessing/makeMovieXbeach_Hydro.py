#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Mar 19 17:15:06 2020

@author: chauchat
"""

from netCDF4 import Dataset
import numpy as np
import os
import yaml

# from pylab import *
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
from datetime import datetime as dt
import sys

sys.path.append("./libs/")
from mydate import datenum


def moviehydro(beach_config):

    with open(
        r"./configurations/" + beach_config + "_config_postproc.yaml"
    ) as file:
        cl = yaml.full_load(file)
    #
    # Change fontsize
    #
    mpl.rcParams.update({"font.size": 16})
    mpl.rcParams["lines.linewidth"] = 3
    mpl.rcParams["lines.markersize"] = 5
    mpl.rcParams["lines.markeredgewidth"] = 1
    #
    # Change subplot sizes

    gs = gridspec.GridSpec(
        7,
        1,
        figure=None,
        left=0.15,
        bottom=0.1,
        right=0.8,
        top=0.95,
        wspace=0.3,
        hspace=0.35,
    )

    #
    # Figure size
    #
    figwidth = 14
    figheight = 10

    #%
    #% Path setup
    #%
    basepathXB = cl["data_path"]["basepathXB"]

    # plot one over ndown grid pointsfor velocity arrows
    ndown = 3

    figPath = os.path.dirname(cl["data_path"]["basepathXB"]) + "/figures/"
    try:
        os.mkdir(figPath)
    except:
        pass
    figPath = figPath + "movieHydro/"
    try:
        os.mkdir(figPath)
    except:
        pass

    date_0 = dt.strptime(cl["parameter"]["date_0"], "%Y-%m-%d %H:%M")
    date_m = dt.strptime(cl["parameter"]["date_m"], "%Y-%m-%d %H:%M")
    date_f = dt.strptime(cl["parameter"]["date_f"], "%Y-%m-%d %H:%M")

    xlimit = [datenum(date_0), datenum(date_f)]
    xxtick = [datenum(date_0), datenum(date_m), datenum(date_f)]
    xxticklabels = [
        date_0.strftime("%Y-%m-%d"),
        date_m.strftime("%Y-%m-%d"),
        date_f.strftime("%Y-%m-%d"),
    ]

    #% 1) Read wave and tide data from FORCAGE file
    ncFor = Dataset(cl["data_path"]["Forcage"])

    timeF = ncFor.variables["time"][:]
    dateF = ncFor.variables["date"][:]
    Hs = ncFor.variables["Hs"][:]
    Tp = ncFor.variables["Tp"][:]
    Dir = ncFor.variables["Dir"][:]
    Tide = ncFor.variables["Tide"][:]

    #%% 2) Extract data from VEC1 and VEC3
    if cl["parameter"]["nbs_vec"] != 0:
        nc1 = Dataset(cl["data_path"]["nc1"])
        time_1 = nc1.variables["time"][:]
        date_1 = nc1.variables["date"][:]
        Hs_1 = nc1.variables["Hs"][:]
        Ux_VEC1 = nc1.variables["Ux"][:]
        Uy_VEC1 = nc1.variables["Uy"][:]
        U_VEC1 = nc1.variables["Umag"][:]

        if cl["parameter"]["nbs_vec"] == 2:
            #% From file VEC3
            nc3 = Dataset(cl["data_path"]["nc2"])
            time_3 = nc3.variables["time"][:]
            date_3 = nc3.variables["date"][:]
            Hs_3 = nc3.variables["Hs"][:]
            Ux_VEC3 = nc3.variables["Ux"][:]
            Uy_VEC3 = nc3.variables["Uy"][:]
            U_VEC3 = nc3.variables["Umag"][:]

    #%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    #%
    #%  READ XBEACH results
    #%
    #%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    dateXB_0 = datenum(date_0)

    #%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    #%
    #%        Read the results from the directories
    #%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    #    %
    #    %  Read the grid file and check the topography
    #    %
    ncFil = Dataset(basepathXB)

    timepXB = ncFil.variables["pointtime"][:]  #% (s)
    HsVEC = ncFil.variables["point_H"][:, :]
    zsVEC = ncFil.variables["point_zs"][:, :]
    UVEC = ncFil.variables["point_u"][:, :]
    VVEC = ncFil.variables["point_v"][:, :]
    xcoordVEC = ncFil.variables["pointx"][:]
    ycoordVEC = ncFil.variables["pointy"][:]

    timepXB = np.array(timepXB)
    Uvec1XB = np.array(UVEC[:, 0])
    Uvec3XB = np.array(UVEC[:, 1])
    Vvec1XB = np.array(VVEC[:, 0])
    Vvec3XB = np.array(VVEC[:, 1])
    Hvec1XB = np.array(HsVEC[:, 0]) * np.sqrt(2)
    Hvec3XB = np.array(HsVEC[:, 1]) * np.sqrt(2)
    Zetavec1XB = np.array(zsVEC[:, 0])
    Zetavec3XB = np.array(zsVEC[:, 1])

    Unormvec1XB = (Uvec1XB ** 2 + Vvec1XB ** 2) ** 0.5
    Unormvec3XB = (Uvec3XB ** 2 + Vvec3XB ** 2) ** 0.5

    timeXB = ncFil.variables["globaltime"][:]

    x1 = ncFil.variables["globalx"][:, :]
    y1 = ncFil.variables["globaly"][:, :]
    zb = ncFil.variables["zb"][:, :]
    zeta = ncFil.variables["zs"][:, :, :]
    hrm = ncFil.variables["H"][:, :, :]
    DR = ncFil.variables["DR"][:, :, :]
    u = ncFil.variables["u"][:, :, :]
    v = ncFil.variables["v"][:, :, :]

    Nt = np.size(timeXB)
    Nx = np.size(x1, 0)
    Ny = np.size(x1, 1)

    date_VEC = dateXB_0 + np.array(timepXB) / (24 * 3600)
    dateXB = dateXB_0 + np.array(timeXB) / (24 * 3600)
    #    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    #    %
    #    %          Plots
    #    %
    #    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    #    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    #    % Plot velocity time series from VEC1
    #    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    minhrm = np.min(hrm)
    maxhrm = np.max(hrm)
    hLevels = np.linspace(minhrm, maxhrm, 101)
    bedLevels = np.linspace(cl["plotting"]["bedlevel_min"], 5, 16)

    # invert x axis and define x=0 at the beach
    # x1=np.max(x1)-x1
    l = 10

    for k in range(Nt):  # use Nt
        kVEC = np.min(np.where(np.array(date_VEC) >= dateXB[k]))
        fig = plt.figure(
            num=k,
            figsize=(figwidth, figheight),
            dpi=60,
            facecolor="w",
            edgecolor="w",
        )

        ax1 = plt.subplot(gs[0:4, 0])
        #% Contour of Significant wave height
        cs = ax1.contourf(
            y1,
            x1,
            hrm[k, :, :] * np.sqrt(2),
            hLevels,
            cmap=cl["plotting"]["colormaps"],
        )
        fig.colorbar(cs, ax=ax1, orientation="horizontal", fraction=0.1)
        #    #% Contour of roller ennergy
        ax1.contour(y1, x1, DR[k, :, :], [0, 1], colors="r")
        #    #% Bed contour
        ax1.contour(y1, x1, zb[k, :, :], bedLevels, colors="k")
        #    #% Add velocity vectors
        ax1.quiver(
            y1[0:Nx:ndown, 0:Ny:ndown],
            x1[0:Nx:ndown, 0:Ny:ndown],
            v[k, 0:Nx:ndown, 0:Ny:ndown],
            u[k, 0:Nx:ndown, 0:Ny:ndown],
        )
        #% Add VEC1 position
        ax1.plot(ycoordVEC[0], xcoordVEC[0], "*y", markersize=24)
        ax1.set_xlabel("Long-shore [m]", size=12)
        ax1.set_ylabel("Cross-shore [m]", size=12)
        ax1.set_xlim([cl["study_area"]["Ymin"], cl["study_area"]["Ymax"]])
        ax1.set_ylim([cl["study_area"]["Xmax"], cl["study_area"]["Xmin"]])

        ax2 = plt.subplot(gs[4, 0])
        if cl["parameter"]["nbs_vec"] != 0:
            ax2.plot(dateF, Tide, color="k", ls=":")
        ax2.plot(date_VEC[0:kVEC], Zetavec1XB[0:kVEC], color="r", ls="-", lw=4)
        ax2.set_ylabel("Tide [m]", size=12)
        ax2.set_xlim(xlimit)
        # ax2.set_ylim([-2.5, 2.5])

        ax2.set_xticks([])

        ax3 = plt.subplot(gs[5, 0])
        if cl["parameter"]["nbs_vec"] != 0:
            ax3.plot(date_1, U_VEC1, marker="o", ls="none")
        ax3.plot(date_VEC[0:kVEC], Unormvec1XB[0:kVEC], color="r", ls="-", lw=4)
        ax3.set_ylabel("U (m/s)", size=12)
        ax3.set_xlim(xlimit)
        ax3.set_xticks([])
        # ax3.set_ylim([0, 1.5])
        ax3.set_title("Flow velocity at VEC1", size=12)
        #
        ax4 = plt.subplot(gs[6, 0])
        ax4.plot(dateF, Hs, color="k", ls=":")
        if cl["parameter"]["nbs_vec"] != 0:
            ax4.plot(date_1, Hs_1, marker="o", ls="none")

        ax4.plot(date_VEC[0:kVEC], Hvec1XB[0:kVEC], color="r", ls="-", lw=4)
        ax4.set_ylabel("Hs [m]")
        ax4.set_xlim(xlimit)
        ax4.set_title(
            "Significant wave height at western boundary and VEC1", size=12
        )
        ax4.set_xticks(xxtick)
        ax4.set_xticklabels(xxticklabels)
        ax4.set_ylim([0, np.max(Hvec1XB) * 1.5])

        plt.show(block=False)

        fig.savefig(figPath + str(l) + ".png", dpi=100, format="png")
        l = l + 1
