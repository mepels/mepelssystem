#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Mar 21 15:53:05 2020

@author: chauchat
"""
from datetime import *


def datenum(d):
    return (
        366
        + d.toordinal()
        + (d - datetime.fromordinal(d.toordinal())).total_seconds()
        / (24 * 60 * 60)
    )


def datenum_to_datetime(datenum):
    """
    Convert Matlab datenum into Python datetime.
    :param datenum: Date in datenum format
    :return:        Datetime object corresponding to datenum.
    """
    days = datenum % 1
    hours = days % 1 * 24
    minutes = hours % 1 * 60
    seconds = minutes % 1 * 60
    return (
        datetime.fromordinal(int(datenum))
        + timedelta(days=int(days))
        + timedelta(hours=int(hours))
        + timedelta(minutes=int(minutes))
        + timedelta(seconds=round(seconds))
        - timedelta(days=366)
    )
