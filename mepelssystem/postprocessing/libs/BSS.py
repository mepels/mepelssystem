def BSS(hpred, hmeas, hini, Nx, Ny):
    #    %
    #    % Author: J. Chauchat
    #    % Date : 2020/03/14
    #    % Description :
    #    %    Compute the Brier skill scores for a predicted morphology
    #    %    given a known bathymetry hpred, a measured bathymetry hmeas and an
    #    %    initial bathymetry hini
    #    %

    VAR = 0
    VARo = 0
    for i in range(Nx):
        for j in range(Ny):
            VAR = VAR + (hpred[j, i] - hmeas[j, i]) ** 2
            VARo = VARo + (hmeas[j, i] - hini[j, i]) ** 2

    VAR = VAR / float(Nx * Ny)
    VARo = VARo / float(Nx * Ny)

    #% BSS
    BSS = 1 - VAR / VARo
    print("BSS=", BSS)

    return BSS
