#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jun 12 08:21:51 2020

@author: chauchat
"""

import numpy as np
from scipy.optimize import curve_fit
import sys

sys.path.append("./libs/")
from peakdet import peakdet
from pylab import *


def func(x, a, b, c):
    return a * pow(x, b) + c


def AnalyseProfiles(Nx, Ny, h, x1, idxmin, idxmax, nbs_bar, pos_bar):
    # Compute the longshore-averaged beach profile
    meanProfile = np.zeros(Nx)
    meanProfile = np.mean(h, axis=0)
    keepData = range(idxmin, idxmax, 1)
    # keepData[:: -1]
    # figure(100)
    # plot(x1[0,:],meanProfile)
    # best fit the profile uswing a Dean profile (see func)
    popt, pcov = curve_fit(
        func, x1[0, keepData], meanProfile[keepData], check_finite=False
    )
    zFitted = func(x1[0, :], popt[0], popt[1], popt[2])
    #% Determine the position of the outer bar
    #% specify the cross-shore position of the maximum and minimum depth

    outerbar = np.where(
        np.logical_and(x1[0, :] >= pos_bar[0, 0], x1[0, :] <= pos_bar[0, 1])
    )
    if nbs_bar == "2":
        innerbar = np.where(
            np.logical_and(x1[0, :] >= pos_bar[0, 1], x1[0, :] <= pos_bar[1, 1])
        )
    #% Initialize the pointer
    ob2DMax = np.zeros(Ny, dtype=np.uint32)
    ob2DMin = np.zeros(Ny, dtype=np.uint32)
    ib2DMax = np.zeros(Ny, dtype=np.uint32)
    ib2DMin = np.zeros(Ny, dtype=np.uint32)
    # % loop over the alongshore direction
    for j in range(Ny):
        # %remove the mean profile from the bathymetry
        ZperturbY = h[j, :] - zFitted
        # # % search for the local minimum/maximum for the outerbar
        #  [temp1,temp3] = peakdet(ZperturbY[outerbar[0][:]], outerbar[0][:])
        #  if np.size(temp1) == 0:
        #      ob2DMax[j] = ob2DMax[j - 1]
        #  else:
        #      ob2DMax[j] = int(temp1[0])
        #      print(j,temp1[0])

        #  if np.size(temp3) == 0:
        #      ob2DMin[j] = ob2DMin[j - 1];
        #  else:
        #      ob2DMin[j] = int(temp3[0])
        #      print(j,temp3[0])
        # % search for the absolute minimum/maximum for the outerbar
        ob2DMax[j] = outerbar[0][0] + np.min(
            np.argmax(ZperturbY[outerbar], axis=0)
        )
        ob2DMin[j] = outerbar[0][0] + np.min(
            np.argmin(ZperturbY[outerbar], axis=0)
        )
        # figure(111)
        # plot(ZperturbY, x1[j,:],'-k')
        # plot(ZperturbY[ ob2DMax[j]], x1[j,ob2DMax[j]],'or')
        # plot(ZperturbY[ ob2DMin[j]], x1[j,ob2DMin[j]],'vg')
        # show()
        # Compute mean bars positions, thickness and amplitude
        Xo = np.mean(x1[0, ob2DMax])
        # Xo=np.min(x1[0,ob2DMax])+np.std(x1[1,ob2DMax])
        # Xo=np.min(x1[0,ob2DMax])
        # Do=np.mean(np.abs(x1[0,ob2DMax]-x1[1,ob2DMin]));
        # Do=np.std(x1[1,ob2DMax])
        Do = np.max(x1[0, ob2DMax]) - np.min(x1[1, ob2DMax])
        # Do=np.max(x1[0,ob2DMax])-np.min(x1[1,ob2DMin])
        Deltao = np.abs(np.min(h[:, ob2DMax]) - np.max(h[:, ob2DMin]))

        # # % search for the local minimum/maximum for the innerbar
        if nbs_bar == "2":
            #      [temp1,temp3] = peakdet(ZperturbY[innerbar[0][:]], innerbar[0][:]);
            #      if np.size(temp1) == 0:
            #          ib2DMax[j] = ib2DMax[j-1]
            #      else:
            #          ib2DMax[j] = int(temp1[0])

            #      if np.size(temp3) == 0:
            #          ib2DMin[j] = ib2DMin[j-1];
            #      else:
            #          ib2DMin[j] = int(temp3[0])
            # % search for the absolute minimum/maximum for the outerbar
            ib2DMax[j] = innerbar[0][0] + np.min(
                np.argmax(ZperturbY[innerbar], axis=0)
            )
            ib2DMin[j] = innerbar[0][0] + np.min(
                np.argmin(ZperturbY[innerbar], axis=0)
            )

            Xi = np.mean(x1[0, ib2DMax])
            # Xi = np.min(x1[0, ib2DMax]) + np.std(x1[1, ib2DMax])
            # Xi = np.max(x1[0,ib2DMax])
            # Di = np.mean(np.abs(x1[0,ib2DMax]-x1[0,ib2DMin]))
            # Di = np.std(x1[1,ib2DMax])
            Di = np.abs(np.max(x1[0, ib2DMax]) - np.min(x1[1, ob2DMin]))
            Deltai = np.abs(np.min(h[:, ib2DMax]) - np.max(h[:, ib2DMax]))
        else:
            ib2DMax = "NaN"
            ib2DMin = "NaN"
            Xi = "NaN"
            Di = "NaN"
            Deltai = "NaN"

    return (
        meanProfile,
        ob2DMax,
        ob2DMin,
        ib2DMax,
        ib2DMin,
        Xo,
        Do,
        Deltao,
        Xi,
        Di,
        Deltai,
    )
