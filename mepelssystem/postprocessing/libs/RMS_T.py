#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Mar 19 20:59:10 2020

@author: Julien CHAUCHAT
"""
import numpy as np


def RMS_T(dateM, HM, UM, VM, date, H, U, V):

    keep_data = np.where(dateM >= date[0])

    HI = np.interp(dateM[keep_data[0][:]], date, H, right=np.NAN)
    UI = np.interp(dateM[keep_data[0][:]], date, U, right=np.NAN)
    VI = np.interp(dateM[keep_data[0][:]], date, V, right=np.NAN)

    RMSH = np.nanstd(HM[keep_data] - HI[:]) / max(
        np.nanstd(HM[keep_data]), np.nanmean(np.abs(HM[keep_data]))
    )
    RMSU = np.nanstd(UM[keep_data] - UI[:]) / max(
        np.nanstd(UM[keep_data]), np.nanmean(np.abs(UM[keep_data]))
    )
    RMSV = np.nanstd(VM[keep_data] - VI[:]) / max(
        np.nanstd(VM[keep_data]), np.nanmean(np.abs(VM[keep_data]))
    )

    return RMSH, RMSU, RMSV
