import numpy as np


def peakdet(v, x):
    #    %PEAKDET Detect peaks in a vector
    #    %        [MAXTAB, MINTAB] = PEAKDET(V, DELTA) finds the local
    #    %        maxima and minima ("peaks") in the vector V.
    #    %        MAXTAB and MINTAB consists of two columns. Column 1
    #    %        contains indices in V, and column 2 the found values.
    #    %
    #    %        With [MAXTAB, MINTAB] = PEAKDET(V, DELTA, X) the indices
    #    %        in MAXTAB and MINTAB are replaced with the corresponding
    #    %        X-values.
    #    %
    #    %        A point is considered a maximum peak if it has the maximal
    #    %        value, and was preceded (to the left) by a value lower by
    #    %        DELTA.
    #    % Eli Billauer, 3.4.05 (Explicitly not copyrighted).
    #    % This function is released to the public domain; Any use is allowed.

    maxtab = []
    mintab = []

    v = v[:]
    #% Just in case this wasn't a proper vector

    delta = 0.35

    mn = np.Inf
    mx = -np.Inf
    mnpos = np.NaN
    mxpos = np.NaN

    lookformax = 1

    for i in np.flip(range(len(v))):
        this = v[i]
        if this > mx:
            mx = this
            mxpos = x[i]
        if this < mn:
            mn = this
            mnpos = x[i]

        if lookformax:
            if this < mx - delta:
                maxtab = [mxpos, mx]
                mn = this
                mnpos = x[i]
                lookformax = 0
        else:
            if this > mn + delta:
                mintab = [mnpos, mn]
                mx = this
                mxpos = x[i]
                lookformax = 1

    return maxtab, mintab
