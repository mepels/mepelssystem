%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%  Make plot from the results of the SANDBAR test case
%
%  Further Information:
%  http://www.crocoagrif.org/
%
%  This file is part of CROCOTOOLS
%

%  CROCOTOOLS is free software; you can redistribute it and/or modify
%  it under the terms of the GNU General Public License as published
%  by the Free Software Foundation; either version 2 of the License,
%  or (at your option) any later version.
%
%  CROCOTOOLS is distributed in the hope that it will be useful, but
%  WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%  GNU General Public License for more details.
%
%  You should have received a copy of the GNU General Public License
%  along with this program; if not, write to the Free Software
%  Foundation, Inc., 59 Temple Place, Suite 330, Boston,
%  MA  02111-1307  USA
%
%  Ref: Penven, P., L. Debreu, P. Marchesiello and J.C. McWilliams,
%       Application of the ROMS embedding procedure for the Central
%      California Upwelling System,  Ocean Modelling, 2006.
%
%  Patrick Marchesiello, IRD 2017,2020
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clear all
close all
clc
%================== User defined parameters ===========================
mycase    = '1b';     % LIP experiment : 1b: offshore migration 
                      %                  1c: onshore migration
if mycase == '1b'
    caseList={'lip1b'};
    labelList={'lip1b'};
else
    caseList={'lip1c'};
    labelList={'lip1c'};    
end
coefList  = [ 0 ];
parameterName = ' - '; 

%
% --- model params ---
%
basepath = '../beaches/lip/morpho/';

%figSavePath    = '../LIP1B/Figures/';
figSavePath         = './Figures/';
TableSavePath       = './Figures/RMSTables/';

figName_bed         = [figSavePath,'crossShoreBed_LIP',mycase,'.jpg'];
figName_Umap        = [figSavePath,'Umap_LIP',mycase,'.jpg'];
figName_Uprofile    = [figSavePath,'UprofilesLIP',mycase,'.jpg'];
figName_Cprofile    = [figSavePath,'CprofilesLIP',mycase,'.jpg'];
figName_CprofileLin = [figSavePath,'CprofilesLinLIP',mycase,'.jpg'];
figName_Akvprofile  = [figSavePath,'AkvProfilesLIP',mycase,'.jpg'];
figName_vsVanderWerf= [figSavePath,'vsDelft3D',mycase,'.jpg'];
figName_undertow    = [figSavePath,'undertow',mycase,'.jpg'];
figName_bar         = [figSavePath,'BarCrossShoreBed_LIP',mycase,'.jpg'];


tableName          = [TableSavePath,'RMSTable.txt'];
tableNameBar       = [TableSavePath,'xBarTable.txt'];
tableNamemaxBedEvo = [TableSavePath,'maxBedEvoTable.txt'];

figNameRMS         = [figSavePath,'RMSFig',mycase,'.jpg'];
figNamexBar        = [figSavePath,'BarPosFig',mycase,'.jpg'];
figNamemaxBedEvo   = [figSavePath,'maxBedEvo',mycase,'.jpg'];

% figNameRMS_Average = [figSavePath,'SensToGrid_RMS_average',mycase,'.jpg'];

fname     = 'sandbar_his.nc'; % croco file name
%                  1C: onshore  migration
contourPlot   = 0;
crosshorePlot = 1;
profilePlot   = 0;
vanderWerfPlot= 0;

myline={'-','--','-.',':'};
mycolor={'r','g','b','m'};
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% read cross-shore profile data from LIP database
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if mycase == '1b',
    load('/fsnet/project/meige/2019/19MEPELS/OPENDATA_MEPELS/lip/explip1b.mat');
    %http://servdap.legi.grenoble-inp.fr/opendap/meige/19MEPELS/lip/explip1b.mat');
    s = lip1b;
else
    load('/fsnet/project/meige/2019/19MEPELS/OPENDATA_MEPELS/lip/explip1c.mat');
    %http://servdap.legi.grenoble-inp.fr/opendap/meige/19MEPELS/lip/explip1c.mat');
    s = lip1c;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% read CROCO results
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
depth = 4.1;
if mycase == '1b'
    morph_fac = 18;       % morphological factor (from sediment.in)
else
    morph_fac = 13;
end

morph_cpl   = 1;        % feedback to currents
Ns=2  ;   %number of sand classes
%
% ---------------------------------------------------------------------
% --- get grid from numerical model ---
% ---------------------------------------------------------------------
%

%loop over multiple cases

Ncase=length(caseList);

for k=1:Ncase
    
    yindex = 2; % Mm=1 with NS no-slip conditions

    nc=netcdf(strcat(basepath,caseList{k},'/',fname),'r');
    
    tindex  =length(nc{'scrum_time'}(:)); % reads last record
    tindex0 =min(tindex,5);               %  2 h
    %
    % horizontal grid
    hr=squeeze(nc{'h'}(yindex,:));
    xr=squeeze(nc{'x_rho'}(yindex,:));
    hu=0.5*(hr(1:end-1)+hr(2:end));
    xu=0.5*(xr(1:end-1)+xr(2:end));
    L=length(hr);
    %
    hini=squeeze(nc{'hmorph'}(0,yindex,:));
    % new bathy from last record
    if morph_cpl
        hnew=squeeze(nc{'hmorph'}(tindex,yindex,:));
        h=hnew;
    else
        h=hr;
    end
    %
    % vertical grid
    N=length(nc{'s_rho'});
    theta_s=nc.theta_s(:);
    theta_b=nc.theta_b(:);
    hc=nc.hc(:);
    zeta=squeeze(nc{'zeta'}(tindex,yindex,:));
    Dcrit=1.1*nc{'Dcrit'}(:);
    zeta(h<Dcrit)=zeta(h<Dcrit)-h(h<Dcrit); %add land topo
    zr=squeeze(zlevs(h,zeta,theta_s,theta_b,hc,N,'r',2));
    zw=squeeze(zlevs(h,zeta,theta_s,theta_b,hc,N,'w',2));
    zru=0.5*(zr(:,1:end-1)+zr(:,2:end));
    zwu=0.5*(zw(:,1:end-1)+zw(:,2:end));
    dz1=zr(1,:)-zw(1,:);
    
    zr_0=squeeze(zlevs(hini,zeta,theta_s,theta_b,hc,N,'r',2));
    zru_0=0.5*(zr_0(:,1:end-1)+zr_0(:,2:end));

    %
    xr2d=repmat(xr,[N 1]);
    xu2d=repmat(xu,[N 1]);
    xw2d=repmat(xr,[N+1 1]);
    D   =zw(N+1,:)-zw(1,:);
    D2d =repmat(D,[N 1]);
    Du  =zwu(N+1,:)-zwu(1,:);
    Du2d=repmat(Du,[N 1]);
    
    % ---------------------------------------------------------------------
    % --- read/compute numerical model fields (index 1) ---
    % --------------------------------------------------------------------
    time=morph_fac/86400*(nc{'scrum_time'}(tindex)- ...
        nc{'scrum_time'}(1));
    
    % ... zonal velocity ...                         ---> xu,zu
    u=squeeze(nc{'u'}(tindex,:,yindex,:));
    
    % ... vertical velocity ...                      ---> xr,zw
    w=squeeze(nc{'w'}(tindex,:,yindex,:));
    
    % ... sediment concentration ...                 ---> xr,zr
    
    C_CROCO_01=squeeze(nc{'sand_01'}(0.5*tindex,:,yindex,:));
    C_CROCO_02=squeeze(nc{'sand_02'}(0.5*tindex,:,yindex,:)); %nnumber of sand classes Ns=2
    C_CROCO=C_CROCO_01+C_CROCO_02;

    % Bottom concentration from Patrick matlab script
    %CbotCROCO=C_CROCO(1,:).*(dz1./0.01).^1.2; % fitting to Rouse profile
    %CbotCROCO=max(0.,CbotCROCO);
    % use first value at the bottom
%     CbotCROCO=C_CROCO(1,:);
    CbotCROCO_i=zeros(size(C_CROCO,2),1);
    for index=1:size(C_CROCO,2)
        CbotCROCO_i(index)=interp1(zr(:,index)-zr(1,index)+dz1(index),C_CROCO(:,index),0.05,'linear','extrap');
    end
    % ... total viscosity
    Akv=squeeze(nc{'AKv'}(tindex,:,yindex,:));
    
    % ... viscosity due to wave breaking ...
    Akb=squeeze(nc{'Akb'}(tindex,:,yindex,:));
    
    % ... wave setup ...
    sup=squeeze(nc{'zeta'}(tindex0,yindex,:)); % init time
    sup(hr<0)=sup(hr<0)+hr(hr<0)-Dcrit;
    
    % ... u undertow ...
    ubotCROCO_i=zeros(size(u,2),1);
    for index=1:size(u,2)
        ubotCROCO_i(index)=interp1(zru(:,index)-zru(1,index)+dz1(index),u(:,index),0.1,'linear','extrap');
    end
    ubotCROCO_y2 =squeeze(nc{'u'}(tindex0,2,yindex,:)); % init time
    ubotCROCO_y3 =squeeze(nc{'u'}(tindex0,3,yindex,:)); % init time
    ubotCROCO_y4 =squeeze(nc{'u'}(tindex0,4,yindex,:)); % init time

    ubotCROCO_mean=zeros(size(u,2));
    for i=1:size(u,2)
        ubotCROCO_mean(i)=trapz(zru(:,i),u(:,i))/Du(i);
    end
    % ... hrms ...
    hrmsCROCO =squeeze(nc{'hrm'}(tindex0,yindex,:)); % init time
    
    close(nc)
    
    zeta(D<Dcrit)=NaN;
    u(Du2d<Dcrit)=NaN;
    sup(D<=max(0.1,Dcrit))=NaN;
    sup(hr<0)=NaN;
    
    MyaxisFontsize = 7;
    %
    %
% Plot contour map of velocity
    %
    %
    if (contourPlot==1)
        fig1=figure(1);
        xmin=40; xmax=190;
        zmin=1; zmax=4.5;
        cmin=-0.7; cmax=0.7; nbcol=14;
        cint=(cmax-cmin)/nbcol;
        map=colormap(jet(nbcol));
        map(nbcol/2  ,:)=[1 1 1];
        map(nbcol/2+1,:)=[1 1 1];
        colormap(map);
        contourf(xu2d,zru+depth,u,[cmin:cint:cmax],'linestyle','none');
        hold on
        colorbar('h');
        leg(1)=plot(s.xb,s.bathy_initial, 'k',  'LineWidth',1);
        leg(2)=plot(s.xb,s.bathy_final,'k--','LineWidth',2);
        leg(3)=plot(xr,depth-h,'color','b','LineWidth',2);
        plot(xr,zeta+depth,'color','g','LineWidth',2);
        legend(leg(1:3),'Measured Initial','Measured Final','CROCO','location','southeast');
        ylabel('Depth [m]','Fontsize',15)
        xlabel('x [m]','Fontsize',15)
        grid on
        axis([xmin xmax zmin zmax])
        caxis([cmin cmax])
        thour=floor(time*24);
        % if mycase=='1B',
        %  title(['SANDBAR EROSION   LIP-1B - U at Time ',num2str(thour),' hour'])
        % else
        %  title(['SANDBAR ACCRETION LIP-1C - U at Time ',num2str(thour),' hour'])
        % end
        set(gca,'Fontsize',8)
        hold off
        saveas(gcf, figName_Umap, 'jpg')
    end
    %
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %  Comparison with experiments
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% RMS Wave height
%
Nprof=size(s.xbr,1);
indexH = zeros(Nprof,1);
for i=1:Nprof
        indexH(i)=find(xr>=s.xbr(i),1,'first');
end
HrmsI=interp1(xr(indexH),hrmsCROCO(indexH),s.xbr,'nearest');
RMS_H=nanstd(s.Hs_report/sqrt(2)-HrmsI)/max(nanstd(s.Hs_report/sqrt(2)),nanmean(abs(s.Hs_report/sqrt(2))));

%
% RMS bed evolution
%
Xmin=19;
keepData = find(xr>=Xmin);
NprofBedEvo=size(keepData,2);
indexBedEvo = zeros(NprofBedEvo,1);
for i=1:NprofBedEvo
        indexBedEvo(i)=find(s.xb(:)>=xr(keepData(i)),1,'first');
end
BedEvoI=interp1(s.xb(indexBedEvo),s.bathy_final(indexBedEvo)-s.bathy_initial(indexBedEvo),xr(keepData),'nearest');
RMS_BedEvo=nanstd(BedEvoI-(hini(keepData)-h(keepData)))/max(nanstd(s.bathy_final-s.bathy_initial),nanmean(s.bathy_final-s.bathy_initial));
% BedEvoI=interp1(xr(indexBedEvo),hini(indexH)-h(indexH),s.xb,'nearest');
% RMS_BedEvo=nanstd((s.bathy_final-s.bathy_initial)-BedEvoI)/max(nanstd(s.bathy_final-s.bathy_initial),nanmean(s.bathy_final-s.bathy_initial))

RMS_U = 0;
RMS_C = 0;

Nuprof = size(s.uprof,2);

for i = 1:Nuprof
    index=find(xr>=s.xrtf(i),1,'first');
    uI=interp1(zru(:,index)-zru(1,index)+dz1(index),u(:,index),s.uprof(i).z,'nearest');
    RMS_U = RMS_U + nanstd(s.uprof(i).u-uI)/max(nanstd(s.uprof(i).u),nanmean(abs(s.uprof(i).u)));
end
RMS_U = RMS_U / double(Nuprof);

%
Ncprof = size(s.cprof,2);

for i = 1:Ncprof
    index=find(xr>=s.xconc(i),1,'first');
    CI=interp1(zr(:,index)-zr(1,index)+dz1(index),C_CROCO(:,index),s.cprof(i).z,'nearest');
    RMS_C = RMS_C + nanstd(s.cprof(i).c-CI)/max(nanstd(s.cprof(i).c),nanmean(abs(s.cprof(i).c)));
end
RMS_C = RMS_C / double(Ncprof);

% write table of RMS

% % U RMS
% meanRMS_UHrms_Vec=[RMS_H;RMS_U];
% meanRMS_UHrms=mean(meanRMS_UHrms_Vec);
% meanRMS_Vec=[RMS_H;RMS_U;RMS_C;RMS_BedEvo];
% meanRMS=mean(meanRMS_Vec);
% % 
% meanRMS_C_Vec=[RMS_C];
% meanRMS_C=mean(meanRMS_C_Vec);
% totMeanRMS_Vec=[RMS_H;RMS_U;RMS_C];
% totMeanRMS=mean(totMeanRMS_Vec);
% 
% Scenario=caseList(k);
% parameter=labelList(k);
% coefValue=coefList(k);
% table_RMS(k,:) = table(Scenario,parameter,coefValue,RMS_H,RMS_U,RMS_C,RMS_BedEvo,meanRMS_UHrms,meanRMS);
% writetable(table_RMS,tableName)
% 
% table_RMS_eachCase(1,:) = table(Scenario,parameter,coefValue,RMS_H,RMS_U,RMS_C,RMS_BedEvo,meanRMS_UHrms,meanRMS);
% RMSTablepath = [basepath,caseList{k}];
% mkdir(RMSTablepath,'RMSTable');
% TableSavePathEachCase      = [RMSTablepath,'/RMSTable'];
% tableNameEachCase          = [TableSavePathEachCase,'/RMS.txt'];
% writetable(table_RMS_eachCase,tableNameEachCase);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% max bed evolution position %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Maximum bed evolution from the experiments
maxBedEvoRange_EXP=find(120<s.xb<150);
maxBedEvo_Exp=max(s.bathy_final(maxBedEvoRange_EXP)-s.bathy_initial(maxBedEvoRange_EXP));
maxBedEvo_Exp_index=find(s.bathy_final(maxBedEvoRange_EXP)-s.bathy_initial(maxBedEvoRange_EXP)==maxBedEvo_Exp);
x_maxBedEvo_Exp=s.xb(maxBedEvo_Exp_index);

% Maximum bed evolution from the model
maxBedEvoRange_model=find(120<xr<150);
maxBedEvo_model=max(hini(maxBedEvoRange_model)-h(maxBedEvoRange_model));
maxBedEvo_model_index=find(hini(maxBedEvoRange_model)-h(maxBedEvoRange_model)==maxBedEvo_model);
x_maxBedEvo_model=xr(maxBedEvo_model_index);

% initial bar position from the experiments
offShoreBarIndexExp=find(s.xb<150);
barHeightExpIni=max(s.bathy_initial(offShoreBarIndexExp));
barIndexExpIni=find(s.bathy_initial==barHeightExpIni);
xbarExpIni=s.xb(barIndexExpIni);

%difference with initial bar position from the experiments
dx_maxBedEvo_Exp=x_maxBedEvo_Exp-xbarExpIni;
dx_maxBedEvo_model=x_maxBedEvo_model-xbarExpIni;
dy_maxBedEvo_Exp=maxBedEvo_Exp;
dy_maxBedEvo_model=maxBedEvo_model;

% write table of max bed evolution
% table_maxBedEvo(k,:) = table(Scenario,parameter,coefValue,dx_maxBedEvo_Exp,...
%     dx_maxBedEvo_model,dy_maxBedEvo_Exp,dy_maxBedEvo_model);
% writetable(table_maxBedEvo,tableNamemaxBedEvo);
% 
% Scenario=caseList(k);
% parameter=labelList(k);
% coefValue=coefList(k);
% table_maxBedEvo_eachCase(1,:) = table(Scenario,parameter,coefValue,dx_maxBedEvo_Exp,...
%     dx_maxBedEvo_model,dy_maxBedEvo_Exp,dy_maxBedEvo_model);
% tableNameEachCase_maxBedEvo          = [TableSavePathEachCase,'/maxBedEvo.txt'];
% writetable(table_maxBedEvo_eachCase,tableNameEachCase_maxBedEvo);


%%% Plot maximum bed evolution figure
% if k==Ncase
%     fig2=figure(2);
%     plot(dx_maxBedEvo_Exp,dy_maxBedEvo_Exp,'sk','linewidth',2)
%     text(dx_maxBedEvo_Exp,dy_maxBedEvo_Exp,'Experiment')   
% %     text(table_xBar.xBarDiffExp,table_xBar.yBarDiffExp,num2str(table_RMS.coefValue,3))   
%     hold on
%     plot(table_maxBedEvo.dx_maxBedEvo_model,table_maxBedEvo.dy_maxBedEvo_model,'*r','linewidth',2)
%     text(table_maxBedEvo.dx_maxBedEvo_model,table_maxBedEvo.dy_maxBedEvo_model,labelList)
%     hold on
% %     plot([0,0],[min(table_BarPos.yBarDiffMod),max(table_BarPos.yBarDiffMod)],':k','LineWidth',2);
%     plot([0,0],[-10,10],'-k','LineWidth',2);
%     plot([-20,20],[0,0],'-k','LineWidth',2);
%     xlabel('X max bed evolution-Xbar(initial) [m]','Fontsize',6)
% %     ylabel('Ybar(final)-Ybar(initial) [m]','Fontsize',6)
%     ylabel('max bed evolution','Fontsize',6)
%     if mycase=='1b'
%         axis([-16,16,-0.7,0.7]);
%         set(gca,'ytick',[-0.7:0.1:0.7]);
%     else
%         axis([-5,5,-1.2,1.2]);
%         set(gca,'ytick',[-1.2:0.2:1.2]);
%         set(gca,'xtick',[-5:1:5]);
%     end
%     grid()
%     saveas(gcf, figNamexBar, 'jpg')
%     
% end


%%% Plot RMS figures
% if k==Ncase
% %     fig1=figure('Name','RMS');
%     fig3=figure(3);
%     plot(table_RMS.coefValue,table_RMS.RMS_C,'sk','linewidth',2)
%     text(table_RMS.coefValue,table_RMS.RMS_C,num2str(table_RMS.RMS_C,3))
%     hold on
%     plot(table_RMS.coefValue,table_RMS.RMS_BedEvo,'r*','linewidth',2)
%     text(table_RMS.coefValue,table_RMS.RMS_BedEvo,num2str(table_RMS.RMS_BedEvo,3))   
%     xlabel(parameterName,'Fontsize',6)
% %     xlabel(parameterName)
%     ylabel('RMS')
%     legend('RMS_C','RMS_{BedEvolution}','orientation', 'horizontal','position',[0.4 0.93 0.01 0.01])
%     saveas(gcf, figNameRMS, 'jpg')
%     
% end

% Plot crossshore profiles
    %
    %
    if (crosshorePlot==1)
        xmin = 100;
        xmax = 175;
        zmin = 2.3;
        zmax = 4.5;
        if(mycase=="1b")
            hmin = -0.1;
            hmax = max(s.hrms)*1.3;
%             hmax = 1.1;
            etamin = -0.1;
            etamax =  0.6;
            Umin = -0.36;
%             Umin = min([min(s.Ubar), min(ubotCROCO)])*1.1;                      
            Umax = 0.1; %max(urms)*1.1;
            Cmax = max([max(CbotCROCO_i), max(s.Cbot)])*1.2;
        elseif (mycase=="1c")
            hmin = 0;
            hmax=0.55;
%             hmax = max(s.hrms);
            etamin = -0.1;
            etamax =  0.4;
%             Umin = min([min(s.Ubar), min(ubotCROCO)])*1.1;
            Umin = -0.36;
            Umax = 0.07; %max(urms)*1.1;
            Cmax = max([max(CbotCROCO_i), max(s.Cbot)])*1.2;
        end
        
        fig4=figure(4);
        
        pos1 = [.08 .68 .8 .28];
        pos2 = [.08 .38 .8 .28];
        pos3 = [.08 .08 .8 .28];
        %
        % subplot 1
        %
        subplot('Position',pos1)
            if k==1
                leg(2)=plot(s.xb,s.bathy_final,'--k','LineWidth',2);
                hold on;
                leg(3)=plot(s.xb,s.bathy_initial,'-k','LineWidth',2);
                hold on
                grid()
            end
            hold on
            plot(xr,depth-h,'LineWidth',2,'LineStyle',myline{k},'Color',mycolor{k});

            %     plot([xmin,xmax],[depth,depth],':k','LineWidth',2);
            hold on
            ylabel('depth (m)','Fontsize',8);
            set(gca,'XTickLabel',[],'Fontsize',8);
            title(mycase)
            axis([xmin,xmax,zmin,zmax])
            set(gca,'Fontsize',MyaxisFontsize);
            if k==Ncase
                plot([xmin,xmax],[depth,depth],':k','LineWidth',2);
                %             legend(labelList{1:k},'Measured bed (intermediate)','location',[.14 .85 .2 .1])
                %             legend(leg(1:2),'final measured bathy','initial measured bathy',labelList{1:k},'location',[.14 .85 .2 .1]);
%                 legend('measured final bathy','measured initial bathy',labelList{1:k},'location',[.14 .85 .2 .1]);
                legend('measured final bed','measured initial bed',labelList{1:k},'location',[.11 .82 .2 .07]);
                hold on
                set(gca,'Fontsize',MyaxisFontsize);
            end
            
            hold off;
            clear leg;
        end
  
     
        %
        % subplot 2
        %
        subplot('Position',pos2)
        hold on
        if k==1
            leg(4)=plot(s.xb,s.bathy_final-s.bathy_initial,'--k','LineWidth',2)
            grid()
        end
        hold on;
        leg(5)=plot(xr,hini-h,'LineWidth',2,'LineStyle',myline{k},'Color',mycolor{k});
        plot([xmin, xmax],[0,0],':k')
        if k==Ncase
            legend('location','northwest')
            legend('measured bed evolution')
            set(gca,'Fontsize',MyaxisFontsize);
        end
        ylabel('Final-Initial (m)','Fontsize',8)
        %         ylim([-0.45 0.45])
        ylim([-0.4 0.5])
        set(gca,'xticklabels',[],'Fontsize',8)
        set(gca,'Xlim',[xmin xmax])
        set(gca,'Fontsize',MyaxisFontsize);
        hold off;
        clear leg;
        %
        % Subplot 3
        %
        subplot('Position',pos3)
        if k==1
            leg(1)=plot(s.xb,s.bathy_initial,'-k','LineWidth',2);
            grid()
            hold on
        end
        hold on;
        len=xmax-xmin;
        if(mycase=="1b")
            cmax_leg = 5;
        elseif (mycase=="1c")
            cmax_leg = 5;
        end
        cmin = 0;
        cscale=0.05*len/cmax_leg;
        
        for i = 1:Ncprof
            index=find(xr>=s.xconc(i),1,'first');
            hold on
            if k==1,
                cvar=xr(index)+s.cprof(i).c*cscale;
                zvar=depth+s.cprof(i).z-hini(index);
                leg(2)=line(cvar,zvar,'LineStyle','none','Marker','o','Color','k',...
                    'linewidth',2,'MarkerSize',6);
            end
            clear cvar zvar
            hold on;
            cvar=xr(index)+C_CROCO(:,index)*cscale;
            zvar=depth+zr_0(:,index);
            line(cvar,zvar,'LineWidth',2,'LineStyle',myline{k},'Color',mycolor{k});
            line(xr(index)*ones(size(zvar)),zvar, ...
                'LineWidth',1,'LineStyle',myline{k},'Color',[0.6 0.6 0.6]);
            clear cvar zvar            
            line([135,135+3*cscale],[2.7,2.7],'LineWidth',2,'LineStyle','-','Color','k')
            text(135+1.5*cscale,2.55, '3 g/L', 'HorizontalAlignment','center','Fontsize',10)
            if k==Ncase
                legend('location','southeast')
                legend('measured initial bathymetry','measured concentration')
                set(gca,'Fontsize',MyaxisFontsize);
            end
            ylabel('depth (m)','Fontsize',8);
            axis([xmin,xmax,zmin,zmax-0.3])
            xlabel('cross-shore (m)','Fontsize',8)
            set(gca,'Fontsize',MyaxisFontsize);
            hold off
            
            saveas(gcf, figName_bar, 'jpg')
            



%         fig5=figure(5);
%         
%         pos1 = [.08 .68 .8 .28];
%         pos2 = [.08 .38 .8 .28];
%         pos3 = [.08 .08 .8 .28];
% %         pos4 = [.08 .25 .8 .165];
% %         pos5 = [.08 .075 .8 .165];
%         %
%         % subplot 1
%         %
%         subplot('Position',pos1)
% %         leg(1)=plot(xr,depth-h,'LineWidth',2,'LineStyle',myline{k},'Color',mycolor{k});
%         if k==1
% %             leg(2)=plot(s.xb,s.bathy_intermediate,'-k','LineWidth',2);
%             leg(1)=plot(s.xb,s.bathy_final,'--k','LineWidth',2);
%             hold on;
%             leg(2)=plot(s.xb,s.bathy_initial,'-k','LineWidth',2);
%             hold on
%         end
%         hold on
%         plot(xr,depth-h,'LineWidth',2,'LineStyle',myline{k},'Color',mycolor{k});
%         %     plot([xmin,xmax],[depth,depth],':k','LineWidth',2);
%         grid()
%         ylabel('depth (m)','Fontsize',8);
%         axis([xmin,xmax,-0.1,5])
%         set(gca,'XTickLabel',[],'Fontsize',8);
%         title(mycase)
%         if k==Ncase
%             plot([xmin,xmax],[depth,depth],':k','LineWidth',2);
% %             legend(labelList{1:k},'Measured bed (intermediate)','location',[.14 .85 .2 .1])
% %             legend(leg(1:2),'final measured bathy','initial measured bathy',labelList{1:k},'location',[.14 .85 .2 .1]);
%             legend('measured final bathy','measured initial bathy',labelList{1:k},'location',[.14 .85 .2 .1]);
%             hold on
%             set(gca,'Fontsize',MyaxisFontsize);
%             grid()
%         end
%         grid()
% 
%         hold off;
%         clear leg;
%         %
%         % subplot 2
%         %
%         subplot('Position',pos2)
%         hold on
%         if k==1
%             plot(s.xb,s.bathy_final-s.bathy_initial,'--k','LineWidth',2)
%             grid()
%         end
%         hold on;
%         plot(xr,hini-h,'LineWidth',2,'LineStyle',myline{k},'Color',mycolor{k});
%         plot([xmin, xmax],[0,0],':k')
%         grid()
%         ylabel('Final-Initial (m)','Fontsize',8)
%         ylim([-0.45 0.45])
%         set(gca,'xticklabels',[],'Fontsize',8)
%         set(gca,'Xlim',[xmin xmax])
%         set(gca,'Fontsize',MyaxisFontsize);
%         grid()
%         hold off;
%         clear leg;
%         
%         %
%         % Subplot 3
%         %
%         subplot('Position',pos3)
%         hold on;
%         plot(xr,CbotCROCO_i,'--b','LineWidth',2,'LineStyle',myline{k},'Color',mycolor{k});
%         hold on;
%         grid()
%         if k==Ncase
% %             leg(1)=plot(s.xconc,s.Cbar,'*k','LineWidth',2);
% %             leg(2)=plot(s.xconc,s.Cbot,'ok','LineWidth',2);
%             leg(1)=plot(s.xconc,s.Cbot,'ok','LineWidth',2);
%             legend('location','northwest')
% %             legend(leg(1:2),{'Measured mean','Measured bottom'})
%             legend(leg(1),{'Measured bottom'})
%             set(gca,'Fontsize',MyaxisFontsize);
%             grid()
%         end
%         ylabel('C (g/l)','Fontsize',8);
%         axis([xmin,xmax,0,Cmax])
%         xlabel('cross-shore (m)','Fontsize',8)
%         set(gca,'Fontsize',MyaxisFontsize);
%         hold off
%         grid()
%         
%         saveas(gcf, figName_bed, 'jpg')
    end    %

end
return
