#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Mar 19 17:15:06 2020

@author: chauchat
"""

from netCDF4 import Dataset
import numpy as np
import os
from pylab import *
import matplotlib.gridspec as gridspec
from datetime import datetime as dt
from scipy.interpolate import griddata
from datetime import *
from scipy.optimize import curve_fit
import pickle

import sys

sys.path.append("./libs/")
from BSS import BSS
from mydate import datenum
from mydate import datenum_to_datetime
from peakdet import peakdet
from AnalyseProfiles import AnalyseProfiles
from AnalyseProfiles import func

# %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
# %%                         User functiions                              %%
# %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
def readCROCO(basepath, casename, CROCOgrid, Xshift):
    #%
    #% Read the croco output file
    #%
    ncfileGRD = Dataset(
        basepath + casename + "/TRUCVERT_" + CROCOgrid + "_GRD.nc"
    )
    x1 = ncfileGRD.variables["x_rho"][:, :]
    y1 = ncfileGRD.variables["y_rho"][:, :]
    # shift CROCO bathymetry cross-shore by -Xshift
    x1 = x1 + Xshift

    ncfile = Dataset(basepath + casename + "/TRUCVERT_" + CROCOgrid + "_his.nc")
    #% Netcdf output file
    timeCROCO = ncfile.variables["time"][:]
    #% (s)
    hmorph = ncfile.variables["hmorph"][:, :, :]
    Nt = np.size(timeCROCO)
    Nx = np.size(x1, 1)
    Ny = np.size(x1, 0)

    hmorph = -hmorph
    hmorph = np.flip(hmorph, axis=1)
    hmorph = np.flip(hmorph, axis=2)

    return Nt, Nx, Ny, timeCROCO, x1, y1, hmorph


##############################################################################
loaddata = 1

debug = 1

basepathDATA = "/.fsnet/project/meige/2019/19MEPELS/Simulations/TRUCVERT/DATA/"
basepathCROCO = "../../beaches/trucvert/croco/"

# List all the files in the folder
#'tauCE', 'ERATE', 'Bedloadcoef' 'optim'
configName = "default"

# cross-shore shift between CROCO and measured bathymetry
Xshift = 0  # 100 m + dx/2

if configName == "default":
    codelist = ["run1"]
    CrocoLegend = ["run1"]
    CROCOgrid = ["70x100sh"]

Ncas = len(codelist)

pickle_file = "CROCO_BSS" + configName + ".py"

Ncas = len(codelist)
BSSCROCO = np.zeros(Ncas)

# Change fontsize
#
matplotlib.rcParams.update({"font.size": 16})
mpl.rcParams["lines.linewidth"] = 3
mpl.rcParams["lines.markersize"] = 5
mpl.rcParams["lines.markeredgewidth"] = 1

#
# Figure size
#
figwidth = 14
figheight = 10

gsMulti = gridspec.GridSpec(1, 1)
gsMulti.update(
    left=0.1, right=0.95, top=0.95, bottom=0.1, wspace=0.125, hspace=0.25
)

#%
#% Path setup
#%

figPath = "../Figures/CROCO_Morpho/"

date_00 = datetime.strptime("2008-02-11 00:00", "%Y-%m-%d %H:%M")
date_0 = datetime.strptime("2008-03-03 00:00", "%Y-%m-%d %H:%M")
date_f = datetime.strptime("2008-04-04 00:00", "%Y-%m-%d %H:%M")

date_0num = datenum(date_0)
date_fnum = datenum(date_f)

xlimit = [date_0num, date_fnum]
xxtick = np.linspace(date_0num, date_fnum, 6)
# xxticklabels=[datenum_to_datetime(i) for i in xxtick];
xxticklabels = [str(datenum_to_datetime(i))[0:10] for i in xxtick]

bedLevels = np.linspace(-15, 5, 101)
bedLticks = np.linspace(-15, 5, 11)
bedLtickLabels = [str(i) for i in bedLticks]

cmapBathy = "seismic"

Zmin = np.min(bedLevels)
Zmax = np.max(bedLevels)

#% 1) Read wave and tide data from FORCAGE file
ncFor = Dataset(os.path.join(basepathDATA, "HYDRO/ForcageLEGIFull.nc"))

timeF = ncFor.variables["time"][:]
dateF = ncFor.variables["date"][:]
Hs = ncFor.variables["Hs"][:]
Tp = ncFor.variables["Tp"][:]
Dir = ncFor.variables["Dir"][:]
Tide = ncFor.variables["Tide"][:]
#
# Read Almar et al. (2010) data
#
tInner, XInner, tmInner, XmInner, tMInner, XMInner = np.loadtxt(
    os.path.join(basepathDATA, "BARRES/donnees_barre_interne.csv"),
    delimiter=",",
    skiprows=2,
    usecols=(0, 1, 2, 3, 4, 5),
    unpack=True,
)

tOuter, XOuter, tmOuter, XmOuter, tMOuter, XMOuter = np.loadtxt(
    os.path.join(basepathDATA, "BARRES/donnees_barre_externe.csv"),
    delimiter=",",
    skiprows=2,
    usecols=(0, 1, 2, 3, 4, 5),
    unpack=True,
)

date0Almar = datetime.strptime("2008-03-06 00:00", "%Y-%m-%d %H:%M")

dateAlmar_Inner = datenum(date0Almar) + np.array(tInner)
dateAlmar_Outer = datenum(date0Almar) + np.array(tOuter)


#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
#%
#%  READ CROCO results
#%
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
dateCROCO_0 = datenum(date_0)
#
#
#

isLoad = 0
if loaddata == 0:
    isLoad = 1

print("configName", "tauCE, ERATE, Bedloadcoef, gamma, Zob")

for i in range(Ncas):
    print(codelist[i])
    #    %
    #    %  Read the grid file and check the topography
    #    %
    [Nt, Nx, Ny, timeCROCO, x1, y1, hmorph] = readCROCO(
        basepathCROCO, codelist[i], CROCOgrid[i], Xshift
    )

    dateCROCO = dateCROCO_0 + timeCROCO / (24 * 3600)

    Xmin = np.min(x1)
    Xmax = np.max(x1)
    Ymin = np.min(y1)
    Ymax = np.max(y1)
    kplotIndex = np.where(dateCROCO >= datenum(date_f))
    if np.size(kplotIndex) == 0:
        kplot = Nt - 1
    else:
        kplot = np.min(np.where(dateCROCO >= datenum(date_f)))

    if isLoad == 0:
        Xraw, Yraw, Zraw = np.loadtxt(
            os.path.join(
                basepathDATA, "Bathymetries/Large/Dune_topo_bathy11_2XYZ.txt"
            ),
            delimiter=",",
            usecols=(0, 1, 2),
            unpack=True,
        )
        # Shift bathy data by -Xshift to fit with CROCO configuration
        # Xraw = Xraw - Xshift

        h11_2 = np.zeros((Ny, Nx))
        h11_2 = griddata((Xraw, Yraw), Zraw, (x1, y1), method="linear")
        del Xraw, Yraw, Zraw

        Xraw, Yraw, Zraw = np.loadtxt(
            os.path.join(
                basepathDATA, "Bathymetries/Large/Dune_topo_bathy4_avrilXYZ.txt"
            ),
            delimiter=",",
            usecols=(0, 1, 2),
            unpack=True,
        )
        # Shift bathy data by -Xshift to fit with CROCO configuration
        # Xraw = Xraw - Xshift
        h4_4 = np.zeros((Ny, Nx))
        h4_4 = griddata((Xraw, Yraw), Zraw, (x1, y1), method="linear")
        del Xraw, Yraw, Zraw

        # Analyse measured bathy to get bar positions
        [
            meanProfile11_2,
            ob2DMax11_2,
            ob2DMin11_2,
            ib2DMax11_2,
            ib2DMin11_2,
            Xo11_2,
            Do11_2,
            Deltao11_2,
            Xi11_2,
            Di11_2,
            Deltai11_2,
        ] = AnalyseProfiles(Nx, Ny, h11_2, x1)

        # Analyse measured bathy to get bar positions
        [
            meanProfile4_4,
            ob2DMax4_4,
            ob2DMin4_4,
            ib2DMax4_4,
            ib2DMin4_4,
            Xo4_4,
            Do4_4,
            Deltao4_4,
            Xi4_4,
            Di4_4,
            Deltai4_4,
        ] = AnalyseProfiles(Nx, Ny, h4_4, x1)

        isLoad = 1

    #%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    #%%%%                        Analyse profiles                                                %%%%
    #%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    meanProfile = np.zeros((Nt, Nx))
    Zp = np.zeros(Nt)
    ob2DMax = np.zeros((Nt, Ny))
    ob2DMin = np.zeros((Nt, Ny))
    ib2DMax = np.zeros((Nt, Ny))
    ib2DMin = np.zeros((Nt, Ny))
    Xo = np.zeros((Nt, 1))
    Xi = np.zeros((Nt, 1))
    Do = np.zeros((Nt, 1))
    Di = np.zeros((Nt, 1))
    Deltao = np.zeros((Nt, 1))
    Deltai = np.zeros((Nt, 1))

    for k in range(Nt):
        [
            meanProfile[k, :],
            ob2DMax[k, :],
            ob2DMin[k, :],
            ib2DMax[k, :],
            ib2DMin[k, :],
            Xo[k],
            Do[k],
            Deltao[k],
            Xi[k],
            Di[k],
            Deltai[k],
        ] = AnalyseProfiles(Nx, Ny, hmorph[k, :, :], x1)

    #% Compute skill estimator
    BSSCROCO[i] = BSS(hmorph[kplot, :, :], h4_4, h11_2, Nx, Ny)

    gs = gridspec.GridSpec(1, 1)
    gs.update(
        left=0.1, right=0.95, top=0.95, bottom=0.1, wspace=0.125, hspace=0.25
    )

    gs2 = gridspec.GridSpec(2, 1)
    gs2.update(
        left=0.1, right=0.92, top=0.95, bottom=0.1, wspace=0.15, hspace=0.15
    )

    gs3 = gridspec.GridSpec(3, 1)
    gs3.update(
        left=0.15, right=0.95, top=0.95, bottom=0.1, wspace=0.125, hspace=0.25
    )
    ##
    ## plot bathymetries
    ##
    fig = figure(
        num=1,
        figsize=(figwidth, figheight),
        dpi=100,
        facecolor="w",
        edgecolor="w",
    )

    ax1 = subplot(gs2[0, 0])
    #% Contour of bed elevation
    cax1 = ax1.contourf(y1, x1, h4_4, bedLevels, cmap=cmapBathy)
    # terrain
    cbar = fig.colorbar(
        cax1, orientation="vertical", ticks=bedLticks, fraction=0.075
    )
    cbar.set_ticklabels(bedLtickLabels)
    for j in range(Ny):
        ax1.plot(y1[j, 0], x1[j, ob2DMax4_4[j]], "k*")
        ax1.plot(y1[j, 0], x1[j, ob2DMin4_4[j]], "rx")
        ax1.plot(y1[j, 0], x1[j, ib2DMax4_4[j]], "b*")
    ax1.plot([Ymin, Ymax], [Xo4_4, Xo4_4], "--k", lw=4)
    ax1.plot([Ymin, Ymax], [Xi4_4, Xi4_4], "--b", lw=4)
    # ax2.set_xlabel('Long-shore (m)')
    ax1.set_ylabel("Cross-shore (m)")
    ax1.set_ylim([0, 1000])
    ax1.set_xlim([Ymin, Ymax])
    ax1.set_xticklabels([])

    ax2 = subplot(gs2[1, 0])
    #% Contour of bed elevation
    cax2 = ax2.contourf(y1, x1, hmorph[kplot, :, :], bedLevels, cmap=cmapBathy)
    # terrain
    for j in range(Ny):
        ax2.plot(y1[j, 0], x1[j, int(ob2DMax[kplot, j])], "k*")
        ax2.plot(y1[j, 0], x1[j, int(ob2DMin[kplot, j])], "rx")
        ax2.plot(y1[j, 0], x1[j, int(ib2DMax[kplot, j])], "b*")
    ax2.plot([Ymin, Ymax], [Xo[kplot], Xo[kplot]], "--k", lw=4)
    ax2.plot([Ymin, Ymax], [Xi[kplot], Xi[kplot]], "--b", lw=4)
    cbar2 = fig.colorbar(
        cax2, orientation="vertical", ticks=bedLticks, fraction=0.075
    )
    cbar2.set_ticklabels(bedLtickLabels)
    ax2.set_ylabel("Cross-shore (m)")
    ax2.set_xlabel("Long-shore (m)")
    ax2.set_ylim([0, 1000])
    ax2.set_xlim([Ymin, Ymax])
    # ax2.set_xticklabels(ax2.get_xticklabels(), rotation=45, ha='right')

    savefig(
        figPath + "Bathy" + codelist[i] + ".png",
        dpi=300,
        facecolor="w",
        edgecolor="w",
        format="png",
    )

    if debug == 1:
        ##
        ## plot bathymetries
        ##
        for k in range(Nt):
            fig = figure(
                num=100 + k,
                figsize=(figwidth, figheight),
                dpi=100,
                facecolor="w",
                edgecolor="w",
            )
            ax2 = subplot(gs3[0:2, 0])
            #% Contour of bed elevation
            cax2 = ax2.contourf(
                y1, x1, hmorph[k, :, :], bedLevels, cmap=cmapBathy
            )
            # terrain
            for j in range(Ny):
                ax2.plot(y1[j, 0], x1[j, int(ob2DMax[k, j])], "k*")
                ax2.plot(y1[j, 0], x1[j, int(ob2DMin[k, j])], "rx")
                ax2.plot(y1[j, 0], x1[j, int(ib2DMax[k, j])], "b*")
            ax2.plot([Ymin, Ymax], [Xo[k], Xo[k]], "--k", lw=4)
            ax2.plot([Ymin, Ymax], [Xi[k], Xi[k]], "--b", lw=4)

            ax2.set_ylabel("Cross-shore (m)")
            ax2.set_xlabel("Long-shore (m)")
            ax2.set_ylim([100, 1000])
            ax2.set_xlim([Ymin, Ymax])
            title(datenum_to_datetime(dateCROCO[k]))
            # ax2.set_xticklabels(ax2.get_xticklabels(), rotation=45, ha='right')
            ax3 = subplot(gs3[2, 0])
            ax3.plot(dateF, Hs, "-k")
            ax3.plot([dateCROCO[k], dateCROCO[k]], [0, 10], "-r")
            ax3.set_xlim(xlimit)
            ax3.set_ylim([0, 8])
            ax3.set_ylabel("Hs (m)")
            ax3.set_xticks(xxtick)
            ax3.set_xticklabels(xxticklabels)
            ax3.set_xlabel("date")
            ax3.set_xticklabels(ax3.get_xticklabels(), rotation=45, ha="right")

            savefig(
                figPath + "tmp/Bathy" + codelist[i] + "_" + str(k) + ".png",
                dpi=300,
                facecolor="w",
                edgecolor="w",
                format="png",
            )
            close(100 + k)
    # =============================================================================
    # plot mean cross-shores profiles
    #
    figure(2, figsize=(10, 6))

    ax1 = subplot(gs[0, 0])
    #% Contour of bed elevation
    ax1.plot(x1[0, :], meanProfile11_2, "-k", label=str(date_00)[:-9])
    ax1.plot(x1[0, :], meanProfile4_4, "--r", label=str(date_f)[:-9])
    ax1.plot(x1[0, :], meanProfile[kplot, :], "-.b", lw=1.5, label=codelist[i])
    # ax1.plot(x1[0,:],meanProfile[0,:],'--b',lw=1.5,label=codelist[i]);
    legend(fontsize=8)
    ax1.set_ylabel("Bathymetry (m)")
    ax1.set_xlabel("Cross-shore (m)")
    ax1.set_xlim([0, 1000])

    savefig(
        figPath + "MeanProfiles" + codelist[i] + ".png",
        dpi=300,
        facecolor="w",
        edgecolor="w",
        format="png",
    )

    # =============================================================================
    # plot mean cross-shores profiles for all cases
    #
    figure(3, figsize=(10, 6))
    ax21 = subplot(gsMulti[0, 0])
    if i == 0:
        ax21.plot(x1[0, :], meanProfile11_2, "-k", label=str(date_00)[:-9])
        ax21.plot(x1[0, :], meanProfile4_4, "--r", label=str(date_f)[:-9])
        ax21.set_ylabel("Bathymetry (m)")
        ax21.set_xlabel("Cross-shore (m)")
        ax21.set_xlim([0, 1000])
    ax21.plot(x1[0, :], meanProfile[kplot, :], "-.", lw=1.5, label=codelist[i])

    if i == Ncas - 1:
        ax21.legend(fontsize=8)

    savefig(
        figPath + "MeanProfiles" + configName + ".png",
        dpi=300,
        facecolor="w",
        edgecolor="w",
        format="png",
    )

    # =============================================================================
    # plot bars positions and thickness
    #
    figure(4, figsize=(10, 16))

    ax1 = subplot(gs3[0, 0])

    ax1.plot(dateAlmar_Inner, XInner, "om", label="barre externe mesurées")
    ax1.plot(dateAlmar_Outer, XOuter, "ok", label="barre interne mesurées")
    ax1.plot(dateCROCO, Xo, "or", ms=12, label="bathy")
    ax1.plot(dateCROCO, Xi, "ob", ms=12, label="bathy")
    ax1.set_ylabel("cross-shore loc. (m)")
    ax1.set_xlim(xlimit)
    ax1.set_xticks(xxtick)
    ax1.set_xticklabels([])
    # legend()

    ax2 = subplot(gs3[1, 0])

    ax2.plot(dateAlmar_Inner, XMInner - XmInner, "om")
    ax2.plot(dateAlmar_Outer, XMOuter - XmOuter, "ok")
    ax2.plot(dateCROCO, Do, "or", ms=12, label="CROCO")
    ax2.plot(dateCROCO, Di, "ob", ms=12, label="CROCO")
    ax2.set_ylabel("Bar thickness (m)")
    ax2.set_xlim(xlimit)
    ax2.set_xticks(xxtick)
    ax2.set_xticklabels([])

    ax3 = subplot(gs3[2, 0])
    ax3.plot(dateF, Hs, "-r")
    ax3.set_xlim(xlimit)
    ax3.set_ylabel("Hs (m)")
    ax3.set_xticks(xxtick)
    ax3.set_xticklabels(xxticklabels)
    ax3.set_xlabel("date")
    ax3.set_xticklabels(ax3.get_xticklabels(), rotation=45, ha="right")

    savefig(
        figPath + "BarsPositions" + codelist[i] + ".png",
        dpi=300,
        facecolor="w",
        edgecolor="w",
        format="png",
    )


RMSdict = {"BSS": BSSCROCO[:], "params": params}

pickle.dump(RMSdict, open(basepathCROCO + pickle_file, "wb"))
