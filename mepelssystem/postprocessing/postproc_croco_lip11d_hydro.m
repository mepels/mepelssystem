%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%  Make plot from the results of the SANDBAR test case
%
%  Further Information:
%  http://www.crocoagrif.org/
%
%  This file is part of CROCOTOOLS
%

%  CROCOTOOLS is free software; you can redistribute it and/or modify
%  it under the terms of the GNU General Public License as published
%  by the Free Software Foundation; either version 2 of the License,
%  or (at your option) any later version.
%
%  CROCOTOOLS is distributed in the hope that it will be useful, but
%  WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%  GNU General Public License for more details.
%
%  You should have received a copy of the GNU General Public License
%  along with this program; if not, write to the Free Software
%  Foundation, Inc., 59 Temple Place, Suite 330, Boston,
%  MA  02111-1307  USA
%
%  Ref: Penven, P., L. Debreu, P. Marchesiello and J.C. McWilliams,
%       Application of the ROMS embedding procedure for the Central
%      California Upwelling System,  Ocean Modelling, 2006.
%
%  Patrick Marchesiello, IRD 2017,2020
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clear all
close all
clc
%================== User defined parameters ===========================
mycase    = '1b';     % LIP experiment : 1b: offshore migration 
                      %                  1c: onshore migration
if mycase == '1b'
    caseList={'lip1b'};
    labelList={'lip1b'};
else
    caseList={'lip1c'};
    labelList={'lip1c'};    
end
coefList  = [ 0 ];
parameterName = ' - '; 

% % %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% --- model params ---
%
basepath = '../beaches/lip/hydro/';


figSavePath         = './Figures/';
TableSavePath       = './Figures/RMSTables/';

figName_bed         = [figSavePath,'crossShoreBed_LIP',mycase,'.jpg'];
figName_Umap        = [figSavePath,'Umap_LIP',mycase,'.jpg'];
figName_Uprofile    = [figSavePath,'UprofilesLIP',mycase,'.jpg'];
figName_Cprofile    = [figSavePath,'CprofilesLIP',mycase,'.jpg'];
figName_CprofileLin = [figSavePath,'CprofilesLinLIP',mycase,'.jpg'];
figName_Akvprofile  = [figSavePath,'AkvProfilesLIP',mycase,'.jpg'];
figName_vsVanderWerf= [figSavePath,'vsDelft3D',mycase,'.jpg'];
figName_undertow    = [figSavePath,'undertow',mycase,'.jpg'];

tableName          = [TableSavePath,'RMSTable.txt'];
figNameRMS         = [figSavePath,'RMS_hydro',mycase,'.jpg'];

fname     = 'sandbar_his.nc'; % croco file name

contourPlot   = 0;
crosshorePlot = 1;
profilePlot   = 0;
vanderWerfPlot= 0;

myline={'-','--','-.',':'};
mycolor={'r','g','b','m'};

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% read cross-shore profile data from LIP database
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if mycase == '1b',
    load('/fsnet/project/meige/2019/19MEPELS/OPENDATA_MEPELS/lip/explip1b.mat');
    %http://servdap.legi.grenoble-inp.fr/opendap/meige/19MEPELS/lip/explip1b.mat');
    s = lip1b;
else
    load('/fsnet/project/meige/2019/19MEPELS/OPENDATA_MEPELS/lip/explip1c.mat');
    %http://servdap.legi.grenoble-inp.fr/opendap/meige/19MEPELS/lip/explip1c.mat');
    s = lip1c;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% read CROCO results
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
depth = 4.1;
if mycase == '1b',
    morph_fac = 18;       % morphological factor (from sediment.in)
else
    morph_fac = 13;
end

morph_cpl   = 1;        % feedback to currents
%
% ---------------------------------------------------------------------
% --- get grid from numerical model ---
% ---------------------------------------------------------------------
%

%loop over multiple cases

Ncase=length(caseList);

for k=1:Ncase
    
    yindex = 2; % Mm=1 with NS no-slip conditions
   
    nc=netcdf(strcat(basepath,caseList{k},'/',fname),'r');
    tindex  =length(nc{'scrum_time'}(:)); % reads last record
    tindex0 =min(tindex,5);               %  2 h
    %
    % horizontal grid
    hr=squeeze(nc{'h'}(yindex,:));
    xr=squeeze(nc{'x_rho'}(yindex,:));
    hu=0.5*(hr(1:end-1)+hr(2:end));
    xu=0.5*(xr(1:end-1)+xr(2:end));
    L=length(hr);
    %
    hini=squeeze(nc{'h'}(0,yindex,:));
    % new bathy from last record
    if morph_cpl
        hnew=squeeze(nc{'h'}(yindex,:));
        h=hnew;
    else
        h=hr;
    end
    %
    % vertical grid
    N=length(nc{'s_rho'});
    theta_s=nc.theta_s(:);
    theta_b=nc.theta_b(:);
    hc=nc.hc(:);
    zeta=squeeze(nc{'zeta'}(tindex,yindex,:));
    Dcrit=1.1*nc{'Dcrit'}(:);
    zeta(h<Dcrit)=zeta(h<Dcrit)-h(h<Dcrit); %add land topo
    zr=squeeze(zlevs(h,zeta,theta_s,theta_b,hc,N,'r',2));
    zw=squeeze(zlevs(h,zeta,theta_s,theta_b,hc,N,'w',2));
    zru=0.5*(zr(:,1:end-1)+zr(:,2:end));
    zwu=0.5*(zw(:,1:end-1)+zw(:,2:end));
    dz1=zr(1,:)-zw(1,:);
    %
    xr2d=repmat(xr,[N 1]);
    xu2d=repmat(xu,[N 1]);
    xw2d=repmat(xr,[N+1 1]);
    D   =zw(N+1,:)-zw(1,:);
    D2d =repmat(D,[N 1]);
    Du  =zwu(N+1,:)-zwu(1,:);
    Du2d=repmat(Du,[N 1]);
    
    % ---------------------------------------------------------------------
    % --- read/compute numerical model fields (index 1) ---
    % --------------------------------------------------------------------
    time=morph_fac/86400*(nc{'scrum_time'}(tindex)- ...
        nc{'scrum_time'}(1));
    
    % ... zonal velocity ...                         ---> xu,zu
%     u=squeeze(nc{'u'}(tindex,:,yindex,:));
    u=squeeze(nc{'u'}(tindex/2,:,yindex,:));
    
    % ... vertical velocity ...                      ---> xr,zw
%     w=squeeze(nc{'w'}(tindex,:,yindex,:));
    w=squeeze(nc{'w'}(tindex/2,:,yindex,:));
    
    % ... sediment concentration ...                 ---> xr,zr
%     C_CROCO=squeeze(nc{'sand_1'}(tindex0,:,yindex,:));
    % Bottom concentration from Patrick matlab script
    %CbotCROCO=C_CROCO(1,:).*(dz1./0.01).^1.2; % fitting to Rouse profile
    %CbotCROCO=max(0.,CbotCROCO);
    % use first value at the bottom
    %CbotCROCO=C_CROCO(1,:);
    
    % ... total viscosity
    Akv=squeeze(nc{'AKv'}(tindex,:,yindex,:));
    
    % ... viscosity due to wave breaking ...
    Akb=squeeze(nc{'Akb'}(tindex,:,yindex,:));
    
    % ... wave setup ...
    sup=squeeze(nc{'zeta'}(tindex0,yindex,:)); % init time
    sup=sup-sup(min(find(xr<=65)));
    sup(hr<0)=sup(hr<0)+hr(hr<0)-Dcrit;
    
    % ... u undertow ...
    ubotCROCO_i=zeros(size(u,2),1);
    for index=1:size(u,2)
        ubotCROCO_i(index)=interp1(zru(:,index)-zru(1,index)+dz1(index),u(:,index),0.1,'linear','extrap');
    end
    ubotCROCO_y2 =squeeze(nc{'u'}(tindex0,2,yindex,:)); % init time
    ubotCROCO_y3 =squeeze(nc{'u'}(tindex0,3,yindex,:)); % init time
    ubotCROCO_y4 =squeeze(nc{'u'}(tindex0,4,yindex,:)); % init time

    ubotCROCO_mean=zeros(size(u,2));
    for i=1:size(u,2)
        ubotCROCO_mean(i)=trapz(zru(:,i),u(:,i))/Du(i);
    end
    % ... hrms ...
    hrmsCROCO =squeeze(nc{'hrm'}(tindex0,yindex,:)); % init time
    
    close(nc)
    
    zeta(D<Dcrit)=NaN;
    u(Du2d<Dcrit)=NaN;
    sup(D<=max(0.1,Dcrit))=NaN;
    sup(hr<0)=NaN;
    
    MyaxisFontsize = 7;
    %
    %
% Plot contour map of velocity
    %
    %
    if (contourPlot==1)
        fig1=figure(1);
        xmin=40; xmax=190;
        zmin=1; zmax=4.5;
        cmin=-0.7; cmax=0.7; nbcol=14;
        cint=(cmax-cmin)/nbcol;
        map=colormap(jet(nbcol));
        map(nbcol/2  ,:)=[1 1 1];
        map(nbcol/2+1,:)=[1 1 1];
        colormap(map);
        contourf(xu2d,zru+depth,u,[cmin:cint:cmax],'linestyle','none');
        hold on
        colorbar('h');
        leg(1)=plot(s.xb,s.bathy_initial, 'k',  'LineWidth',1);
        leg(2)=plot(s.xb,s.bathy_final,'k--','LineWidth',2);
        leg(3)=plot(xr,depth-h,'color','b','LineWidth',2);
        plot(xr,zeta+depth,'color','g','LineWidth',2);
        legend(leg(1:3),'Measured Initial','Measured Final','CROCO','location','southeast');
        ylabel('Depth [m]','Fontsize',15)
        xlabel('x [m]','Fontsize',15)
        grid on
        axis([xmin xmax zmin zmax])
        caxis([cmin cmax])
        thour=floor(time*24);
        % if mycase=='1B',
        %  title(['SANDBAR EROSION   LIP-1B - U at Time ',num2str(thour),' hour'])
        % else
        %  title(['SANDBAR ACCRETION LIP-1C - U at Time ',num2str(thour),' hour'])
        % end
        set(gca,'Fontsize',8)
        hold off
        saveas(gcf, figName_Umap, 'jpg')
    end
    %
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %  Comparison with experiments
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% RMS Wave height
%
Nprof=size(s.xbr,1);
indexH = zeros(Nprof,1);
for i=1:Nprof
        indexH(i)=find(xr>=s.xbr(i),1,'first');
end
HrmsI=interp1(xr(indexH),hrmsCROCO(indexH),s.xbr,'nearest');
RMS_H=nanstd(s.Hs_report/sqrt(2)-HrmsI)/max(nanstd(s.Hs_report/sqrt(2)),nanmean(abs(s.Hs_report/sqrt(2))))

%
% RMS bed evolution
%
Nprof=size(s.xbr,1);
indexH = zeros(Nprof,1);
for i=1:Nprof
        indexH(i)=find(xr>=s.xbr(i),1,'first');
end
HrmsI=interp1(xr(indexH),hrmsCROCO(indexH),s.xbr,'nearest');
RMS_H=nanstd(s.Hs_report/sqrt(2)-HrmsI)/max(nanstd(s.Hs_report/sqrt(2)),nanmean(abs(s.Hs_report/sqrt(2))))

%
%
% Plot vertical profiles
%
%
% Initialize RMS
%
RMS_U = 0;
RMS_C = 0;

Nuprof = size(s.uprof,2);

for i = 1:Nuprof
    index=find(xr>=s.xrtf(i),1,'first');
    uI=interp1(zru(:,index)-zru(1,index)+dz1(index),u(:,index),s.uprof(i).z,'nearest');
    RMS_U = RMS_U + nanstd(s.uprof(i).u-uI)/max(nanstd(s.uprof(i).u),nanmean(abs(s.uprof(i).u)));
end
RMS_U = RMS_U / double(Nuprof)


% write table of RMS
% 
% meanRMS_UHrms_Vec=[RMS_H;RMS_U];
% meanRMS_UHrms=mean(meanRMS_UHrms_Vec);
% meanRMS_Vec=[RMS_H;RMS_U];
% meanRMS=mean(meanRMS_Vec);
% 
% Scenario=caseList(k);
% parameter=labelList(k);
% coefValue=coefList(k);
% table_RMS(k,:) = table(Scenario,parameter,coefValue,RMS_H,RMS_U,meanRMS_UHrms,meanRMS)
% writetable(table_RMS,tableName)
% 
% table_RMS_eachCase(1,:) = table(Scenario,parameter,coefValue,RMS_H,RMS_U,meanRMS_UHrms,meanRMS);
% RMSTablepath = [basepath,caseList{k}];
% mkdir(RMSTablepath,'RMSTable');
% TableSavePathEachCase      = [RMSTablepath,'/RMSTable'];
% tableNameEachCase          = [TableSavePathEachCase,'/RMS.txt'];
% writetable(table_RMS_eachCase,tableNameEachCase)
% 
% if k==Ncase
%     figure('Name','RMS');
%     plot(table_RMS.coefValue,table_RMS.RMS_H,'sk','linewidth',2)
%     text(table_RMS.coefValue,table_RMS.RMS_H,num2str(table_RMS.RMS_H,3))
%     hold on
%     plot(table_RMS.coefValue,table_RMS.RMS_U,'*b','linewidth',2)
%     text(table_RMS.coefValue,table_RMS.RMS_U,num2str(table_RMS.RMS_U,3))
%     hold on
%     plot(table_RMS.coefValue,table_RMS.meanRMS_UHrms,'or','linewidth',2)
%     text(table_RMS.coefValue,table_RMS.meanRMS_UHrms,num2str(table_RMS.meanRMS_UHrms,3))
%     xlabel(parameterName,'Fontsize',6)
% %     xlabel(parameterName)
%     ylabel('RMS')
%     legend('RMS_H','RMS_U','RMS_{mean}','orientation', 'horizontal','position',[0.4 0.93 0.01 0.01])
%     saveas(gcf, figNameRMS, 'jpg')
%     
% %     figure('Name','RMS_Average');
% %     plot(table_RMS.coefValue,table_RMS.meanRMS_UHrms,'-s')
% %     hold on
% %     plot(table_RMS.coefValue,table_RMS.meanRMS,'-*')
% %     xlabel(parameterName)
% %     ylabel('RMS_{average}')
% %     legend('mean\_U-Hrms','mean\_C-U-Hrms')
% %     saveas(gcf, figNameRMS_Average, 'jpg')
    
%end

    %
    %
    % 
% Plot crossshore profiles
    %
    %
    if (crosshorePlot==1)
        xmin = 50;
        xmax = 180;
        if(mycase=="1b")
            hmin = -0.1;
            hmax = max(s.hrms)*1.3;
            etamin = -0.05;
            etamax =  0.6;
            Umin = -0.36;
            Umax = 0.1; 
        elseif (mycase=="1c")
            hmin = 0;
            hmax=0.55;
            etamin = -0.05;
            etamax =  0.3;
            Umin = -0.36;
            Umax = 0.1; %max(urms)*1.1;
        end    
        fig2=figure(2);
        
        pos1 = [.08 .68 .8 .28];
        pos2 = [.08 .38 .8 .28];
        pos3 = [.08 .08 .8 .28];
%         pos4 = [.08 .25 .8 .165];
%         pos5 = [.08 .075 .8 .165];
        %
        % subplot 1
        %
        subplot('Position',pos1)
        
        len=xmax-xmin;
        if(mycase=="1b")
            umin_leg = -0.4;
        elseif (mycase=="1c")
            umin_leg = -0.3;
        end
        umax_leg = -umin_leg;
        uscale=0.05*len/umax_leg;
%         uscale=0.05*len;
        for i = 1:Nuprof
            index=find(xr>=s.xrtf(i),1,'first');
            if k==1
                uvar=xr(index)+s.uprof(i).u*uscale;
                zvar=depth-h(index)+s.uprof(i).z;
                leg(1)=plot(uvar,zvar,'LineStyle','none','Marker','o','Color','k',...
                    'linewidth',2,'MarkerSize',4);
                grid on
            end
            clear uvar zvar
%             plot(xr,-h,  'k',  'LineWidth',2);
            uvar=xr(index)+u(:,index)*uscale;
            zvar=depth+zru(:,index);
            line(uvar,zvar,'LineWidth',2,'LineStyle',myline{k},'Color',mycolor{k});
            line(xr(index)*ones(size(zvar)),zvar, ...
                'LineWidth',1,'LineStyle',myline{k},'Color',[0.6 0.6 0.6]);
%             if k==1
%                 line(xr,depth-h,'LineWidth',2,'LineStyle','-','Color','k');
%                 line(xr(index)*ones(size(zvar)),zvar, ...
%                     'LineWidth',1,'LineStyle','--','Color',[0.6 0.6 0.6]);
%                 %             hold on
%             end
%             if k==Ncase
%                 legend('measurement',labelList{1:k},'location',[.68 .7 .2 .1],'Fontsize',MyaxisFontsize-2)
%             end
%             f=gradient(gradient(uvar));
%             iflx=find(f(1:end-1)>0 & f(2:end)<0,1); % inflection
%             text(uvar(iflx),zvar(iflx),'*','Color','r','fontsize',20);

            clear uvar zvar

                            
%             text(162,zmin+0.9,'o Data','color','k','Fontsize',20)
%             text(162,zmin+0.9-0.25*k,[myline{k},' ',labelList{k}], ...
%                 'color',mycolor{k},'Fontsize',20)

            if k==1
                line([120,120+0.4*uscale],[1.8,1.8],'LineWidth',2,'LineStyle','-','Color','k')
                text(120+0.2*uscale,1.65, '0.4 m/s', 'HorizontalAlignment','center','Fontsize',8)
                hold on
                plot(s.xb,s.bathy_intermediate,'-k','LineWidth',2);
                hold on
                plot([xmin,xmax],[depth,depth],':k','LineWidth',2);
                hold on
            end
%             xlabel('X (m)');
            ylabel('depth (m)','Fontsize',8);
            axis([xmin,xmax,1.2,4.3])
            set(gca,'XTickLabel',[],'Fontsize',8);
            title(mycase)
%             ylabel('Z (m)');
%             axis([xmin,xmax,zmin,zmax])
            set(gca,'Fontsize',MyaxisFontsize);

        end
        hold off
        
%         
%         hold on
%         if k==Ncase
%             leg(2)=plot(s.xb,s.bathy_intermediate,'-k','LineWidth',2);
% %             leg(1)=plot(s.xb,s.bathy_final,'-k','LineWidth',2);
% %             hold on;
% %             leg(2)=plot(s.xb,s.bathy_initial,'--k','LineWidth',2);
% %             hold on
%         end
%         %     plot([xmin,xmax],[depth,depth],':k','LineWidth',2);
%         grid()
%         ylabel('depth (m)','Fontsize',8);
%         axis([xmin,xmax,1.2,4.3])
%         set(gca,'XTickLabel',[],'Fontsize',8);
%         title(mycase)
%         if k==Ncase
%             plot([xmin,xmax],[depth,depth],':k','LineWidth',2);
% %             legend(labelList{1:k},'Measured bed (intermediate)','location',[.14 .85 .2 .1])
%             legend(labelList{1:k},'location',[.14 .85 .2 .1])
%           hold on
%             set(gca,'Fontsize',MyaxisFontsize);
%             grid()
%         end
%         grid()
% 
%         %     hold off;
%         clear leg;
%         %
%         % subplot 2
%         %
%         subplot('Position',pos2)
%         hold on
%         if k==1
%             plot(s.xb,s.bathy_final-s.bathy_initial,'--k','LineWidth',2)
%             grid()
%         end
%         hold on;
%         plot(xr,h-hini,'LineWidth',2,'LineStyle',myline{k},'Color',mycolor{k});
%         plot([xmin, xmax],[0,0],':k')
%         grid()
%         ylabel('Final-Initial (m)','Fontsize',8)
%         ylim([-0.2 0.2])
%         set(gca,'xticklabels',[],'Fontsize',8)
%         set(gca,'Xlim',[xmin xmax])
%         set(gca,'Fontsize',MyaxisFontsize);
%         grid()
%         hold off;
%         clear leg;
        
        %
        % subplot 2
        %
        subplot('Position',pos2)
%         left axis
        yyaxis left;
        hold on
%         plot(xr,hrmsCROCO,'b','LineWidth',2,'LineStyle',myline{k},'Color',mycolor{k});
        plot(xr,hrmsCROCO,'LineWidth',2,'LineStyle',myline{k},'Color',mycolor{k});
        hold on;
        if k==Ncase
%             leg(1)=plot(s.xp,s.hrms,'ok','LineWidth',2);
            leg(1)=plot(s.xbr,s.Hs_report/sqrt(2),'ok','LineWidth',2);
        end
        ylabel('Hrms (m)','Fontsize',8)
        set(gca,'xticklabels',[],'Fontsize',8)
        set(gca,'Xlim',[xmin xmax])
        ylim([hmin hmax])
        hold off
%         right axis
        yyaxis right;
        hold on;
        plot(xr,sup,'--r','LineWidth',2,'LineStyle',myline{k},'Color',mycolor{k});
        hold on;
        grid();
        if k==Ncase
%             plot([xmin, xmax],[0,0],':r')
%             leg(2)=plot(s.xeta,s.eta-s.depth,'*k','LineWidth',2);
            leg(2)=plot(s.xbr,s.eta_report-s.eta_report(1),'*k','LineWidth',2);
            if mycase == '1b',
                legend('location',[.12 .45 .2 .06]);
            else
                legend('location',[.12 .5 .2 .06]);
            end
            legend(leg(1:2),'Measured Hrms','Measured wave set-up');
            grid()
        end
        ylabel('eta (m)','Fontsize',8)
        ylim([etamin etamax])
        set(gca,'xticklabels',[],'Fontsize',8)
        set(gca,'Xlim',[xmin xmax])
        hold off;
        set(gca,'Fontsize',MyaxisFontsize);
        clear leg;
        grid()

        %
        % subplot 3
        %
        subplot('Position',pos3)
        hold on
        if k==1
            %             leg(1)=plot(s.xrtf,s.Ubar,'*k','LineWidth',2);
            %             hold on;
            plot(s.xrtf,s.Ubot,'ok','LineWidth',2);
            %             legend('location',[.1 .12 .2 .06])
            %             legend(leg(1:2),{'Measured mean','Measured bottom'})
            %             legend(leg(2),'Measured bottom')
            set(gca,'Fontsize',MyaxisFontsize);
            grid()
        end
        plot(xu,ubotCROCO_i,'--b','LineWidth',2,'LineStyle',myline{k},'Color',mycolor{k});
        hold on;

        if k==Ncase
            legend('measurement',labelList{1:k},'location',[.66 .75 .2 .0],'Fontsize',MyaxisFontsize)
        end
        ylabel('U (m/s)','Fontsize',8);
        axis([xmin,xmax,Umin,Umax])
        xlabel('cross-shore (m)','Fontsize',8)
%         set(gca,'xticklabels',[],'Fontsize',8)
        set(gca,'Fontsize',MyaxisFontsize);
        hold off;
%         %
%         % Subplot 5
%         %
%         subplot('Position',pos5)
%         hold on;
%         plot(xr,CbotCROCO,'--b','LineWidth',2,'LineStyle',myline{k},'Color',mycolor{k});
%         hold on;
%         grid()
%         if k==Ncase
%             leg(1)=plot(s.xconc,s.Cbar,'*k','LineWidth',2);
%             leg(2)=plot(s.xconc,s.Cbot,'ok','LineWidth',2);
%             legend('location','northwest')
%             legend(leg(1:2),{'Measured mean','Measured bottom'})
%             set(gca,'Fontsize',MyaxisFontsize);
%             grid()
%         end
%         ylabel('C (g/l)','Fontsize',8);
%         axis([xmin,xmax,0,Cmax])
%         xlabel('cross-shore (m)','Fontsize',8)
%         set(gca,'Fontsize',MyaxisFontsize);
%         hold off
%         grid()
        
        saveas(gcf, figName_bed, 'jpg')
    end
    %
    %
% Plot vertical profiles
    %
    if (profilePlot == 1)
        %
        % plot u profiles
        %
        fig3 = figure(3);%,'Position', [100 100 800 600]);
        zmin = 0;
        zmax = 1.2;
        if(mycase=="1b")
            umin = -0.4;
        elseif (mycase=="1c")
            umin = -0.3;
        end
        umax = -umin;
        
        Nuprof = size(s.uprof,2);
        
        for i = 1:Nuprof
            subplot(3,3,i)
            if k==1
                plot(s.uprof(i).u,s.uprof(i).z,'ok','LineWidth',2);
                grid()
            end
            hold on
            index=find(xr>=s.xrtf(i),1,'first');
            plot(u(:,index),zru(:,index)-zru(1,index)+dz1(index),'LineWidth',2,'LineStyle',myline{k},'Color',mycolor{k});
            if i==Nuprof
                legend('Measured',labelList{1:k},'location',[.765,.22,.07,.1])
            end
            title(['x=',num2str(s.xrtf(i)),' m'])
            if i==7 || i==8 || i==9
                xlabel('u (m/s)');
%             else
%                 set(gca,'xtick',[]);
            end
            if i==1 || i==4 || i==7
                ylabel('z (m)');
            end
            axis([umin,umax,zmin, zmax])
            set(gca,'Fontsize',MyaxisFontsize);
            hold off
        end
        saveas(gcf,figName_Uprofile, 'jpg')
%         %
%         % plot conc profiles (logscale)
%         %
%         fig4 = figure(4);%,'Position', [100 100 800 600]);
%         zmin = 0;
%         zmax = 1.8;
%         concmin = 0;
%         concmax = 1e1;
%         %
%         Ncprof = size(s.cprof,2);
%         
%         for i = 1:Ncprof
%             subplot(3,3,i)
%             if k==1
%                 semilogx(s.cprof(i).c,s.cprof(i).z,'ok','LineWidth',2)
%                 grid()
%             end
%             hold on;
%             index=find(xr>=s.xconc(i),1,'first');
%             semilogx(C_CROCO(:,index),zr(:,index)-zr(1,index)+dz1(index),'LineWidth',2,'LineStyle',myline{k},'Color',mycolor{k});
%             if i==Ncprof
%                 legend('Measured',labelList{1:k},'location',[.765,.22,.07,.1])
%             end
%             title(['x=',num2str(s.xconc(i)),' m'])
%             if i==7 || i==8 || i==9
%                 xlabel('c (g/L)');
%             end
%             if i==1 || i==4 || i==7
%                 ylabel('z (m)');
%             end
%             %         axis([concmin,concmax,zmin, zmax])
%             axis([concmin,concmax,zmin, zmax])
%             set(gca,'Fontsize',MyaxisFontsize);
%             hold off
%         end
%         saveas(gcf,figName_Cprofile, 'jpg')
%         %
%         % plot conc profiles
%         %
%         fig5 = figure(5);%,'Position', [100 100 800 600]);
%         zmin = 0;
%         zmax = 1.8;
%         concmin = 0;
%         concmax = 1e1;
%         concmaxLin = 3.25;
% 
%         %
%         Ncprof = size(s.cprof,2);
%         
%         for i = 1:Ncprof
%             subplot(3,3,i)
%             if k==1
%                 plot(s.cprof(i).c,s.cprof(i).z,'ok','LineWidth',2)
%                 grid()
%             end
%             hold on;
%             index=find(xr>=s.xconc(i),1,'first');
%             plot(C_CROCO(:,index),zr(:,index)-zr(1,index)+dz1(index),'LineWidth',2,'LineStyle',myline{k},'Color',mycolor{k});
%             if i==Ncprof
%                 legend('Measured',labelList{1:k},'location',[.765,.22,.07,.1])
%             end
%             title(['x=',num2str(s.xconc(i)),' m'])
%             if i==7 || i==8 || i==9
%                 xlabel('c (g/L)');
%             end
%             if i==1 || i==4 || i==7
%                 ylabel('z (m)');
%             end
%             axis([concmin,concmaxLin,zmin, zmax])
%             set(gca,'Fontsize',MyaxisFontsize);
%             hold off
%         end
%         saveas(gcf,figName_CprofileLin, 'jpg')
%         
                
        % plot mixing (Akv) profiles 
        %
        fig6 = figure(6);%,'Position', [100 100 800 600]);
        zmin = 0;
        zmax = 2;
        Akvmin = 0;
        Akvmax = 0.1;
%         Akvmax = 0.015;
%         if mycase==1b
%             Akvmax = 0.1;
%         else
%             Akvmax = 0.006;
%         end
        %
        Nuprof = size(s.uprof,2);

        for i = 1:Nuprof
            subplot(3,3,i)
            index=find(xr>=s.xrtf(i),1,'first');
            hold on
            plot(Akv(:,index),zw(:,index)-zw(1,index),'LineWidth',2,'LineStyle',myline{k},'Color',mycolor{k});
            if i==Nuprof
                legend(labelList{1:k},'location',[.765,.22,.07,.1])
            end
            title(['x=',num2str(s.xrtf(i)),' m'])
            if i==7 || i==8 || i==9
                xlabel('Akv');
            end
            if i==1 || i==4 || i==7
                ylabel(Akv);
            end
            if k==Ncase
                grid()
            end
            axis([Akvmin,Akvmax,zmin, zmax])
            set(gca,'Fontsize',MyaxisFontsize);
            hold off
        end
        saveas(gcf,figName_Akvprofile, 'jpg')
        
    end
        %
        
        if (vanderWerfPlot == 1)
        %
        % plot u and c profiles comparison with ver der Werf
        %
        fig7 = figure(7);
        zmin = 0;
        zmax = 1;
        if(mycase=="1b")
            umin = -0.4;
        elseif (mycase=="1c")
            umin = -0.3;
        end
%         umax = -umin;
        umax = 0.1;
%         title('LIP1B')


        Nuprof = size(s.uprof,2);
        
        subplot(1,3,1)
        if k==1
            plot(s.vdw(1).u,s.vdw(1).zu,'-k','LineWidth',2);
            hold on
            indexExp=find(s.xrtf>=s.vdw(1).x,1,'first');
            plot(s.uprof(indexExp).u,s.uprof(indexExp).z,'*k','LineWidth',2);
            hold on
            grid()
        end
        hold on
        index=find(xr>=s.vdw(1).x,1,'first');
        plot(u(:,index),zru(:,index)-zru(1,index)+dz1(index),'LineWidth',2,'LineStyle',myline{k},'Color',mycolor{k});
%         if i==Nuprof
%             legend('Measured',labelList{1:k},'location',[.765,.22,.07,.1])
%         end
        title(['x=',num2str(s.vdw(1).x),' m'])
        xlabel('u (m/s)');
        ylabel('z (m)');
        axis([umin,umax,zmin, zmax])
        set(gca,'Fontsize',MyaxisFontsize);
        hold off
        
        saveas(gcf,figName_vsVanderWerf, 'jpg')
        %
 
        
        subplot(1,3,2)
        if k==1
            plot(s.vdw(2).u,s.vdw(2).zu,'-k','LineWidth',2);
            hold on
            indexExp=find(s.xrtf>=s.vdw(2).x,1,'first');
            plot(s.uprof(indexExp).u,s.uprof(indexExp).z,'*k','LineWidth',2);            hold on
            grid()
        end
        hold on
        index=find(xr>=s.vdw(2).x,1,'first');
        plot(u(:,index),zru(:,index)-zru(1,index)+dz1(index),'LineWidth',2,'LineStyle',myline{k},'Color',mycolor{k});
%         if i==Nuprof
%             legend('Measured',labelList{1:k},'location',[.765,.22,.07,.1])
%         end
        title(['x=',num2str(s.vdw(2).x),' m'])
        xlabel('u (m/s)');
%         ylabel('z (m)');
        axis([umin,umax,zmin, zmax])
        set(gca,'Fontsize',MyaxisFontsize);
        hold off
        
        saveas(gcf,figName_vsVanderWerf, 'jpg')
        
        
        
        subplot(1,3,3)
        if k==1
            plot(s.vdw(3).u,s.vdw(3).zu,'-k','LineWidth',2);
            hold on
            indexExp=find(s.xrtf>=s.vdw(3).x,1,'first');
            plot(s.uprof(indexExp).u,s.uprof(indexExp).z,'*k','LineWidth',2);
            hold on
            grid()
        end
        hold on
        index=find(xr>=s.vdw(3).x,1,'first');
        plot(u(:,index),zru(:,index)-zru(1,index)+dz1(index),'LineWidth',2,'LineStyle',myline{k},'Color',mycolor{k});
%         if i==Nuprof
%             legend('Measured',labelList{1:k},'location',[.765,.22,.07,.1])
%         end
%         legend('vanderWerf model','LIPExp-vanderWerf',labelList{1:k},'location',[.765,.9,.07,.05])
        title(['x=',num2str(s.vdw(3).x),' m'])
        legend('Delft3D-VanderWerf','LIPExp-vanderWerf',labelList{1:k},'location',[.49,.82,.07,.05])
        xlabel('u (m/s)');
%         ylabel('z (m)');
        axis([umin,umax,zmin, zmax])
        set(gca,'Fontsize',MyaxisFontsize);
        hold off
        
        saveas(gcf,figName_vsVanderWerf, 'jpg')
        
        
        fig8 = figure(8);
        subplot('Position',pos1)
        hold on
        plot(xu,ubotCROCO_y4,'--b','LineWidth',2,'LineStyle',myline{k},'Color',mycolor{k});
        hold on;
        if k==Ncase
            leg(1)=plot(s.xrtf,s.Ubar,'*k','LineWidth',2);
            hold on;
            leg(2)=plot(s.xrtf,s.Ubot,'ok','LineWidth',2);
%             legend('location',[.1 .12 .2 .06])
            legend(labelList{1:k},'location',[.14 .75 .2 .1])
            set(gca,'Fontsize',MyaxisFontsize);
            grid()
        end
        ylabel('U\_{bot}(m/s)(@Ny=4)','Fontsize',8);
        axis([xmin,xmax,Umin,Umax])
        xlabel('cross-shore (m)','Fontsize',8)
%         set(gca,'xticklabels',[],'Fontsize',8)
        set(gca,'Fontsize',MyaxisFontsize);
        hold off;
        
        subplot('Position',pos2)
        hold on
        plot(xu,ubotCROCO_y3,'--b','LineWidth',2,'LineStyle',myline{k},'Color',mycolor{k});
        hold on;
        if k==Ncase
            leg(1)=plot(s.xrtf,s.Ubar,'*k','LineWidth',2);
            hold on;
            leg(2)=plot(s.xrtf,s.Ubot,'ok','LineWidth',2);
            set(gca,'Fontsize',MyaxisFontsize);
            grid()
        end
        ylabel('U\_{bot}(m/s)(@Ny=3)','Fontsize',8);
        axis([xmin,xmax,Umin,Umax])
        xlabel('cross-shore (m)','Fontsize',8)
%         set(gca,'xticklabels',[],'Fontsize',8)
        set(gca,'Fontsize',MyaxisFontsize);
        hold off;
        
        subplot('Position',pos3)
        hold on
        plot(xu,ubotCROCO_y2,'--b','LineWidth',2,'LineStyle',myline{k},'Color',mycolor{k});
        hold on;
        if k==Ncase
            leg(1)=plot(s.xrtf,s.Ubar,'*k','LineWidth',2);
            hold on;
            leg(2)=plot(s.xrtf,s.Ubot,'ok','LineWidth',2);
            legend(leg(1:2),{'Measured mean','Measured bottom'},'location',[.14 .17 .2 .1])
            set(gca,'Fontsize',MyaxisFontsize);
            grid()
        end
        ylabel('U\_{bot}(m/s)(@Ny=2)','Fontsize',8);
        axis([xmin,xmax,Umin,Umax])
        xlabel('cross-shore (m)','Fontsize',8)
%         set(gca,'xticklabels',[],'Fontsize',8)
        set(gca,'Fontsize',MyaxisFontsize);
        hold off;
        saveas(gcf, figName_undertow, 'jpg')

%         zmin = 0;
%         zmax = 1;
%         Cmin=-1;
%         if(mycase=="1b")
%             Cmax = 3.5;
%         elseif (mycase=="1c")
%             Cmax = 1;
%         end
%         
%         subplot(2,3,4)
%         if k==1
%             plot(s.vdw(1).c,s.vdw(1).zc,'-k','LineWidth',2);
%             hold on
%             indexExp=find(s.xrtf>=s.vdw(1).x,1,'first');
%             plot(s.cprof(indexExp).c,s.cprof(indexExp).z,'*k','LineWidth',2);            hold on
%             grid()
%         end
%         hold on
%         index=find(xr>=s.vdw(1).x,1,'first');
%         plot(C_CROCO(:,index),zr(:,index)-zr(1,index)+dz1(index),'LineWidth',2,'LineStyle',myline{k},'Color',mycolor{k});
% %         if i==Nuprof
% %             legend('Measured',labelList{1:k},'location',[.765,.22,.07,.1])
% %         end
%         title(['x=',num2str(s.vdw(1).x),' m'])
%         xlabel('C (g/l)');
%         ylabel('z (m)');
%         axis([Cmin,Cmax,zmin, zmax])
%         set(gca,'Fontsize',MyaxisFontsize);
%         hold off
%         
%         saveas(gcf,figName_vsVanderWerf, 'jpg')
%         
%         
%                 
%         subplot(2,3,5)
%         if k==1
%             plot(s.vdw(2).c,s.vdw(2).zc,'-k','LineWidth',2);
%             hold on
%             indexExp=find(s.xrtf>=s.vdw(2).x,1,'first');
%             plot(s.cprof(indexExp).c,s.cprof(indexExp).z,'*k','LineWidth',2);
%             hold on
%             grid()
%         end
%         hold on
%         index=find(xr>=s.vdw(2).x,1,'first');
%         plot(C_CROCO(:,index),zr(:,index)-zr(1,index)+dz1(index),'LineWidth',2,'LineStyle',myline{k},'Color',mycolor{k});
% %         if i==Nuprof
% %             legend('Measured',labelList{1:k},'location',[.765,.22,.07,.1])
% %         end
%         title(['x=',num2str(s.vdw(2).x),' m'])
%         xlabel('C (g/l)');
% %         ylabel('z (m)');
%         axis([Cmin,Cmax,zmin, zmax])
%         set(gca,'Fontsize',MyaxisFontsize);
%         hold off
%         
%         saveas(gcf,figName_vsVanderWerf, 'jpg')
%         
%         
%                 
%         subplot(2,3,6)
%         if k==1
%             plot(s.vdw(3).c,s.vdw(3).zc,'-k','LineWidth',2);
%             hold on
%             indexExp=find(s.xrtf>=s.vdw(3).x,1,'first');
%             plot(s.cprof(indexExp).c,s.cprof(indexExp).z,'*k','LineWidth',2);
%             hold on
%             grid()
%         end
%         hold on
%         index=find(xr>=s.vdw(3).x,1,'first');
%         plot(C_CROCO(:,index),zr(:,index)-zr(1,index)+dz1(index),'LineWidth',2,'LineStyle',myline{k},'Color',mycolor{k});
% %         if i==Nuprof
%         legend('Delft3D','LIPExp-vanderWerf',labelList{1:k},'location',[.765,.42,.07,.05])
% %         end
%         title(['x=',num2str(s.vdw(3).x),' m'])
%         xlabel('C (g/l)');
% %         ylabel('z (m)');
%         axis([Cmin,Cmax,zmin, zmax])
%         set(gca,'Fontsize',MyaxisFontsize);
%         hold off
%         
%         saveas(gcf,figName_vsVanderWerf, 'jpg')
    end
    %

end
return
