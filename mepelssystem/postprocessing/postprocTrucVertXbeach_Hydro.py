#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Mar 19 17:15:06 2020

@author: chauchat / ferraris
"""

from netCDF4 import Dataset
import numpy as np
import os
import yaml
import matplotlib
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
from datetime import datetime as dt

import sys

sys.path.append("./libs/")
from mydate import datenum


def analysehydro(beach_config):
    # %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    # %%                         User functiions                              %%
    # %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    def readXB(basepath, num_vec):
        #%
        #% Read the XB output file
        #%
        ncfile = Dataset(basepath)

        #% Netcdf output file
        timeXB = ncfile.variables["meantime"][:]  #% (s)
        x1 = ncfile.variables["globalx"][:, :]
        y1 = ncfile.variables["globaly"][:, :]
        hrm = ncfile.variables["H_mean"][:, :, :] * np.sqrt(2)
        u = ncfile.variables["u_mean"][:, :, :]
        v = ncfile.variables["v_mean"][:, :, :]

        #
        # Locate VEC 1 & 3 stations
        Xvec = cl["parameter"]["Xvec" + str(num_vec)]
        Yvec = cl["parameter"]["Yvec" + str(num_vec)]

        Ivec = np.max(np.where(x1[0, :] <= Xvec))
        Jvec = np.max(np.where(y1[:, 0] <= Yvec))

        Nt = np.size(timeXB, 0)
        UvecXB = np.zeros((Nt))
        VvecXB = np.zeros((Nt))
        HvecXB = np.zeros((Nt))

        for k in range(Nt):
            UvecXB[k] = u[k, Jvec, Ivec]
            VvecXB[k] = v[k, Jvec, Ivec]
            HvecXB[k] = hrm[k, Jvec, Ivec]

        return timeXB, HvecXB, UvecXB, VvecXB

    def extract(path_nc):
        ncnbs = Dataset(path_nc)
        time_vec = ncnbs.variables["time"][:]
        date_vec = ncnbs.variables["date"][:]
        Hs_vec = ncnbs.variables["Hs"][:]
        Ux_VEC_vec = ncnbs.variables["Ux"][:]
        Uy_VEC_vec = ncnbs.variables["Uy"][:]
        U_VEC_vec = ncnbs.variables["Umag"][:]

        return time_vec, date_vec, Hs_vec, Ux_VEC_vec, Uy_VEC_vec, U_VEC_vec

    def read(basepath, num_vec, pointvar):
        #    %
        #    %  Read the grid file and check the topography
        #    %
        filename = basepathXB

        if pointvar == 1:
            ncfile = Dataset(filename)

            timeXB = ncfile.variables["pointtime"][:]  #% (s)
            HsVEC = ncfile.variables["point_H"][:, :] * np.sqrt(2)
            UVEC = ncfile.variables["point_u"][:, :]
            VVEC = ncfile.variables["point_v"][:, :]

            HvecXB = HsVEC[:, num_vec - 1]
            UvecXB = UVEC[:, num_vec - 1]
            VvecXB = VVEC[:, num_vec - 1]

        else:
            [timeXB, HvecXB, UvecXB, VvecXB] = readXB(basepathXB, num_vec)

        dateXB = dateXB_0 + timeXB / (24 * 3600)

        return dateXB, HvecXB, UvecXB, VvecXB

    ###############################################################################

    with open(
        r"./configurations/" + beach_config + "_config_postproc.yaml"
    ) as file:
        cl = yaml.full_load(file)

    figPath = os.path.dirname(cl["data_path"]["basepathXB"]) + "/figures/"
    try:
        os.mkdir(figPath)
    except:
        pass
    figPath = figPath + "analyseHydro/"
    try:
        os.mkdir(figPath)
    except:
        pass
    # Change fontsize
    #
    matplotlib.rcParams.update({"font.size": 16})
    mpl.rcParams["lines.linewidth"] = 3
    mpl.rcParams["lines.markersize"] = 5
    mpl.rcParams["lines.markeredgewidth"] = 1
    #
    # Change subplot sizes
    #
    gs1 = gridspec.GridSpec(
        1,
        1,
        figure=None,
        left=0.15,
        bottom=0.15,
        right=0.8,
        top=0.8,
        wspace=0.3,
        hspace=0.25,
    )
    gs2 = gridspec.GridSpec(
        1,
        2,
        figure=None,
        left=0.15,
        bottom=0.15,
        right=0.8,
        top=0.8,
        wspace=0.3,
        hspace=0.25,
    )
    gs4 = gridspec.GridSpec(
        2,
        2,
        figure=None,
        left=0.15,
        bottom=0.15,
        right=0.8,
        top=0.8,
        wspace=0.3,
        hspace=0.25,
    )

    gs = gridspec.GridSpec(3, 1)
    gs.update(
        left=0.1, right=0.95, top=0.95, bottom=0.1, wspace=0.125, hspace=0.25
    )
    gs2 = gridspec.GridSpec(3, 2)
    gs2.update(
        left=0.1, right=0.95, top=0.95, bottom=0.1, wspace=0.15, hspace=0.25
    )
    #
    # Figure size
    #
    figwidth = 14
    figheight = 10

    #%
    #% Path setup
    #%
    basepathXB = cl["data_path"]["basepathXB"]

    pointvar = cl["parameter"]["pointvar"]

    codelist = "xboutput"
    labellist = cl["parameter"]["labellist"]

    figName = cl["parameter"]["figName"] + "xboutput.png"

    ###############################################################################
    date_0 = dt.strptime(cl["parameter"]["date_0"], "%Y-%m-%d %H:%M")
    date_m = dt.strptime(cl["parameter"]["date_m"], "%Y-%m-%d %H:%M")
    date_f = dt.strptime(cl["parameter"]["date_f"], "%Y-%m-%d %H:%M")

    xlimit = [datenum(date_0), datenum(date_f)]
    xxtick = [datenum(date_0), datenum(date_m), datenum(date_f)]
    xxticklabels = [
        date_0.strftime("%Y-%m-%d"),
        date_m.strftime("%Y-%m-%d"),
        date_f.strftime("%Y-%m-%d"),
    ]

    #% 1) Read wave and tide data from FORCAGE file
    ncFor = Dataset(cl["data_path"]["Forcage"])

    timeF = ncFor.variables["time"][:]
    dateF = ncFor.variables["date"][:]
    Hs = ncFor.variables["Hs"][:]
    Tp = ncFor.variables["Tp"][:]
    Dir = ncFor.variables["Dir"][:]
    Tide = ncFor.variables["Tide"][:]

    #%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    #%
    #%  READ XBEACH results
    #%
    #%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    dateXB_0 = datenum(date_0)

    # %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    # % Plot velocity time series from vec3 XY
    # %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    k = 1
    while k <= cl["parameter"]["nbs_vec"]:

        fig1 = plt.figure(
            num=k,
            figsize=(figwidth, figheight),
            dpi=60,
            facecolor="w",
            edgecolor="w",
        )
        nc_path_nbs = "nc" + str(k)
        time_vec, date_vec, Hs_vec, Ux_VEC_vec, Uy_VEC_vec, U_VEC_vec = extract(
            cl["data_path"][nc_path_nbs]
        )
        dateXB, HvecXB, UvecXB, VvecXB = read(basepathXB, k, pointvar)
        ax1 = plt.subplot(
            gs[0, 0]
        )  # look with gs2 to try to put two graph on the same page
        (l1,) = ax1.plot(dateF, Hs, ":k", lw=2, label="Off-shore")
        (l3,) = ax1.plot(date_vec, Hs_vec, "xk", lw=2, label="VEC " + str(k))

        (l5,) = ax1.plot(dateXB, HvecXB, lw=2, label=labellist)
        ax1.set_ylabel("Hs (m)")
        ax1.set_xticks(xxtick)
        ax1.set_xticklabels([])
        ax1.set_xlim(xlimit)
        ax1.set_ylim([0, 2])
        ax1.grid()

        ax2 = plt.subplot(gs[1, 0])
        (l1,) = ax2.plot(date_vec, -Ux_VEC_vec, "xk", lw=2, label="VEC " + str(k))

        (l2,) = ax2.plot(dateXB, UvecXB, lw=2, label=codelist)

        ax2.set_ylabel("U (m/s)")
        ax2.set_xticks(xxtick)
        ax2.set_xticklabels([])
        ax2.set_xlim(xlimit)
        ax2.set_ylim([-0.5, 0.65])
        ax2.grid()

        ax3 = plt.subplot(gs[2, 0])
        (l1,) = ax3.plot(date_vec, -Uy_VEC_vec, "xk", lw=2, label="VEC " + str(k))

        (l2,) = ax3.plot(dateXB, VvecXB, lw=2, label=codelist)

        ax3.set_ylabel("V (m/s)")
        ax3.set_xlim(xlimit)
        ax3.set_ylim([-1, 0.25])
        ax3.set_xticks(xxtick)
        ax3.set_xticklabels(xxticklabels)
        ax3.grid()

        plt.show(block=False)

        fig1.savefig(
            figPath + cl["parameter"]["figName"] + "VEC" + str(k) + ".png",
            dpi=300,
            facecolor="w",
            edgecolor="w",
            format="png",
        )
        k += 1
