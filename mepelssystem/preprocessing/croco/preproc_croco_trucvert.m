%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%  Build a CROCO RIP configuration.
%  Create a grid file for the Rip Current experiment
%  (for the parent and the child grid).
%  This file is based on make_vortex.m
%
%  Further Information:
%  http://www.croco-ocean.org
%
%  This file is part of CcdfROCOTOOLS
%
%  CROCOTOOLS is free software; you can redistribute it and/or modify
%  it under the terms of the GNU General Public License as published
%  by the Free Software Foundation; either version 2 of the License,
%  or (at your option) any later version.
%
%  CROCOTOOLS is distributed in the hope that it will be useful, but
%  WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%  GNU General Public License for more details.
%
%  You should have received a copy of the GNU General Public License
%  along with this program; if not, write to the Free Software
%  Foundation, Inc., 59 Temple Place, Suite 330, Boston,
%  MA  02111-1307  USA
%
%  Truc Vert Beach configuration
%
%  Julien CHAUCHAT, LEGI 2019
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clear all
close all


%%%%%%%%%%%%%%%%%%%%% USERS DEFINED VARIABLES %%%%%%%%%%%%%%%%%%%%%%%%
%
%  Title
%
ROMS_title='TRUCVERT';
%
%  Names
%
%% Parameters definitions for Case
% Choice of configuration
% config = 0 : hydro
% config = 1 : morpho
config = 1;

%--------------
%Forcing conditions
ncFor = fullfile('/fsnet/project/meige/2019/19MEPELS/Simulations/TRUCVERT/DATA/HYDRO/ForcageLEGIFull.nc');

% output folder
if config == 0
    % charging bathymetry
    bathyfile = '../../inputdata/bathy/trucvert/trucvert2008-04-04_Filter_bathy.nc';
    % output path
    basePath='../../beaches/trucvert/croco/run0/';
    baseName = 'TRUCVERT_70x100sh';
elseif config == 1
    % charging bathymetry
    bathyfile = '../../inputdata/bathy/trucvert/trucvert2008-02-11_Filter_bathy.nc';
    % output path
    basePath='../../beaches/trucvert/croco/run3/';
%    baseName = 'TRUCVERT_70x100sh';
    baseName = 'TRUCVERT_140x200sh';
else
    disp('Config does not exist - please check !');
end
%
% grid size
%
gridSize = 10;%10
%
% number of vertical layers
%
if config ==0
    N=10;
elseif config ==1    
    N=20;
end
%
% number of sediment class (not implemented yet)
%
Ns=2;
%
% number of bed layers
%
Sb=2;

% choose dates
if config ==0
    date_0 = datenum('2008-04-06 00:00:00');
    date_f = datenum('2008-04-08 00:00:00');
elseif config ==1    
    date_0 = datenum('2008-03-03 00:00:00');
    date_f = datenum('2008-04-04 00:00:00');
    
    dt=1;
    Nstep = (date_f-date_0)*24*3600/dt
end
%
% File to create?
%
writeGRD = 1;
writeINI = 1;
writeBRY = 1;
writeWKB = 1;

if config == 0
    InitMorpho = 0;
elseif config == 1
    InitMorpho = 1;
    %
    % sediment parameters
    %
    rhow = 1024;
    srho = 2650;
    visk=1.3e-3/rhow;  % m2/s
    g=9.8;             % m/s2
    Sd=0.3;            % mm
    d50 = Sd*1e-3;     % m
    gam0=1e-3;         % in 1E-3 - 1E-5 (Smith & McLean, Drake & al)
    
    bedThickness = 1;
    bedPorosity = 0.4;
    
    % Compute settling velocity
    %WSED      Settling velocity of size class [mm/s].
    %            Typically WSED  = 1E3*  {visk*(sqrt(10.36^2+1.049*D^3)-10.36)/d50} [mm/s] (Soulsby 97)
    %            with  D=d50*(g*(srho/rhow-1)/(visk^2))^0.33333
    %
    D=d50*(g*(srho/rhow-1)/(visk^2))^0.33333;
    WSED  = 1E3* (visk*(sqrt(10.36^2+1.049*D^3)-10.36)/d50)
    %in [mm/s]
    
    %ERATE     Erosion rate of size class [kg/m2/s].
    %            Typically ERATE  =gam0*WSED*1.e-3*SRHO [kg/m2s];
    %            with gam0=1E-3 - 1E-5 (Smith & McLean, Drake&al)
    ERATE = gam0*(WSED*1.e-3)*srho
    % in [kg/m2s]
    
    %  TAU_CE    Critical shear stress for sediment motion [N/m2]
    %            (initiation of bedload for coarses, suspension for fines).
    %            Typically TAU_CE =tau_cb=thetcr.*(g*(srho-rhow).*d50) [N/m2]; (Shields, bedload)
    %            with thetcr = 0.3./(1+1.2*D) + 0.055*(1-exp(-0.02*D)) (Soulsby & Whitehouse 97)
    %            TAU_CE   = rhow*(0.8 WSED*1E-3)^2 [N/m2] (suspended load)
    thetcr   = 0.3./(1+1.2*D) + 0.055*(1-exp(-0.02*D)) %(Soulsby & Whitehouse 97)
    TAU_CE   = thetcr.*(g*(srho-rhow).*d50)
    %TAU_CE   = rhow*(0.8*WSED*1E-3)^2
    %in [N/m2] (suspended load)
end
%
% Filenames
%
grdname = [basePath,baseName,'_GRD.nc'];
bryname = [basePath,baseName,'_BRY.nc'];
wkbname = [basePath,baseName,'_WKB.nc'];
ininame = [basePath,baseName,'_INI.nc'];

%
% ocean density
%
rhow = 1024;
%
% min depth
%
hc=-20;

%
% Boundary conditions
%      S E N W
% Tide
obcT = [0 0 0 1];
% Waves
obcW = [0 0 0 1];

%% 1) Read wave and tide data from FORCAGE file
nc = netcdf(ncFor, 'nowrite');

timeF = nc{'time'}(:);
dateF = nc{'date'}(:);
Hs   = nc{'Hs'}(:);
Tp   = nc{'Tp'}(:);
Dir  = nc{'Dir'}(:);
Tide = nc{'Tide'}(:);
close(nc)

% synchronize files
keepData = find((dateF>date_0) & (dateF<=date_f));
MSL   = mean(Tide(keepData));

fig2=figure;
xlimit=([date_0 date_f]);
xxtick=[xlimit(1):7:xlimit(2)];

subplot(4,1,1)
plot(dateF(keepData),Hs(keepData),'LineWidth',2,'Color','k')
ylabel('Hs (m)')
set(gca,'XTickLabel',[]);
%xlim([min(dateF(keepData)) max(dateF(keepData))])
set(gca,'Xlim',xlimit)

subplot(4,1,2)
plot(dateF(keepData),Tp(keepData),'LineWidth',2,'Color','r')
ylabel('Tp (s)')
set(gca,'XTickLabel',[]);
%xlim([min(dateF(keepData)) max(dateF(keepData))])
set(gca,'Xlim',xlimit)

subplot(4,1,3)
plot(dateF(keepData),Dir(keepData),'LineWidth',2,'Color','g')
ylabel('Direction (?)')
set(gca,'XTickLabel',[]);
%xlim([min(dateF(keepData)) max(dateF(keepData))])
set(gca,'Xlim',xlimit)

subplot(4,1,4)
plot(dateF(keepData),Tide(keepData),'LineWidth',2,'Color','b')
hold on;
plot([min(dateF(keepData)) max(dateF(keepData))],[MSL MSL],'--k');
%plot(dateF(min(keepData))+datestr(timeCroco/(24*3600)),TideCroco,'LineWidth',2,'Color','g')
ylabel('Tide (m)')
axis([min(dateF(keepData)) max(dateF(keepData)) -2.75 2.75])
%datetick('x','HH:MM')
xlabel('date')

set(gca,'Xtick',xxtick','Xticklabel',xxtick)
set(gca,'Xlim',xlimit)
datetick('x',19,'keeplimits','keepticks')
%
% Initial water depth
%
%zeta0 = Tide(min(keepData));
zeta0 = Tide(min(keepData));
zetaMax=max(Tide);

if (writeGRD==1)
    %% 4) Grid and Bathymetry profile
    nc=netcdf(bathyfile,'nowrite');

    Xraw=nc{'X'}(:);
    Yraw=nc{'Y'}(:);
    Zraw=nc{'h'}(:);
    
    Xraw = Xraw(:);
    Yraw = Yraw(:);
    Zraw = Zraw(:);
    close(nc)
    
    
    %
    % Parameters for the Grid
    %
    Xmin= 0;
    Ymin= -1000;
    
    Nx= 1400./gridSize;
    Ny= 2000./gridSize;
    
    dx=gridSize;
    dy=gridSize;
    
    Xmax= Xmin + dx*Nx;
    Ymax= Ymin + dy*Ny;
    
    Zmin= min(Zraw);
    Zmax= max(Zraw);
    %
    %
    % Horzontal Grid
    %
    x=linspace(Xmin,Xmax,Nx);
    y=linspace(Ymin,Ymax,Ny);
    [X,Y]=meshgrid(x,y);
    
    h=zeros(Ny,Nx);
    h=griddata(Xraw,Yraw,Zraw,X,Y,'natural');
    
    %
    %%%%%%%%%%%%%%%%%%% END USERS DEFINED VARIABLES %%%%%%%%%%%%%%%%%%%%%%%
    %
    % Coriolis term (beta plane)
    %
    f = 0.;
    
    %%
    %%  create grid
    %%
    %
    % Create the grid file
    %
    disp(' ')
    disp(' Create the grid file...')
    [M,L]=size(X);
    M=M-1;
    L=L-1;
    disp([' LLm = ',num2str(L-1)])
    disp([' MMm = ',num2str(M-1)])
    
    create_grid(L,M,grdname,ROMS_title)
    
    %
    %  Compute the metrics
    %
    disp(' ')
    disp(' Compute the metrics...')
    %[pm,pn,dndx,dmde]=get_metrics(grdname);
    pm=1/dx; %1./repmat(dx,size(X,1),size(X,2));
    pn=1/dy; %1./repmat(dy,size(X,1),size(X,2));
    dndx=0.; %repmat(0,size(X,1),size(X,2));
    dmde=0; %repmat(0,size(X,1),size(X,2));
    
    xr=X;
    yr=Y;
    
    [xu,xv,xp]=rho2uvp(xr);
    [yu,yv,yp]=rho2uvp(yr);
    dx=1./pm;
    dy=1./pn;
    dxmax=max(max(dx/1000));
    dxmin=min(min(dx/1000));
    dymax=max(max(dy/1000));
    dymin=min(min(dy/1000));
    disp(' ')
    disp([' Min dx=',num2str(dxmin),' km - Max dx=',num2str(dxmax),' km'])
    disp([' Min dy=',num2str(dymin),' km - Max dy=',num2str(dymax),' km'])
    %
    %  Angle between XI-axis and the direction
    %  to the EAST at RHO-points [radians].
    %
    %angle = 0*angle ;
    rotat_angle = 0;
    %
    %  Coriolis parameter
    %
    %f=4*pi*sin(pi*Latr/180)/(24*3600);
    %f=ones(size(Latr))*1.04510e-4;
    f=ones(size(xr))*0.;
    %
    % Fill the grid file
    %
    disp(' ')
    disp(' Fill the grid file...')
    nc=netcdf(grdname,'write');
    nc{'pm'}(:)=pm;
    nc{'pn'}(:)=pn;
    nc{'dndx'}(:)=dndx;
    nc{'dmde'}(:)=dmde;
    nc{'x_u'}(:)=xu;
    nc{'y_u'}(:)=yu;
    nc{'x_v'}(:)=xv;
    nc{'y_v'}(:)=yv;
    nc{'x_rho'}(:)=xr;
    nc{'y_rho'}(:)=yr;
    nc{'x_psi'}(:)=xp;
    nc{'y_psi'}(:)=yp;
    nc{'angle'}(:)=rotat_angle;
    nc{'f'}(:)=f;
    nc{'h'}(:)=h;
    nc{'spherical'}(:)='F';
    %
    % Compute the mask
    %
    % mettre un facteur de secu
    maskr=h+zetaMax>0;
    maskr=process_mask(maskr);
    % test without mask
    %maskr(:,:)=1;
    [masku,maskv,maskp]=uvp_mask(maskr);
    %
    %  Write it down
    %
    nc{'h'}(:)=h;
    nc{'mask_u'}(:)=masku;
    nc{'mask_v'}(:)=maskv;
    nc{'mask_psi'}(:)=maskp;
    nc{'mask_rho'}(:)=maskr;
    
    close(nc);
    
    %
    % Plot
    %
    fig1=figure;
    mesh(X,Y,h);
    az = -24;
    el = 28;
    view(az, el);
    %axis([Xmin Xmax Zmin 770 -7 1 ]);
    xlabel('crosshore direction');
    zlabel('depth');
    colormap jet;
    cmap = colormap;
    cmap = flipud(cmap);
    colormap(cmap);
    colorbar;
    
    
    fig2=figure;
    contourf(xr,yr,h);
    %axis([Xmin Xmax Zmin 770 -7 1 ]);
    xlabel('cross-shore (m)');
    ylabel('long-shore (m)');
    colormap jet;
    cmap = colormap;
    cmap = flipud(cmap);
    colormap(cmap);
    colorbar;
    
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%                 create initial condition file
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if (writeINI==1)
    %
    % Initialize the file
    %
    create_inifile(ininame,grdname,ROMS_title,...
        0,0,hc,N,0,'clobber',1)
    %
    %  Read the grid file
    %
    nc=netcdf(grdname,'r');
    h=nc{'h'}(:);
    maskr=nc{'mask_rho'}(:);
    close(nc);
    hmin=min(min(h(maskr==1)));
    
    [Mp,Lp]=size(h);
    L=Lp-1;
    M=Mp-1;
    Np=N+1;
    %
    %  open the initial file
    %
    nc = netcdf(ininame,'write');
    %
    % Write variables
    %
    time = 0.;%dateF(min(keepData));
    nc{'spherical'}(:)='F';
    nc{'tstart'}(:) =  time;
    nc{'tend'}(:) =  time;
    nc{'scrum_time'}(1) =  time*24*3600;
    nc{'ocean_time'}(1) =  time*24*3600;
    nc{'zeta'}(:) = repmat(zeta0,1,Mp,Lp);
    
    nc{'h'} = ncdouble('time','eta_rho','xi_rho') ;
    %
    nc{'h'}.long_name = ncchar('moving bathymetry at RHO-points');
    nc{'h'}.long_name = 'moving bathymetry at RHO-points';
    nc{'h'}.units = ncchar('meter');
    nc{'h'}.units = 'meter';
    
    nc{'h'}(:) = h;
    %
    if (InitMorpho==1)
        nc('s_b') = Sb;
        
        nc{'sand_1'} = ncdouble('time','s_rho','eta_rho','xi_rho') ;
        nc{'sand_1'}.long_name = ncchar('sand_1 sediment');
        nc{'sand_1'}.long_name = 'sand_1 sediment';
        nc{'sand_1'}.units = ncchar('kg/m3');
        nc{'sand_1'}.units = 'kg/m3';
        sand_1=zeros(1,N,Mp,Lp);
        for k=1:N
            sand_1(1,k,:,:) = repmat(0,Mp,Lp);
        end
        nc{'sand_1'}(:) = sand_1;
        %
        nc{'bed_thick'} = ncdouble('time','s_b','eta_rho','xi_rho') ;
        %
        nc{'bed_thick'}.long_name = ncchar('bed-layer-thickness');
        nc{'bed_thick'}.long_name = 'bed layer thickness';
        nc{'bed_thick'}.units = ncchar('meter');
        nc{'bed_thick'}.units = 'meter';
        bed_thick=zeros(1,Sb,Mp,Lp);
        for k=1:Sb
            bed_thick(1,k,:,:) = repmat(bedThickness,Mp,Lp);%.*maskr;
        end
        nc{'bed_thick'}(:) = bed_thick;
        
        nc{'bed_poros'} = ncdouble('time','s_b','eta_rho','xi_rho') ;
        nc{'bed_poros'}.long_name = ncchar('Porosity of sediment bed layer');
        nc{'bed_poros'}.long_name = 'Porosity of sediment bed layer';
        nc{'bed_poros'}.units = ncchar('no units');
        nc{'bed_poros'}.units = 'no units';
        bed_poros=zeros(1,Sb,Mp,Lp);
        for k=1:Sb
            bed_poros(1,k,:,:) = repmat(bedPorosity,Mp,Lp);%.*maskr;
        end
        nc{'bed_poros'}(:) = bed_poros;
        
        nc{'bed_frac_sand_1'} = ncdouble('time','s_b','eta_rho','xi_rho') ;
        nc{'bed_frac_sand_1'}.long_name = ncchar('vol. fraction of sand in bed layer');
        nc{'bed_frac_sand_1'}.long_name = 'vol. fraction of sand in bed layer';
        nc{'bed_frac_sand_1'}.units = ncchar('no units');
        nc{'bed_frac_sand_1'}.units = 'no units';
        bed_frac_sand_1=zeros(1,Sb,Mp,Lp);
        for k=1:Sb
            bed_frac_sand_1(1,k,:,:) = repmat(1,Mp,Lp);%.*maskr;
        end
        nc{'bed_frac_sand_1'}(:) = bed_frac_sand_1;
        
        nc{'Hripple'} = ncdouble('time','eta_rho','xi_rho') ;
        nc{'Hripple'}.long_name = ncchar('Bed ripple height');
        nc{'Hripple'}.long_name = 'Bed ripple height';
        nc{'Hripple'}.units = ncchar('m');
        nc{'Hripple'}.units = 'm';
        nc{'Hripple'}(:) = zeros(1,Mp,Lp);
        
        nc{'Lripple'} = ncdouble('time','eta_rho','xi_rho') ;
        nc{'Lripple'}.long_name = ncchar('Bed ripple length');
        nc{'Lripple'}.long_name = 'Bed ripple length';
        nc{'Lripple'}.units = ncchar('m');
        nc{'Lripple'}.units = 'm';
        nc{'Lripple'}(:) = zeros(1,Mp,Lp);
    end
    %
    % Synchronize on disk
    %
    close(nc);
end


% create tidal boundary file
if (writeBRY==1)
    %
    % Idealized Tide
    %
    %     % Tidal period
    %     Ttide = 12.25*3600;
    %     freq = 2*pi/Ttide;
    %
    %     % Tidal amplitude
    %     TideAmp = 3.7;
    %
    %     %
    %     time = linspace(0,432000,121*6);
    %
    %     zeta_tide = zeta0 + TideAmp * sin(freq*time);
    %
    %     timeDays = time / (24*3600.);
    % Replace with data from Forcage.dat
    zeta_tide = Tide(keepData);
    timeBRY  = timeF(keepData)  - timeF(min(keepData));
    timeDays = transpose(timeBRY/ (24*3600.));
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    disp(' ')
    disp([' Creating the file : ',bryname])
    disp(' ')
    create_bryfile(bryname,grdname,ROMS_title,obcT,...
        0,0,hc,N,...
        timeDays,120,'clobber',1);
    %
    %  Read the grid file and check the topography
    %
    nc = netcdf(grdname, 'nowrite');
    h=nc{'h'}(:);
    maskr=nc{'mask_rho'}(:);
    Lp=length(nc('xi_rho'));
    Mp=length(nc('eta_rho'));
    status=close(nc);
    hmin=min(min(h(maskr==1)));
    
    L=Lp-1;
    M=Mp-1;
    Np=N+1;
    
    % BC Type
    obc = obcT;
    %
    % Write variables
    %
    nc = netcdf(bryname, 'write');
    
    nc{'spherical'}(:)='F';
    
    nc{'tstart'}(:) =  min([min(timeDays) min(timeDays) min(timeDays)]);
    nc{'tend'}(:) =  max([max(timeDays) max(timeDays) max(timeDays)]);
    nc{'bry_time'}(:) =  timeDays;
    nc{'zeta_time'}(:) =  timeDays;
    
    if obc(1)==0
        nc{'zeta_south'}(:) =  0;
    else
        nc{'zeta_south'}(:) =  repmat(zeta_tide,1,Lp);
    end
    if obc(2)==0
        nc{'zeta_east'}(:) =  0;
    else
        nc{'zeta_east'}(:) =  repmat(zeta_tide,1,Mp);
    end
    if obc(3)==0
        nc{'zeta_north'}(:) =  0;
    else
        nc{'zeta_north'}(:) =  repmat(zeta_tide,1,Lp);
    end
    if obc(4)==0
        nc{'zeta_west'}(:) =  0;
    else
        nc{'zeta_west'}(:) =  repmat(zeta_tide,1,Mp);
    end
    close(nc)
end

% create wave boundary file
if (writeWKB==1)
    %
    %  Read the grid file
    %
    nc=netcdf(grdname,'r');
    h=nc{'h'}(:);
    mask=nc{'mask_rho'}(:);
    close(nc);
    hmin=min(min(h(mask==1)));
    
    [Mp,Lp]=size(h);
    L=Lp-1;
    M=Mp-1;
    Np=N+1;
    %
    %
    %
    timeBRY  = timeF(keepData)  - timeF(min(keepData));
    timeDays = transpose(timeBRY/ (24*3600.));
    %
    create_bryfile_wkb(wkbname,grdname,ROMS_title,obcW,...
        timeDays,120,'clobber');
    %time = linspace(0,432000,120);
    
    % The wave action wac is defined by : A=E/freq where
    % E=1/2 rhow g A^2 is the depth-integrated wave energy.
    % freq = (g*k*tanh(k*h))^0.5
    
    %
    % Compute wave parameters
    %
    %
    % Wave period: Tp
    freq = 2*pi./Tp(keepData);
    
    % Wave amplitude: Hs
    E = 0.5*rhow*9.81*(Hs(keepData)./(2.*sqrt(2))).^2;
    Etilde = E/rhow;
    
    hwest = max(max(h));
    % Wave length
    L = 9.81*Tp(keepData).^2/(2*pi);
    kwave = 2*pi ./ L;
    for j=1:size(kwave)
        for i=1:30
            kwave(j) = freq(j)^2/(9.81*tanh(hwest*kwave(j)));
        end
    end
    kwave=transpose(kwave);
    L = 2*pi./kwave;
    % wave action
    wac_west0 = transpose(Etilde./freq);
    % Wave numbers
    wkx_west0=transpose(kwave'.*cosd( Dir(keepData)));
    wke_west0=transpose(kwave'.*sind(-Dir(keepData)));
    
    fig10=figure;
    subplot(3,1,1)
    plot(timeDays,Etilde,'-r','LineWidth',2)
    ylabel('E (m^2)')
    set(gca,'XTickLabel',[]);
    subplot(3,1,2)
    plot(timeDays,wac_west0,'-r','LineWidth',2)
    ylabel('A (m^3/s)')
    set(gca,'XTickLabel',[]);
    subplot(3,1,3)
    plot(timeDays,kwave,'-r','LineWidth',2)
    hold on
    plot(timeDays,wkx_west0,'-.g','LineWidth',2)
    plot(timeDays,wke_west0,'--b','LineWidth',2)
    ylabel('k (1/m)')
    
    
    
    % BC Type
    obc = obcW;
    
    %
    %  Read the grid file and check the topography
    %
    nc = netcdf(grdname, 'nowrite');
    h=nc{'h'}(:);
    maskr=nc{'mask_rho'}(:);
    Lp=length(nc('xi_rho'));
    Mp=length(nc('eta_rho'));
    status=close(nc);
    hmin=min(min(h(maskr==1)));
    
    L=Lp-1;
    M=Mp-1;
    
    %
    % time
    %
    nc = netcdf(wkbname, 'write');
    
    %
    % Write variables
    %
    nc{'tstart'}(:) =  min([min(timeDays) min(timeDays) min(timeDays)]);
    nc{'tend'}(:) =  max([max(timeDays) max(timeDays) max(timeDays)]);
    nc{'brywkb_time'}(:) =  timeDays;
    
    if obc(1)==1
        nc{'wac_south'}(:) =  0;
        nc{'wkx_south'}(:) =  0;
        nc{'wke_south'}(:) =  0;
    end
    if obc(2)==1
        nc{'wac_east'}(:) =  0;
        nc{'wkx_east'}(:) =  0;
        nc{'wke_east'}(:) =  0;
    end
    if obc(3)==1
        nc{'wac_north'}(:) =  0;
        nc{'wkx_north'}(:) =  0;
        nc{'wke_north'}(:) =  0;
    end
    if obc(4)==1
        nc{'wac_west'}(:) =  repmat(wac_west0,1,Mp);
        nc{'wkx_west'}(:) =  repmat(wkx_west0,1,Mp);
        nc{'wke_west'}(:) =  repmat(wke_west0,1,Mp);
    end
    close(nc)
end
return
