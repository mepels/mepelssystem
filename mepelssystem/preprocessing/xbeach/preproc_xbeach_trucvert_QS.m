
clear all;
close all;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                              XBEACH TOOLS                          %
%                                                                    %
%%%%%%%%%%%%%%%%%%%%%%%%% USERS DEFINED VARIABLES %%%%%%%%%%%%%%%%%%%%

%----------------
% COPY THE UNDER LINE AN PAST IT ON THE TERMINAL TO RUN IT (IMPORT PACKAGE
% FOR THE CODE)
% run ('/fsnet/project/meige/2019/19MEPELS/XBEACH_TOOLS/xbeach_release_10May2013/oetsettings.m')

%% PARTIE 1
%#############
%% Parameters definitions for Case

%--------------
% saving
basePath = '/fsnet/project/meige/2019/19MEPELS/Sylvain/MEPELSsysteme/mepelssystem/beaches/trucvert/xbeach/case_trucvert/quasi_stationnaire';
if ~exist(basePath, 'dir')
    mkdir(basePath)
end

%--------------
% charging bathymetry
ncpath = './../../inputdata/bathy/trucvert/trucvert2008-04-04_nonFilter_bathy.nc'
% 1 if you used a remodelling bathymetry else 0
remodelling = 0;
%--------------
%charging conditions
ncFor = fullfile('http://servdap.legi.grenoble-inp.fr/opendap/meige/19MEPELS/trucvert/ForcageLEGIFull.nc');
%--------------
% COMPUTE 
sparam = 30;  % parameter for cosinus (spectral density)

%%%%%%%%%%%%%%%%%%%%%
%---------------------
% domaine size
Lx = 1400;
Ly = 2000;  % total width
Ymin = -(Ly / 2) ;
Xmin = 0;
%---------------------
gridsize = 10; % gridsize for matlab, different than the bathymetry gridsize already used on preprocessing_mnt code
%----------------
% sensor position (meter)
sensorName = {'VEC1','VEC3'};
vec1(1) = 1308.70; % (x pos)
vec1(2) = -430.27; % (y pos)
vec3(1) = 1256.70; % (x pos)
vec3(2) = -455.80; % (y pos)
%----------------
%##########################################################################
%model settings
fw = 0.15;
gamma = 0.55;
bedfriccoef = 0.1;
morfac = 1;  %Morphological acceleration factor (begin at 1)
morphology = 0;  %activate morphological calcul  0 : no/ 1 : yes
%----------------
%sedtrans = 0 : no sediment transport
%sedtrans = 1 : sediment transport | quasi-statique
sedtrans = 0;
%----------------
% 'stationary' : 0 stationary mode
% 'surfbeat' : 1  SurfBeat mode
%'nonh' : non-hydrostatic
wavemodel = 0;
%----------------
duration_cst = 1.8e3;   % time between each compute
%----------------
% case date

date_0 = datenum('2008-04-04 00:00:00');
date_f = datenum('2008-04-06 00:00:00');


%---------------
%##########################################################################
% have initial conditions 0 :no / 1 : yes
condition_knows = 1;          % put constant position
tintg = 3600
tintm = 1800    % Interval time of mean, var, max, min output  
%---------------
% CASE CONDITIONS KNOWS = 0
amp = 4;                % tide amplitude
number_of_tide = 4;     % during the study time
Hs_cst = 7;             % Heigth wave
Tp_cst = 16;             % period
Dir_cst = 0;            % direction according to the normal long-shore
%----------------
% Wave direction shift for nautical directions (270? = shore-normal if alfa=0)
DirShift = 270.0;  %normal to the long-shore if the bathymetry is E / N oriented
alfa = 0;         %mesh rotation, if used preproc_mnt DON'T touch it


%###########################################################################
%###########################################################################
%    PART 2
%###########################################################################
%###########################################################################

xlimit = ([datenum(date_0) datenum(date_f)]);
xxtick = (xlimit(1): 1: xlimit(2));  % if does'nt work put [ ] on the edge end del ( )


%% 1) Read wave and tide data from FORCAGE file
if condition_knows == 1
    timeF = nc_varget(ncFor,'time');
    dateF = nc_varget(ncFor,'date');
    Hs   = nc_varget(ncFor,'Hs');
    Tp   = nc_varget(ncFor,'Tp');
    Dir  = nc_varget(ncFor,'Dir');
    Tide = nc_varget(ncFor,'Tide');
    
    keep_data = find(dateF >= date_0 & dateF < date_f);
    
    datestr(min(dateF(keep_data)))
    datestr(max(dateF(keep_data)))
    
    Nt=length(keep_data);
    duration = repmat(timeF(2) - timeF(1), Nt, 1);
else
    Nt                 = 97; %Param_CI(6,1);       %nombre de prise de mesure sur le temps total de l'étude
    keep_data = linspace(1, Nt, Nt);
    
    f_tide = 1 / (Nt / number_of_tide);
    t_tide = linspace(1, Nt, Nt);
    func = sin(2 * pi * f_tide * t_tide) * (amp / 2);
    
    Hs(keep_data)       = Hs_cst;
    Tp(keep_data)       = Tp_cst; %Param_CI(2,1);        %période
    Dir(keep_data)      = Dir_cst;  %Param_CI(3,1);       %orientation
    Tide(keep_data)     = func;  %Param_CI(4,1);       %variation marée
    duration(keep_data) = duration_cst; %Param_CI(5,1);       %env 30mn pour la valeur par défaut
    
    Hs = Hs';
    Tp = Tp';
    Dir = Dir';
    Tide = Tide';
    duration = duration';
    
    TimeF(1: Nt, 1) = linspace(0, Nt * duration(1, 1), Nt); %(1,1) par ce qu'on a défini duration comme un vecteur plus haut (vecteur avec la meme valeur)
    dateF = (date_0: minutes(30): date_f)';
    xlimit = ([datenum(date_0) datenum(date_f)]);
    
end


%
% Wave boundary condition
%
dtbc = repmat(1,Nt,1);

mainang = Dir(keep_data) + DirShift;
gammajsp = repmat(3.3, Nt, 1);
s = repmat(sparam, Nt, 1);



% consider no rotation with respect to x axis so that normal waves are 270?
%    in directions (must use thetanautical=1) %_280.8%
%    mainang in Xbeach is measured clockwise, ref: 270 deg ---> frontal
%    Dir in forcage is clockwise, ref: 0 deg ---> frontal
mainang = Dir(keep_data) + DirShift;
gammajsp = repmat(3.3, Nt, 1);
s = repmat(sparam, Nt, 1);


fid = fopen([basePath,'/wave.txt'], 'w');
% %      <Hm0> <Tp> <mainang> <gammajsp> <s> <duration> <dtbc>
% %stationary = [4 14 301 3.3 30 48*3600 1];
stationary = [Hs(keep_data) Tp(keep_data) mainang gammajsp s duration dtbc];
fprintf(fid,'%.2f %.2f %.1f %.1f %.0f %.0f %.0f\n', stationary');
fclose(fid);


figure(3);
subplot(3, 1, 1)
plot(dateF(keep_data), Hs(keep_data), '-k', 'LineWidth', 2)
ylabel('Hs (m)')
ylim([0 1.1 * max(Hs(keep_data))]);
set(gca, 'xticklabel', {[]})
subplot(3, 1, 2)
plot(dateF(keep_data), Tp(keep_data), '-k', 'LineWidth', 2)
ylabel('Tp (s)')
ylim([0 1.1 * max(Tp(keep_data))]);
set(gca, 'xticklabel', {[]})
subplot(3, 1, 3)
plot(dateF(keep_data), Dir(keep_data)+DirShift, '-k', 'LineWidth', 2)
ylabel('dir (°)')
ylim([0 360]);

%     set(gca, 'Xlim', xlimit)
%     set(gca, 'Xtick', xxtick', 'Xticklabel', xxtick)
%     datetick('x', 1, 'keeplimits', 'keepticks')



%         % write filelist and BC file
%         fid = fopen(strcat(basePath, '/filelist.txt'), 'w');
%         fprintf(fid, '%s \n', 'FILELIST');
        fid = fopen([basePath, '/wave.txt'], 'w');
        % %      <Hm0> <Tp> <mainang> <gammajsp> <s> <duration> <dtbc>
        % %stationary = [4 14 301 3.3 30 48*3600 1];
        stationary = [Hs(keep_data) Tp(keep_data) mainang gammajsp s duration dtbc];
        fprintf(fid,'%.2f %.2f %.1f %.1f %.0f %.0f %.0f\n', stationary');
        fclose(fid);

        
        figure(3);
        subplot(3, 1, 1)
        plot(dateF(keep_data), Hs(keep_data), '-k', 'LineWidth', 2)
        ylabel('Hs (m)')
        ylim([0 1.1 * max(Hs(keep_data))]);
        set(gca, 'xticklabel', {[]})
        subplot(3, 1, 2)
        plot(dateF(keep_data), Tp(keep_data), '-k', 'LineWidth', 2)
        ylabel('Tp (s)')
        ylim([0 1.1 * max(Tp(keep_data))]);
        set(gca, 'xticklabel', {[]})
        subplot(3, 1, 3)
        plot(dateF(keep_data), Dir(keep_data)+DirShift, '-k', 'LineWidth', 2)
        ylabel('dir (°)')
        ylim([0 360]);

%% 3) Water level
if condition_knows == 1
    zs0 = Tide(keep_data(1)); % water level due to tide alone (m)
    timeTide = timeF(keep_data) - timeF(keep_data(1));
    figure(2);
    plot(dateF(keep_data), Tide(keep_data), ':k', 'LineWidth', 2)
    ylabel('Zeta (m)')
    set(gca,'Xlim',xlimit)
    set(gca,'Xtick',xxtick','Xticklabel',xxtick)
    datetick('x',1,'keeplimits','keepticks')

    % Save tide input in XBeach structure
    xb_tide = xb_generate_tide('time',timeTide,'front',Tide(keep_data),'back',zs0);
else
    zs0 = Tide(1);
    timeTide = TimeF;
    figure(2);
    plot(dateF, Tide, ':k', 'LineWidth', 2)
    ylabel('Zeta (m)')
    %set(gca,'Xlim',xlimit)
    %set(gca,'Xtick',xxtick','Xticklabel',xxtick)
    datetick('x',1,'keeplimits','keepticks')

    % Save tide input in XBeach structure
    xb_tide = xb_generate_tide('time', timeTide, 'front', Tide, 'back', zs0);
end

%% 4) Grid and Bathymetry profile
ncfile = fullfile(ncpath);

Xraw = nc_varget(ncfile, 'X');
Yraw = nc_varget(ncfile, 'Y');
if remodelling == 1
    hraw = nc_varget(ncfile, 'h_modelling');
else 
    hraw = nc_varget(ncfile, 'h');
end

Xraw = Xraw(:);
Yraw = Yraw(:);
hraw = hraw(:);

dx = gridsize;
dy = gridsize;

Nx = Lx/gridsize;
Ny = Ly/gridsize;


Xmax = Xmin + dx*Nx;
Ymax = Ymin + Ny * dy;

Zmin = min(hraw);
Zmax = max(hraw);

% Create meshgrid (needed for interpolation later)
x = linspace(Xmin,Xmax,Nx+1);
y = linspace(Ymin,Ymax,Ny+1);
h = zeros(Ny,Nx);

% Now generate a mesh and interpolate the bathymetry
[X,Y] = meshgrid(x,y);
h = griddata(Xraw,Yraw,hraw,X,Y,'natural');

% Supress Nan of the beach
nanh = isnan(h);
for i=1:Ny
    for j=2:Nx
        if nanh(i,j)==1
            h(i,j) = h(i,j-1);
        end
    end
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Make a plot just to show what the profile / bathymetry look like
fig1 = figure;
mesh(X,Y,h);
az = -24;
el = 28;
view(az, el);
% axis([Xmin Xmax Zmin 770 -7 1 ]);
xlabel('crosshore direction');
zlabel('depth');
colormap jet;
cmap = colormap;
cmap = flipud(cmap);
colormap(cmap);
colorbar;

% Save grid/bathy in XBeach structure
xb_bat = xb_generate_bathy('x', X, 'y', Y, 'z', h,...
    'crop',false,'finalise',false,'world_coordinates',false,...
    'rotate',false,'optimize',false);

%% 5) General Model settings
% Directly save model settings in XBeach structure
if wavemodel == 0
    xb_set = xb_generate_settings('sedtrans',sedtrans,'posdwn',1,'single_dir',1,...
        'zs0',zs0,...
        'tstart',0,'tstop', max(round(timeTide-0.5)), 'tintg', tintg, 'tintm',10800,'cfl',0.6,...
        'outputformat','netcdf',...
        'bedfriction','white-colebrook',...
        'bedfriccoef',bedfriccoef,...
        'alfa',alfa,...
        'thetanaut',1,...
        'thetamin',215,...
        'thetamax',325,...
        'dtheta_s',10,...
        'wavemodel',0,...
        'cyclic',1,...
        'instat','stat_table',...
        'wbctype','jonstable',...
        'bcfile','wave.txt',...
        'wci',0,...
        'gamma',gamma,...
        'fw',fw,...
        'morfac',morfac,...
        'morphology',morphology,...
        'nglobalvar',{'H','zs','zb','hh','u','v','ue','ve','cx','cy','DR'},...
        'npoints',{[num2str(vec1(1)), ' ', num2str(vec1(2)), ' ', 'VEC1'],[num2str(vec3(1)), ' ', num2str(vec3(2)), ' ', 'VEC3']},...
        'npointvar',{'H','zs','zb','u','v'});
else
    xb_set = xb_generate_settings('sedtrans',sedtrans,'posdwn',1,...
        'zs0',zs0,...
        'tstart',0,'tstop',max(round(timeTide)),'tintp', 1200,'tintg', tintg,'tintm', tintm,'cfl',0.6,...
        'outputformat','netcdf',...
        'bedfriction','white-colebrook',...
        'bedfriccoef',bedfriccoef,...
        'alfa', alfa,...
        'thetanaut',1,...
        'thetamin',215,...
        'thetamax',325,...
        'dtheta_s',10,...
        'wavemodel',wavemodel,...
        'cyclic',1,...
        'instat','stat_table',...
        'wbctype','jonstable',...
        'bcfile','wave.txt',...
        'gamma',gamma,...
        'fw',fw,...
        'morfac', morfac,...
        'morphology',morphology,...
        'nmeanvar',{'H','zs','zb','hh','u','v','ue','ve','cx','cy','DR'},...
        'npoints',{[num2str(vec1(1)), ' ', num2str(vec1(2)), ' ', 'VEC1'],[num2str(vec3(1)), ' ', num2str(vec3(2)), ' ', 'VEC3']},...
        'npointvar',{'H','zs','zb','hh','u','v','ue','ve'});
end
% 
% % %% 6) Save model setup
xb_tot = xs_join(xb_bat,xb_tide,xb_set);
xb_write_input(strcat(basePath,'/params.txt'),xb_tot);


if condition_knows == 0
    delete (basePath+"forcage.nc")
    % %%
    dateF = linspace(0,48*60,Nt)';
    
    % create Netcdf file for conditions
    %ncid = netcdf.create("forcage.nc",
    nccreate(basePath+"forcage.nc", "time",...
         "Dimensions", {'timeDim',Nt,'y',1});
    ncwrite(basePath+"forcage.nc", "time", TimeF);

    nccreate(basePath+"forcage.nc", "date",...
         "Dimensions", {'timeDim',Nt,'y',1});
    ncwrite(basePath+"forcage.nc", "date", dateF);
    
    nccreate(basePath+"forcage.nc", "Hs",...
        "Dimensions", {'timeDim',Nt,'y',1});
    ncwrite(basePath+"forcage.nc", "Hs", Hs);
    
    nccreate(basePath+"forcage.nc", "Tp",...
        "Dimensions", {'timeDim',Nt,'y',1});
    ncwrite(basePath+"forcage.nc", "Tp", Tp);
    
    nccreate(basePath+"forcage.nc", "Dir",...
        "Dimensions", {'timeDim',Nt,'y',1});
    ncwrite(basePath+"forcage.nc", "Dir", Dir);
    
    nccreate(basePath+"forcage.nc", "Tide",...
        "Dimensions", {'timeDim',Nt,'y',1});
    ncwrite(basePath+"forcage.nc", "Tide", Tide);


end


