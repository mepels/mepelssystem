#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Sep 10 13:35:54 2021

@author: ferraris6s
"""

import numpy as np
import matplotlib.pyplot as plt
import yaml
import time
from scipy.optimize import curve_fit
from netCDF4 import Dataset
from scipy.ndimage.filters import gaussian_filter
import matplotlib.gridspec as gridspec
import os
from urllib.request import urlopen
import sys

sys.path.append("./libs/")
from indata_function import indata
from replacementValues_function import replacementValues

#%%
def func(x, a, b, c):
    return a * pow(x, b) + c


#%%         choice configuration
"""
    Choice study and load data from the configuration file
"""
beach_letter = input(
    "Beach you can used :"
    "\n biscarrosse as 1,"
    "\n trucvert as 2,"
    "\n gravelines as 3,"
    "\n 4: other,"
    "\n Enter the corresponding number : "
)
if beach_letter == "1":
    beach_config = "biscarrosse"
elif beach_letter == "2":
    beach_config = "trucvert"
elif beach_letter == "3":
    beach_config = "gravelines"
elif beach_letter == "4":
    beach_config = input("Enter the yaml file name (without config.yaml):")
with open(r"./configurations/" + beach_config + "_configuration.yaml") as file:
    cl = yaml.full_load(file)
print("beach config choose: ", beach_config)
"""
    Importation data with the configuration file indication
    normally the bathy file is a natcdf file but it could also be a txt file
"""
web = str(False)
# .nc
X, Y, h = indata(cl["data_path_saved"]["inputPath"], "nc", "", web)
h = -h  # Z neg
size_matrix = np.shape(X)
Ny = size_matrix[0]
Nx = size_matrix[1]
print("using .nc data \n ")
Xmin = np.min(X)
Xmax = np.max(X)
Ymin = np.min(Y)
Ymax = np.max(Y)
cmapBathy = cl["parameter"]["colormaps"]
#%% Params0
# =============================================================================
# plot raw bathymetry
# =============================================================================
#%%               Profile
# slide at the center of y direction along x direction (cross-shore profil)
plt.figure(num=2)
"""
    compute the mean cross-shore profile and plot it
"""
xpos = X[0, :]
k = 0
kk = 0
meanProfile = np.zeros(Nx)
profile = np.zeros(Ny)
meanProfile = np.mean(h, axis=0)
while k < Ny:
    profile = h[k, :]
    if k == 1:
        plt.plot(
            xpos,
            profile,
            color="0.8",
            linestyle="--",
            label="cross shore profile",
        )
    else:
        plt.plot(xpos, profile, color="0.8", linestyle="--")
    k += 1
del k, kk
#%%            depth closure
"""
    determination of the DOC point with data input on configuration file
    One methode is used but it's possible to use the others comment formulation
"""
g = 9.81
T12_Y = cl["storm"]["T12_Y"]
H12_Y = cl["storm"]["H12_Y"]
T_mean = cl["mean"]["T_mean"]
H_mean = cl["mean"]["H_mean"]
D_50 = float(cl["sand_size"]["D50"])
doc_Hallermeier_storm = -(
    2.28 * H12_Y - 68.5 * ((H12_Y ** 2) / (g * (T12_Y ** 2)))
)
# d_l2 = -(1.75 * H12_Y - 57.9 * ((H12_Y**2) / (g * (T12_Y**2))))
# Dcc1 = -6.75 * H_mean
# Dcc2 = -8.9 * H_mean
# https://apps.dtic.mil/sti/pdfs/ADA578584.pdf
print("\n Hallermeier 12h/Y :", doc_Hallermeier_storm, "m")
# print('Birkemeier 12h/Y:', d_l2)
# print('Dc1', Dcc1)
# print('Dc2', Dcc2)
plt.axhline(doc_Hallermeier_storm, label="doc : Hallermeier (storm event)")
# plt.plot(xpos, d_ll2, 'y*', label='hdc : Birkemeier - 12h/Y')
# plt.plot(xpos, Dc1, 'k--', label="hdc : Houston's - mean data")
# plt.plot(xpos, Dc2, 'b--', label='hdc : Hallermeier - mean data, ')
#%%                 Zero level
"""
    search some interest points
    HTl, zero with is defintion, zero hydrographique, LTL
"""
zero_level_val = 0
def_zero = cl["sealevel"]["defined_zero"]
if def_zero == 0:
    def_zero = str("IGN69")
elif def_zero == 1:
    def_zero = str("middle tide")
elif def_zero == 2:
    def_zero = str("zero hydrographic")
if cl["sealevel"]["diff_to_zero_hydro"] != "NaN":
    zero_hydro_val = zero_level_val + cl["sealevel"]["diff_to_zero_hydro"]
    plt.axhline(
        zero_hydro_val, color="gray", linestyle="-.", label="zero_hydrographique"
    )
if cl["sealevel"]["HTL"] != "NaN":
    HTL_val = zero_level_val + cl["sealevel"]["HTL"]
    z_max = HTL_val + cl["sealevel"]["diff_to_zero_hydro"]
    plt.axhline(z_max, color="red", linestyle="-.", label="HTL")
else:
    print("Tide heigth THL unknow")
    z_max = np.max(profile) * cl["sealevel"]["HTL_approx"]
if cl["sealevel"]["LTL"] != "NaN":
    LTL_val = (
        zero_level_val
        + cl["sealevel"]["LTL"]
        + cl["sealevel"]["diff_to_zero_hydro"]
    )
    plt.axhline(LTL_val, color="blue", linestyle="-.", label="LTL ")
else:
    print("Tide height LTL unknow")
plt.plot(X[0, :], meanProfile, label="mean cross-shore profile", color="black")
#%%       replacement profile
# utilisation du critère de Hallermeier moyenné
"""
    definition of the modification area (with morphodynamique effect) with the 
    Doc and the max point of water or a porcentage of the elevation of the bathy
"""
assert (
    doc_Hallermeier_storm > meanProfile[0]
), "H value to big, or bathymetry to small"
k = 0
while meanProfile[k] <= doc_Hallermeier_storm:
    k += 1
k_x_min = k
hr_x_min = meanProfile[k_x_min]
if np.max(meanProfile) < z_max:
    k_x_max = np.argmax(meanProfile)
    hr_x_max = meanProfile[k_x_max]
else:
    k = 0
    while meanProfile[k] <= z_max:
        k += 1
    k_x_max = k
    hr_x_max = meanProfile[k_x_max]
length_modif = hr_x_max - hr_x_min
#%%               DEAN PROFILE
"""
    defined the dean profile on the profile modify (see upper)
    fit the actual profile with a dean profile to replace it
"""
profile_short = meanProfile[k_x_min:k_x_max]
xpos_short = xpos[k_x_max] - xpos[k_x_min:k_x_max]
popt, pcov = curve_fit(func, xpos_short, profile_short, check_finite=False)
zFitted = func(xpos_short, *popt)
profile = np.zeros(len(xpos))
profile[0:k_x_min] = meanProfile[0:k_x_min]
profile[k_x_max::] = meanProfile[k_x_max::]
profile[k_x_min:k_x_max] = zFitted[:]
for k in range(cl["fit_dissipative"]["nbs_fit"]):
    profile = gaussian_filter(
        profile, sigma=cl["fit_dissipative"]["sigma"], mode="nearest"
    )
plt.plot(
    xpos, profile, "green", linewidth=2, label="fitted profile (gaussian filter)"
)
plt.xlabel("Cross-shore [m]", size=12)
plt.ylabel("Elevation [m]", size=12)
plt.grid(True)
plt.legend(loc="upper left", prop={"size": 8})
plt.show(block=False)  # plt figure 2


#%%
# % Compute settling velocity
# %WSED      Settling velocity [m/s].
# %            Typically WSED  = {visk*(sqrt(10.36^2+1.049*D^3)-10.36)/d50} [m/s] (Soulsby 97)
# %            with  D=d50*(g*(srho/rhow-1)/(visk^2))^0.33333
# %
rhow = 1024
srho = 2650
visk = 1.3e-3 / rhow  # m2/s
D = D_50 * (g * (srho / rhow - 1) / (visk ** 2)) ** (1.0 / 3.0)
WSED = visk * (np.sqrt(10.36 ** 2 + 1.049 * D ** 3) - 10.36) / D_50  # m/s
# compute wave length
# Wave length
L12_Y = g * T12_Y ** 2 / (2 * np.pi)
for i in range(30):
    L12_Y = (
        (g * T12_Y ** 2)
        / (2 * np.pi)
        * np.tanh((2 * np.pi * np.abs(doc_Hallermeier_storm)) / L12_Y)
    )
L_mean = g * T_mean ** 2 / (2 * np.pi)
for i in range(30):
    L_mean = (
        (g * T_mean ** 2)
        / (2 * np.pi)
        * np.tanh((2 * np.pi * np.abs(doc_Hallermeier_storm)) / L_mean)
    )
# beach slope at closure depth
Beta = np.abs(popt[0] * popt[1] * pow(xpos_short[0], (popt[1] - 1)))
# Compute beach indicators
# range of significant wave heigth
Hb = np.linspace(H_mean * 0.5, H12_Y * 1.5, 100)
# relative tidal range (RTR)
if cl["sealevel"]["tidalrange"] != "nan":
    RTR = cl["sealevel"]["tidalrange"] / Hb
else:
    RTR = np.abs(HTL_val - LTL_val) / Hb
# Dean number
Omega_mean = Hb / (WSED * T_mean)
Omega12_Y = Hb / (WSED * T12_Y)
# surf similarity parameter
zeta_mean = np.tan(Beta) / np.sqrt(Hb / (g * T_mean ** 2 / (2 * np.pi)))
zeta12_Y = np.tan(Beta) / np.sqrt(Hb / (g * T12_Y ** 2 / (2 * np.pi)))
plt.figure(num=22)
plt.title("Breaker type")
plt.plot(Hb, zeta_mean, label="Mean conditions")
plt.plot(Hb, zeta12_Y, label="Storm conditions")
plt.xlabel("Hb [m]")
plt.ylabel(
    r"Surf similarity: tan(Beta) / $\sqrt{Hb / L}$"
)  #': tan(Beta) / (Hb / L)^(0.5)')
plt.text(0.2, 0.2, "spilling")  # < 0.5
plt.text(0.2, 1.8, "plunging")  # 0.5 - 3.3
plt.text(0.2, 2.8, "collapsing")  # 3.3 - 5
# plt.text(0.2, 0.3, "surging") # > 5
plt.axhline(y=0.4, color="gray", ls="--")
plt.axhline(y=2, color="gray", ls="--")
plt.ylim([0, max(np.max(zeta12_Y), 3)])
plt.legend()
plt.show()

plt.figure(num=23)
plt.plot(Omega_mean, RTR, label="Mean conditions")
plt.plot(Omega12_Y, RTR, label="Storm conditions")
plt.xlabel("Dean: Hb / (Ws  T)")
plt.ylabel("RTR: TR / Hb")
plt.axhline(y=3, color="gray", ls="--")
plt.axhline(y=7, color="gray", ls="--")
plt.axhline(y=15, color="gray", ls="--")
plt.axvline(x=2, color="gray", ls="--")
plt.axvline(x=5, ymin=(1 - 0.4375), ymax=1.0, color="gray", ls="--")
plt.text(0.1, 0.8, "reflective")
plt.text(2.1, 0.8, "barred")
plt.text(5.1, 0.8, "barred dissipative")
plt.text(0.1, 3.8, "low tide terrace")
plt.text(0.1, 4.5, "+ rip")
plt.text(2.1, 3.8, "low tide bar/rip")
plt.text(5.1, 3.8, "non-barred dissipative")
plt.text(0.1, 7.8, "low tide terrace")
plt.text(4, 7.8, "ultra-dissipative")
plt.xlim([0, 8])
plt.ylim([16, 0])
plt.legend(loc="lower right", prop={"size": 8})
plt.show()

print("Script endding")
