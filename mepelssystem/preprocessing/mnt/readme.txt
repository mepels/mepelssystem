./libs/
=======
Python librairies

./configurations/
==================
Pre-processing configurations

./backupInterp/
===============
Folder to save the interpolation done

preprocessing_mnt.py
====================
1st script of the pre-processing

analyse_preprocessing_mnt.py
============================
2nd script of the pre-processing

modif_bathy_preproc.py
=======================
3rd script of the pre-processing

search_point.py
===============
Change coordinate of few points
