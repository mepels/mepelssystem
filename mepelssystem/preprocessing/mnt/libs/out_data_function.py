#!/usr/bin/env python3
# -*- coding: utf-8 -*-


#%%
import os
import numpy as np
import time
import yaml
from netCDF4 import Dataset


#%%


def out_data(
    X,
    Y,
    Nx,
    Ny,
    h,
    filterBathy,
    save_Plot_Bathy,
    fig1,
    fig2,
    fig3,
    calGradH,
    configname,
    date_bathy,
):

    """
    This function is for recovers calculated parameters and saved the
    data and figures in the files choice for each.

    Args:
        X : array
            matrix of point position according to x axis
        Y : array
            matrix of point position according to y axis
        Nx: int
            number of mesh on x axis
        Ny: int
            number of mesh on y axis
        h: array
            matrix size (Ny,Nx) with z value for each point on the mesh
        filterBathy: int
            0 or 1
        save_Plot_Bathy: int
            0, 1 or 2
        fig1:
            figure or np.nan
        fig2:
            figure or np.nan
        fig3:
            figure or np.nan
        calGradH: int
            0 or 1
        configname: str
            name of the configation choice
        date_bathy: str
            date of the bathymetry used
    Return:
        Nothing on the code but save all different data
    """

    with open(r"./configurations/" + configname + "_configuration.yaml") as file:
        cl = yaml.full_load(file)

    if filterBathy == 0:
        outputPath = (
            cl["data_path"]["savePath_preproc"]
            + configname
            + date_bathy
            + "_nonFilter"
        )
    else:
        outputPath = (
            cl["data_path"]["savePath_preproc"]
            + configname
            + date_bathy
            + "_Filter"
        )

    print(outputPath)

    if save_Plot_Bathy == 0 or save_Plot_Bathy == 2:
        if fig2 != "nan":
            fig2.savefig(outputPath + "_gradh.png", dpi=100)
        if fig1 != "nan":
            fig1.savefig(
                outputPath + "_bathy.png",
                format="png",
                dpi=100,
            )
        if fig3 != "nan":
            fig3.savefig(outputPath + "_bathyLarge.png", dpi=100)

    if save_Plot_Bathy != 0:

        fileName = outputPath + "_bathy.nc"

        x = X[0, 0 : int(Nx)]
        y = Y[0 : int(Ny), 0]

        # open netcdf file
        rootgrp = Dataset(fileName, "w", format="NETCDF4")

        # create dimensions (vectors)
        x = rootgrp.createDimension("x", Nx)
        y = rootgrp.createDimension("y", Ny)

        # for more informations on netcdf4
        # https://unidata.github.io/netcdf4-python/#variables-in-a-netcdf-file

        var = rootgrp.createVariable("X", "f8", ("y", "x"))
        var[:, :] = X
        var.long_name = "X crosss-shore-position"
        var.units = "meter"

        var = rootgrp.createVariable("Y", "f8", ("y", "x"))
        var[:, :] = Y
        var.long_name = "Y long-shore-position"
        var.units = "meter"

        h_var = rootgrp.createVariable("h", "f8", ("y", "x"))
        h_var[:, :] = h
        h_var.long_name = "Bathymetry"
        h_var.units = "meter"

        # attributs
        rootgrp.description = "Positionning and remodeling raw bathymetry"
        if cl["changeStudyArea"]["changeArea"] == 1:
            rootgrp.Xorig_seaward = cl["changeStudyArea"]["Xorig"]
            rootgrp.Yorig_seaward = cl["changeStudyArea"]["Yorig"]
            rootgrp.cross_shore_length = cl["changeStudyArea"]["Lx"]
            rootgrp.long_shore_length = cl["changeStudyArea"]["Ly"]
            rootgrp.orignie_side = cl["changeStudyArea"]["origstudy"]
        else:
            print(
                'To save the area definition (Lx, Ly, Xorig, Yorig and origine side) the parameter "changeArea" need to be egal to 1 \
                   on the configuration file'
            )
        rootgrp.rotation_degrees = cl["changeRotation"]["axisrotation"]
        rootgrp.rotation_sense_def = "0 : trigo sense / 1 : anti-trigo sense"
        rootgrp.orientation_rotation = cl["changeRotation"]["trigosense"]

        if cl["projection"]["before"] == 1:
            rootgrp.info_projection1 = "projection make before interpolation"
            rootgrp.Coordinate_initialy = cl["projection"]["coorIn_B"]
            rootgrp.Coordinate_finally = cl["projection"]["coorOut_B"]
        else:
            rootgrp.info_projection1 = "no projection before interpolation"

        if cl["projection"]["after"] == 1:
            rootgrp.info_projection2 = "projection make after data replacement"
            rootgrp.Coordinate_initialy = cl["projection"]["coorIn_A"]
            rootgrp.Coordinate_finally = cl["projection"]["coorOut_A"]
        else:
            rootgrp.info_projection2 = "no projection after data replacement"

        rootgrp.local_landmark_def = (
            "0 : no local coordinate / 1 : local coordinate"
        )
        rootgrp.local_landmark = cl["projection"]["changeCoordGeo"]

        rootgrp.outputPath = cl["data_path"]["savePath_preproc"]

        rootgrp.history = "Created " + time.ctime(time.time())

        rootgrp.close()

        np.savetxt(
            outputPath + ".txt",
            np.column_stack(
                (
                    np.reshape(X, Nx * Ny),
                    np.reshape(Y, Nx * Ny),
                    np.reshape(h, Nx * Ny),
                )
            ),
            delimiter=",",
            newline=os.linesep,
            fmt="%10.5f",
        )

    print("bathymetry save into:", cl["data_path"]["savePath_preproc"])
    return
