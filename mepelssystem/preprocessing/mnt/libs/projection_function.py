#!/usr/bin/env python3
# -*- coding: utf-8 -*-


#%%
from pyproj import CRS
from pyproj import Transformer


#%%


def projection(varX, varY, coorIn, coorOut):

    """
    This function transforms the geographic coordinates on an other
    geographique coordinates (Ex :WGS to Lambbert-93).
    The transformation is possible two times, at the beginning and
    at the end.

    Parameters
    ----------
    varX : array
        matrix of point position according to x axis
    varY : array
        matrix of point position according to y axis
    coorIn: str
        EPSG of bathymetry input
    coorOut: str
        EPSG of bathymetry output


    Returns
    -------
    rot_var_X : array
        new projection matrix of point position according to x axis
    rot_var_Y : array
        new projection matrix of point position according to y axis

    """

    crs_input = CRS(coorIn)
    crs_output = CRS(coorOut)
    print("Original projection system :", crs_input.name)
    print("Output projection system :", crs_output.name)

    transformer = Transformer.from_crs(crs_input, crs_output, always_xy=True)
    Xold = varX
    Yold = varY
    rot_varX, rot_varY = transformer.transform(Xold, Yold, radians=False)

    return rot_varX, rot_varY
