#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#%%
import sys

sys.path.append("./")
from interpolation_function import interpolation
from indata_function import indata
from scipy.interpolate import griddata
import numpy as np
import matplotlib.pyplot as plt

#%%
def replacementValues(
    missVal_param,
    H_bathy,
    missingBathyPath,
    val_lim_y_raw,
    missMore,
    partition,
    Nx,
    Ny,
    extension,
    InterpMethod,
    X,
    Y,
    n,
    delimPonctMisssVal,
    web,
):

    """
    This function can replace the missing values with two options:
    1 - Using an other bathymetry
    2 - By approximation of the point
    With a big number of missing values, preferred to use other bathymetry data

    Parameters
    ----------
    missVal_param : int
        defined the method to refill the hole on bathymetry.
    H_bathy : array
        bathymetry data.
    missingBathyPath : str
        path to load the bathymetry used for replaced missing values.
    val_lim_y_raw : int
        inferior limite under we don't calculate/modify values.
    missMore : int
        to define the method of fill when missVal_param is choice to use an
        other bathymetry.
    partition : int
        define the number of partitial area to reduce the RAM use.
    Nx : float
        meshgrid number according to x axis.
    Ny : float
        meshgrid number according to y axis.
    extension : str
        extension of the initial file .txt/.xyz/.grd/.nc.
    InterpMethod : str
        determine the method used for interpolate.
    X : array
        matrix of point position according to x axis
    Y : array
        matrix of point position according to y axis
    n : int
        determine the number of point used in 1 direction to approximate
        the values of the points.
    delimPonctMisssVal : str
        give the argement between two data row in the file
    web:
        if the bathymetry is on the web
    Returns
    -------
    H_bathy : array
        new bathymetry with values on each point of the grid.
    """

    missVal = np.argwhere(np.isnan(H_bathy))
    if missVal_param == 1:
        print("missVal :", len(missVal))
        # if missing values exist then complete with large scale bathymetry
        if np.size(missVal) >= 1:
            print("At least 1 missing value")

            # x_raw2, y_raw2, z_raw2 = np.loadtxt(missingBathyPath,
            #                                     delimiter=delimPonctMisssVal, usecols=(0, 1, 2), unpack=True)
            x_raw2, y_raw2, z_raw2 = indata(
                missingBathyPath, extension, delimPonctMisssVal, web
            )
            fig = plt.figure(
                num=4, figsize=(10, 10), dpi=100, facecolor="w", edgecolor="w"
            )
            ax = fig.add_subplot()
            if extension == "grd":
                surf = ax.contourf(y_raw2, x_raw2, z_raw2, cmap="gist_earth")
            else:
                surf = ax.scatter(
                    y_raw2,
                    x_raw2,
                    c=z_raw2,
                    vmin=np.min(z_raw2),
                    vmax=0,
                    cmap="gist_earth",
                )

                plt.grid()
            plt.ylim([np.min(x_raw2), np.max(x_raw2)])
            ax.invert_yaxis()
            plt.ylabel("Cross-shore  [m]", size=12)
            plt.xlabel("Long-shore  [m]", size=12)
            plt.colorbar(surf, spacing="uniform", orientation="horizontal")
            plt.title("missing values contour")
            plt.show(block=False)
            z_raw2[np.where(y_raw2 < val_lim_y_raw)] = np.NaN

            if len(missVal) >= missMore:
                print(
                    "Replacement values by an other Bathymetry:", missingBathyPath
                )
                X = x_raw2.reshape(Ny, Nx)
                Y = y_raw2.reshape(Ny, Nx)
                H_bathy = interpolation(
                    np.zeros((Ny, Nx)),
                    partition,
                    Nx,
                    Ny,
                    extension,
                    x_raw2,
                    y_raw2,
                    z_raw2,
                    InterpMethod,
                    X,
                    Y,
                )
            else:
                print(
                    "Replacement ponctual values by other bathymetry:",
                    missingBathyPath,
                )
                H_bathy[missVal] = griddata(
                    (x_raw2, y_raw2),
                    z_raw2,
                    (X[missVal], Y[missVal]),
                    InterpMethod,
                )
            H_bathy[H_bathy == 0] = np.NaN
            missVal = np.argwhere(np.isnan(H_bathy))
            print("missing values stay:", missVal)
    if missVal_param == 2:
        print("missVal :", len(missVal))
        # # rplcVal == replace value
        rplcVal = 0
        rplcValx = 0
        rplcValy = 0
        passage = 0
        while len(missVal) >= 1:
            H_bathy = np.nan_to_num(H_bathy)
            for k in range(len(missVal)):
                if missVal[k, 0] > n:
                    if H_bathy[missVal[k, 0] - (1), missVal[k, 1]] != 0:
                        for kk in range(n):
                            rplcVal = (
                                rplcVal
                                + H_bathy[missVal[k, 0] - (1 + kk), missVal[k, 1]]
                                - H_bathy[missVal[k, 0] - (2 + kk), missVal[k, 1]]
                            )

                        rplcValx = (
                            rplcVal / n
                            + H_bathy[missVal[k, 0] - 1, missVal[k, 1]]
                        )
                        rplcVal = 0
                    elif (
                        missVal[k, 0] + (1) < len(H_bathy[:, 0])
                        and H_bathy[missVal[k, 0] + (1), missVal[k, 1]] != 0
                    ):
                        for kk in range(n):
                            rplcVal = (
                                rplcVal
                                + H_bathy[missVal[k, 0] + (1 + kk), missVal[k, 1]]
                                - H_bathy[missVal[k, 0] + (2 + kk), missVal[k, 1]]
                            )

                        rplcValx = (
                            rplcVal / n
                            + H_bathy[missVal[k, 0] - 1, missVal[k, 1]]
                        )
                        rplcVal = 0
                    else:
                        rplcValx = "nan"
                        rplcVal = 0
                else:
                    if H_bathy[missVal[k, 0] + (1), missVal[k, 1]] != 0:
                        for kk in range(n):
                            rplcVal = (
                                rplcVal
                                + H_bathy[missVal[k, 0] + (1 + kk), missVal[k, 1]]
                                - H_bathy[missVal[k, 0] + (2 + kk), missVal[k, 1]]
                            )

                        rplcValx = (
                            rplcVal / n
                            + H_bathy[missVal[k, 0] + 1, missVal[k, 1]]
                        )
                        rplcVal = 0
                    else:
                        rplcValx = "nan"
                        rplcVal = 0
                if missVal[k, 1] > n:
                    if H_bathy[missVal[k, 0], missVal[k, 1] - (1)] != 0:
                        for kk in range(n):
                            rplcVal = rplcVal + (
                                H_bathy[missVal[k, 0], missVal[k, 1] - (1 + kk)]
                                - H_bathy[missVal[k, 0], missVal[k, 1] - (2 + kk)]
                            )
                        rplcValy = (
                            rplcVal / n
                            + H_bathy[missVal[k, 0], missVal[k, 1] - 1]
                        )
                        rplcVal = 0
                    elif (
                        missVal[k, 1] + (1) < len(H_bathy[0, :])
                        and H_bathy[missVal[k, 0], missVal[k, 1] + (1)] != 0
                    ):
                        for kk in range(n):
                            rplcVal = rplcVal + (
                                H_bathy[missVal[k, 0], missVal[k, 1] + (1 + kk)]
                                - H_bathy[missVal[k, 0], missVal[k, 1] + (2 + kk)]
                            )

                        rplcValy = (
                            rplcVal / n
                            + H_bathy[missVal[k, 0], missVal[k, 1] - 1]
                        )
                        rplcVal = 0
                    else:
                        rplcValy = "nan"
                        rplcVal = 0
                else:
                    if H_bathy[missVal[k, 0], missVal[k, 1] + (1)] != 0:
                        for kk in range(n):
                            rplcVal = rplcVal + (
                                H_bathy[missVal[k, 0], missVal[k, 1] + (1 + kk)]
                                - H_bathy[missVal[k, 0], missVal[k, 1] + (2 + kk)]
                            )
                        rplcValy = (
                            rplcVal / n
                            + H_bathy[missVal[k, 0], missVal[k, 1] + 1]
                        )
                        rplcVal = 0
                    else:
                        rplcValy = "nan"
                if rplcValx == "nan" and rplcValy != "nan":
                    rplcValF = 0
                elif rplcValx != "nan" and rplcValy == "nan":
                    rplcValF = 0
                elif rplcValx != "nan" and rplcValy != "nan":
                    rplcValF = (rplcValx + rplcValy) / 2
                elif rplcValx == "nan" and rplcValy == "nan":
                    rplcValF = 0
                H_bathy[missVal[k, 0], missVal[k, 1]] = (
                    H_bathy[missVal[k, 0], missVal[k, 1]] + rplcValF
                )
            rplcVal = 0
            rplcValx = 0
            rplcValy = 0
            missVal = np.zeros(())
            H_bathy[H_bathy == 0] = np.NaN
            missVal = np.argwhere(np.isnan(H_bathy))
            passage += 1
        print(passage)

    return H_bathy
