#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#%%
import numpy as np

#%%
def rotation(X, Y, axisrotation, Xorig, Yorig, trigoSense):
    """
    This function induce rotation to modify position of one point or a vector of point.
    The function appears two times, in the biginning and at the end.

    Parameters
    ----------
    X : array
        matrix of point position according to x axis
        or just a x point position
    Y : array
        matrix of point position according to y axis
        or just a y point position
    axisrotation : int
        defined the rotation angle in degres.
    Xorig : array
        x position used to induce rotation
    Yorig : array
        y position used to induce rotation
    trigoSense : int
        define the sense of rotation (trigonometric def)

    Returns
    -------
    X : array
        new rotation matrix/point : position of points according to x axis
    Y : array
        new rotation matrix/point : position of points according to y axis
    """

    Xold = X
    Yold = Y
    if trigoSense == 0:
        print(
            "Apply rotation in trigonometric direction with angle :",
            axisrotation,
            " degrees",
        )
        axisrotationR = axisrotation * np.pi / 180.0
        X = (
            Xorig
            + (Xold - Xorig) * np.cos(axisrotationR)
            - (Yold - Yorig) * np.sin(axisrotationR)
        )
        Y = (
            Yorig
            + (Xold - Xorig) * np.sin(axisrotationR)
            + (Yold - Yorig) * np.cos(axisrotationR)
        )

    else:
        print(
            "Apply rotation in anti-trigonometric direction with angle :",
            axisrotation,
            " degrees",
        )
        axisrotationR = axisrotation * np.pi / 180.0
        X = (
            Xorig
            + (Xold - Xorig) * np.cos(axisrotationR)
            + (Yold - Yorig) * np.sin(axisrotationR)
        )
        Y = (
            Yorig
            - (Xold - Xorig) * np.sin(axisrotationR)
            + (Yold - Yorig) * np.cos(axisrotationR)
        )

    return X, Y
