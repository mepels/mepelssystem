#!/usr/bin/env python3
# -*- coding: utf-8 -*-


#%%
from netCDF4 import Dataset
import numpy as np

#%%
def indata(inputPath, extension, delimponct, web):
    """
    Args:
        inputPaht: str
            file location
        extension: .txt/.xyz/.grd/.nc
            Bathymetry file extension
        delimponct: str
            ponctuation between column (.txt or .xyz)
        web: str
            True (web path) or False (local path)

    Returns:
        x_raw: Array of points according to x direction\n
        y_raw: Array of points according to y direction\n
        z_raw: Array of values for the mesh defined by x_raw and y_raw\n
    """
    # load  variable focntion of extension
    if extension == "txt" or extension == "xyz":
        x_raw, y_raw, z_raw = np.loadtxt(
            inputPath,
            delimiter=delimponct,
            usecols=(0, 1, 2),
            unpack=True,
        )

    if extension == "grd":
        nc_fil = Dataset(inputPath)
        Xread = nc_fil.variables["x"][:]
        Yread = nc_fil.variables["y"][:]
        z_raw = nc_fil.variables["z"][:, :]
        [x_raw, y_raw] = np.meshgrid(Xread.data, Yread.data)

    if extension == "nc":
        nc_fil = Dataset(inputPath)
        x_raw = nc_fil.variables["X"][:, :]
        y_raw = nc_fil.variables["Y"][:, :]
        z_raw = nc_fil.variables["h"][:, :]

    return x_raw, y_raw, z_raw
