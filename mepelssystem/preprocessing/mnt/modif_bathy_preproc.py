#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Sep 10 14:28:07 2021

@author: ferraris6s
"""

import numpy as np
import matplotlib.pyplot as plt
import yaml
import time
from scipy.optimize import curve_fit
from netCDF4 import Dataset
from scipy.ndimage.filters import gaussian_filter
import matplotlib.gridspec as gridspec
import os
from urllib.request import urlopen
import sys

sys.path.append("./libs/")
from indata_function import indata
from replacementValues_function import replacementValues

#%%
def func(x, a, b, c):
    return a * pow(x, b) + c


def gaussian(x, mu, sigma, heigth):
    return heigth * np.exp((-1 / 2) * ((x - mu) / sigma) ** 2)


def bar_croissant(X, Y, Ny, nbs_croissant, ampl, pos_croissant, t, f):
    return ampl * np.sin(2 * np.pi * f * t) + pos_croissant


#%%         choice configuration
"""
    Choice study and load data from the configuration file
"""
beach_letter = input(
    "Beach you can used :"
    "\n biscarrosse as 1,"
    "\n trucvert as 2,"
    "\n gravelines as 3,"
    "\n 4: other,"
    "\n Enter the corresponding number : "
)
if beach_letter == "1":
    beach_config = "biscarrosse"
elif beach_letter == "2":
    beach_config = "trucvert"
elif beach_letter == "3":
    beach_config = "gravelines"
elif beach_letter == "4":
    beach_config = input("Enter the yaml file name (without config.yaml):")
with open(r"./configurations/" + beach_config + "_configuration.yaml") as file:
    cl = yaml.full_load(file)
print("beach config choose: ", beach_config)
"""
    Importation data with the configuration file indication
    normally the bathy file is a natcdf file but it could also be a txt file
"""
web = str(False)
# .nc
X, Y, h = indata(cl["data_path_saved"]["inputPath"], "nc", "", web)
h = -h  # Z neg
size_matrix = np.shape(X)
Ny = size_matrix[0]
Nx = size_matrix[1]
print("using .nc data \n ")
Xmin = np.min(X)
Xmax = np.max(X)
Ymin = np.min(Y)
Ymax = np.max(Y)
cmapBathy = cl["parameter"]["colormaps"]
#%% Params0
# =============================================================================
# plot raw bathymetry
# =============================================================================
#%%               Profile
# slide at the center of y direction along x direction (cross-shore profil)
"""
    compute the mean cross-shore profile and plot it
"""
xpos = X[0, :]
meanProfile = np.zeros(Nx)
profile = np.zeros(Ny)
meanProfile = np.mean(h, axis=0)
#%%            depth closure
"""
    determination of the DOC point with data input on configuration file
    One methode is used but it's possible to use the others comment formulation
"""
g = 9.81
T12_Y = cl["storm"]["T12_Y"]
H12_Y = cl["storm"]["H12_Y"]
T_mean = cl["mean"]["T_mean"]
H_mean = cl["mean"]["H_mean"]
D_50 = float(cl["sand_size"]["D50"])
doc_Hallermeier_storm = -(
    2.28 * H12_Y - 68.5 * ((H12_Y ** 2) / (g * (T12_Y ** 2)))
)
# d_l2 = -(1.75 * H12_Y - 57.9 * ((H12_Y**2) / (g * (T12_Y**2))))
# Dcc1 = -6.75 * H_mean
# Dcc2 = -8.9 * H_mean
# https://apps.dtic.mil/sti/pdfs/ADA578584.pdf
print("\n Hallermeier 12h/Y :", doc_Hallermeier_storm)
# print('Birkemeier 12h/Y:', d_l2)
# print('Dc1', Dcc1)
# print('Dc2', Dcc2)
# plt.axhline(doc_Hallermeier_storm, label='doc : Hallermeier (strom event)')
# plt.plot(xpos, d_ll2, 'y*', label='hdc : Birkemeier - 12h/Y')
# plt.plot(xpos, Dc1, 'k--', label="hdc : Houston's - mean data")
# plt.plot(xpos, Dc2, 'b--', label='hdc : Hallermeier - mean data, ')
#%%                 Zero level
"""
    search some interest points
    HTl, zero with is defintion, zero hydrographique, LTL
"""
zero_level_val = 0
def_zero = cl["sealevel"]["defined_zero"]
if def_zero == 0:
    def_zero = str("IGN69")
elif def_zero == 1:
    def_zero = str("middle tide")
elif def_zero == 2:
    def_zero = str("zero hydrographic")
if cl["sealevel"]["diff_to_zero_hydro"] != "NaN":
    zero_hydro_val = zero_level_val + cl["sealevel"]["diff_to_zero_hydro"]
    # plt.axhline(zero_hydro_val, color='gray', linestyle='-.', label='zero_hydrographique')
if cl["sealevel"]["HTL"] != "NaN":
    HTL_val = zero_level_val + cl["sealevel"]["HTL"]
    z_max = HTL_val + cl["sealevel"]["diff_to_zero_hydro"]
    # plt.axhline(z_max, color='red', linestyle='-.', label='HTL')
else:
    print("Tide heigth THL unknow")
    z_max = np.max(profile) * cl["sealevel"]["HTL_approx"]
if cl["sealevel"]["LTL"] != "NaN":
    LTL_val = (
        zero_level_val
        + cl["sealevel"]["LTL"]
        + cl["sealevel"]["diff_to_zero_hydro"]
    )
    # plt.axhline(LTL_val, color='blue', linestyle='-.', label='LTL ')
else:
    print("Tide height LTL unknow")
# plt.plot(X[0, :], meanProfile, label='mean cross-shore profile', color='black')
#%%       replacement profile
# utilisation du critère de Hallermeier moyenné
"""
    definition of the modification area (with morphodynamique effect) with the 
    Doc and the max point of water or a porcentage of the elevation of the bathy
"""
assert (
    doc_Hallermeier_storm > meanProfile[0]
), "H value to big, or bathymetry to small"
k = 0
while meanProfile[k] <= doc_Hallermeier_storm:
    k += 1
k_x_min = k
hr_x_min = meanProfile[k_x_min]
if np.max(meanProfile) < z_max:
    k_x_max = np.argmax(meanProfile)
    hr_x_max = meanProfile[k_x_max]
else:
    k = 0
    while meanProfile[k] <= z_max:
        k += 1
    k_x_max = k
    hr_x_max = meanProfile[k_x_max]
length_modif = hr_x_max - hr_x_min
#%%               DEAN PROFILE
"""
    defined the dean profile on the profile modify (see upper)
    fit the actual profile with a dean profile to replace it
"""
Profile_short = meanProfile[k_x_min:k_x_max]
xpos_short = xpos[k_x_max] - xpos[k_x_min:k_x_max]
k = 0
h_reflec = np.zeros((Ny, Nx))
profile_slice = np.zeros(Nx)
while k < Ny:
    popt, pcov = curve_fit(func, xpos_short, Profile_short, check_finite=False)
    zFitted = func(xpos_short, *popt)
    Profile = h[int(k), :]
    #    h_reflec[k, 0: hr_x_min] = Profile[0: hr_x_min]
    profile_slice[k_x_max::] = Profile[k_x_max::]
    profile_slice[0:k_x_min] = Profile[0:k_x_min]
    #    Profile_short = Profile[hr_x_min: hr_x_max]

    profile_slice[k_x_min:k_x_max] = zFitted
    h_reflec[k, k_x_min:k_x_max] = zFitted
    for kk in range(cl["fit_dissipative"]["nbs_fit"]):
        profile_slice = gaussian_filter(
            profile_slice, sigma=cl["fit_dissipative"]["sigma"], mode="nearest"
        )
    h_reflec[k, :] = profile_slice
    k += 1
zminPlot = np.nanmin(h_reflec)
zmaxPlot = np.nanmax(h_reflec)
h_reflec = replacementValues(
    2,
    h_reflec,
    "NaN",
    "NaN",
    "NaN",
    "NaN",
    "NaN",
    "NaN",
    "NaN",
    "NaN",
    "NaN",
    "NaN",
    4,
    "NaN",
    web,
)
see = "NaN"
while see != "yes" or see != "no":
    see = input(
        "Do you want to see the Dean profile on the all the cross-short profiles, 3D plot ? y/n: \n "
    )
    if see == "y":
        fig1 = plt.figure(num=3)
        ax = fig1.gca(projection="3d")
        surf = ax.plot_surface(
            Y,
            X,
            h_reflec,
            cmap=cmapBathy,
            rstride=1,
            cstride=1,
            vmin=zminPlot,
            vmax=zmaxPlot,
        )
        ax.set_zlim(zminPlot, zmaxPlot)
        plt.colorbar(surf, shrink=0.5, aspect=20, orientation="horizontal")
        plt.xlabel("Long-shrore [m]", size=12)
        plt.ylabel("Cross-shore [m]", size=12)
        plt.title("Fit with Dean profile", size=12)
        plt.show()
        break
    else:
        break
#%% ADD BAR
"""
   adding bar(s) and croissant(s)
"""
fig7 = plt.figure(num=7)
# implémentation croissant
min_long_shore = np.min(Y)
max_long_shore = np.max(Y)
t = np.linspace(min_long_shore, max_long_shore, Ny)
long_shore_width = abs(max_long_shore) + abs(min_long_shore)
k = 0
kk = 0
h_modelling = np.zeros((np.shape(h_reflec)))
filename = os.path.basename(cl["data_path_saved"]["inputPath"])
file, extension = os.path.splitext(filename)
"""
    zero bar
"""
if cl["sandbar"]["number_of_bar"] == 0:
    h_modelling = h_reflec
    file = file + str("Dean")
    pass
    """
    adding outerbar with or without croissant
    """
elif cl["sandbar"]["number_of_bar"] == 1 or cl["sandbar"]["number_of_bar"] == 2:
    sigma_bar1 = float(cl["sandbar"]["outerbar"]["width"] / 3)
    print(
        "Outer bar modelisation",
        "\n" "width:",
        cl["sandbar"]["outerbar"]["width"],
        "\n" "height:",
        cl["sandbar"]["outerbar"]["height"],
        "\n" "position:",
        cl["sandbar"]["outerbar"]["position"],
        "\n",
    )
    while k < Ny:
        # take each cross-shore profile according to long-shore orientation (S/N)
        cross_shore = X[int(k), :]
        cross_profile = h_reflec[int(k), :]  # profile reflective create upper
        bar1_profile = np.zeros((np.shape(h_reflec)))
        outbar = np.zeros(Nx)
        # creation bar by using a gaussian mathematical
        if cl["sandbar"]["outerbar"]["lambda"] == "NaN":
            outbar[:] = gaussian(
                cross_shore,
                cl["sandbar"]["outerbar"]["position"],
                sigma_bar1,
                cl["sandbar"]["outerbar"]["height"],
            )
        else:
            nbs_croiss = long_shore_width / cl["sandbar"]["outerbar"]["lambda"]
            f = 1 / (long_shore_width / nbs_croiss)
            print("Add croissant on outer bar", "\n" "wave length: \t", f, "\n")
            func_croissant = bar_croissant(
                X,
                Y,
                Ny,
                nbs_croiss,
                cl["sandbar"]["outerbar"]["width"],
                cl["sandbar"]["outerbar"]["position"],
                t,
                f,
            )
            outbar[:] = gaussian(
                cross_shore,
                func_croissant[k],
                sigma_bar1,
                cl["sandbar"]["outerbar"]["height"],
            )
        bar1_profile[k, :] = cross_profile[:] + outbar[:]
        h_modelling[k, :] = bar1_profile[k, :]
        k += 1
        """
        adding innerbar with or without croissant
        """
    if cl["sandbar"]["number_of_bar"] == 2:
        sigma_bar2 = float(cl["sandbar"]["innerbar"]["width"] / 3)
        print(
            "Inner bar modelisation",
            "\n" "width:",
            cl["sandbar"]["innerbar"]["width"],
            "\n" "height:",
            cl["sandbar"]["innerbar"]["height"],
            "\n" "position:",
            cl["sandbar"]["innerbar"]["position"],
            "\n",
        )
        while kk < Ny:
            # take each cross-shore profile according to long-shore orientation (S/N)
            cross_shore = X[int(kk), :]
            cross_profile = h_modelling[int(kk), :]
            inbar = np.zeros(Nx)
            # creation bar by using a gaussian mathematical
            if cl["sandbar"]["innerbar"]["lambda"] == "NaN":
                inbar[:] = gaussian(
                    cross_shore,
                    cl["sandbar"]["innerbar"]["position"],
                    sigma_bar2,
                    cl["sandbar"]["innerbar"]["height"],
                )
            else:
                nbs_croiss = (
                    long_shore_width / cl["sandbar"]["innerbar"]["lambda"]
                )
                f = 1 / (long_shore_width / nbs_croiss)
                print(
                    "Add croissant on inner bar", "\n" "wave length: \t", f, "\n"
                )
                func_croissant = bar_croissant(
                    X,
                    Y,
                    Ny,
                    nbs_croiss,
                    cl["sandbar"]["innerbar"]["width"],
                    cl["sandbar"]["innerbar"]["position"],
                    t,
                    f,
                )
                inbar[:] = gaussian(
                    cross_shore,
                    func_croissant[kk],
                    sigma_bar2,
                    cl["sandbar"]["innerbar"]["height"],
                )
            h_modelling[kk, :] = h_modelling[kk, :] + inbar[:]
            kk += 1
"""
modify the file name fonction of the additionnal bar(s) on the dean profile
"""
if (
    cl["sandbar"]["number_of_bar"] == 1
    and cl["sandbar"]["outerbar"]["lambda"] != "NaN"
):
    file = file + str("Dean_1bar_croissant") + cl["sandbar"]["outerbar"]["lambda"]
elif (
    cl["sandbar"]["number_of_bar"] == 1
    and cl["sandbar"]["outerbar"]["lambda"] == "NaN"
):
    file = file + str("Dean_1bar")
elif (
    cl["sandbar"]["number_of_bar"] == 2
    and cl["sandbar"]["outerbar"]["lambda"] != "NaN"
):
    file = (
        file + str("Dean_2bars_croissant") + cl["sandbar"]["outerbar"]["lambda"]
    )
elif (
    cl["sandbar"]["number_of_bar"] == 2
    and cl["sandbar"]["outerbar"]["lambda"] == "NaN"
):
    file = file + str("Dean_2bars")
ax = fig7.gca(projection="3d")
surf = ax.plot_surface(
    Y,
    X,
    h_modelling,
    cmap=cmapBathy,
    rstride=1,
    cstride=1,
    vmin=zminPlot,
    vmax=zmaxPlot,
)
ax.set_zlim(zminPlot, zmaxPlot)
plt.colorbar(surf, shrink=0.5, aspect=20, orientation="horizontal")
plt.title("Remodelling Dean profile")
plt.xlabel("Long-shrore [m]", size=12)
plt.ylabel("Cross-shore [m]", size=12)
plt.legend()
plt.show(block=False)
# trying maybe to remove or include on the code
gauss2 = gaussian(h_modelling[int(Ny / 2), :], 880, 33, 2)
profile_2gauss = h_modelling[int(Ny / 2), :] + gauss2[:]
# visualisation 2D
plt.figure(num=8)
plt.plot(
    X[int(Ny / 2), :],
    h[int(Ny / 2), :],
    color="black",
    linestyle="-",
    label="center profile",
)
plt.plot(
    X[int(Ny / 2), :],
    h_modelling[int(Ny / 2), :],
    color="red",
    linestyle="-",
    label="Dean mean cross shore profile",
)
if cl["sandbar"]["outerbar"]["position"] != "nan":
    plt.axvline(
        cl["sandbar"]["outerbar"]["position"], linestyle="-.", label="outer bar"
    )
if cl["sandbar"]["innerbar"]["position"] != "nan":
    plt.axvline(
        cl["sandbar"]["innerbar"]["position"], linestyle="-.", label="inner bar"
    )
plt.xlabel("Cross-shore [m]", size=12)
plt.ylabel("Elevation [m]", size=12)
plt.title("Re-modelling cross-shore profile 2D ", size=12)
plt.legend(loc="upper left", prop={"size": 8})
plt.show()
#%%       SHORELINE

# """
# search the shoreline according to the maximal water elevation (SHOM def)
# """
# def find_nearest(array, value):
#     array = np.asarray(array)
#     idx = (np.abs(array - value)).argmin()
#     return idx
# plt.figure(num=9)
# plt.contourf(Y, X, h)

# k = 0
# shoreline = np.zeros((2,Ny))
# while k < Ny:
#     slice_profile = h[k, :]
#     pos = find_nearest(slice_profile, hr_x_max)
#     shoreline[0, k] = X[k, pos]
#     shoreline[1, k] = Y[k, 0]
#     if k == 0:
#         plt.plot(Y[k, 0], X[k, pos], '^r', label='shorline points')
#         plt.legend(loc='upper left', prop={'size':8})
#     else:
#         plt.plot(Y[k, 0], X[k, pos], '^r')
#     k += 1
# plt.title('HTL position on the bathymerty', size=12)
# plt.xlabel('Long-shore [m]', size=12)
# plt.ylabel('Cross-shore [m]', size=12)
# plt.ylim((Xmax, Xmin))
# plt.show(block=False)


#%%
#   FILTER BATHYMETRY
"""
    possibility of filter bathymetry
    possible to try and give up modificatin if there not good
"""
gs1 = gridspec.GridSpec(1, 2)
gs1.update(left=0.10, right=0.95, top=0.95, bottom=0.3, wspace=0.250, hspace=0.5)
rep = "NaN"
while rep != "y" or rep != "n":
    plt.figure(num=10)
    plt.contourf(Y, X, h_modelling, cmap=cmapBathy)
    plt.title("Bathymetry without filter", size=12)
    plt.xlabel("Long-shore [m]", size=12)
    plt.ylabel("Cross-shore [m]", size=12)
    plt.xlim([Ymin, Ymax])
    plt.ylim([Xmax, Xmin])
    plt.colorbar()
    plt.show()
    rep = input("Filter bathymetry ? y/n \t")
    if rep == "y":
        std_filter = input(
            "standard deviations of the Gaussian filter (default is 10): \t"
        )
        h = gaussian_filter(h_modelling, float(std_filter))
        ax1 = plt.subplot(gs1[0, 0])
        ax1.contourf(Y, X, h_modelling, cmap=cmapBathy)
        ax1.set_title("Bathymetry before filter act", size=12)
        ax1.set_xlabel("Long-shore [m]", size=12)
        ax1.set_ylabel("Cross-shore [m]", size=12)
        ax1.set_xlim([Ymin, Ymax])
        ax1.set_ylim([Xmax, Xmin])
        ax2 = plt.subplot(gs1[0, 1])
        ax2.contourf(Y, X, h, cmap=cmapBathy)
        ax2.set_title("Resulting filter on bathymetry", size=12)
        ax2.set_xlim([Ymin, Ymax])
        ax2.set_yticklabels([])
        ax2.set_ylim([Xmax, Xmin])
        plt.show()
        rep2 = "NaN"
        while rep2 != "y" or rep2 != "N":
            rep2 = input("Filter is good ? y/N \t")
            if rep2 == "y":
                break
            else:
                break
        if rep2 == "y":
            break
    else:
        h = h_modelling
        break

del h_modelling
#%%  SAVE DATA
h_modelling = -h
"""
saving data with .nc extention
possibility to create a file if a txt file was used a the beginning
"""
toexclude = ["ExcludeVar1", "ExcludeVar2"]
with Dataset(cl["data_path_saved"]["inputPath"]) as src, Dataset(
    os.path.dirname(cl["data_path_saved"]["inputPath"]) + "/" + file + extension,
    "w",
    format="NETCDF4",
) as dst:
    # copy global attributes all at once via dictionary
    dst.setncatts(src.__dict__)
    # # copy dimensions
    for name, dimension in src.dimensions.items():
        dst.createDimension(
            name, (len(dimension) if not dimension.isunlimited() else None)
        )
    # copy all file data except for the excluded
    for name, variable in src.variables.items():
        if name not in toexclude:
            x = dst.createVariable(name, variable.datatype, variable.dimensions)
            dst[name][:] = src[name][:]
            # copy variable attributes all at once via dictionary
            dst[name].setncatts(src[name].__dict__)
    var = dst.createVariable("h_modelling", "f8", ("y", "x"))
    var[:, :] = h_modelling
    var.long_name = "Bathymetry with dean profile"
    var.units = "meter"
    dst.depth_of_closure_cross_shore = hr_x_min
    dst.maximum_heigth_take_for_profile_modification = hr_x_max
    dst.length_of_modification_profil_since_DOC = length_modif
    dst.norm_for_0_level = def_zero
    # dst.type_of_beach_restructured = type_beach
    if cl["sealevel"]["diff_to_zero_hydro"] == "NaN":
        dst.zero_hydro_height = "Unknow"
    else:
        dst.zero_hydro_height = 0 + cl["sealevel"]["diff_to_zero_hydro"]
    dst.Period_DOC = T12_Y
    dst.Height_wave_DOC = H12_Y

print("script endding")
