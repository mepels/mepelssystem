#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#%%
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm
import os
import yaml
from scipy.ndimage.filters import gaussian_filter
from matplotlib import patches
import time
from netCDF4 import Dataset
from pyproj import CRS
import matplotlib.gridspec as gridspec


from urllib.request import urlopen

import sys

sys.path.append("./libs/")
from indata_function import indata
from out_data_function import out_data
from rotation_function import rotation
from projection_function import projection
from interpolation_function import interpolation
from replacementValues_function import replacementValues

start_time = time.time()
#%%
"""
open:
    Go read the .yaml file to follow the indicate path to the bathymetry
    The .yaml read depend on the beach choice
"""
beach_letter = input('Select your beach:'
                      '\n 1: biscarrosse,'
                      '\n 2: trucvert,'
                      '\n 3: gravelines,'
                      '\n 4: lip,'
                      '\n 5: sands,'
                      '\n 6: other,'
                      '\n Enter number : ')
if beach_letter == '1':
    beach_config = 'biscarrosse'
elif beach_letter == '2':
    beach_config = 'trucvert'
elif beach_letter == '3':
    beach_config = 'gravelines'
elif beach_letter == '4':
    beach_config = 'lip'
elif beach_letter == '5':
	    beach_config = 'sands'
elif beach_letter == '6':
    beach_config= input('Enter the yaml file name (without config.yaml):')

with open(r"./configurations/" + beach_config + "_configuration.yaml") as file:
    cl = yaml.full_load(file)
print("Selected beach configuration: ", beach_config)

#%% PLOTTING
"""
    define colormaps and size of all plots
"""
cmapBathy = cl["parameter"]["colormaps"]
figwidth = 10
figheight = 10
# In case one figure plot
gs1 = gridspec.GridSpec(
    1,
    1,
    figure=None,
    left=0.175,
    bottom=0.15,
    right=0.92,
    top=0.95,
    wspace=0.125,
    hspace=0.25,
)
# In case two figures plots
gs2 = gridspec.GridSpec(
    1,
    2,
    figure=None,
    left=0.175,
    bottom=0.15,
    right=0.92,
    top=0.95,
    wspace=0.125,
    hspace=0.25,
)
#%% import data
"""
    Function of the input on the configuration file, the function under go search 
    the file to read and use for study
"""
if cl["data_path"]["web_path"] != " ":
    extension = cl["data_path"]["extension"]
    if extension == "grd" or "nc":
        readfile = cl["data_path"]["web_path"]
    else:
        r = urlopen(cl["data_path"]["web_path"])
        readfile = r
        del r
    web = str(True)
else:
    readfile = cl["data_path"]["local_Path"]
    web = str(False)
    extension = readfile.split(".")[-1]  # search file extension

print("Read bathymetry file :", readfile)
print("File extension :", extension)
print("Original projection system :", CRS(cl["projection"]["coorIn_A"]).name)

x_raw, y_raw, z_raw = indata(
    readfile, extension, cl["data_path"]["delimponct"], web
)
#%% Params0
# =============================================================================
# plot raw bathymetry
# =============================================================================
"""
    1 visualization of the bathymetry
    without modification
"""
if cl["changeStudyArea"]["changeArea"] == 0:
    fig1 = plt.figure(
        num=1,
        figsize=(figwidth, figheight),
        dpi=100,
        facecolor="w",
        edgecolor="w",
    )
    ax = fig1.add_subplot()
    if extension == "grd":
        surf = ax.contourf(x_raw, y_raw, z_raw, cmap=cmapBathy)
        plt.title("Raw bathymetry", size=12)
        plt.ylabel("Y axis", size=12)
        plt.xlabel("X axis", size=12)
    else:
        surf = ax.scatter(
            x_raw,
            y_raw,
            c=z_raw,
            vmin=np.min(z_raw),
            vmax=np.max(z_raw),
            cmap=cmapBathy,
        )
        plt.grid()
        plt.title("Raw bathymetry", size=12)
        plt.ylabel("Y axis", size=12)
        plt.xlabel("X axis", size=12)
    plt.colorbar(surf, spacing="uniform", orientation="horizontal")
    plt.show(block=False)
"""
    defined study area
    since the configuration file or direcly into the code (interface)
"""
rep = "NaN"
while rep != "y" or rep != "n":
    if cl["changeStudyArea"]["changeArea"] == 0 or rep == "n":
        Xorig = input("Enter the x coordinate of origine point: ")
        Yorig = input("Enter the y coordinate of origine point: ")
        Lx = input(
            "Enter the length of the study area in meter (X orientation): "
        )
        Ly = input("Enter the width of the study area in meter (Y orientation): ")
        Xorig = float(Xorig)
        Yorig = float(Yorig)
        Lx = float(Lx)
        Ly = float(Ly)
        origstudy = input(
            "Where is the seaward side ? " "write: up, down, left or right : "
        )
    else:
        Xorig = cl["changeStudyArea"]["Xorig"]
        Yorig = cl["changeStudyArea"]["Yorig"]
        Lx = float(cl["changeStudyArea"]["Lx"])
        Ly = float(cl["changeStudyArea"]["Ly"])
        origstudy = cl["changeStudyArea"]["origstudy"]
    print("Domain origin:")
    print("  Xorig=", Xorig)
    print("  Yorig=", Yorig)
    print("Domain size:")
    print("  Lx=", Lx)
    print("  Ly=", Ly)
    """
        automatic compute of the study area angles
        compute doing on the bathymetry benchmark
    """
    A = np.zeros(2)
    B = np.zeros(2)
    if origstudy == "left":
        A[0] = Xorig
        A[1] = Yorig - Ly / 2
        B[0] = Xorig + Lx
        B[1] = Yorig + Ly / 2
    if origstudy == "up":
        A[0] = Xorig - Lx / 2
        A[1] = Yorig - Ly
        B[0] = Xorig + Lx / 2
        B[1] = Yorig
    if origstudy == "right":
        A[0] = Xorig - Lx
        A[1] = Yorig - Ly / 2
        B[0] = Xorig
        B[1] = Yorig + Ly / 2
    if origstudy == "down":
        A[0] = Xorig - Lx / 2
        A[1] = Yorig
        B[0] = Xorig + Lx / 2
        B[1] = Yorig + Ly
    C = [B[0], A[1]]
    D = [A[0], B[1]]
    # rotation limit study area and visu
    """
    modification banchmark of the study area to be according with the Est/North
    benchmark in the bathymetry benchmark
    """
    Arot = A.copy()
    Arot[0], Arot[1] = rotation(
        A[0],
        A[1],
        cl["changeRotation"]["axisrotation"],
        Xorig,
        Yorig,
        cl["changeRotation"]["trigosense"] + 1,
    )
    fig1 = plt.figure(
        num=101,
        figsize=(figwidth, figheight),
        dpi=100,
        facecolor="w",
        edgecolor="w",
    )
    ax = fig1.add_subplot()
    if extension == "grd":
        surf = ax.contourf(x_raw, y_raw, z_raw, cmap=cmapBathy)
        plt.plot(Xorig, Yorig, "*r", label="Origine")
        plt.title("Raw bathymetry", size=12)
        plt.ylabel("Y axis", size=12)
        plt.xlabel("X axis", size=12)
    else:
        surf = ax.scatter(
            x_raw,
            y_raw,
            c=z_raw,
            vmin=np.min(z_raw),
            vmax=np.max(z_raw),
            cmap=cmapBathy,
        )
        plt.grid()
        plt.plot(Xorig, Yorig, "*r", label="Origine")
        plt.title("Raw bathymetry", size=12)
        plt.ylabel("Y axis", size=12)
        plt.xlabel("X axis", size=12)
    plt.colorbar(surf, spacing="uniform", orientation="horizontal")
    plt.legend()
    if (cl["changeRotation"]["trigosense"] + 1) == 1:
        var = -1
    else:
        var = 1
    rect = patches.Rectangle(
        (Arot[0], Arot[1]),
        Lx,
        Ly,
        angle=cl["changeRotation"]["axisrotation"] * var,
        edgecolor="r",
        color=None,
        fill=None,
    )
    del var
    ax.add_patch(rect)
    plt.show(block=False)
    rep = input("Is the selected area (in red) correct ? [y]/[n] \t")
    if rep == "y":
        del Arot, rep
        break
#%% mesh_grid
"""
    creation of the mesh according to the study area + one mech on each edge
    for limited the edge effects
    Nx and Ny are the number mesh in the meshgrid along the axis X and Y
    dx and dy are the space between two points (width of one sub-mesh)
"""
gridsize = float(cl["parameter"]["gridsize"])
Nx = int(Lx / float(gridsize))
Ny = int(Ly / float(gridsize))
dx = float(gridsize)
dy = float(gridsize)
Xmin = A[0] - float(gridsize)
Ymin = A[1] - float(gridsize)
Xmax = Xmin + Nx * dx + 2 * float(gridsize)
Ymax = Ymin + Ny * dy + 2 * float(gridsize)
val_lim_y_raw = Ymax
"""
for simplify the interpolation, we verify that the number of mesh on X and Y axis are pair.
In case of the number is odd, we round the number at the superior pair value.
"""
if (Nx % 2) != 0:
    Nx += 1
if (Ny % 2) != 0:
    Ny += 1

print("Grid size:")
print("  gridsize=", gridsize)  #!!!!!
print("  Nx = ", Nx)
print("  Ny = ", Ny)

x = np.linspace(Xmin, Xmax, Nx)
y = np.linspace(Ymin, Ymax, Ny)
[X, Y] = np.meshgrid(x, y)
#%% Projection 1 and rotation 1
# first rotation : rotate the rectangle of study and take the values
# on the bathymetry
""" 
    apply the rotation onto the study meshgrid to be similar with the definition
    give upper
"""
if cl["changeRotation"]["rotArea"] == 1:
    X, Y = rotation(
        X,
        Y,
        cl["changeRotation"]["axisrotation"],
        Xorig,
        Yorig,
        cl["changeRotation"]["trigosense"] + 1,
    )
    x = X[0, :]
    y = Y[:, 0]

"""
    projection I to modify the coordinate system if it's necessary to replace
    missing values
"""
if cl["projection"]["before"] == 1:
    X, Y = projection(
        X, Y, cl["projection"]["coorIn_B"], cl["projection"]["coorOut_B"]
    )
    Xorig, Yorig = projection(
        Xorig, Yorig, cl["projection"]["coorIn_B"], cl["projection"]["coorOut_B"]
    )
    x = X[0, :]
    y = Y[:, 0]
#%% découpage de la zone d'étude en sous partie et interpolation
"""
    To put a value on each point of the meshgrid
    interpolation divise the meshgrid according to the user choice to reduce 
    the memory needed to compute
"""
if cl["interpolation"]["backupInterp"] == 0:
    hraw = np.zeros((Ny, Nx))  # global matrix
    h_interp = interpolation(
        hraw,
        cl["interpolation"]["partition"],
        Nx,
        Ny,
        extension,
        x_raw,
        y_raw,
        z_raw,
        cl["interpolation"]["interpMethod"],
        X,
        Y,
    )
    # save data
    if cl["interpolation"]["savehraw"] == 1:
        np.savetxt(
            "./backupInterp/" + beach_config + "_backup.txt",
            np.reshape(h_interp, Nx * Ny),
            newline=os.linesep,
            fmt="%10.5f",
        )
else:
    h_interp = np.loadtxt(
        cl["interpolation"]["folderBackupInterp"] + beach_config + "_backup.txt"
    )
    h_interp = h_interp.reshape(Ny, Nx)
#%% missing_values
"""
    Replace the missing values on the meshgrid
    Could use an other bathymetry to replace some values or the entire bathy
    or used a approximation by gradient if no other bathymetry are available 
    to replace the values on the meshgrid
"""
h_replaced = replacementValues(
    cl["missingValues"]["missVal"],
    h_interp,
    cl["data_path"]["missingBathyPath"],
    val_lim_y_raw,
    cl["missingValues"]["missmore"],
    cl["missingValues"]["partition"],
    Nx,
    Ny,
    extension,
    cl["missingValues"]["interpMethod"],
    X,
    Y,
    cl["missingValues"]["ptsapprox"],
    cl["data_path"]["delimPonctMissVal"],
    web,
)
#%% Projection 2 and rotation 2
"""
    put the bathymetry on the Est/North benchmark to be according to Xbeach
"""
if cl["changeRotation"]["rotArea"] == 1:
    print("rentrer dans la rotation")
    X, Y = rotation(
        X,
        Y,
        cl["changeRotation"]["axisrotation"],
        Xorig,
        Yorig,
        cl["changeRotation"]["trigosense"],
    )
    x = X[0, :]
    y = Y[:, 0]
Xmin = np.min(x)
Xmax = np.max(x)
Ymin = np.min(y)
Ymax = np.max(y)

print("taille de X et Y old ;", np.shape(X), np.shape(Y))

"""
    modify the coordinate system if the coordinate system is not on the metric
    system or if you just want to modify the coordinate system
"""
if cl["projection"]["after"] == 1:
    print(
        "Coordinate",
        cl["projection"]["coorIn_A"],
        "to",
        cl["projection"]["coorOut_A"],
    )
    # for all points
    Xmin, Ymin = projection(
        Xmin, Ymin, cl["projection"]["coorIn_A"], cl["projection"]["coorOut_A"]
    )
    Xmax, Ymax = projection(
        Xmax, Ymax, cl["projection"]["coorIn_A"], cl["projection"]["coorOut_A"]
    )
    X_row = np.linspace(Xmin, Xmax, Nx)
    Y_row = np.linspace(Ymin, Ymax, Ny)
    [X, Y] = np.meshgrid(X_row, Y_row)

    print("taille de X et Y new ;", np.shape(X), np.shape(Y))

    # fig = plt.figure(num=4, figsize=(10, 10), dpi=100, facecolor='w',
    #                 edgecolor='w')
    # ax = fig.add_subplot()
    # plt.title('Bathymetry 2D global coordinate', size=12)
    # surf = ax.contourf(Y, X, h_interp, cmap=cmapBathy)
    # plt.colorbar(surf, spacing='uniform', orientation='horizontal')
    # plt.show(block=False)
#%% filtrage
gs1 = gridspec.GridSpec(1, 2)
gs1.update(left=0.10, right=0.95, top=0.95, bottom=0.3, wspace=0.250, hspace=0.5)
rep = "NaN"
while rep != "y" or rep != "n":
    plt.figure(num=10, figsize=(10, 10), dpi=100, facecolor="w", edgecolor="w")
    plt.contourf(Y, X, h_replaced, cmap=cmapBathy)
    plt.title("Bathymetry before filtering", size=12)
    plt.xlabel("Long-shore [m]", size=12)
    plt.ylabel("Cross-shore [m]", size=12)
    plt.xlim([Ymin, Ymax])
    plt.ylim([Xmax, Xmin])
    plt.colorbar(spacing="uniform", orientation="horizontal")
    plt.show()
    """
        filter to smooth the bathymetry
        it's a loop so it's possible to try some filter and give up if no one 
        interrest you
    """
    rep = input("Filter bathymetry ? [y]/[n] \t")
    if rep == "y":
        fig11 = plt.figure(
            num=11, figsize=(10, 10), dpi=100, facecolor="w", edgecolor="w"
        )
        std_filter = input(
            "standard deviations of the Gaussian filter (default is 10): \t"
        )
        h_filter = gaussian_filter(h_replaced, float(std_filter))
        ax1 = plt.subplot(gs1[0, 0])
        ax1.contourf(Y, X, h_replaced, cmap=cmapBathy)
        ax1.set_title("Bathymetry before filtering", size=12)
        ax1.set_xlabel("Long-shore [m]", size=12)
        ax1.set_ylabel("Cross-shore [m]", size=12)
        ax1.set_xlim([Ymin, Ymax])
        ax1.set_ylim([Xmax, Xmin])
        ax2 = plt.subplot(gs1[0, 1])
        PCM = ax2.contourf(Y, X, h_filter, cmap=cmapBathy)
        # cax2 = ax2.contourf(Y, X, h_filter, cmap=cmapBathy)
        # cbar = fig11.colorbar(cax2, orientation='vertical', fraction=.75)
        ax2.set_title("Bathymetry after filtering", size=12)
        ax2.set_xlabel("Long-shore [m]", size=12)
        ax2.set_xlim([Ymin, Ymax])
        ax2.set_yticklabels([])
        ax2.set_ylim([Xmax, Xmin])
        plt.colorbar(PCM, spacing="uniform", orientation="horizontal")
        plt.show()
        rep2 = "NaN"
        while rep2 != "y" or rep2 != "n":
            rep2 = input("Filter is good ? [y]/[n] \t")
            if rep2 == "y":
                break
            else:
                break
        if rep2 == "y":
            h = h_filter
            del h_filter
            filterBathy = 1
            break
    else:
        h = h_replaced
        filterBathy = 0
        break
del h_replaced
#%% modify coord to be metric
"""
    change the values of alonf the axis (x, y) to create a local benchmark
    where the origine point is (O, O) 
"""
if cl["projection"]["changeCoordGeo"] == 1:
    del Xmin, Xmax, Ymin, Ymax
    # point values are not the same after changeCoord, that while we recalcul
    # the min and max after the condition loop
    xorig = np.min(X)
    yorig = (np.max(Y) + np.min(Y)) / 2
    Xold = X
    Yold = Y
    X = Xold - xorig
    Y = Yold - yorig
Xmin = np.min(X)
Xmax = np.max(X)
Ymin = np.min(Y)
Ymax = np.max(Y)
#%% dimension of z for the plotting
zminPlot = np.nanmin(h)
zmaxPlot = np.nanmax(h)
zlevels = np.linspace(zminPlot, zmaxPlot, 21)
zclevels = np.arange(zminPlot, zmaxPlot, 5)
#%% plot
""" 
    plot 3d
"""
fig1 = plt.figure(num=5, figsize=(10, 10), dpi=100, facecolor="w", edgecolor="w")
ax = fig1.gca(projection="3d")
surf = ax.plot_surface(
    Y, X, h, cmap=cmapBathy, rstride=1, cstride=1, vmin=zminPlot, vmax=zmaxPlot
)
ax.set_zlim(zminPlot, zmaxPlot)
plt.colorbar(surf, spacing="uniform", orientation="horizontal")
plt.title("Bathymetry in the local coordinate system", size=12)
plt.ylabel("Cross-shore [m]", size=12)
plt.xlabel("Long-shore [m]", size=12)
plt.ylim([Xmax, Xmin])
ax.view_init(30, 45)
plt.show(block=False)
"""
    plot 2d
"""
fig3 = plt.figure(num=6, figsize=(10, 10), dpi=100, edgecolor="w")
ax = plt.subplot(111)
surf = ax.contourf(Y, X, h, cmap=cmapBathy, levels=zlevels)
plt.colorbar(surf, spacing="uniform", orientation="horizontal")
ax.contour(Y, X, h, levels=zclevels)
plt.title("Bathymetry in the local coordinate system", size=12)
plt.ylabel("Cross-shore [m]", size=12)
plt.xlabel("Long-shore  [m]", size=12)
# plt.xlim([Ymin, Ymax])
plt.ylim([Xmax, Xmin])
plt.show(block=False)
"""
compute the gradient elevation on the bathymetry
"""
if cl["parameter"]["calGradH"] == 1:
    gradh = (
        ((h - np.roll(h, 1, axis=0)) / dx) ** 2
        + ((h - np.roll(h, 1, axis=1)) / dy) ** 2
    ) ** 0.5
    fig2 = plt.figure(
        num=7, figsize=(10, 10), dpi=100, facecolor="w", edgecolor="w"
    )
    plt.contourf(Y, X, gradh, cmap=cmapBathy, levels=np.linspace(0, 0.5, 41))
    plt.colorbar(spacing="uniform", orientation="horizontal")
    plt.title("Bathymetry gradient", size=12)
    plt.ylabel("Cross-shore [m]", size=12)
    plt.xlabel("Long-shore  [m]", size=12)
    # plt.xlim([Ymin, Ymax])
    plt.ylim([Xmax, Xmin])
    plt.show()
else:
    fig2 = str("nan")
#%% Inversion h
"""
    leave the choice to reverse the z axis to reverse the bathymetry
    Xbeach need a positif depth
"""
var = input("Do you need to invert the bathymetry ? [y]/[n] \t")
if var == "y":
    h = -h
    zminPlot = np.nanmin(h)
    zmaxPlot = np.nanmax(h)
    zlevels = np.linspace(zminPlot, zmaxPlot, 21)
    zclevels = np.arange(zminPlot, zmaxPlot, 5)
    cmapBathy = cmapBathy + "_r"
else:
    pass
"""
    final plot 3d
"""
fig1 = plt.figure(num=8, figsize=(10, 10), dpi=100, facecolor="w", edgecolor="w")
ax = fig1.gca(projection="3d")
surf = ax.plot_surface(
    Y, X, h, cmap=cmapBathy, rstride=1, cstride=1, vmin=zminPlot, vmax=zmaxPlot
)
ax.set_zlim(zminPlot, zmaxPlot)
plt.colorbar(surf, spacing="uniform", orientation="horizontal")
plt.title("Bathymetry", size=12)
plt.ylabel("Cross-shore  [m]", size=12)
plt.xlabel("Long-shore  [m]", size=12)
# plt.xlim([Ymax, Ymin])
plt.ylim([Xmax, Xmin])
plt.show(block=False)
"""
    final plot 2d
"""
fig3 = plt.figure(num=9, figsize=(10, 10), dpi=100, edgecolor="w")
ax = plt.subplot(111)
surf = ax.contourf(Y, X, h, cmap=cmapBathy, levels=zlevels)
plt.colorbar(surf, spacing="uniform", orientation="horizontal")
ax.contour(Y, X, h, levels=zclevels)
plt.title("Bathymetry 2D after computing", size=12)
plt.ylabel("Cross-shore  [m]", size=12)
plt.xlabel("Long-shore  [m]", size=12)
# plt.xlim([Ymax, Ymin])
plt.ylim([Xmax, Xmin])
plt.show()
#%% save file format .txt and .nc
"""
    saving data on netcdf and txt format with the fonction
"""
out_data(
    X,
    Y,
    Nx,
    Ny,
    h,
    # cl['data_path']['savePath_preproc'],
    filterBathy,
    cl["parameter"]["savePlotBathy"],
    fig1,
    fig2,
    fig3,
    cl["parameter"]["calGradH"],
    beach_config,
    cl["parameter"]["date_bathy"],
)

print("Preprocessing_mnt finished successfully")
print("--- %s seconds ---" % (time.time() - start_time))
