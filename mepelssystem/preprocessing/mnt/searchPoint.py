#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Search point on bathymetry
**************************

This little script as for goal to defined the position of a sppecial point.

"""


##########
# Package
import sys

sys.path.append("./libs/")
from matplotlib import cm
from rotation_function import rotation
from indata_function import indata

import numpy as np
import matplotlib.pyplot as plt
import yaml
import os
import time

start_time = time.time()

# position referentiel prise mesure (pas forcément utile maintenant)
E0 = 316469.844
N0 = 277345.495
Nx = 380
Ny = 506
pts1 = [-191.3, -430.27]
pts2 = [-243.3, -455.8]

beach_letter = input(
    "Beach you can used :"
    "\n biscarrosse as 1,"
    "\n trucvert raw as 2,"
    "\n gravelines ZOOM as 3,"
    "\n Enter the corresponding number : "
)

if beach_letter == "1":
    beach_config = "biscarrosse"
elif beach_letter == "2":
    beach_config = "trucvert"
elif beach_letter == "3":
    beach_config = "gravelineZOOM"


with open(r"./config_plage/" + beach_config + "config.yaml") as file:
    cl = yaml.full_load(file)
print("beach config choose: ", beach_config)

Xseaward = cl["changeStudyArea"]["Xorig"]
Yseaward = cl["changeStudyArea"]["Yorig"]

bathy = "../../../beaches/trucvert/trucvert_0/trucvert_Filter_bathy.nc"
extension = "nc"
x, y, h = indata(bathy, extension, ",")

h = -h  # position depth

# plotting condition
zminPlot = np.min(h)
zmaxPlot = np.max(h)

# plot 1
# fig = plt.figure(num = 1, figsize = (10, 10), dpi = 100, facecolor = 'w', edgecolor = 'w')
# ax = fig.add_subplot()
# surf = ax.scatter(x, y, c=-h, vmin=zminPlot, vmax=zmaxPlot, cmap = cmap2)
# plt.grid()
# plt.colorbar(surf, spacing='uniform', orientation = 'horizontal')
# plt.show(block=False)

# restructured mesh
X = x.reshape(Ny, Nx)
Y = y.reshape(Ny, Nx)
h = h.reshape(Ny, Nx)

# modifi local to global coordinates
if cl["projection"]["changeCoordGeo"] == 1:
    Xold = X
    Yold = Y
    X_glob = Xold + Xseaward
    Y_glob = Yold + Yseaward
    del Xold, Yold

    # put the reference point in the right place
    E0_glob, N0_glob = rotation(
        E0,
        N0,
        cl["changeRotation"]["axisrotation"],
        cl["changeStudyArea"]["Xorig"],
        cl["changeStudyArea"]["Yorig"],
        cl["changeRotation"]["trigosense"],
    )

if cl["projection"]["changeCoordGeo"] == 1:
    Xold = X_glob
    Yold = Y_glob
    X_loc = Xold - E0_glob
    Y_loc = Yold - N0_glob
    del Xold, Yold

# plot 2
zlevels = np.linspace(-50, 20, 21)
zclevels = np.arange(-50, 20, 5)
landwardOrig = [0, 0]

fig3 = plt.figure(num=2, figsize=(10, 10), dpi=100, edgecolor="w")
ax = plt.subplot(111)
surf = ax.contourf(X_loc, Y_loc, h, cmap="gist_earth", levels=zlevels)
plt.colorbar(surf, shrink=0.5, aspect=20, orientation="horizontal")
plt.plot(landwardOrig[0], landwardOrig[1], "sr", label="origine landward")
plt.plot(pts1[0], pts1[1], "g*", label="capt1")  # capt1
plt.plot(pts2[0], pts2[1], "r*", label="capt3")  # capt3
ax.contour(X_loc, Y_loc, h, levels=zclevels)
plt.title("Bathymetry")
plt.ylabel("Y (m)")
plt.xlabel("X (m)")
plt.legend()
plt.show(block=False)

if cl["projection"]["changeCoordGeo"] == 1:
    # passage repère global
    pts1[0] += E0_glob
    pts1[1] += N0_glob
    pts2[0] += E0_glob
    pts2[1] += N0_glob
    landwardOrig[0] += E0_glob
    landwardOrig[1] += N0_glob
    # passage en local
    pts1[0] -= Xseaward
    pts1[1] -= Yseaward
    pts2[0] -= Xseaward
    pts2[1] -= Yseaward
    landwardOrig[0] -= Xseaward
    landwardOrig[1] -= Yseaward


fig3 = plt.figure(num=3, figsize=(10, 10), dpi=100, edgecolor="w")
ax = plt.subplot(111)
surf = ax.contourf(X, Y, h, cmap="gist_earth", levels=zlevels)
plt.colorbar(surf, shrink=0.5, aspect=20, orientation="horizontal")
plt.plot(0.0, 0.0, "r*", label="sea orig")
plt.plot(pts1[0], pts1[1], "g*", label="capt1")  # capt1
plt.plot(pts2[0], pts2[1], "r*", label="capt3")  # capt3
plt.plot(landwardOrig[0], landwardOrig[1], "sr", label="land orig")
ax.contour(X, Y, h, levels=zclevels)
plt.title("Bathymetry")

print("pts1", pts1[0], pts1[1])
print("pts2", pts2[0], pts2[1])
plt.ylabel("Y (m)")
plt.xlabel("X (m)")
plt.legend()
plt.show(block=False)


print("Compute finish")
print("--- %s seconds ---" % (time.time() - start_time))
