#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Apply rotation on bathymetry
===============================================================================

This example is base on real data on Trucvert Beach and add a rotation at the
area. Base the area is on the good position, this demonstration will place 
the beach on the wrong direction with -90° rotation
"""

###############################################################################
# Firts : Import data
# ----------------------------------------------------------------
#
# ..note:: The variables Nx and Ny are calculated on the script
# preprocessing_mnt.py

import numpy as np
import sys
sys.path.append('../mepelssystem/preprocessing/mnt/libs/')
from indata_function import indata

x, y, h = indata('../data_for_example/trucvert2008-11-02_nonFilter.txt',
                                'txt',
                                ',',
                                0)

Ny = 506
Nx = 380

# reshape the vector to matrix
X = x.reshape(Ny,Nx)
Y = y.reshape(Ny,Nx)
H = h.reshape((Ny,Nx))


print('size X:', np.shape(X),'\n'
      'size Y:', np.shape(Y),'\n'
      'size H:', np.shape(H))

###############################################################################
# Second : Parameters definition
# ----------------------------------------
#
# ..note:: The rotation follow the trigonometric standard

import numpy as np

# definition of vector x and y 
x = X[0,:]
y = Y[:,0]

# degree rotation
axisrotation = 90

# choice rotation sense
direction_rot = 1

Xorig = np.min(x)
Yorig = 0.5 * (np.max(y) + np.min(y))



###############################################################################
# Three : Visualisation before rotation
# -----------------------------------------
#
# ..note::Normally you don't have to do this step, you will know the orientation
# of the study area

import matplotlib.pyplot as plt
from matplotlib import cm

# colormap
viridis = cm.get_cmap('viridis', 12)

# maximum and minimum height of bathymetry
zminPlot = np.nanmin(H)
zmaxPlot = np.nanmax(H)

fig1 = plt.figure(num = 1, figsize = (10, 10), dpi = 100, facecolor = 'w', edgecolor = 'w')
ax = fig1.gca(projection = '3d')
surf = ax.plot_surface(Y, X, H, cmap = viridis, rstride = 1, cstride = 1,\
                            vmin = zminPlot, vmax = zmaxPlot)
ax.set_zlim(zminPlot, zmaxPlot)
plt.colorbar(surf, shrink = 0.5, aspect = 20, orientation = 'horizontal')

#settings title and axis labels
plt.title('Bathymetry without rotation')
plt.ylabel('Cross-shore  [m]', size=12)
plt.xlabel('Long-shore  [m]', size=12)
# plt.ylim([np.min(rot_var_X), np.max(rot_var_X)])
# plt.xlim([np.min(rot_var_Y), np.max(rot_var_Y)])


plt.show()


###############################################################################
# Fourth : Make the rotation
# ----------------------------
#
import sys
sys.path.append('../mepelssystem/preprocessing/mnt/libs/')
from rotation_function import rotation

Xrotated, Yrotated = rotation(
                      X,
                      Y,
                      axisrotation,
                      Xorig,
                      Yorig,
                      direction_rot
                      )

###############################################################################
# Five : Rotation visualisation
# -------------------------------
#

import matplotlib.pyplot as plt
from matplotlib import cm


viridis = cm.get_cmap('viridis', 12)

# maximum and minimum height of bathymetry
zminPlot = np.nanmin(H)
zmaxPlot = np.nanmax(H)

fig1 = plt.figure(num=1, figsize=(10, 10), dpi=100, facecolor='w', edgecolor='w')
ax = fig1.gca(projection = '3d')
surf = ax.plot_surface(Yrotated, Xrotated, H, cmap='gist_earth', rstride=1, cstride=1,\
                            vmin = zminPlot, vmax=zmaxPlot)
ax.set_zlim(zminPlot, zmaxPlot)
plt.colorbar(surf, shrink=0.5, aspect=20, orientation='horizontal')

#settings title and axis labels
plt.title('Bathymetry with rotation')
plt.ylabel('Cross-shore  [m]', size=12)
plt.xlabel('Long-shore  [m]', size=12)
# plt.ylim([np.min(rot_var_X), np.max(rot_var_X)])
# plt.xlim([np.min(rot_var_Y), np.max(rot_var_Y)])


plt.show()
