"""
Read and Plot contour of bathymetry
=============================================================================

This example doesn't do much, it just reads and makes a plot of bathymetry.
It use value found during the entire calcul on preprocessing_mnt.py of 
Trucvert beach for Nx and Ny
"""

###############################################################################
# Firts : read the parameters and the mesh 
# ----------------------------------------
# used for create the mesh ([X,Y]) for recreate the
# mesh and put on each point the appropriate bathymetry value
#
# .. note:: It just read mesh and put value on each point, the mesh is store on
#           X and Y variable and the values of bathymetry is store in h variable.

import numpy as np
import sys
sys.path.append('../mepelssystem/preprocessing/mnt/libs/')
from indata_function import indata

x, y, h = indata('../data_for_example/trucvert2008-11-02_nonFilter.txt',
                                'txt',
                                ',',
                                0)

Ny = 506
Nx = 380
# reshape the vector to matrix
X = x.reshape(Ny,Nx)
Y = y.reshape(Ny,Nx)
H = h.reshape((Ny,Nx))


###############################################################################
# Now plots the contour of the bathymetry beach : Trucvert beach
# --------------------------------------------------------------
#
# In this example the plot is the real plot founding with the 
# preprocessing_mnt.py for Trucvert beach

import numpy as np
import matplotlib.pyplot as plt


zlevels = np.linspace(np.min(H), np.max(H), 21)
zclevels = np.arange(np.min(H), np.max(H),5)

fig1 = plt.figure(num=1, figsize=(10, 10), dpi=100, edgecolor='w')
ax = plt.subplot(111)
surf = ax.contourf(Y, X, H, cmap='gist_earth', levels=zlevels)
plt.colorbar(surf, shrink=0.5, aspect=20, orientation='horizontal')
ax.contour(Y, X, H, levels=zclevels)

#settings title and axis labels
plt.title('Contour Bathymetry 2D')
plt.ylabel('Cross-shore  [m]', size=12)
plt.xlabel('Long-shore  [m]', size=12)
plt.ylim([np.max(X), np.min(X)])
plt.show()

