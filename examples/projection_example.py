#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Modification geographics coordinate
===============================================================================

This example is base on real coordinate used for the Trucvert Beach. Initialy 
the coordinate are in Lambert93, in this case we change the geographics 
coordinate to WGS84

"""

###############################################################################
# Firts : Import data 
# ----------------------------------------------------------------
#
# ..note:: We just used the 10 first values of X and Y to show the operation.

import numpy as np
import sys
sys.path.append('../mepelssystem/preprocessing/mnt/libs/')
from indata_function import indata

x, y, h = indata('../data_for_example/trucvert2008-11-02_nonFilter.txt',
                                'txt',
                                ',',
                                0)

Ny = 506
Nx = 380
# reshape the vector to matrix
X = x.reshape(Ny,Nx)
Y = y.reshape(Ny,Nx)
H = h.reshape((Ny,Nx))


print('size X:', np.shape(X),'\n'
      'size Y:', np.shape(Y),'\n'
      'size H:', np.shape(H))

###############################################################################
# Second : Parameters coordinate and visualisation
# ------------------------------------------------
#

# import numpy
coorIn = "EPSG:27573"
coorOut = "EPSG:4326"

#     WGS : 4326
#     lambert93 : 2154
#     lambert1 : 27571
#     lambert2 : 27572
#     lambert3 : 27573
#     lambert4 : 27574

import matplotlib.pyplot as plt
from matplotlib import cm



# maximum and minimum height of bathymetry
zminPlot = np.nanmin(H)
zmaxPlot = np.nanmax(H)

fig1 = plt.figure(num = 1, figsize = (10, 10), dpi = 100, facecolor = 'w', edgecolor = 'w')
ax = fig1.gca(projection = '3d')
surf = ax.plot_surface(Y, X, H, cmap='gist_earth', rstride=1, cstride=1,\
                            vmin=zminPlot, vmax=zmaxPlot)
ax.set_zlim(zminPlot, zmaxPlot)
plt.colorbar(surf, shrink = 0.5, aspect = 20, orientation = 'horizontal')

#settings title and axis labels
plt.title('Bathymetry original coordinate system, EPSG:27573, lambert3')
plt.ylabel('Cross-shore  [m]', size=12)
plt.xlabel('Long-shore  [m]', size=12)
plt.show()

###############################################################################
# Third : Computing and visualisation
# ----------------------------------------
#
#
import sys
sys.path.append('../mepelssystem/preprocessing/mnt/libs/')
from projection_function import projection
import matplotlib.pyplot as plt
from matplotlib import cm



rot_var_X, rot_var_Y = projection(X, Y, coorIn, coorOut)



# maximum and minimum height of bathymetry
zminPlot = np.nanmin(H)
zmaxPlot = np.nanmax(H)

fig1 = plt.figure(num=1, figsize=(10, 10), dpi=100, facecolor='w', edgecolor='w')
ax = fig1.gca(projection='3d')
surf = ax.plot_surface(rot_var_Y, rot_var_X, H, cmap='gist_earth', rstride=1, cstride=1,\
                            vmin=zminPlot, vmax=zmaxPlot)
ax.set_zlim(zminPlot, zmaxPlot)
plt.colorbar(surf, shrink = 0.5, aspect = 20, orientation = 'horizontal')

#settings title and axis labels
plt.title('Bathymetry modified coordinate system, EPSG:4326, WGS')
plt.ylabel('Cross-shore  [m]', size=12)
plt.xlabel('Long-shore  [m]', size=12)
plt.show()


