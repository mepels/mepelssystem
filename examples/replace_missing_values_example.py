#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Replacing missing values
===============================================================================

This example read the bathymetry, search if there some missing values and 
replace the missing value by a new one direcly with an other bathymetrie or by
simualating the gradient near the point

This example is based on a real case, so some parameters are fill with the 
parameters found/calculated during the execution of the complete case.
"""

###############################################################################
# 1 : read the data and grid creation 
# -----------------------------------
#
# Reading the bathymetry and create with the data an associate meshgrid. 
#
# ..note:: The reading data is the bathymetry obtained after interpolate.

import numpy as np
from netCDF4 import Dataset
import sys
sys.path.append('../mepelssystem/preprocessing/mnt/libs/')
from indata_function import indata



x, y, h = indata('../data_for_example/trucvert2008-11-02_nonFilter.txt',
                                'txt',
                                ',',
                                0)

Ny = 506
Nx = 380
# reshape the vector to matrix
X = x.reshape(Ny,Nx)
Y = y.reshape(Ny,Nx)
H = h.reshape((Ny,Nx))

print('h:', np.shape(h),
      'H:', np.shape(H))


###############################################################################
# 2 : Define the method of replacement and choice parameters
# ----------------------------------------------------------

# 3 proposition for replacement (missVal_param):
# 0: no replacement (sure there are no missing value),
# 1: replacement with an other bathymetry file (prety good if your're a lot of 
# missing value),
# 2: replacement with an approximation (quick an accuracy if you've got low hole)
#
# ..note::Using an other bathymetry is recommended if you've can have one with
# a close bathymetry.


# method of interpolation
InterpMethod = 'linear'

# if the bathymetry need a other bathymetry to fill the missing values
# used if missVal_param = 1
missingBathyPath = '../data_for_example/trucvert2008-11-02_nonFilter.txt'
delimPonctMisssVal = ','

extension = 'txt'

# number of point used to simulate gradient
# used if missVal_param = 2
n = 8

# limite on y axis for replacing points
val_lim_y = -np.max(X)

# minimum number to involve the whole replacement by other bathymetry file
missMore = 7000

# desired number of subdivisions (real partition = partition²)
partition = 2

# ###############################################################################
# # 3 : Delate values to simulate the potential missing values and plot
# # -------------------------------------------------------------------
# #
# # The raw bathymetry is frequently incomplete. For this exemple we used the
# # result calculated an thus complete.

import matplotlib.pyplot as plt
from matplotlib import cm

H[15:70, 15:70] = np.NaN
H[120:180, 120:180] = np.NaN
# maximum and minimum height of bathymetry
zminPlot = np.nanmin(H)
zmaxPlot = np.nanmax(H)

fig1 = plt.figure(num = 1, figsize = (10, 10), dpi = 100, facecolor = 'w', edgecolor = 'w')
ax = fig1.gca(projection = '3d')
surf = ax.plot_surface(Y, X, H, cmap='gist_earth', rstride=1, cstride=1,\
                            vmin=zminPlot, vmax=zmaxPlot)
ax.set_zlim(zminPlot, zmaxPlot)
plt.colorbar(surf, shrink=0.5, aspect=20, orientation='horizontal')
#settings title and axis labels
plt.title('Bathymetry with missing values')
plt.ylabel('Cross-shore [m]')
plt.xlabel('long-shore [m]')
plt.show()


# ###############################################################################
# # 4 : Data replacement : with an other bathymetry
# # -----------------------------------------------
# # In function of the method used for replacement, here the data can be 
# # replaced by other bathymetry with an interpolation

# # ..note::By modify the methoduse and the partition, it's possible to play 
# # on the time and resources than the interpolation use. Incresed the 
# # partition reduced the RAM needed.

import sys
sys.path.append('../mepelssystem/preprocessing/mnt/libs/')
from replacementValues_function import replacementValues
import numpy as np

missVal_param = 1

H = replacementValues(missVal_param, H, missingBathyPath, val_lim_y,
                          missMore, partition, Nx, Ny, extension, InterpMethod,
                          X, Y, 0, delimPonctMisssVal, 0)

# ###############################################################################
# # 5 : Plot the result on 3D graphique
# # -----------------------------------
# #
# # plotting the result of replacement values on a 3D graphic.

import matplotlib.pyplot as plt
from matplotlib import cm

# maximum and minimum height of bathymetry
zminPlot = np.nanmin(H)
zmaxPlot = np.nanmax(H)

fig2 = plt.figure(num = 2, figsize = (10, 10), dpi = 100, facecolor = 'w', edgecolor = 'w')
ax = fig2.gca(projection = '3d')
surf = ax.plot_surface(Y, X, H, cmap='gist_earth', rstride=1, cstride=1,\
                            vmin=zminPlot, vmax=zmaxPlot)
ax.set_zlim(zminPlot, zmaxPlot)
plt.colorbar(surf, shrink=0.5, aspect=20, orientation='horizontal')
#settings title and axis labels
plt.title('Bathymetry refill')
plt.ylabel('Cross-shore [m]')
plt.xlabel('long-shore [m]')

plt.show()

###############################################################################
# 6 : Data replacement : with approximation
# -----------------------------------------
#
# In this case we approximate the value by meaning the difference between 
# near point on x and y axis. This method can be used when you have a small
# area of missing value
import sys
sys.path.append('../mepelssystem/preprocessing/mnt/libs/')
from replacementValues_function import replacementValues
import matplotlib.pyplot as plt
import numpy as np

missVal_param = 2

# reinitialisation of missing values
H[15:25, 15:25] = np.NaN
H[120:135, 120:135] = np.NaN
zminPlot = np.nanmin(H)
zmaxPlot = np.nanmax(H)

fig1 = plt.figure(num = 1, figsize = (10, 10), dpi = 100, facecolor = 'w', edgecolor = 'w')
ax = fig1.gca(projection = '3d')
surf = ax.plot_surface(Y, X, H, cmap='gist_earth', rstride = 1, cstride = 1,\
                            vmin = zminPlot, vmax = zmaxPlot)
ax.set_zlim(zminPlot, zmaxPlot)
plt.colorbar(surf, shrink = 0.5, aspect = 20, orientation = 'horizontal')

#settings title and axis labels
plt.title('Bathymetry with missing values')
plt.ylabel('Cross-shore [m]')
plt.xlabel('long-shore [m]')
plt.show()

H2 = replacementValues(missVal_param,
                    H,
                    'nan',
                    'nan',
                    'nan',
                    'nan',
                    'nan',
                    'nan',
                    'nan',
                    'nan',
                    'nan',
                    'nan',
                    n,  #number of point used for approximation
                    'nan',
                    0)
###############################################################################
# 7 : Plot the result on 3D graphique
# -----------------------------------
#
# Plot the result of replacement values by approximation
# on a 3D graphic.

import matplotlib.pyplot as plt
from matplotlib import cm

# maximum and minimum height of bathymetry
zminPlot = np.nanmin(H)
zmaxPlot = np.nanmax(H)

fig3 = plt.figure(num = 3, figsize = (10, 10), dpi = 100, facecolor = 'w', edgecolor = 'w')
ax = fig3.gca(projection = '3d')
surf = ax.plot_surface(Y, X, H2, cmap='gist_earth', rstride = 1, cstride = 1,\
                            vmin = zminPlot, vmax = zmaxPlot)
ax.set_zlim(zminPlot, zmaxPlot)
plt.colorbar(surf, shrink = 0.5, aspect = 20, orientation = 'horizontal')
#settings title and axis labels
plt.title('Bathymetry refill by replacement')
plt.ylabel('Cross-shore [m]')
plt.xlabel('long-shore [m]')
plt.show()

dx = 4
dy = 4
gradh = (((H2 - np.roll(H2, 1, axis=0)) / dx)**2 + ((H2 - np.roll(H2, 1, axis=1)) / dy)**2)**0.5
fig2 = plt.figure(num = 7, figsize = (10, 10), dpi = 100, facecolor = 'w', edgecolor = 'w')
plt.contourf(Y, X, gradh, cmap='gist_earth', levels = np.linspace(0, 0.5, 41))
plt.colorbar(spacing='uniform', orientation='horizontal')
plt.title('Bathymetry gradient', size=12)
plt.ylabel('Cross-shore [m]', size=12)
plt.xlabel('Long-shore  [m]', size=12)
# plt.xlim([Ymin, Ymax])
plt.ylim([np.max(X), np.min(X)])
plt.show()

print ('compute finish')

