#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Read raw data, interpolate and plot
===============================================================================

This example read the raw data, determine the dimension of mesh with the 
gridsize value and make an interpolation to fill the bathymetry matrix
and plot a surface plot of the bathymetrie calculated
"""

###############################################################################
# Firts : read the data 
# ----------------------------------------------------------------
#
# ..note:: It just read the raw data and put each column in a variable

import numpy as np

import numpy as np
import sys
sys.path.append('../mepelssystem/preprocessing/mnt/libs/')
from indata_function import indata

extension = 'txt'
ponctuation = ','

x_raw, y_raw, h_raw = indata('../data_for_example/trucvert2008-11-02_nonFilter.txt',
                                extension,
                                ponctuation,
                                0)


###############################################################################
# Second : Define some parameters of the mesh used for interpolation
# ------------------------------------------------------------------
# The next variables defined the study areaand created the meshgrid used in 
# the interpolation. The only one variable for the meshgrid whose can be
# modifie by user is gridsize, the others variables are defined automatically



import numpy as np

gridsize = 20

Xmin = np.min(x_raw)
Ymin = np.min(y_raw)

Xmax = np.max(x_raw)
Ymax = np.max(y_raw)

# width of the mesh
Lx = Xmax - Xmin
Ly = Ymax - Ymin

# numpber of grid on the mesh
Nx = int(Lx / gridsize)
Ny = int(Ly / gridsize)

if (Nx % 2) != 0:
    Nx += 1
if (Ny % 2) != 0:
    Ny += 1

# point position
x = np.linspace(Xmin, Xmax, Nx)
y = np.linspace(Ymin, Ymax, Ny)

[X, Y] = np.meshgrid(x, y)


print('Nx:', Nx,'\n'
      'Ny:', Ny,'\n'
      'Lx:', Lx,'\n'
      'Ly:', Ly,
      )

###############################################################################
# Third : Make the interpolation
# ------------------------------
#
# Calculate the bathymetry by add a value on each point of the mesh
# defined by [X,Y]
#
# ..note::By modify the methoduse and the partition, it's possible to play 
# on the time and resources than the interpolation use. Incresed the 
# partition reduced the RAM using.

import numpy as np
import sys
sys.path.append('../mepelssystem/preprocessing/mnt/libs')
from interpolation_function import interpolation 

extension = 'txt'

# methode used 
methoduse = 'linear'

partition = 2

hraw = np.zeros((Ny,Nx))

h_interp = interpolation(hraw, partition, Nx, Ny,
                       extension, x_raw, y_raw, h_raw, methoduse, X, Y)

print('method use:', methoduse,'\n'
      'partition:', partition,'\n'
      'hraw:', np.shape(hraw))


###############################################################################
# Fourth : Plot the result on 3D graphique
# ----------------------------------------
#
# The data that has just been calculated is placed on the mesh to crete a 3D plot

import matplotlib.pyplot as plt
from matplotlib import cm


viridis = cm.get_cmap('viridis', 12)

# maximum and minimum height of bathymetry
zminPlot = np.nanmin(h_interp)
zmaxPlot = np.nanmax(h_interp)

fig1 = plt.figure(num=1, figsize=(10, 10), dpi=100, facecolor='w', edgecolor='w')
ax = fig1.gca(projection = '3d')
surf = ax.plot_surface(Y, X, h_interp, cmap='gist_earth', rstride=1, cstride=1,\
                            vmin=zminPlot, vmax=zmaxPlot)
ax.set_zlim(zminPlot, zmaxPlot)
plt.colorbar(surf, shrink=0.5, aspect=20, orientation='horizontal')

#settings title and axis labels
plt.title('Bathymetry interpolated')
plt.ylabel('Cross-shore  [m]', size=12)
plt.xlabel('Long-shore  [m]', size=12)
plt.show()
